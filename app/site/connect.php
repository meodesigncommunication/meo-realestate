<?php

$client_base = '../client/wp';

if ($_SERVER['SERVER_ADDR'] == "192.168.67.103") {
	$_SERVER['SERVER_ADDR'] = "127.0.0.1";
}

if ($_SERVER['SERVER_ADDR'] == "127.0.0.1" ) {
	$client_base = '../../client/site/wp';
}

## Load the client site
include_once $client_base . '/wp-load.php';

global $meova; // MeoVisionApp Object, instantiated in the plugin in clients

$user = $_POST['user'] ? $_POST['user'] : $_GET['user'];
$pass = $_POST['pass'] ? $_POST['pass'] : $_GET['pass'];

$result = $meova->validateUser($user, $pass);

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

echo json_encode($result);

exit;
