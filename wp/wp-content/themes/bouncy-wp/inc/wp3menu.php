<?php
add_theme_support('nav-menus');
add_action('init','register_my_menus');

function register_my_menus() {
	register_nav_menus(
		array(
			'nav-menu' => __( 'Navigation Bar Menu',THB_THEME_NAME ),
      'footer-menu' => __( 'Footer Bar Menu',THB_THEME_NAME )
		)
	);
}

?>