<?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
	<div class="four columns item">
		<div class="postcontainer">
<?php } ?>
<article <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
	<div class="row">
		<?php if(!is_single() && !is_admin()) { ?>
		<aside class="two columns hide-for-small">
			<?php get_template_part('inc/postformats/post-date'); ?>
		</aside>
		<?php } ?>
		<div class="<?php if(!is_single() && !is_page_template('template-blog-masonry.php') && !is_admin()) { echo 'ten'; } else { echo 'twelve'; } ?> columns">
			<?php $text = get_post_meta($post->ID, 'post_link_text', true); ?>
			<?php $url = get_post_meta($post->ID, 'post_link_url', true); ?>
			<div class="post-gallery link">
				<?php if($text && $url) { echo '<a href="'.$url.'" title="'.$text.'" target="_blank">'. $text.'</a>'; } ?>
			</div>
		  <div class="post-content">
		    <?php if ( is_search() || is_archive() ) { the_excerpt(); }  else { the_content('Read More'); } ?>
		    <?php if ( is_single()) { wp_link_pages(); } ?>
		  </div>
		  <?php if ( is_single()) { get_template_part('inc/postformats/post-meta'); } ?>
	  </div>
  </div>
</article>
<?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
	</div>
</div>
<?php } ?>