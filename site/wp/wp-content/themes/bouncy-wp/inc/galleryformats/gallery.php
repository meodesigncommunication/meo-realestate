<?php get_template_part('inc/galleryformats/portfolio-header'); ?>
<?php get_template_part('inc/galleryformats/portfolio-meta'); ?>
<div class="row">
	<div class="twelve columns">
		<article <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
			<div class="post-gallery flex-start flex">
			  <ul class="slides">
			  <?php 
			      $args = array(
			          'orderby'    => 'menu_order',
			          'post_type'      => 'attachment',
			          'post_parent'    => get_the_ID(),
			          'post_mime_type' => 'image',
			          'post_status'    => null,
			          'numberposts'    => -1,
			      );
			      $attachments = get_posts($args);
			  ?>
			  <?php foreach ($attachments as $attachment) : ?>
			      
			      <?php 
			          $src = wp_get_attachment_image_src( $attachment->ID, 'portfolio'); 
			          $image_id = $attachment->ID;
			          $image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0];
			      ?>
			      
			      <li>
			          <img 
			          alt="<?php echo apply_filters('the_title', $attachment->post_title); ?>" 
			          src="<?php echo $src[0]; ?>" 
			          />
			          <a class="enlarge" href="<?php echo $image_url; ?>"><i class="icon-resize-full"></i></a>
			      </li>
			  <?php endforeach; ?>
			  </ul>
			</div>
			<?php $client = get_post_meta($post->ID, 'portfolio_client', TRUE);
						$date = get_post_meta($post->ID, 'portfolio_date', TRUE);
						$services = get_post_meta($post->ID, 'portfolio_services', TRUE);
						$link = get_post_meta($post->ID, 'portfolio_link', TRUE); ?>
			
			<?php if ($client || $date || $services || $link) { ?>
		  <div class="row portfolio-attributes">
		  	<?php if ($client) { ?>
		  	<div class="three mobile-two columns">
		  		<strong><?php _e( 'Client:', THB_THEME_NAME ); ?></strong>
		  		<p><?php echo $client; ?></p>
		  	</div>
		  	<?php } ?>
		  	<?php if ($date) { ?>
		  	<div class="three mobile-two columns">
		  		<strong><?php _e( 'Date:', THB_THEME_NAME ); ?></strong>
		  		<p><?php echo $date; ?></p>
		  	</div>
		  	<?php } ?>
		  	<?php if ($services) { ?>
		  	<div class="three mobile-two columns">
		  		<strong><?php _e( 'Services:', THB_THEME_NAME ); ?></strong>
		  		<p><?php echo $services; ?></p>
		  	</div>
		  	<?php } ?>
		  	<?php if ($link) { ?>
		  	<div class="three mobile-two columns">
		  		<strong><?php _e( 'Link:', THB_THEME_NAME ); ?></strong>
		  		<p><a href="<?php echo $link; ?>" target="_blank"><?php echo $link ?></a></p>
		  	</div>
		  	<?php } ?>
		  </div>
		  <?php } ?>
		  	
	    <div class="post-content">
	      <?php the_content(); ?>
	    </div>  
		  	
		</article>
	</div>
</div>
	