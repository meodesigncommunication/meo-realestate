<?php
/*
Template Name: Blog
*/
global $more;
?>
<?php get_header(); ?>
<div class="row">
<section class="blog nine columns">
	<?php
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$args = array('offset'=> 0, 'paged'=>$paged);
	$all_posts = new WP_Query($args);
	if (have_posts()) :  while($all_posts->have_posts()) : $all_posts->the_post();
	$more = 0;
	?>
		<?php
		// The following determines what the post format is and shows the correct file accordingly
		$format = get_post_format();
		if ($format) {
		get_template_part( 'inc/postformats/'.$format );
		} else {
		get_template_part( 'inc/postformats/standard' );
		}
		?>
  <?php endwhile; ?>
      <?php theme_pagination($all_posts->max_num_pages, 1); ?>
  <?php else : ?>
    <p><?php _e( 'Please add posts from your WordPress admin page.', THB_THEME_NAME ); ?></p>
  <?php endif; ?> 
</section>
  <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>