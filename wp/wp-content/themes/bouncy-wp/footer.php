</div>
<!-- End Content -->
</div>

<?php if (ot_get_option('footer') != 'no') { ?>
<!-- Start Footer -->
<footer>
		<?php if (ot_get_option('twitter_bar') != 'no') { ?>
		<section class="twitterbar">
			<div class="row">
				<div class="one columns hide-for-medium-down">
					<a href="http://twitter.com/<?php echo ot_get_option('twitter_bar_username', 'anteksiler'); ?>" class="user"><i class="icon-twitter"></i></a>
				</div>
				<div class="nine columns tweets">
					<ul>
						<?php echo get_theme_tweets(ot_get_option('twitter_bar_username'), ot_get_option('twitter_bar_consumerkey'), ot_get_option('twitter_bar_consumersecret'), ot_get_option('twitter_bar_accesstoken'), ot_get_option('twitter_bar_accesstokensecret')); ?>
					</ul>
					
					<div class="controls">
						<a id="twitter-prev" class="smallarrow prev" href="#"><i class="icon-angle-left"></i></a>
						<a id="twitter-next" class="smallarrow next" href="#"><i class="icon-angle-right"></i></a>
					</div>
				</div>
				<div class="two columns hide-for-medium-down">
					<a class="btn blue" href="http://twitter.com/<?php echo ot_get_option('twitterbarusername', 'anteksiler'); ?>"><?php _e("Follow us on Twitter", THB_THEME_NAME); ?></a>
				</div>
			</div>
		</section>
		<?php } ?>
  	<div class="row">

  		<?php if (ot_get_option('footer_columns') == 'fourcolumns') { ?>
	    <div class="three columns">
	    	<?php dynamic_sidebar('footer1'); ?>
	    </div>
	    <div class="three columns">
	    	<?php dynamic_sidebar('footer2'); ?>
	    </div>
	    <div class="three columns">
		    <?php dynamic_sidebar('footer3'); ?>
	    </div>
	    <div class="three columns">
		    <?php dynamic_sidebar('footer4'); ?>
	    </div>
	    <?php } elseif (ot_get_option('footer_columns') == 'threecolumns') { ?>
	    <div class="four columns">
	    	<?php dynamic_sidebar('footer1'); ?>
	    </div>
	    <div class="four columns">
	    	<?php dynamic_sidebar('footer2'); ?>
	    </div>
	    <div class="four columns">
	        <?php dynamic_sidebar('footer3'); ?>
	    </div>
	    <?php } elseif (ot_get_option('footer_columns') == 'twocolumns') { ?>
	    <div class="six columns">
	    	<?php dynamic_sidebar('footer1'); ?>
	    </div>
	    <div class="six columns">
	    	<?php dynamic_sidebar('footer2'); ?>
	    </div>
	    <?php } elseif (ot_get_option('footer_columns') == 'doubleleft') { ?>
	    <div class="six columns">
	    	<?php dynamic_sidebar('footer1'); ?>
	    </div>
	    <div class="three columns">
	    	<?php dynamic_sidebar('footer2'); ?>
	    </div>
	    <div class="three columns">
	        <?php dynamic_sidebar('footer3'); ?>
	    </div>
	    <?php } elseif (ot_get_option('footer_columns') == 'doubleright') { ?>
	    <div class="three columns">
	    	<?php dynamic_sidebar('footer1'); ?>
	    </div>
	    <div class="three columns">
	    	<?php dynamic_sidebar('footer2'); ?>
	    </div>
	    <div class="six columns">
	        <?php dynamic_sidebar('footer3'); ?>
	    </div>
	    <?php } ?>
    </div>
    <?php if (ot_get_option('subfooter') != 'no') { ?>
    <!-- Start Sub-Footer -->
  	<section class="subfooter">
  		<div class="row">
  			<div class="three columns">
  				<p><?php echo ot_get_option('copyright','Copyright 2012'); ?> </p>
  			</div>
  			<div class="nine columns">
  				<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'depth' => 1, 'container' => false) ); ?>
  			</div>
  		</div>
  	</section>
  	<!-- End Sub-Footer -->
  	<?php } ?>
</footer>
<!-- End Footer -->
<?php } ?>
<?php if (ot_get_option('scrolltotop') != 'no') { ?>
	<a id="scrolltotop" href="#"><i class="icon-angle-up"></i></a>
<?php } ?>
<?php echo ot_get_option('ga'); ?>

<div id="style-switcher">
	<div class="style-toggle"><i class="icon-cog"></i></div>
	<h2>STYLE SWITCHER</h2>
	<div class="style-content">	
		<h3>Layout Style</h3>
		<ul class="switch" data-name="boxed">
			<li><a href="#" class="active" data-class="">Wide</a></li>
			<li><a href="#" data-class="boxed">Boxed</a></li>
		</ul>
		<h3>Header Style</h3>
		<ul class="switch" data-name="header">
			<li><a href="#" class="active" data-class="" data-class2="fixed">Fixed</a></li>
			<li><a href="#" data-class="notfixed" data-class2="">Not Fixed</a></li>
		</ul>
		<h3>Accent Color</h3>
			<input type="text" class="miniColors" data-default="#e24f3d"/>
	</div>
</div>
<link rel="stylesheet" id="demo-css" href="<?php echo get_template_directory_uri(). '/assets/demo/jquery.minicolors.css'; ?>" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(). '/assets/demo/jquery.minicolors.js'; ?>"></script>


<?php 
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	 wp_footer(); 
?>
</body>
</html>