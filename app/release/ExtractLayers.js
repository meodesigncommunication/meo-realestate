function save_png_for_web(saveFile) {
	var sfwOptions = new ExportOptionsSaveForWeb();
		sfwOptions.format = SaveDocumentType.PNG; //-24 //JPEG, COMPUSERVEGIF, PNG-8, BMP
		sfwOptions.transparency = true ;
		sfwOptions.blur = 0.0 ;
		sfwOptions.includeProfile = false ;
		sfwOptions.interlaced = false ;
		sfwOptions.optimized = true ;
		sfwOptions.quality = 100 ;
		sfwOptions.PNG8 = false ;

	$.writeln("  " + saveFile);

	if(saveFile.exists){
		saveFile.remove();
	}
	activeDocument.exportDocument(saveFile, ExportType.SAVEFORWEB, sfwOptions);
}


function hide_all_layers() {
	var len = app.activeDocument.layers.length;
	for (var i = 0; i < len; i++) {
		var layer = app.activeDocument.layers[i];
		layer.visible = false;
	}
}

function dump_all_layers() {
	var len = app.activeDocument.layers.length;
	for (var i = 0; i < len; i++) {
		var layer = app.activeDocument.layers[i];
		$.writeln("Processing " + layer.name);

		layer.visible = true;

//		save_png_for_web(File("E:/Development/meo-realestate/app/release/out/plans/large/" + layer.name + ".png"));
		save_png_for_web(File("E:/Development/meo-realestate/app/release/out/plans/thumbs/" + layer.name + ".png"));
		layer.visible = false;

	}
}

function main() {
	hide_all_layers();
	dump_all_layers();

}

main();
