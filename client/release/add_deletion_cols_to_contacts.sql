ALTER TABLE `wp_meo_contacts` 
ADD COLUMN `date_deleted` DATETIME NULL DEFAULT NULL AFTER `date_added`,
ADD COLUMN `deleting_user_id` BIGINT(20) NULL DEFAULT NULL AFTER `date_deleted`;
