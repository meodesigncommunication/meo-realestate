<?php

$permitted_mime_types = array(
	// Image formats.
	'image/jpeg',
	'image/gif',
	'image/png',
	'image/bmp',
	'image/tiff',
	'image/x-icon',

	// Video formats.
	'video/x-ms-asf',
	'video/x-ms-wmv',
	'video/x-ms-wmx',
	'video/x-ms-wm',
	'video/avi',
	'video/divx',
	'video/x-flv',
	'video/quicktime',
	'video/mpeg',
	'video/mp4',
	'video/ogg',
	'video/webm',
	'video/x-matroska',
	'video/3gpp', // Can also be audio
	'video/3gpp2', // Can also be audio

	// PDFs
	'application/pdf'
);

$url = $_GET['url'];

if (! filter_var($url, FILTER_VALIDATE_URL)){ // $url is invalid
	// Decode, just in case it's been double-encoded (eg by the reel360.js)
	$url = urldecode($url);
}

if (! filter_var($url, FILTER_VALIDATE_URL)){
	header('HTTP/1.0 403 Forbidden');
	echo 'Access denied';
	exit;
}

$headers = get_headers($url, 1);

if (!isset($headers['Content-Type']) or !in_array($headers['Content-Type'], $permitted_mime_types)) {
	header('HTTP/1.0 403 Forbidden');
	echo 'Access denied';
	exit;
}

header('Content-Type: ' . $headers['Content-Type']);
header("Access-Control-Allow-Origin: *");
@readfile($url);

exit;
