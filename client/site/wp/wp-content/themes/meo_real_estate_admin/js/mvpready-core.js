/* ========================================================
*
* MVP Ready - Lightweight & Responsive Admin Template
*
* ========================================================
*
* File: mvpready-core.js
* Version: 1.0.0
* Author: Jumpstart Themes
* Website: http://mvpready.com
*
* ======================================================== */

var mvpready_core = function () {

  "use strict";

  var getLayoutColors = function () {
    var colors;

    colors = ['#D74B4B', '#475F77', '#BCBCBC', '#777777', '#6685a4', '#E68E8E']; // Slate

    return colors;
  };

  var isLayoutCollapsed = function () {
    return jQuery('.navbar-toggle').css ('display') == 'block';
  };

  var initFormValidation = function( ) {
    if (jQuery.fn.parsley) {
      jQuery('.parsley-form').each (function () {
        jQuery(this).parsley ({
          trigger: 'change',
          errors: {
            container: function (element, isRadioOrCheckbox) {
              if (element.parents ('form').is ('.form-horizontal')) {
                return element.parents ("*[class^='col-']");
              }
              return element.parents ('.form-group');
            }
          }
        });
      });
    }
  };

  var initAccordions = function () {
    jQuery('.accordion-simple, .accordion-panel').each (function (i) {
      var accordion = jQuery(this),
          toggle = accordion.find ('.accordion-toggle'),
          activePanel = accordion.find ('.panel-collapse.in').parent ();

      activePanel.addClass ('is-open');

      if (accordion.is ('.accordion-simple')) {
        toggle.prepend('<i class="fa accordion-caret"></i>');
      }

      toggle.on ('click', function (e) {
        var panel = jQuery(this).parents ('.panel');

        panel.toggleClass ('is-open');
        panel.siblings ().removeClass ('is-open');
      });
    });
  };

  var initTooltips = function () {
    jQuery('.ui-tooltip').tooltip ({ container: 'body' });
    jQuery('.ui-popover').popover ({ container: 'body' });
  };

  var initBackToTop = function () {
    var backToTop = jQuery('<a>', { id: 'back-to-top', href: '#top' }),
        icon = jQuery('<i>', { 'class': 'fa fa-chevron-up' });

    backToTop.appendTo ('body');
    icon.appendTo (backToTop);

    backToTop.hide ();

    jQuery(window).scroll (function () {
      if (jQuery(this).scrollTop () > 150) {
        backToTop.fadeIn ();
      } else {
        backToTop.fadeOut ();
      }
    });

    backToTop.click (function (e) {
      e.preventDefault ();

      jQuery('body, html').animate({
        scrollTop: 0
      }, 600);
    });
  };

  var navEnhancedInit = function () {
    jQuery('.mainnav-menu').find ('> .active').addClass ('is-open');

    jQuery('.mainnav-menu > .dropdown').on ('show.bs.dropdown', function () {
      jQuery(this).addClass ('is-open');
      jQuery(this).siblings ().removeClass ('is-open');
    });
  };

  var navHoverInit = function (config) {
    jQuery('[data-hover="dropdown"]').each (function () {
      var $this = jQuery(this),
          defaults = { delay: { show: 1000, hide: 1000 } },
          $parent = $this.parent (),
          settings = jQuery.extend (defaults, config),
          timeout;

      if (!('ontouchstart' in document.documentElement)) {
        $parent.find ('.dropdown-toggle').click (function (e) {
            if (!isLayoutCollapsed ()) {
              e.preventDefault ();
              e.stopPropagation ();
            }
        });
      }

      $parent.mouseenter(function () {
        if (isLayoutCollapsed ()) { return false; }

        timeout = setTimeout (function () {
          $parent.addClass ('open');
          $parent.trigger ('show.bs.dropdown');
        }, settings.delay.show);
      });

      $parent.mouseleave(function () {
        if (isLayoutCollapsed ()) { return false; }

        clearTimeout (timeout);

        timeout = setTimeout (function () {
          $parent.removeClass ('open keep-open');
          $parent.trigger ('hide.bs.dropdown');
        }, settings.delay.hide);
      });
    });
  };

  var initLightbox = function () {
    if (jQuery.fn.magnificPopup) {
      jQuery('.ui-lightbox').magnificPopup ({
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: true,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom',
        image: {
          verticalFit: true,
          tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
      });

      jQuery('.ui-lightbox-video, .ui-lightbox-iframe').magnificPopup ({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
      });

      jQuery('.ui-lightbox-gallery').magnificPopup ({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
          enabled: true,
          navigateByImgClick: true,
          preload: [0,1]
        },
        image: {
          tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
          titleSrc: function(item) {
            return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
          }
        }
      });
    }
  };

  return {
    navEnhancedInit: navEnhancedInit,
    navHoverInit: navHoverInit,

    initAccordions: initAccordions,
    initFormValidation: initFormValidation,
    initTooltips: initTooltips,
    initBackToTop: initBackToTop,
    initLightbox: initLightbox,
    isLayoutCollapsed: isLayoutCollapsed,

    layoutColors: getLayoutColors ()
  };

}();
