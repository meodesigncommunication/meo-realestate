<?php

$development = $_GET['development'];

$development_urls = array(
	'gilamont' => 'http://www.cassiopee-vevey.ch/wp/wp-admin/admin-ajax.php'
);

if (! array_key_exists($development, $development_urls)) {
	header("HTTP/1.0 404 Not Found");
	exit;
}

$url = $development_urls[$development] . "?action=get_apartment_list";

$raw = file_get_contents($url);
$json = json_decode($raw);

$result = array();
foreach ($json->aaData as $lot) {
	$result[$lot->lot_id] = array(
		'lot_id'       => $lot->lot_id,
		'code'         => $lot->lot,
		'availability' => $lot->availability
	);
}


header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

echo json_encode($result);
