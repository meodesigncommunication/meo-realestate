<?php
/**
 * Contact page for CRM (populated in place)
 */
function mrea_show_error_page($message) {
	get_header(); ?>

	<div class="portlet details">

		<h3 class="portlet-title">
			<u>Contact <span class="contact-name">&nbsp;</span></u>
		</h3>

		<?php echo $message; ?>

	</div> <!-- /.portlet -->

	<?php get_footer();
	exit;
}

function mrea_assemble_pdf_data($contact, $analytics_data) {
	setlocale(LC_ALL, array("fr_CH", "fr_FR", "fr_FR.UTF-8", "fr_FR@euro" ));

	$result = array(
		"submit_time" => strftime ('%d-%m-%Y %H:%M:%S', strtotime($contact->first_contact)),
		"surname"     => $contact->surname,
		"first_name"  => $contact->first_name,
		"email"       => $contact->email,
		"phone"       => $contact->phone,
		"address"     => $contact->address,
		"postcode"    => $contact->postcode,
		"city"        => $contact->city,
		"country"     => $contact->country,
		"language"    => $contact->language,
		"lots_downloaded" => $contact->lots,

		"lots_viewed"        => $analytics_data['lots_viewed'],
		"pages_viewed"       => $analytics_data['pages_viewed'],
		"rooms_per_lots"     => $analytics_data['rooms_per_lots'],
		"lot_sizes"          => $analytics_data['lot_sizes'],
		"total_rooms_viewed" => $analytics_data['total_rooms_viewed'],
		"referrer"           => $analytics_data['referrer']
	);

	return $result;
}

function mrea_get_custom_field_markup($custom_fields, $slug, $current_value) {
	$result = array(
		'label' => '&nbsp;',
		'class'  => $slug,
		'markup' => '&nbsp;'
	);

	foreach ($custom_fields as $custom_field) {
		if ($custom_field['slug'] == $slug) {
			$result['label'] = $custom_field['name'];
			$markup = '<select class="contact-custom-field" name="contact-custom-field-' . $custom_field['id'] . '" data-contact-custom-field-id=' . $custom_field['id'] . '>';
			$markup .= '<option value="-1"></option>';
			foreach ($custom_field['fields'] as $option) {
				$markup .= '<option' . ( $option['id'] == $current_value ? ' selected="selected"' : '' ) . ' value="' . $option['id'] . '">' . $option['label'] . '</option>';
			}
			$markup .= '</select>';

			$markup .= '<div class="contact-custom-field-message contact-custom-field-message-' . $custom_field['id'] . '"></div>';

			$result['markup'] = $markup;
		}
	}

	return $result;
}

function mrea_get_linked_lots_markup($linked_lots, $lots) {
	$result = array(
		'label' => 'Linked lots',
		'class'  => 'linked-lots',
		'markup' => '&nbsp;'
	);

	if (empty($lots)) {
		return $result;
	}

	if (empty($linked_lots)) {
		$linked_lots[] = '';
	}

	$markup = '<form class="contact-linked-lot-form">';
	$markup .= '<div class="contact-linked-lot-message"></div>';
	$markup .= '<a class="add-contact-linked-lot" href="#"><i class="fa fa-plus-circle"></i></a>';
	foreach ($linked_lots as $lot_code) {
		$markup .= '<select class="contact-linked-lot" name="contact-linked-lot[]">';
		$markup .= '<option value="-1"></option>';
		foreach ($lots as $lot) {
			$markup .= '<option' . ( $lot->name == $lot_code ? ' selected="selected"' : '' ) . ' value="' . $lot->id . '">' . $lot->name . '</option>';
		}
		$markup .= '</select>';
		$markup .= '<br>';
	}
	$markup .= '</form>';



	$markup .= '<script type="text/javascript">' . "\n";
	$markup .= '    var contact_linked_lot_select = "';

	$markup .= "<select class='contact-linked-lot' name='contact-linked-lot[]'>";
	$markup .= "<option value='-1' selected='selected'></option>";
	foreach ($lots as $lot) {
		$markup .= "<option value='" . $lot->id . "'>" . $lot->name . "</option>";
	}
	$markup .= "</select>";
	$markup .= "<br>";


	$markup .= '";' . "\n";
	$markup .= '</script>';



	$result['markup'] = $markup;

	return $result;
}



// --------------------------------------------------------------------------------------------------- //

global $meocrm;

if (!current_user_can( 'read_contacts' )) {
	mrea_show_error_page(__( 'You do not have sufficient permissions to view contacts.', 'meo_real_estate_admin' ));
}

if ($_GET['edit']) {
	get_template_part( 'contact', 'crm-edit' );
	exit;
}

$site_id = mrea_get_site_id();
$contact_id = (int) $_GET['contact_id'];
$contacts = $meocrm->getContacts($site_id, $contact_id);
if (empty($contacts)) {
	mrea_show_error_page(__( "Contact not found (or you don't have permissions to view it)", 'meo_real_estate_admin' ));
}

// Exception Gefiswiss Estavayer site id 71
$contact_relation_states_activated = false;
if($site_id == 71){
	$contact_relation_states_activated = true;
}

$sites = mrea_get_sites_for_user();
$site = $sites[$site_id];
$contact = $contacts[$contact_id];

$analytics_data = array(
	'lots_viewed' => array(),
	'pages_viewed' => array(),
	'lot_sizes' => array(),
	'total_rooms_viewed' => 0,
	'referrer' => ''
);

$development_data = mrea_get_development_data($site);
$lots = $development_data->lots;

if (!empty($contact->analytics_id)) {
	$analytics_id = join(",", $contact->analytics_id);
	$analytics_data = mrea_get_piwik_data($site, $analytics_id, $lots);
}

$custom_fields = $meocrm->getCustomFieldsForUser(true);

if ($_GET['pdf']) {
        global $wpdb;        
	$template = get_attached_file($site['pdf_template']['id']);       
    $interactions = $wpdb->get_results('SELECT * FROM wp_meo_contact_interactions WHERE contact_id='.$contact_id.'');

    /* Exception Estavayer */
    $relationstates = $wpdb->get_results('SELECT * FROM wp_meo_contact_relation_states WHERE contact_id='.$contact_id.'');
    /* /Estavayer */
	require_once('classes/class-contact-pdf.php');
        
	$data = mrea_assemble_pdf_data($contact, $analytics_data);
        
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/
        
	$pdf = new ContactPdf('en', $template);
	$pdf->generate($data,$interactions, $relationstates);
	
	$pdf->Output();

	exit;
}

get_header();


$fields = array(
	array(
		'label'  => 'First Name',
		'class'  => 'first-name',
		'markup' => $contact->first_name
	),
	array(
		'label'  => 'Surname',
		'class'  => 'surname',
		'markup' => $contact->surname
	),
	array(
		'label'  => 'Address',
		'class'  => 'address',
		'markup' => $contact->address
	),
	array(
		'label'  => 'City',
		'class'  => 'city',
		'markup' => $contact->city
	),
	array(
		'label'  => 'Post Code',
		'class'  => 'postcode',
		'markup' => $contact->postcode
	),
	array(
		'label'  => 'Country',
		'class'  => 'country',
		'markup' => $contact->country
	),
	array(
		'label'  => 'Telephone',
		'class'  => 'phone',
		'markup' => $contact->phone
	),
	array(
		'label'  => 'email',
		'class'  => 'email',
		'markup' => $contact->email
	),
	array(
		'label'  => 'Source',
		'class'  => 'source',
		'markup' => $analytics_data['referrer']
	)
);
if (!empty($custom_fields)) {
	foreach ($custom_fields as $custom_field) {
		// $fields[] = mrea_get_custom_field_markup($custom_fields, $custom_field['slug'], isset($contact->field_values['relationship-status']) ? $contact->field_values[$custom_field['slug']]['id'] : null);
		$current_field_value = isset($contact->field_values[$custom_field['slug']]) ? $contact->field_values[$custom_field['slug']]['id'] : null;
		$fields[] = mrea_get_custom_field_markup($custom_fields, $custom_field['slug'], $current_field_value);
	}
}

if (!empty($lots)) {
	$fields[] = mrea_get_linked_lots_markup($contact->linked_lots, $lots);
}
$fields[] = array(
		'label'  => 'Note',
		'class'  => 'note',
		'markup' => $contact->note
	);
// print_r($analytics_data);
?>
	<div class="portlet details contact-crm-template">
		<h3 class="portlet-title">
			<u>Contact <span class="contact-name"><?php echo $contact->first_name; ?> <?php echo $contact->surname; ?></span></u>
			<a class="pdf-download" href="<?php the_permalink(); ?>?contact_id=<?php echo urlencode($contact_id); ?>&amp;pdf=1"><i class="fa fa-file-pdf-o"></i></a>
			<a class="edit-contact" href="<?php the_permalink(); ?>?contact_id=<?php echo urlencode($contact_id); ?>&amp;edit=1"><i class="fa fa-pencil"></i></a>
		</h3>

		<div class="portlet-body">

			<h4>First contact: <span class="first-contact"><?php echo strftime ('%d-%m-%Y %H:%M:%S', strtotime($contact->first_contact)); ?></span></h4>

			<table class="table">
				<tbody>
					<?php $field_index = 0;
					foreach ($fields as $field) {
						if ($field_index % 2 == 0) {
							?><tr><?php
						} ?>
						<th style="width: 16.6%"><?php echo $field['label']; ?></th>
						<td style="width: 33.3%" class="<?php echo $field['class']; ?>"><?php echo $field['markup']; ?></td>
						<?php if ($field_index % 2 != 0) {
							?></tr><?php
						}
						$field_index++;
					}
					if ($field_index % 2 != 0) { ?>
							<th style="width: 16.6%">&nbsp;</th>
							<td style="width: 33.3%">&nbsp;</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			
			<?php 
			// Exception Estavayer
			if($contact_relation_states_activated){
			?>
			<table class="table table-relationstates">
				<tbody>
					<tr>
						<th style="width: 16.6%"><?php _e('Stade de la relation', 'meo_real_estate_admin') ?></th>
						<td style="width: 33.3%" class="previous-relationstates"><?php 
						
							$stateoptions = array (
									'GEFISWISS : Attribution du prospect au courtier',
									'COURTIER : Prise de contact',
									'COURTIER : Relance du contact',
									'COURTIER : Pas de r&eacute;ponse, message laiss&eacute; et/ou &agrave; recontacter',
									'COURTIER : Vente agend&eacute;e',
									'COURTIER : Vente sign&eacute;e',
									'Refus : Prix',
									'Refus : Situation',
									'Refus : Qualit&eacute;',
									'Refus : Financement',
									'Refus : Sans suite / pas de r&eacute;ponse',
									'Refus : Autre raison'
							);
							
							if (!empty($contact->relationstates)) {
								$relationstate_index = 0;
								foreach ($contact->relationstates as $relationstate) { ?>
									<div class="relationstate relationstate-<?php echo $relationstate_index + 1; ?>">
										<div class="relationstate-date"><?php echo strftime ('%d-%m-%Y', strtotime($relationstate['date_added'])); ?> <?php ?><span class="timeago" title="<?php echo $relationstate['date_added']; ?>">&nbsp;</span></div>
										<div class="relationstate-state">
										<select class="edit-state">
                                         <?php 
                                         foreach ($stateoptions as $stateoption){
                                         	echo '<option value="'.$stateoption.'" ';
                                         	if($stateoption == htmlentities($relationstate['state'])) { echo ' selected="selected"'; }
                                         	echo '>'.$stateoption.'</option>';
                                         }
                                          
                                         ?>                                     
                                        </select>
                                        </div>
                                        <div class="relationstate-edit">
                                             <button class="edit-relationstate" id="<?php echo $relationstate['id'] ?>">Editer</button>
                                        </div>
									</div><?php
									$relationstate_index++;
								}
								if ($relationstate_index > 1) {
									?><a href="#" class="show-relationstates"><?php echo sprintf( __('Show the other %d relation state(s)', 'meo_real_estate_admin'), $relationstate_index - 1); ?></a><?php
								}
							}
							else {
								echo '&nbsp;';
							}
						?></td>
						<td style="width: 50.0%" class="new-relationstate">
							<form class="new-relationstate-form" action="<?php the_permalink(); ?>" method="post">
								<input class="new-relationstate-date" name="relationstate-date" type="text" value="<?php echo date('d-m-Y'); ?>"><br>
								<select class="new-relationstate-state" name="relationstate-state">
                                         <?php 
                                         foreach ($stateoptions as $stateoption){
                                         	echo '<option value="'.$stateoption.'" ';
                                         	if($stateoption == htmlentities($relationstate['state'])) { echo ' selected="selected"'; }
                                         	echo '>'.$stateoption.'</option>';
                                         }
                                          
                                         ?>                                     
                                        </select><br>
								<input class="button" type="submit" name="submit" value="<?php _e('add relationstate', 'meo_real_estate_admin'); ?>">
								<div class="new-relationstate-message"></div>
							</form>
						</td>
					</tr>
				</tbody>
			</table>
			<?php 
			}
			// /Exception
			?>
			
			<table class="table table-interactions">
				<tbody>
					<tr>
						<th style="width: 16.6%"><?php _e('Interactions', 'meo_real_estate_admin') ?></th>
						<td style="width: 33.3%" class="previous-interactions"><?php
							if (!empty($contact->interactions)) {
								$interaction_index = 0;
								foreach ($contact->interactions as $interaction) { ?>
									<div class="interaction interaction-<?php echo $interaction_index + 1; ?>">
										<div class="interaction-date"><?php echo strftime ('%d-%m-%Y', strtotime($interaction['date_added'])); ?> <?php ?><span class="timeago" title="<?php echo $interaction['date_added']; ?>">&nbsp;</span></div>
										<div class="interaction-description">
                                                                                        <?php echo '<textarea class="edit-description" style="width: 100%; height: 40px; text-align: left;">'.str_replace('/\r?\n|\r/', '', trim($interaction['description'])).'</textarea>'; ?>
                                                                                    
                                                                                </div>
                                                                                <div class="interaction-edit">
                                                                                    <button class="edit-interaction" id="<?php echo $interaction['id'] ?>">Editer</button>
                                                                                </div>
									</div><?php
									$interaction_index++;
								}
								if ($interaction_index > 1) {
									?><a href="#" class="show-interactions"><?php echo sprintf( __('Show the other %d interaction(s)', 'meo_real_estate_admin'), $interaction_index - 1); ?></a><?php
								}
							}
							else {
								echo '&nbsp;';
							}
						?></td>
						<td style="width: 50.0%" class="new-interaction">
							<form class="new-interaction-form" action="<?php the_permalink(); ?>" method="post">
								<input class="new-interaction-date" name="interaction-date" type="text" value="<?php echo date('d-m-Y'); ?>"><br>
								<textarea class="new-interaction-description" name="interaction-description" rows="4"></textarea><br>
								<input class="button" type="submit" name="submit" value="<?php _e('add interaction', 'meo_real_estate_admin'); ?>">
								<div class="new-interaction-message"></div>
							</form>
						</td>
					</tr>
				</tbody>
			</table>
			<a class="map-wrapper" href="http://maps.google.com/maps?q=<?php echo $contact->address; ?>+<?php echo $contact->postcode; ?>+<?php echo $contact->city; ?>,<?php echo $contact->country; ?>&amp;t=m&amp;z=12" target="_blank">
				<img src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $contact->address; ?>+<?php echo $contact->postcode; ?>+<?php echo $contact->city; ?>,<?php echo $contact->country; ?>&amp;markers=<?php echo $contact->address; ?>+<?php echo $contact->postcode; ?>+<?php echo $contact->city; ?>,<?php echo $contact->country; ?>&amp;zoom=13&amp;size=1140x150&amp;maptype=roadmap&amp;scale=2">
			</a>

			<table class="table analytics">
				<tbody>
					<tr>
						<th style="width: 50%">Lots viewed</th>
						<th style="width: 50%">Level of interest</th>
					</tr>
					<tr>
						<td><span class="lots-viewed"><?php
							if (!empty($analytics_data['lots_viewed']) && count($analytics_data['lots_viewed']) > 0) {
								$seen = array();
								foreach ($analytics_data['lots_viewed'] as $lot) {
									if (!isset($seen[$lot->label])) {
										$rooms = $analytics_data['rooms_per_lots'][$lot->label];
										if ($rooms == 1) {
											$rooms = "atelier/arcade";
										}
										else {
											$rooms .= " rooms";
										}
										echo '<div class="page-name-wrapper"><span class="page-name">' . $lot->label . '</span>&nbsp;' . $rooms . '</div>';
										$seen[$lot->label] = 1;
									}

								}
							}
						?></span></td>
						<td><span class="sizes-viewed"><?php
							if (!empty($analytics_data['lot_sizes']) && count($analytics_data['lot_sizes']) > 0) {
								// Array ( [4.5] => 11 [3.5] => 7 [1] => 4 [2.5] => 3 )
								foreach ($analytics_data['lot_sizes'] as $lot_size => $lot_size_total) {

									if (empty($lot_size_total)) {
										continue;
									}

									$label = ($lot_size == 1 ? 'atelier/arcade' : $lot_size . ' pièces');
									$percentage = ( $analytics_data['total_rooms_viewed'] ? round($lot_size_total / $analytics_data['total_rooms_viewed'] * 100) : 0);

									echo '<div class="page-name-wrapper"><span class="page-name">' . $label . '</span> <span class="bar" style="width: ' . ( $percentage / 2.5) . '%;">&nbsp;</span>&nbsp;' . $percentage . '%</div>';
								}
							}
						?></span></td>
					</tr>

					<tr>
						<th>Items downloaded</th>
						<th>Pages viewed</th>
					</tr>
					<tr>
						<td><span class="lots-downloaded"><?php
							if (!empty($contact->lots)) {
								$seen = array();
								foreach ($contact->lots as $lot) {
									if (!isset($seen[$lot])) {
										echo $lot . '<br>';
										$seen[$lot] = 1;
									}
								}
							}
						?></span></td>
						<td><span class="pages-viewed"><?php
							if (!empty($analytics_data['pages_viewed']) && count($analytics_data['pages_viewed']) > 0) {
								$first_width = null;
								foreach ($analytics_data['pages_viewed'] as $page) {
									if ($page->label) {
										if ($first_width === null) {
											$first_width = empty($page->sum_time_spent) ? 1 : $page->sum_time_spent;
										}

										echo '<div class="page-name-wrapper"><span class="page-name">' . $page->label . '</span> <span class="bar" style="width: ' . round( $page->sum_time_spent / $first_width * 100 / 2.5) . '%;">&nbsp;</span>&nbsp;' . gmdate("i:s", $page->sum_time_spent) . '</div>';
									}
								}
							}
						?></span></td>
					</tr>
				</tbody>
			</table>

		</div> <!-- /.portlet-body -->

	</div> <!-- /.portlet -->
        <script type="text/javascript">
            window.$ = jQuery;
            $('.edit-interaction').click(function(){
                $textarea = $(this).parent().parent().find('.edit-description').val();
                jQuery.ajax({
                    url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                    method: 'POST',
                    data: {
                        'action':'update_contact_interaction',
                        'id' : $(this).attr('id'),
                        'description' : $textarea
                    },
                    success:function(data){
                        if(data){
                            alert('Modification Effectuée !');
                        }else{
                            alert('Erreur de Modification !');
                        }
                    },
                    error: function(errorThrown){
                        console.log(errorThrown);
                    }
                });
            });
            //interaction-description
            
            /* Exception Estavayer */
			if ( $( ".edit-relationstate" ).length ) {
				$('.edit-relationstate').click(function(){
	                $selectval = $(this).parent().parent().find('.edit-state').val();
	                jQuery.ajax({
	                    url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
	                    method: 'POST',
	                    data: {
	                        'action':'update_contact_relationstate',
	                        'id' : $(this).attr('id'),
	                        'state' : $selectval
	                    },
	                    success:function(data){
	                        if(data){
	                            alert('Modification Effectuée !');
	                        }else{
	                            alert('Erreur de Modification !');
	                        }
	                    },
	                    error: function(errorThrown){
	                        console.log(errorThrown);
	                    }
	                });
	            });
			}
			/*/Exception */
        </script>
<?php get_footer(); ?>
