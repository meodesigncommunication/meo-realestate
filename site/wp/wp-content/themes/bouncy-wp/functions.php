<?php

/*-----------------------------------------------------------------------------------

	Here we have all the custom functions for the theme
	Please be extremely cautious editing this file.
	You have been warned!

-------------------------------------------------------------------------------------*/


// Define Theme Name for localization
define('THB_THEME_NAME', 'bouncy');

// Translation
add_action('after_setup_theme', 'lang_setup');
function lang_setup(){
	load_theme_textdomain(THB_THEME_NAME, get_template_directory() . '/inc/languages');
}

// Option-Tree Theme Mode
add_filter( 'ot_show_pages', '__return_false' );
add_filter( 'ot_show_new_layout', '__return_false' );
add_filter( 'ot_theme_mode', '__return_true' );
include_once( 'inc/ot-fonts.php' );
include_once( 'inc/ot-radioimages.php' );
include_once( 'admin/ot-loader.php' );
include_once( 'inc/ot-meta-boxes.php' );
include_once( 'inc/ot-theme-options.php' );

// Theme Update notification
function thb_update_setup() {
	require_once("inc/class-pixelentity-theme-update.php");
	PixelentityThemeUpdate::init(ot_get_option('buyer_username'), ot_get_option('buyer_apikey'),'turkhitbox');
}
add_action( 'after_setup_theme', 'thb_update_setup' );

// Script Calls
require_once('inc/script-calls.php');

// Script Calls
require_once('inc/breadcrumbs.php');

// Excerpts
require_once('inc/excerpts.php');

// Custom Titles
require_once('inc/wptitle.php');

// Pagination
require_once('inc/wp-pagenavi.php');

// Post Formats
add_theme_support('post-formats', array('video', 'image', 'gallery', 'quote', 'link'));

// Enable Featured Images
require_once('inc/postthumbs.php');

// Activate WP3 Menu Support
require_once('inc/wp3menu.php');

// Enable Sidebars
require_once('inc/sidebar.php');

// Custom Comments
require_once('inc/comments.php');

// Widgets
require_once('inc/widgets.php');

// Create Custom Post Types
require_once('inc/posttypes.php');

// Like functionality
require_once('inc/themelike.php');

// Masonry Load More
require_once('inc/masonry-ajax.php');
add_action("wp_ajax_nopriv_load_more_posts", "load_more_posts");
add_action("wp_ajax_load_more_posts", "load_more_posts");

// Related
require_once('inc/related.php');

// Custom Login Logo
require_once('inc/customloginlogo.php');

// Visual Post Editor Button for Shortcodes
require_once ( 'inc/tinymce/tinymce-class.php' );	
require_once ( 'inc/tinymce/shortcode-processing.php' );
add_filter('widget_text', 'do_shortcode');

// Misc 
require_once('inc/misc.php');

// Twitter oAuth
require_once('inc/twitter_oauth.php');
require_once('inc/twitter_gettweets.php');

?>