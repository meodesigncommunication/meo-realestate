<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoMiniCrm {
	const DB_VERSION = '1.0.0';

	private $contactHandler = null;
	private $remoteLotHandler = null;
	private $customFieldHandler = null;

	public function __construct() {
		require_once( dirname(__FILE__) . '/class-meo-custom-field-handler.php');
		$this->customFieldHandler = new MeoCustomFieldHandler();

		require_once( dirname(__FILE__) . '/class-meo-remote-lot-handler.php');
		$this->remoteLotHandler = new MeoRemoteLotHandler();

		require_once( dirname(__FILE__) . '/class-meo-contact-handler.php');
		$this->contactHandler = new MeoContactHandler($this->customFieldHandler, $this->remoteLotHandler);

		add_action( 'admin_enqueue_scripts', array($this, 'enqueueAdminScriptsAndStyles' ) );

		$this->createLotsMetaboxForSites();
	}

	public function enqueueAdminScriptsAndStyles() {
		wp_enqueue_style('meo-mini-crm', plugins_url('../css/meo-mini-crm.css', __FILE__), array(), filemtime(dirname(__FILE__) . '/../css/meo-mini-crm.css'));
		wp_enqueue_script('meo-mini-crm', plugins_url('../js/meo-mini-crm.js', __FILE__), array('jquery'), filemtime(dirname(__FILE__) . '/../js/meo-mini-crm.js'));
	}


	public static function activate() {
		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$charset_collate = '';

		if ( ! empty( $wpdb->charset ) ) {
			$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		}

		if ( ! empty( $wpdb->collate ) ) {
			$charset_collate .= " COLLATE {$wpdb->collate}";
		}


		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_contacts (
			id        bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			client_id bigint(20) unsigned NOT NULL,
			site_id   bigint(20) unsigned NOT NULL,
			surname varchar(100) NOT NULL,
			first_name varchar(100) NULL,
			contact_type ENUM('contact','file','manual','alert','day_info','newsletter','achat') DEFAULT 'manual',
			email varchar(100) NULL,
			phone varchar(100) NULL,
			address varchar(100) NULL,
			postcode varchar(100) NULL,
			city varchar(100) NULL,
			country varchar(100) NULL,
			note text,
			language varchar(100) NOT NULL,
			date_added datetime DEFAULT NULL,
			date_deleted datetime DEFAULT NULL,
			deleting_user_id bigint(20) unsigned DEFAULT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY email (email)
		) $charset_collate;";

		dbDelta( $sql );


		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_contact_analytics_codes (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			contact_id bigint(20) unsigned NOT NULL,
			analytics_code varchar(128) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY analytics_code (contact_id, analytics_code),
			CONSTRAINT analytics_contact_fk FOREIGN KEY (contact_id) REFERENCES " . $wpdb->prefix . "meo_contacts (id) ON UPDATE CASCADE
		) $charset_collate;";

		dbDelta( $sql );


		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_contact_download_names (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			contact_id bigint(20) unsigned NOT NULL,
			filename varchar(128) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY download_name (contact_id, filename),
			CONSTRAINT download_contact_fk FOREIGN KEY (contact_id) REFERENCES " . $wpdb->prefix . "meo_contacts (id) ON UPDATE CASCADE
		) $charset_collate;";

		dbDelta( $sql );

		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_contact_interactions (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			contact_id bigint(20) unsigned NOT NULL,
			date_added datetime NOT NULL,
			description text NOT NULL,
			PRIMARY KEY (id),
			CONSTRAINT interaction_contact_fk FOREIGN KEY (contact_id) REFERENCES " . $wpdb->prefix . "meo_contacts (id) ON UPDATE CASCADE
		) $charset_collate;";

		dbDelta( $sql );

		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_site_lots (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			site_id bigint(20) unsigned NOT NULL,
			remote_lot_id bigint(20) unsigned,
			name varchar(100) NOT NULL,
			note text NULL,
			PRIMARY KEY (id),
			UNIQUE KEY remote_lot_id (site_id, remote_lot_id),
			UNIQUE KEY lot_name (site_id, name)
		) $charset_collate;";

		dbDelta( $sql );


		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_contact_lots (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			contact_id bigint(20) unsigned NOT NULL,
			site_lot_id bigint(20) unsigned NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY site_lot_id (site_lot_id),
			CONSTRAINT lot_contact_fk FOREIGN KEY (contact_id) REFERENCES " . $wpdb->prefix . "meo_contacts (id) ON UPDATE CASCADE,
			CONSTRAINT lot_lot_fk FOREIGN KEY (site_lot_id) REFERENCES " . $wpdb->prefix . "meo_site_lots (id) ON UPDATE CASCADE
		) $charset_collate;";

		dbDelta( $sql );


		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_custom_fields (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			name varchar(100) NOT NULL,
			slug varchar(100) NOT NULL,
			client_id bigint(20) unsigned NOT NULL,
			is_active enum('yes', 'no') NOT NULL DEFAULT 'yes',
			is_required enum('yes', 'no') NOT NULL DEFAULT 'no',
			PRIMARY KEY (id),
			UNIQUE KEY site_lot_id (client_id, name),
			UNIQUE KEY client_field_slug (client_id, slug)
		) $charset_collate;";

		dbDelta( $sql );


		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_custom_field_values (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			custom_field_id bigint(20) unsigned NOT NULL,
			label varchar(100) NOT NULL,
			is_default enum('yes'),
			is_deleted enum('yes', 'no') NOT NULL DEFAULT 'no',
			PRIMARY KEY (id),
			UNIQUE KEY custom_field_label (custom_field_id, label),
			CONSTRAINT field_value_field_fk FOREIGN KEY (custom_field_id) REFERENCES " . $wpdb->prefix . "meo_custom_fields (id) ON UPDATE CASCADE
		) $charset_collate;";

		dbDelta( $sql );


		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_contact_custom_field_values (
			contact_id bigint(20) unsigned NOT NULL,
			custom_field_value_id bigint(20) unsigned NOT NULL,
			KEY contact_custom_field_contact_fk (contact_id),
			CONSTRAINT contact_custom_field_contact_fk FOREIGN KEY (contact_id) REFERENCES " . $wpdb->prefix . "meo_contacts (id) ON UPDATE CASCADE,
			CONSTRAINT contact_custom_field_cfv_fk FOREIGN KEY (custom_field_value_id) REFERENCES " . $wpdb->prefix . "meo_custom_field_values (id) ON UPDATE CASCADE,
			UNIQUE KEY custom_field_label (contact_id, custom_field_value_id)
		) $charset_collate;";

		dbDelta( $sql );


		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_contact_custom_field_values_h (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			contact_id bigint(20) unsigned NOT NULL,
			custom_field_value_id bigint(20) unsigned NOT NULL,
			date_added datetime NOT NULL,
			change_type enum('I', 'U', 'D') NOT NULL,
			change_by_user_id bigint(20) unsigned NOT NULL,
			PRIMARY KEY (id),
			KEY custom_field_contact_h_contact(contact_id),
			CONSTRAINT contact_custom_field_contact_h_fk FOREIGN KEY (contact_id) REFERENCES " . $wpdb->prefix . "meo_contacts (id) ON UPDATE CASCADE
		) $charset_collate;";

		dbDelta( $sql );


		add_option( 'msc_db_version', self::DB_VERSION );
	}

	public static function deactivate() {
		// Do nothing
	}

	private function createLotsMetaboxForSites() {
		add_action( 'add_meta_boxes', array( $this, 'addSiteMetabox' ) );
	}

	public function addSiteMetabox() {
		add_meta_box( 'site_lots', 'Site lots', array( $this, 'showSiteLots' ), CPT_SITE, 'normal' );
	}

	public function getRemoteLots($site_id) {
		return $this->remoteLotHandler->getRemoteLots((int) $site_id);
	}

	public function showSiteLots() {
		global $post;

		$sites = mrea_get_sites_for_user();
		if (empty($sites[$post->ID])) {
			return;
		}

		$lots = $this->getRemoteLots($post->ID); ?>

		<div class="refresh-lots">
			<img style="display: none;" class="lots-loader" src="<?php echo plugins_url('../images/loader.gif', __FILE__); ?>" alt="Loading..."/>
			<a class="refresh-lots-link add-new-h2" href="<?php echo admin_url('admin-ajax.php?action=update_lot_list&amp;site_id=' . $post->ID); ?>"><?php _e('Reload from source', 'meo_mini_crm'); ?></a>
			<div style="display: none;" class="refresh-message">&nbsp;</div>
		</div>

		<table class="widefat">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$class = ' class="alternate"';
				foreach($lots as $lot) { ?>
					<tr<?php echo $class;?>>
						<td><?php echo $lot->remote_lot_id; ?></td>
						<td><?php echo $lot->name; ?></td>
					</tr><?php
					$class = empty($class) ? ' class="alternate"' : '';
				} ?>
			</tbody>
		</table><?php
	}

	public function getContacts($site_id, $contact_id = null) {
		$sites = mrea_get_sites_for_user();
		$contacts = $this->contactHandler->getContactDetails($sites[$site_id], $contact_id);
		return $contacts;
	}

	public function getCustomFieldsForUser($only_active = false) {
		$result = $this->customFieldHandler->getCustomFieldsForUser($only_active);

		return $result;
	}

	public function setLinkedContactForLot($site_id, $remote_lot_id, $contact_id) {
		$result = $this->contactHandler->setLinkedContactForLot((int) $site_id, (int) $remote_lot_id, (int) $contact_id);
		return $result;
	}

	public function canDeleteContacts() {
		return $this->contactHandler->canDeleteContacts();
	}

	public function canAddAppUsers() {
		return $this->contactHandler->canAddAppUsers();
	}
}
