var first_contact_extractor = function( data, type, extra ) {
									var result = data["first_contact"];

									if (type === "display") { // Leave unchanged for sorting
										var today = new Date(),
										    todayString = today.toISOString().substring(0, 10),
										    yesterday = new Date(); // variable name is misleading until setDate() call, below

										yesterday.setDate(today.getDate()-1);

										var yesterdayString  = yesterday.toISOString().substring(0, 10);

										result = result.replace(todayString,     meo_settings['translations']['today']);
										result = result.replace(yesterdayString, meo_settings['translations']['yesterday']);
									}
									return result;
								};
