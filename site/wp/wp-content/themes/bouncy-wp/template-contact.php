<?php
/*
Template Name: Contact
*/
wp_enqueue_script('gmapdep');
wp_enqueue_script('gmap');
?>
<?php get_header(); ?>
<div id="contact-map" class="google_map twelve columns" data-map-zoom="<?php echo ot_get_option('contact_zoom', 15); ?>" data-map-center-lat="<?php echo ot_get_option('map_center_lat', '53.381129'); ?>" data-map-center-long="<?php echo ot_get_option('map_center_long', '-1.470085'); ?>" data-pin-info="<?php echo ot_get_option('map_pin_info'); ?>" data-pin-image="<?php echo ot_get_option('map_pin_image', get_template_directory_uri(). '/assets/img/pin.png'); ?>"></div>
<div class="row">
<section class="twelve columns fullwidth">
  <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
  	<article <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
  	  <div class="post-content">
  	    <?php if ( is_search() ) { the_excerpt(); }  else { the_content('<span>Continue Reading &rarr;</span>'); } ?>
  	    <?php if ( is_single()) { wp_link_pages(); } ?>
  	  </div>
  	</article>
  <?php endwhile; else : endif; ?>
</section>
</div>
<?php get_footer(); ?>