<?php

#-----------------------------------------------------------------
# Columns
#-----------------------------------------------------------------

$thb_shortcodes['header_1'] = array( 
	'type'=>'heading', 
	'title'=>__('Columns', THB_THEME_NAME)
);

$thb_shortcodes['two_columns'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('(1/2) + (1/2)', THB_THEME_NAME )
);

$thb_shortcodes['three_columns'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('(1/3) + (1/3) + (1/3)', THB_THEME_NAME )
);

$thb_shortcodes['four_columns'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('(1/4) + (1/4) + (1/4) + (1/4)', THB_THEME_NAME )
);

$thb_shortcodes['six_columns'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('(1/6) + (1/6) + (1/6) + (1/6) + (1/6) + (1/6)', THB_THEME_NAME )
);

$thb_shortcodes['two_third_one_third'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('(2/3) + (1/3)', THB_THEME_NAME )
);

$thb_shortcodes['one_third_two_third'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('(1/3) + (2/3)', THB_THEME_NAME )
);

$thb_shortcodes['one_forth_three_forth'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('(1/4) + (3/4)', THB_THEME_NAME )
);

$thb_shortcodes['three_forth_one_forth'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('(3/4) + (1/4)', THB_THEME_NAME )
);



#-----------------------------------------------------------------
# Elements 
#-----------------------------------------------------------------

$thb_shortcodes['header_2'] = array( 
	'type'=>'heading', 
	'title'=>__('Elements', THB_THEME_NAME )
);

//Heading
$thb_shortcodes['headingtag'] = array( 
	'type'=>'regular', 
	'title'=>__('Heading', THB_THEME_NAME ), 
	'attr'=>array( 
		'heading_tag'=>array(
			'type'=>'radio', 
			'title'=>__('Heading Tag', THB_THEME_NAME), 
			'opt'=>array(
				'h1'=>'h1',
				'h2'=>'h2',
				'h3'=>'h3',
				'h4'=>'h4',
				'h5'=>'h5',
				'h6'=>'h6'
			)
		),
		'text'=>array(
			'type'=>'text', 
			'title'=>'Heading Text'
		),
		'centered'=> array(
			'type'=>'checkbox', 
			'title'=>__('Centered?', THB_THEME_NAME ),
			'desc'=>'Center the heading?'
		)
	)
);

$thb_shortcodes['quote'] = array( 
	'type'=>'regular', 
	'title'=>__('Quotes', THB_THEME_NAME ), 
	'attr'=>array( 
		'align'=>array(
			'type'=>'radio', 
			'title'=>__('Alignment', THB_THEME_NAME), 
			'opt'=>array(
				''=>'Normal',
				'pullleft'=>'Pull Left',
				'pullright'=>'Pull Right'
			)
		),
		'content'=>array(
			'type'=>'textarea', 
			'title'=>__('Content', THB_THEME_NAME)
		),
		'author'=>array(
			'type'=>'text', 
			'title'=>'Quote Author'
		)
		
	)
);
$thb_shortcodes['btn'] = array( 
	'type'=>'regular', 
	'title'=>__('Button', THB_THEME_NAME ), 
	'attr'=>array(
		'size'=>array(
			'type'=>'radio', 
			'title'=>__('Size', THB_THEME_NAME),
			'opt'=>array(
				'small'=>'small',
				'medium'=>'medium',
				'large'=>'large'
			)
		),
		'color'=>array(
			'type'=>'radio', 
			'title'=>__('Color', THB_THEME_NAME),
			'opt'=>array(
				'black'=>'Black',
				'blue'=>'Blue',
				'red'=>'Red',
				'green'=>'Green',
				'red'=>'Red',
				'purple'=>'Purple',
				'pink'=>'Pink',
				'grey'=>'Grey',
				'smoked'=>'Smoked',
				'brown'=>'Brown',
				'yellow'=>'Yellow',
				'darkblue'=>'Dark Blue',
				'bordeaux'=>'Bordeaux',
			)
		),
		'rounded'=> array(
			'type'=>'checkbox', 
			'title'=>__('Rounded?', THB_THEME_NAME )
		),
		'icon'=>array(
			'type'=>'text', 
			'title'=>__('Icon', THB_THEME_NAME)
		),
		'title'=>array(
			'type'=>'text', 
			'title'=>__('Buton Text', THB_THEME_NAME)
		),
		'link'=>array(
			'type'=>'text', 
			'title'=>__('Buton Link', THB_THEME_NAME)
		)
	)
);

$thb_shortcodes['pricing_table'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('Pricing Table', NECTAR_THEME_NAME ), 
	'attr'=>array( 
		'columns'=>array(
			'type'=>'radio', 
			'title'=>__('Columns', NECTAR_THEME_NAME), 
			'desc' => __('How many columns would you like?', NECTAR_THEME_NAME),
			'opt'=>array(
				'2'=>'Two',
				'3'=>'Three',
				'4'=>'Four',
				'5'=>'Five'
			)
		)
	)
);

$thb_shortcodes['clients'] = array( 
	'type'=>'dynamic', 
	'title'=>__('Clients', THB_THEME_NAME ), 
	'attr'=>array(
		'count'=>array(
			'type'=>'radio', 
			'title'=>__('Count', NECTAR_THEME_NAME),
			'opt'=>array(
				'2'=>'Two',
				'3'=>'Three',
				'4'=>'Four',
				'6'=>'Six'
			)
		),
		'clients'=>array('type'=>'custom', 'title'  => __('Image',THB_THEME_NAME))
	)
);

$thb_shortcodes['team_member'] = array( 
	'type'=>'regular', 
	'title'=>__('Team Member', THB_THEME_NAME ), 
	'attr'=>array( 
		'image'=>array('type'=>'custom', 'title'  => __('Image',THB_THEME_NAME)),
		'pname'=>array('type'=>'text', 'title'=>__('Name', THB_THEME_NAME)),
		'email'=>array(
			'type'=>'text', 
			'title'=> __('Email', THB_THEME_NAME),
			'desc'=> __('By entering an email, you will get an email button over the image.', THB_THEME_NAME)
		),
		'job_pos'=>array('type'=>'text', 'title'=>__('Job Position', THB_THEME_NAME)),
		'description'=>array('type'=>'textarea', 'title'=> __('Description', THB_THEME_NAME)), 
	)
);

$thb_shortcodes['divider'] = array( 
	'type'=>'direct_to_editor', 
	'title'=>__('Divider', THB_THEME_NAME )
);

$thb_shortcodes['video'] = array( 
	'type'=>'regular', 
	'title'=>__('Video', THB_THEME_NAME ), 
	'attr'=>array(
		'type'=>array(
			'type'=>'radio', 
			'title'=>__('Source', THB_THEME_NAME), 
			'opt'=>array(
				'youtube'=>'Youtube',
				'video'=>'Vimeo',
				'dailymotion'=>'Dailymotion'
			)
		),
		'id'=>array(
			'type'=>'text', 
			'title'=>__('Video ID', THB_THEME_NAME)
		),
		'format'=>array(
			'type'=>'radio', 
			'title'=>__('Video Format', THB_THEME_NAME), 
			'opt'=>array(
				'widescreen'=>'16:9',
				'normal'=>'4:3'
			),
			'desc' => __('Choose if you want to display a modern 16:9 or classic 4:3 Video', THB_THEME_NAME)
		)
	)
);

$thb_shortcodes['highlight'] = array( 
	'type'=>'regular', 
	'title'=>__('Highlight', THB_THEME_NAME ), 
	'attr'=>array(
		'color'=>array(
			'type'=>'radio', 
			'title'=>__('Color', NECTAR_THEME_NAME),
			'opt'=>array(
				'red'=>'Red',
				'black'=>'Black',
				'yellow'=>'Yellow'
			)
		),
		'text'=>array(
			'type'=>'text', 
			'title'=>__('Text', THB_THEME_NAME)
		)
	)
);

$thb_shortcodes['notifications'] = array( 
	'type'=>'regular', 
	'title'=>__('Notification', THB_THEME_NAME ), 
	'attr'=>array(
		'type'=>array(
			'type'=>'radio', 
			'title'=>__('Type', NECTAR_THEME_NAME),
			'opt'=>array(
				'success'=>'Success',
				'error'=>'Error',
				'information'=>'Information',
				'warning'=>'Warning',
				'note'=>'Note'
			)
		),
		'title'=> array(
			'type'=>'text', 
			'title'=>__('Title', THB_THEME_NAME )
		),
		'content'=>array(
			'type'=>'textarea', 
			'title'=>__('Content', THB_THEME_NAME)
		)
	)
);

#-----------------------------------------------------------------
# Interface Elements 
#-----------------------------------------------------------------

$thb_shortcodes['header_6'] = array( 
	'type'=>'heading', 
	'title'=>__('Interface Elements', THB_THEME_NAME )
);


$thb_shortcodes['tabs'] = array( 
	'type'=>'dynamic', 
	'title'=>__('Tabs', THB_THEME_NAME ), 
	'attr'=>array(
		'tabs'=>array('type'=>'custom')
	)
);

$thb_shortcodes['accordion'] = array( 
	'type'=>'dynamic', 
	'title'=>__('Accordion', THB_THEME_NAME ), 
	'attr'=>array(
		'accordion'=>array('type'=>'custom')
	)
);


$thb_shortcodes['toggle'] = array( 
	'type'=>'regular', 
	'title'=>__('Toggle', THB_THEME_NAME ), 
	'attr'=>array( 
		'title'=> array(
			'type'=>'text', 
			'title'=>__('Title', THB_THEME_NAME )
		),
		'content'=>array(
			'type'=>'textarea', 
			'title'=>__('Content', THB_THEME_NAME)
		)
	)
);

$thb_shortcodes['testimonials'] = array( 
	'type'=>'dynamic', 
	'title'=>__('Testimonial Carousel', THB_THEME_NAME ), 
	'attr'=>array(
		'testimonials'=>array('type'=>'custom')
	)
);
#-----------------------------------------------------------------
# Recent Posts/Projects 
#-----------------------------------------------------------------

$thb_shortcodes['header_7'] = array( 
	'type'=>'heading', 
	'title'=>__('Recent Posts/Work', THB_THEME_NAME )
);

$thb_shortcodes['recent_posts'] = array( 
	'type'=>'regular', 
	'title'=>__('Recent Blog Posts', THB_THEME_NAME ), 
	'attr'=>array( 
		'columns'=>array(
			'type'=>'radio', 
			'title'=>__('Columns', THB_THEME_NAME), 
			'opt'=>array(
				'2column'=>'2 Columns',
				'3column'=>'3 Columns',
				'4column'=>'4 Columns'
			)
		),
		'count'=>array(
			'type'=>'text', 
			'title'=>__('Number of blog posts', THB_THEME_NAME)
		)
	)
);

$thb_shortcodes['recent_portfolio'] = array( 
	'type'=>'regular', 
	'title'=>__('Recent Portfolio Items', THB_THEME_NAME ), 
	'attr'=>array( 
		'columns'=>array(
			'type'=>'radio', 
			'title'=>__('Columns', THB_THEME_NAME), 
			'opt'=>array(
				'2column'=>'2 Columns',
				'3column'=>'3 Columns',
				'4column'=>'4 Columns'
			)
		),
		'count'=>array(
			'type'=>'text', 
			'title'=>__('Number of portfolio items', THB_THEME_NAME)
		)
	)
);

$thb_shortcodes['recent_posts_carousel'] = array( 
	'type'=>'regular', 
	'title'=>__('Recent Blog Posts - Carousel', THB_THEME_NAME ), 
	'attr'=>array( 
		'columns'=>array(
			'type'=>'radio', 
			'title'=>__('Columns', THB_THEME_NAME), 
			'opt'=>array(
				'2column'=>'2 Columns',
				'3column'=>'3 Columns',
				'4column'=>'4 Columns'
			)
		),
		'count'=>array(
			'type'=>'text', 
			'title'=>__('Number of blog posts', THB_THEME_NAME)
		),
		'display_link'=> array(
			'type'=>'checkbox', 
			'title'=>__('Display Blog Link', THB_THEME_NAME )
		),
		'link_url'=>array(
			'type'=>'text', 
			'title'=>__('Blog Link', THB_THEME_NAME)
		)
	)
);

$thb_shortcodes['portfolio_posts_carousel'] = array( 
	'type'=>'regular', 
	'title'=>__('Recent Portfolio Items - Carousel', THB_THEME_NAME ), 
	'attr'=>array( 
		'columns'=>array(
			'type'=>'radio', 
			'title'=>__('Columns', THB_THEME_NAME), 
			'opt'=>array(
				'2column'=>'2 Columns',
				'3column'=>'3 Columns',
				'4column'=>'4 Columns'
			)
		),
		'count'=>array(
			'type'=>'text', 
			'title'=>__('Number of portfolio items', THB_THEME_NAME)
		),
		'display_link'=> array(
			'type'=>'checkbox', 
			'title'=>__('Display Portfolio Link', THB_THEME_NAME )
		),
		'link_url'=>array(
			'type'=>'text', 
			'title'=>__('Portfolio Link', THB_THEME_NAME)
		)
	)
);
#-----------------------------------------------------------------
# Icons
#-----------------------------------------------------------------

$thb_shortcodes['header_8'] = array( 
	'type'=>'heading', 
	'title'=>__('Icon Related', THB_THEME_NAME )
);

$thb_shortcodes['single_icon'] = array( 
	'type'=>'regular', 
	'title'=>__('Single Icon', THB_THEME_NAME ), 
	'attr'=>array( 
		'icon'=>array(
			'type'=>'text', 
			'title'=>__('Icon', THB_THEME_NAME)
		),
		'size'=>array(
			'type'=>'radio', 
			'title'=>__('Icon Size', THB_THEME_NAME),
			'opt'=>array(
				'icon-smallsize'=>'1x',
				'icon-large'=>'1.3x',
				'icon-2x'=>'2x',
				'icon-3x'=>'3x',
				'icon-4x'=>'4x',
				'icon-5x'=>'5x',
			)
		),
		'boxed'=> array(
			'type'=>'checkbox', 
			'title'=>__('Boxed?', THB_THEME_NAME ),
			'desc'=>'Boxed contains the icon inside a box'
		),
		'icon_link'=>array(
			'type'=>'text', 
			'title'=>__('Icon Link', THB_THEME_NAME),
			'desc'=>'If you would like to link the icon to an url, enter it here. "Boxed" should be checked.'
		)
	)
);

$thb_shortcodes['icon_list'] = array( 
	'type'=>'dynamic', 
	'title'=>__('Icon List', THB_THEME_NAME ), 
	'attr'=>array(
		'icon'=>array(
			'type'=>'text', 
			'title'=>__('Icon', THB_THEME_NAME)
		),
		'icon_list'=>array('type'=>'custom')
	)
);

$thb_shortcodes['icon_box'] = array( 
	'type'=>'regular', 
	'title'=>__('Icon Box', THB_THEME_NAME ), 
	'attr'=>array(
		'type'=>array(
			'type'=>'radio', 
			'title'=>__('Box Type', THB_THEME_NAME),
			'opt'=>array(
				'left'=>'Icon on the left',
				'heading'=>'Small icon inline with title',
				'hexagon'=>'Icon on top, inside hexagon'
			)
		),
		'icon'=>array(
			'type'=>'text', 
			'title'=>__('Icon', THB_THEME_NAME)
		),
		'title'=>array(
			'type'=>'text', 
			'title'=>__('Title Text', THB_THEME_NAME)
		),
		'content'=>array(
			'type'=>'textarea', 
			'title'=>__('Content', THB_THEME_NAME)
		)
	)
);

#-----------------------------------------------------------------
# Google
#-----------------------------------------------------------------

$thb_shortcodes['header_9'] = array( 
	'type'=>'heading', 
	'title'=>__('Google', THB_THEME_NAME )
);

$thb_shortcodes['google_map'] = array( 
	'type'=>'regular', 
	'title'=>__('Google Map', THB_THEME_NAME ), 
	'attr'=>array( 
		'latitude'=>array(
			'type'=>'text', 
			'title'=>__('Latitude', THB_THEME_NAME)
		),
		'lontitude'=>array(
			'type'=>'text', 
			'title'=>__('Longtitude', THB_THEME_NAME)
		),
		'zoom'=>array(
			'type'=>'text', 
			'title'=>__('Zoom', THB_THEME_NAME)
		),
		'content'=>array(
			'type'=>'textarea', 
			'title'=>__('Text', THB_THEME_NAME)
		),
		'maptype'=>array(
			'type'=>'radio', 
			'title'=>__('Map Type', THB_THEME_NAME), 
			'opt'=>array(
				'HYBRID'=>'HYBRID',
				'ROADMAP'=>'ROADMAP',
				'SATELLITE'=>'SATELLITE',
				'TERRAIN'=> 'TERRAIN'
			)
		),
		'controls'=> array(
			'type'=>'checkbox', 
			'title'=>__('Display Controls?', THB_THEME_NAME )
		)
	)
);

$thb_shortcodes['google_chart'] = array( 
	'type'=>'regular', 
	'title'=>__('Google Chart', THB_THEME_NAME ), 
	'attr'=>array( 
		'charttype'=>array(
			'type'=>'radio', 
			'title'=>__('Chart Type', THB_THEME_NAME), 
			'opt'=>array(
				'pie'=>'pie',
				'line'=>'line',
				'xyline'=>'xyline',
				'pie2d'=>'pie2d',
				'scatter'=> 'scatter'
			)
		)
	)
);
?>