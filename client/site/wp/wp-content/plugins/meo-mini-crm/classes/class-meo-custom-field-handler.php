<?php

class MeoCustomFieldHandler {

	public function __construct() {
		$this->addHooks();
	}

	public function addHooks() {
		add_action( 'wp_ajax_update_custom_field', array($this, 'updateCustomField') );
	}

	public function getCustomFieldsForUser($only_active) {
		global $meo_site_id;
		if (empty($meo_site_id)) {
			$meo_site_id = mrea_get_site_id();
		}

		$sites = mrea_get_sites_for_user();

		if (!array_key_exists($meo_site_id, $sites)) {
			return array();
		}

		$clients = array($sites[$meo_site_id]['client_id']);

		$result = $this->getCustomFields($clients, $only_active);

		return $result;

	}

	public function getCustomFields($client_ids, $only_active) {
		global $wpdb;

		$result = array();

		if (empty($client_ids)) {
			return $result;
		}

		$sql = "select cf.id as custom_field_id,
				       cf.name,
				       cf.slug,
				       cfv.id as custom_field_value_id,
				       cfv.label,
				       cfv.is_default
				  from " . $wpdb->prefix . "meo_custom_fields cf
				       left join " . $wpdb->prefix . "meo_custom_field_values cfv on cfv.custom_field_id = cf.id ";
		if ($only_active) {
			$sql .= " and cfv.is_deleted != 'yes' ";
		}
		$sql .= "where cf.client_id in (" . join(", ", $client_ids) . ") ";
		if ($only_active) {
			$sql .= " and cf.is_active = 'yes' ";

		}
		$sql .= " order by cfv.custom_field_id, cfv.label ";

		$raw = $wpdb->get_results( $sql );

		foreach( $raw as $row ) {
			if (!array_key_exists($row->custom_field_id, $result)) {
				$result[$row->custom_field_id] = array(
					'id'     => $row->custom_field_id,
					'name'   => $row->name,
					'slug'   => $row->slug,
					'fields' => array()
				);
			}
			$result[$row->custom_field_id]['fields'][$row->custom_field_value_id] = array(
				'id'         => $row->custom_field_value_id,
				'label'      => $row->label,
				'is_default' => $row->is_default
			);
		}

		return $result;
	}

	public function updateCustomField() {
		global $wpdb;

		header('Content-Type: application/json');

		$custom_field_id = (int) $_POST['custom_field_id'];
		$fields = $this->getCustomFieldsForUser(false);

		if (!array_key_exists($custom_field_id, $fields)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to update that field", 'meo_mini_crm')
			));
			exit;
		}

		// Delete fields
		foreach ($fields[$custom_field_id]['fields'] as $field_value) {
			if (empty($_POST['field_value']) || !array_key_exists($field_value['id'], $_POST['field_value'])) {
				$sql = $wpdb->prepare("
				    update " . $wpdb->prefix . "meo_custom_field_values
				       set is_deleted = 'yes'
				     where custom_field_id = %d
				       and id = %d", $custom_field_id, $field_value['id']);
				$wpdb->query($sql);
			}
		}

		// Update existing fields
		if (!empty($_POST['field_value'])) {
			foreach ($_POST['field_value'] as $field_value_id => $field_value_label) {
				if ($field_value_id < 0) {
					// New field
					continue;
				}
				$sql = $wpdb->prepare("
				    update " . $wpdb->prefix . "meo_custom_field_values
				       set label = %s,
				           is_deleted = 'no'
				     where custom_field_id = %d
				       and id = %d", $field_value_label, $custom_field_id, $field_value_id);

				$wpdb->query($sql);
			}
		}

		// Add new fields
		if (!empty($_POST['field_value'])) {
			foreach ($_POST['field_value'] as $field_value_id => $field_value_label) {
				if ($field_value_id >= 0) {
					// New field
					continue;
				}

				// Undelete if the label already exists
				$result = $wpdb->update(
					$wpdb->prefix . "meo_custom_field_values",
					array('is_deleted' => 'no'),
					array(
						'custom_field_id' => $custom_field_id,
						'label' => $field_value_label
					),
					array('%s'),
					array('%d', '%s')
				);

				if ($result === 0) {
					// Label didn't exist; insert it
					$wpdb->insert(
						$wpdb->prefix . "meo_custom_field_values",
						array(
							'custom_field_id' => $custom_field_id,
							'label'           => $field_value_label
						),
						array(
							'%d',
							'%s'
						)
					);
				}
			}
		}

		echo json_encode(array(
			'success' => 1,
			'message' => __("Updated", 'meo_mini_crm')
		));
		exit;
	}

	public function getContactCustomFieldValues($site_id, $contact_id = null) {
		global $wpdb;

		$result = array();
		$contact_id = (int) $contact_id;

		$sites = mrea_get_sites_for_user();

		if (!array_key_exists($site_id, $sites)) {
			return $result;
		}

		$sql = "
		     select ccfv.contact_id,
		            cf.slug,
		            cfv.label,
		            cfv.id
		       from " . $wpdb->prefix . "meo_contact_custom_field_values ccfv
		            join " . $wpdb->prefix . "meo_custom_field_values cfv on ccfv.custom_field_value_id = cfv.id
		            join " . $wpdb->prefix . "meo_custom_fields cf on cfv.custom_field_id = cf.id
		      where cf.is_active = 'yes' ";
		if ($contact_id) {
			$sql .= " and ccfv.contact_id = " . $contact_id;
		}

		$rows = $wpdb->get_results($sql);

		// Assumes only one value for each field
		foreach ($rows as $row) {
			if (!array_key_exists($row->contact_id, $result)) {
				$result[$row->contact_id] = array();
			}
			$result[$row->contact_id][$row->slug] = array(
				'label' => $row->label,
				'id'    => $row->id
			);
		}

		return $result;
	}
}
