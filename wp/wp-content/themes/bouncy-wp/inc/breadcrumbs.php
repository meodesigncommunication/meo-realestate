<?php
function thb_breadcrumb() {
        global $post;
        echo '<ul>';
        echo '<li>'.__('You are here: ', THB_THEME_NAME).'</li>';
        if ( !is_front_page() ) {
        echo '<li><a href="';
        echo home_url();
        echo '">'.__('Home', THB_THEME_NAME);
        echo "</a></li>";
        }
        
        if (is_category() && !is_singular('portfolio')) {
            $category = get_the_category();
            $ID = $category[0]->cat_ID;
            echo '<li><i class="icon-angle-right"></i> '.get_category_parents($ID, TRUE, '', FALSE ).'</li>';
        }

        if(is_singular('portfolio')) {
        		$portfolio_main = get_post_meta($post->ID, 'portfolio_main', TRUE);
        		if ($portfolio_main) {
        			$portfolio_link = get_permalink($portfolio_main);
        		} else {
        			$portfolio_link = get_portfolio_page_link(get_the_ID()); 
        		}
            echo '<li><i class="icon-angle-right"></i>  <a href="' . $portfolio_link . '">' . __( 'Portfolio', THB_THEME_NAME ) . '</a></li>'; 
            echo '<li><i class="icon-angle-right"></i> '.get_the_title().'</li>'; 
        }

        if(is_home()) { echo '<li><i class="icon-angle-right"></i> '.__('Blog', THB_THEME_NAME).'</li>'; }
        if(is_page() && !is_front_page()) {
            $parents = array();
            $parent_id = $post->post_parent;
            while ( $parent_id ) :
                $page = get_page( $parent_id );
                    $parents[]  = '<li><i class="icon-angle-right"></i> <a href="' . get_permalink( $page->ID ) . '" title="' . get_the_title( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a></li>';
                $parent_id  = $page->post_parent;
            endwhile;
            $parents = array_reverse( $parents );
            echo join( ' ', $parents );
            echo '<li><i class="icon-angle-right"></i> '.get_the_title().'</li>';
        }
        if(is_single() && !is_singular('portfolio')) {
            $categories = get_the_category( ' ' );
            if ( $categories ) :
                foreach ( $categories as $cat ) :
                    $cats[] = '<li><i class="icon-angle-right"></i> <a href="' . get_category_link( $cat->term_id ) . '" title="' . $cat->name . '">' . $cat->name . '</a></li>';
                endforeach;
                echo join( ' ', $cats );
            endif;
            echo '<li><i class="icon-angle-right"></i> '.get_the_title().'</li>';
        }
        if(is_tag()){ echo '<li><i class="icon-angle-right"></i> '.__("Tag:", THB_THEME_NAME) .single_tag_title('',FALSE).'</li>'; }
        if(is_404()){ echo '<li><i class="icon-angle-right"></i> '.__("404 - Page not Found", THB_THEME_NAME).'</li>'; }
        if(is_search()){ echo '<li><i class="icon-angle-right"></i> '.__("Search", THB_THEME_NAME).'</li>'; }
        if(is_year()){ echo '<li><i class="icon-angle-right"></i> '.get_the_time('Y').'</li>'; }

        echo "</ul>";
} ?>