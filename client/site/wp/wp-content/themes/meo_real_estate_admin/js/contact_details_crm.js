(function($){

	$(function() {
		$("span.timeago").timeago();

		$('.show-interactions').click(function(e) {
			e.preventDefault();
			$(this).hide();
			$('.interaction').slideDown();
		});
		


		$('.new-interaction-form').submit(function(e) {
			e.preventDefault();

			var url = meo_settings['ajax_url'],
			    contact_id = contact_details["contact_id"],
			    site = meo_settings['sites'][meo_settings['site_id']],
			    api_key = site['api_key'],
			    $form = $(this),
			    data = $form.serialize(),
			    $message = $('.new-interaction-message');

			data += '&action=add_new_interaction';
			data += '&contact_id=' + contact_id;
			data += '&api-key=' + api_key;

			$message.slideUp();

			$.ajax({
				url: url,
				type: 'POST',
				data: data
			})
			.done(function(result) {
				if (result.success) {
					$('.previous-interactions').prepend(
						'<div class="interaction interaction-1">' +
						'	<div class="interaction-date">' + $('.new-interaction-date').val() + '</div>' +
						'	<div class="interaction-description">' + $('.new-interaction-description').val() + '</div>' +
                                            '</div>'
					);

					$('.new-interaction-description').val('');


					$('.interaction-1').slideDown();
				} else {
					var message_html = '<div class="alert alert-danger">' +
					                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
					                       result.message +
					                   '</div>';

					$message.html(message_html).slideDown();
				}
			})
			.fail(function() {
				var message_html = '<div class="alert alert-warning">' +
				                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
				                       'Unexpected error' +
				                   '</div>';

				$message.html(message_html).slideDown();
			});


		});
		

		/* Exception Estavayer */
		if ( $( ".show-relationstates" ).length ) {
			 
			$('.show-relationstates').click(function(e) {
				e.preventDefault();
				$(this).hide();
				$('.relationstate').slideDown();
			});
		 
		}
		if ( $( ".new-relationstate-form" ).length ) {
			$('.new-relationstate-form').submit(function(e) {
				e.preventDefault();
				
				var url = meo_settings['ajax_url'],
				    contact_id = contact_details["contact_id"],
				    site = meo_settings['sites'][meo_settings['site_id']],
				    api_key = site['api_key'],
				    $form = $(this),
				    data = $form.serialize(),
				    $message = $('.new-state-message');
	
				data += '&action=add_new_relationstate';
				data += '&contact_id=' + contact_id;
				data += '&api-key=' + api_key;
				
				$message.slideUp();
	
				$.ajax({
					url: url,
					type: 'POST',
					data: data
				})
				.done(function(result) {
					if (result.success) {
						$('.previous-relationstates').prepend(
							'<div class="relationstate relationstate-1">' +
							'	<div class="relationstate-date">' + $('.new-relationstate-date').val() + '</div>' +
							'	<div class="relationstate-state">' + $('.new-relationstate-state').val() + '</div>' +
	                                            '</div>'
						);
	
						$('.new-relationstate-state').val('');
	
	
						$('.relationstate-1').slideDown();
					} else {
						var message_html = '<div class="alert alert-danger">' +
						                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
						                       result.message +
						                   '</div>';
	
						$message.html(message_html).slideDown();
					}
				})
				.fail(function() {
					var message_html = '<div class="alert alert-warning">' +
					                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
					                       'Unexpected error' +
					                   '</div>';
	
					$message.html(message_html).slideDown();
				});
	
	
			});
		}
		/*/Exception */


		$('.add-contact-linked-lot').click( function(e) {
			e.preventDefault();
			$('.contact-linked-lot-form').append(contact_linked_lot_select);
		} );

		$('.contact-linked-lot').live('change', function(){
			var url = meo_settings['ajax_url'],
			    contact_id = contact_details["contact_id"],
			    site = meo_settings['sites'][meo_settings['site_id']],
			    api_key = site['api_key'],
			    $form = $(this).closest('form'),
			    data = $form.serialize(),
			    $message = $('.contact-linked-lot-message');

			data += '&action=update_contact_linked_lots';
			data += '&contact_id=' + contact_id;
			data += '&api-key=' + api_key;

			$message.slideUp();

			$.ajax({
				url: url,
				type: 'POST',
				data: data
			})
			.done(function(result) {
				var message_class = result.success ? 'alert-success' : 'alert-danger';

				var message_html = '<div class="alert ' + message_class + '">' +
				                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
				                       result.message +
				                   '</div>';

				$message.html(message_html).slideDown();
			})
			.fail(function() {
				var message_html = '<div class="alert alert-warning">' +
				                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
				                       'Unexpected error' +
				                   '</div>';

				$message.html(message_html).slideDown();
			});
		});

		$('select.contact-custom-field').change(function(){

			var url = meo_settings['ajax_url'],
			    contact_id = contact_details["contact_id"],
			    site = meo_settings['sites'][meo_settings['site_id']],
			    api_key = site['api_key'],
			    field_id = $(this).data("contact-custom-field-id"),
			    field_value = $(this).val(),
			    $message = $('.contact-custom-field-message-' + field_id);

			$message.slideUp();

			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				data: {
					'action'      : 'update_contact_custom_field',
					'contact_id'  : contact_id,
					'api-key'     : api_key,
					'field_id'    : field_id,
					'field_value' : field_value
				}
			})
			.done(function(result) {
				var message_class = result.success ? 'alert-success' : 'alert-danger';

				var message_html = '<div class="alert ' + message_class + '">' +
				                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
				                       result.message +
				                   '</div>';

				$message.html(message_html).slideDown();
			})
			.fail(function() {
				var message_html = '<div class="alert alert-warning">' +
				                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
				                       'Unexpected error' +
				                   '</div>';

				$message.html(message_html).slideDown();
			});
		});

		$('.edit-contact-form').submit(function(e) {
			e.preventDefault();

			var url = meo_settings['ajax_url'],
			    site = meo_settings['sites'][meo_settings['site_id']],
			    api_key = site['api_key'],
			    $form = $(this),
			    data = $form.serialize(),
			    $message = $('.edit-contact-form-message');

			data += '&api-key=' + api_key;

			$message.slideUp();

			$.ajax({
				url: url,
				type: 'POST',
				data: data
			})
			.done(function(result) {
				if (result.success) {
					var redirect_to = meo_settings['contact_url'] + '?contact_id=' + result.contact_id;
					window.location = redirect_to;
					return;
				}
				var message_html = '<div class="alert alert-danger">' +
				                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
				                       result.message +
				                   '</div>';

				$message.html(message_html).slideDown();
			})
			.fail(function() {
				var message_html = '<div class="alert alert-warning">' +
				                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
				                       'Unexpected error' +
				                   '</div>';

				$message.html(message_html).slideDown();
			});
		});

	});

})(jQuery);
