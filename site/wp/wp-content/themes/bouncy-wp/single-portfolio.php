<?php get_header(); ?>

  <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
  <?php
    // The following determines what the portfolio format is and shows the correct file accordingly
    $format = get_post_meta($post->ID, 'portfolio_type', true);
    if ($format) {
    get_template_part( 'inc/galleryformats/'.$format );
    } else {
    get_template_part( 'inc/galleryformats/standard' );
    }
  ?>
  <?php get_template_part( 'inc/galleryformats/portfolio-related' ); ?>
  <?php endwhile; else : endif; ?>

<?php get_footer(); ?>