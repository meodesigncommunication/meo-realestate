<?php
/*
Template Name: Sitemap
*/
?>
<?php get_header(); ?>
<div class="row">
<section class="twelve columns fullwidth sitemap">
  <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
  	<article <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
  	  <div class="post-content">
  	  	<div class="row">
	  	    <div class="three columns">
	  	    	<div class="widget">
		        <h3><?php _e("PAGES ", THB_THEME_NAME); ?></h3>
		        <ul>
		        <?php wp_list_pages("title_li=" ); ?>
		        </ul>
		        </div>
	        </div>
	        <div class="three columns">
	        	<div class="widget">
	          <h3><?php _e("PORTFOLIO ", THB_THEME_NAME); ?></h3>
         		<ul>
              <?php $query = new WP_Query(); $query->query('post_type=portfolio&posts_per_page=-1'); ?>
              <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
              <li><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
              <?php endwhile; endif; ?>
              <?php wp_reset_query(); ?> 
            </ul>
            </div>
	        </div>
	        <div class="three columns">
	        	<div class="widget">
	          <h3><?php _e("ARCHIVES ", THB_THEME_NAME); ?></h3>
          	<ul>
              <?php wp_get_archives('type=monthly&show_post_count=true'); ?>
            </ul>
            </div>
            <div class="widget">
            <h3><?php _e("ARCHIVES BY CATEGORIES", THB_THEME_NAME); ?></h3>
            <ul>
              <?php wp_list_categories('sort_column=name&optioncount=1&hierarchical=0&feed=RSS&title_li='); ?>
            </ul>
            </div>
            <div class="widget">
            <h3><?php _e("FEEDS ", THB_THEME_NAME); ?></h3>
            <ul>
              <li><a title="Full content" href="feed:<?php bloginfo('rss2_url'); ?>">Main RSS</a></li>
              <li><a title="Comment Feed" href="feed:<?php bloginfo('comments_rss2_url'); ?>">Comment Feed</a></li>
            </ul>
            </div>
	        </div>
	        <div class="three columns">
	        	<div class="widget">
	          <h3><?php _e("LAST 20 POSTS ", THB_THEME_NAME); ?></h3>
	          <ul>
              <?php wp_get_archives('title_li=&type=postbypost&limit=20'); ?>
            </ul>
            </div>
	        </div>
        </div>
  	  </div>
  	</article>
  <?php endwhile; else : endif; ?>
</section>
</div>
<?php get_footer(); ?>