<?php

// This file is an ugly workaround for Infomaniak's limited cron functionality.  I can't
// create cron jobs on client.meo-realestate.com, so this file allows the equivalent
// of calling the email-pdfs routine

// Doing a 302 results in a 403 forbidden error in the cron. ie this doesn't work
// header("Location: http://client.meo-realestate.com/email-pdfs/?api-key=" . $_GET['api-key']);
// exit;


define('IS_PRODUCTION', $_SERVER['SERVER_ADDR'] != "127.0.0.1");

$client_root = dirname(__FILE__) . ( IS_PRODUCTION ? '/client' : '/../client/site' ) . '/wp';

include_once $client_root . '/wp-load.php';
include_once $client_root . '/wp-content/themes/meo_real_estate_admin/email-pdfs.php';
