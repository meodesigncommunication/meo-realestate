<?php if (!is_page_template('template-blog-masonry.php')) { ?>
<div class="post-date">
	<div class="date">
		<span><?php echo get_the_date('M'); ?></span>
		<small><?php echo get_the_date('d'); ?></small>
	</div>
	
		<a href="<?php comments_link(); ?>" class="commentcount">
			<i class="icon-comment"></i> 
			 <?php comments_number('0', '1', '%' ); ?> 
		</a>

</div>
<?php }?>