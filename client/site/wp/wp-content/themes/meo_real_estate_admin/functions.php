<?php
/**
 * MEO real estate admin functions and definitions
 *
 * @package MEO real estate admin
 */

define('CPT_SITE', 'meo-site');
define('CPT_CLIENT', 'meo-client');
define('COOKIE_SELECTED_SITE', 'selected_site');

define('PAGE_ID_CONTACT', 8);
define('PAGE_ID_CONTACT_JSON', 38);
define('PAGE_ID_CONTACT_CSV',  52);

define('REPORT_ERRORS_TO', 'digital@meomeo.ch');

define('MAIL_HOST', 'mail.infomaniak.com');
define('MAIL_PORT', 587);
define('MAIL_USER', 'wordpress@meo-realestate.com');
define('MAIL_PASS', 'bandleadroadgrade');


/* =================================================================================================================
 * Set the content width based on the theme's design and stylesheet.
 * ================================================================================================================= */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'meo_real_estate_admin_setup' ) ) :
/*
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function meo_real_estate_admin_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on MEO real estate admin, use a find and replace
	 * to change 'meo_real_estate_admin' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'meo_real_estate_admin', get_template_directory() . '/languages' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'meo_real_estate_admin' ),
		'settings' => __( 'Settings', 'meo_real_estate_admin' ),
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'meo_real_estate_admin_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // meo_real_estate_admin_setup
add_action( 'after_setup_theme', 'meo_real_estate_admin_setup' );


/* =================================================================================================================
 * Allow email in dev
 * ================================================================================================================= */
add_action( 'phpmailer_init', 'mrea_phpmailer_init' );
function mrea_phpmailer_init( PHPMailer $phpmailer ) {
	if (!IS_PRODUCTION) {
		$phpmailer->Host = MAIL_HOST;
		$phpmailer->Port = MAIL_PORT;
		$phpmailer->Username = MAIL_USER;
		$phpmailer->Password = MAIL_PASS;
		$phpmailer->SMTPAuth = true;

		$phpmailer->IsSMTP();
	}
}


/* =================================================================================================================
 * Add ACF options page
 * ================================================================================================================= */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page('MEO Settings');
}


/* =================================================================================================================
 * Enqueue scripts and styles.
 * ================================================================================================================= */
function meo_real_estate_admin_scripts() {
	wp_enqueue_style( 'google-fonts', 'http://fonts.googleapis.com/css?family=Open Sans%3A400%2C400italic%2C600%2C600 italic%2C800%2C800 italic|Oswald%3A400%2C300%2C700' );

	wp_enqueue_style( 'meo_real_estate_admin-style', get_stylesheet_uri(), array(), filemtime( get_stylesheet_directory() . '/style.css') );

	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css',       array(), filemtime( get_stylesheet_directory() . '/css/bootstrap.min.css') );
	wp_enqueue_style( 'mvpready-admin', get_stylesheet_directory_uri() . '/css/mvpready-admin.css', array(), filemtime( get_stylesheet_directory() . '/css/mvpready-admin.css') );
	wp_enqueue_style( 'mvpready-flat', get_stylesheet_directory_uri() . '/css/mvpready-flat.css',   array(), filemtime( get_stylesheet_directory() . '/css/mvpready-flat.css') );


	wp_enqueue_script( 'meo_real_estate_admin-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'meo_real_estate_admin-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.0.3', true );
	wp_enqueue_script( 'mvpready-core', get_template_directory_uri() . '/js/mvpready-core.js', array(), '1.0.0', true );
	wp_enqueue_script( 'mvpready-admin', get_template_directory_uri() . '/js/mvpready-admin.js', array(), '1.0.0', true );

	wp_enqueue_script( 'mrea', get_template_directory_uri() . '/js/theme.js', array('jquery'), filemtime( get_stylesheet_directory() . '/js/theme.js') );

	wp_localize_script('mrea', 'meo_settings', array(
		'ajax_url'           => admin_url('admin-ajax.php'),
		'site_id'            => mrea_get_site_id(),
		'sites'              => mrea_get_sites_for_user(),
		'contact_url'        => get_permalink(PAGE_ID_CONTACT),
		'search_placeholder' => __( 'Search...', 'meo_real_estate_admin' ),
		'all_placeholder'    => __( 'All', 'meo_real_estate_admin' ),
		'can_edit'           => current_user_can( 'edit_sales' ) ? 1 : 0,
		'stylesheet_url'     => get_template_directory_uri(),
		'translations'       => array(
			'yesterday'  => __( "yesterday",    'meo_real_estate_admin' ),
			'today'      => __( "today",        'meo_real_estate_admin' ),
			'clicktoadd' => __( "Click to add", 'meo_real_estate_admin' )
		),
		'datatable_trans'    => array(
			"oAria" => array(
				"sSortAscending"  => __( ": activate to sort column ascending", 'meo_real_estate_admin' ),
				"sSortDescending" => __( ": activate to sort column descending", 'meo_real_estate_admin' )
			),
			"oPaginate" => array(
				"sFirst"    => __( "First", 'meo_real_estate_admin' ),
				"sLast"     => __( "Last", 'meo_real_estate_admin' ),
				"sNext"     => __( "Next", 'meo_real_estate_admin' ),
				"sPrevious" => __( "Previous", 'meo_real_estate_admin' )
			),
			"sEmptyTable"     => __( 'Nothing found', 'meo_real_estate_admin' ),
			"sInfo"           => __("Showing _START_ to _END_ of _TOTAL_ entries", 'meo_real_estate_admin' ),
			"sInfoEmpty"      => __("Showing 0 to 0 of 0 entries", 'meo_real_estate_admin' ),
			"sInfoFiltered"   => __("(filtered from _MAX_ total entries)", 'meo_real_estate_admin' ),
			"sInfoThousands"  => __(",", 'meo_real_estate_admin' ),
			"sLengthMenu"     => __("Show _MENU_ entries", 'meo_real_estate_admin' ),
			"sLoadingRecords" => __("Loading...", 'meo_real_estate_admin' ),
			"sProcessing"     => __("Processing...", 'meo_real_estate_admin' ),
			"sSearch"         => "",
			"sZeroRecords"    => __( "No matching records found", 'meo_real_estate_admin' )
		)
	) );

	$has_data_tables = is_page_template( 'sales-status.php' ) || is_page_template( 'contacts.php' ) || is_page_template( 'contact.php' );
	if ( $has_data_tables ) {
		wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/dataTables.bootstrap.css', array(), '1.0.0' );

		wp_enqueue_script( 'jquery-datatables', get_template_directory_uri() . '/js/jquery.dataTables.js', array('jquery'), '1.9.4', true );
		wp_enqueue_script( 'datatables-bootstrap', get_template_directory_uri() . '/js/dataTables.bootstrap.js', array('jquery-datatables'), '1.0.0', true );
	}

	$site_id = mrea_get_site_id();
	$uses_crm = mrea_uses_crm($site_id);
	if (is_page_template( 'contacts.php' )) {
		wp_enqueue_script( 'jquery-timeago', get_template_directory_uri() . '/js/jquery.timeago.js', array('jquery'), '1.4.1', true );
		wp_enqueue_script( 'contacts-table-shared', get_template_directory_uri() . '/js/contacts_table_shared.js', array('datatables-bootstrap', 'jquery-timeago'), filemtime( get_stylesheet_directory() . '/js/contacts_table_shared.js'), true );
		if ($uses_crm) {
			wp_enqueue_script( 'contacts-table', get_template_directory_uri() . '/js/contacts_table_crm.js', array('contacts-table-shared'), filemtime( get_stylesheet_directory() . '/js/contacts_table_crm.js'), true );
		}
		else {
			wp_enqueue_script( 'contacts-table', get_template_directory_uri() . '/js/contacts_table.js', array('contacts-table-shared'), filemtime( get_stylesheet_directory() . '/js/contacts_table.js'), true );
		}
	}

	if (is_page_template( 'sales-status.php' )) {
		wp_enqueue_script( 'sales-table-shared', get_template_directory_uri() . '/js/sales_table_shared.js', array('datatables-bootstrap'), filemtime( get_stylesheet_directory() . '/js/sales_table_shared.js'), true );
		if ($uses_crm) {
			wp_enqueue_script( 'jeditable', get_template_directory_uri() . '/js/jquery.jeditable.js', array('jquery'), filemtime( get_stylesheet_directory() . '/js/jquery.jeditable.js'), true );
			wp_enqueue_script( 'sales-table', get_template_directory_uri() . '/js/sales_table_crm.js', array('sales-table-shared'), filemtime( get_stylesheet_directory() . '/js/sales_table_crm.js'), true );
		}
		else {
			wp_enqueue_script( 'sales-table', get_template_directory_uri() . '/js/sales_table.js', array('sales-table-shared'), filemtime( get_stylesheet_directory() . '/js/sales_table.js'), true );
		}
	}


	if (is_page_template( 'contact.php' )) {
		if ($uses_crm) {
			wp_enqueue_script( 'jquery-timeago', get_template_directory_uri() . '/js/jquery.timeago.js', array('jquery'), '1.4.1', true );
			wp_enqueue_script( 'contacts-details', get_template_directory_uri() . '/js/contact_details_crm.js', array('jquery-timeago'), filemtime( get_stylesheet_directory() . '/js/contact_details_crm.js'), true );
			wp_localize_script('contacts-details', 'contact_details', array(
				'contact_id' => (int) $_GET['contact_id']
			));
		}
		else {
			wp_enqueue_script( 'purl', get_template_directory_uri() . '/js/purl.js', array(), '2.3.1', true );
			wp_enqueue_script( 'contacts-details', get_template_directory_uri() . '/js/contact_details.js', array(), filemtime( get_stylesheet_directory() . '/js/contact_details.js'), true );

			wp_localize_script('contacts-details', 'contact_details', array(
				'data_url'     => get_permalink(PAGE_ID_CONTACT_JSON),
				'analytics_id' => $_GET['analytics_id']
			));
		}
	}
}
add_action( 'wp_enqueue_scripts', 'meo_real_estate_admin_scripts' );


/* =================================================================================================================
 * Backward compatibility for old versions of IE
 * ================================================================================================================= */
add_action('wp_head', 'mrea_html5_backward_compatability');
function mrea_html5_backward_compatability() { ?>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]--><?php
}

add_action('wp_footer', 'mrea_excanvas_ie', 50);
function mrea_excanvas_ie() { ?>
	<!--[if lt IE 9]>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/excanvas.compiled.js"></script>
	<![endif]--><?php
}


/* =================================================================================================================
 * Custom template tags for this theme.
 * ================================================================================================================= */
require get_template_directory() . '/inc/template-tags.php';


/* =================================================================================================================
 * Custom functions that act independently of the theme templates.
 * ================================================================================================================= */
require get_template_directory() . '/inc/extras.php';


/* =================================================================================================================
 * Customizer additions.
 * ================================================================================================================= */
require get_template_directory() . '/inc/customizer.php';


/* =================================================================================================================
 * Load Jetpack compatibility file.
 * ================================================================================================================= */
require get_template_directory() . '/inc/jetpack.php';


/* =================================================================================================================
 * Error handler
 * ================================================================================================================= */
function mrea_report_error($file, $line, $message) {
	/*if (!defined('REPORT_ERRORS_TO') || empty(REPORT_ERRORS_TO) ) {
		return '';
	}
	$content = "Error in $file, line $line:\n\n" . print_r($message, true);
	$headers = array(
		'From: MEO Real Estate Admin <' . MAIL_USER . '>'
	);
	wp_mail( REPORT_ERRORS_TO, 'client.meo-realestate.com :: error', $content, $headers );*/
}


/* =================================================================================================================
 * Login page customisation
 * ================================================================================================================= */
// Based on WP Custom Login plugin
if( !isset($_REQUEST['interim-login']) )
{
	if( file_exists( get_theme_root() . '/' . get_stylesheet() . '/css/wp-custom-login.css' ) ) {
		wp_register_style( 'wp-custom-login', get_stylesheet_directory_uri() . '/css/wp-custom-login.css' );
	}


	add_action( 'login_head', 'wp_custom_login_header', 1 );
	function wp_custom_login_header() {
		wp_enqueue_style( 'wp-custom-login' );

		ob_start();
		get_header();
	}

	add_action( 'login_footer', 'wp_custom_login_footer', 99999 );
	function wp_custom_login_footer() {
		get_footer();
		$login_page_content = ob_get_clean();

		// There are two <body> tags - one from the header.php, one from the standard login page
		// We need to move the classes from the second to the first, and delete the unnecessary
		// code around the second.

		// The first body class is empty, so use + in the regexp to ensure we get the second
		preg_match('/<body class="([^"]+)">/', $login_page_content, $matches);
		$body_classes = $matches[1];

		// Delete stuff in content that should be in the header
		$login_page_content = preg_replace('/<div class="container">\s*<meta name=.robots.*<body class="[^"]+">/s', '<div class="container">', $login_page_content);

		// Add the extracted body classes to the correct tag
		$login_page_content = str_replace('<body class="', '<body class="' . $body_classes, $login_page_content);


		// Delete what will be duplicated opening tags
		$login_page_content = preg_replace('/^.*<link rel="pingback" href="[^"]+">/s', '', $login_page_content);

		// Delete what will be duplicated closing tags
		$login_page_content = preg_replace('/<\/body>\s*<\/html>/s', '', $login_page_content);

		// Change classes on buttons
		$login_page_content = preg_replace('/(<input type="submit"[^>]+class=")[^"]+/', '${1}btn btn-primary btn-block btn-lg', $login_page_content);

		// Move "Lost your pasword" link before button
		if (!isset($_GET['action'])) {
			$login_page_content = preg_replace('/(<p class="submit">.*<\/form>\s*)(<p id="nav">.*?<\/p>)/s', '${2}${1}', $login_page_content);
		}

		echo $login_page_content;
	}
}

add_filter('login_body_class', 'mrea_login_body_class');
function mrea_login_body_class($classes) {
	$classes[] = 'account-bg'; // ensure page has grey background
	return $classes;
}


add_filter( 'login_headerurl', 'mrea_login_headerurl' );
function mrea_login_headerurl( $url ) {
	return 'http://www.meo-realestate.com/';
}

add_filter( 'login_headertitle', 'mrea_login_headertitle' );
function mrea_login_headertitle( $title ) {
	return __( 'MEO Real Estate', 'meo_real_estate_admin' );
}

add_filter( 'login_message', 'mrea_login_message' );
function mrea_login_message( $message ) {
	if (empty($message)) {
		$message = "<p>" . __( 'Please sign in to get access.', 'meo_real_estate_admin' ) . "</p>";
	}
	return $message;
}

/* =================================================================================================================
 * Custom site custom post type
 * ================================================================================================================= */
add_action('init', 'mrea_create_custom_post_types', 5);
function mrea_create_custom_post_types() {
	register_post_type(CPT_SITE, array(
		'labels' => array(
			'name' => __('Sites', 'meo_real_estate_admin'),
			'singular_name' => __('Site', 'meo_real_estate_admin'),
			'add_new_item' => __('Add a site', 'meo_real_estate_admin'),
			'edit_item' => __('Edit site', 'meo_real_estate_admin'),
			'new_item' => __('Add a site', 'meo_real_estate_admin'),
			'search_items' => __('Find a site', 'meo_real_estate_admin'),
			'not_found' => __('No site found', 'meo_real_estate_admin'),
			'not_found_in_trash' => __('No site found in the trash', 'meo_real_estate_admin'),
		),
                'taxonomies' => array('category',),
		'public' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'query_var' => true,
		'show_ui' => true,
		'can_export' => true,
		'supports' => array('title')
	));

	p2p_register_connection_type( array(
		'name' => 'sites_to_users',
		'from' => CPT_SITE,
		'to' => 'user',
		'to_query_vars' => array( 'role' => 'client-or-app-user' ), // Dummy role, since I can't see a way to add both.  See mrea_revise_role_query() below
		'admin_column' => 'from'
	) );

	add_action('pre_user_query', 'mrea_revise_role_query');

	register_post_type(CPT_CLIENT, array(
		'labels' => array(
			'name' => __('Clients', 'meo_real_estate_admin'),
			'singular_name' => __('Client', 'meo_real_estate_admin'),
			'add_new_item' => __('Add a client', 'meo_real_estate_admin'),
			'edit_item' => __('Edit client', 'meo_real_estate_admin'),
			'new_item' => __('Add a client', 'meo_real_estate_admin'),
			'search_items' => __('Find a client', 'meo_real_estate_admin'),
			'not_found' => __('No client found', 'meo_real_estate_admin'),
			'not_found_in_trash' => __('No client found in the trash', 'meo_real_estate_admin'),
		),
		'public' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'query_var' => true,
		'show_ui' => true,
		'can_export' => true,
		'supports' => array('title')
	));


	p2p_register_connection_type( array(
		'name' => 'sites_to_clients',
		'from' => CPT_SITE,
		'to' => CPT_CLIENT,
		'admin_column' => 'from',
		'cardinality' => 'many-to-one'
	) );
}

function mrea_revise_role_query($query) {
	global $wpdb;

	if (preg_match('/client-or-app-user/', $query->query_where)) {
		$meta_table = _get_meta_table( 'user' );

		$query->query_where = preg_replace('/\([^\(\)]*CAST\([^\(\)]*\)[^\(\)]*client-or-app-user[^\(\)]*\)/',
			"(
				{$meta_table}.meta_key = 'wp_capabilities' AND {$meta_table}.meta_value LIKE '%\"client\"%'
				OR
				{$meta_table}.meta_key = 'wp_capabilities' AND {$meta_table}.meta_value LIKE '%\"app_user\"%'
			)",
			$query->query_where);
	}
}

/* =================================================================================================================
 * User / site functions
 * ================================================================================================================= */
function mrea_get_site_id($set_cookie = true) {
	$sites = mrea_get_sites_for_user();
	if (count($sites) < 1) {
		return -1;
	}

	// Take the first site by default
	reset($sites);
	$site_id = key($sites);

	if (isset($_GET['site_id']) and array_key_exists((int) $_GET['site_id'], $sites)) {
		$site_id = (int) $_GET['site_id'];
	}
	elseif(isset($_COOKIE[COOKIE_SELECTED_SITE]) and array_key_exists((int) $_COOKIE[COOKIE_SELECTED_SITE], $sites)) {
		$site_id = (int) $_COOKIE[COOKIE_SELECTED_SITE];
	}

	if ($set_cookie) {
		setcookie(COOKIE_SELECTED_SITE, $site_id, time()+60*60*24*30, '/', null, null, true );
	}

	return $site_id;
}


function mrea_get_site_selector_form() {
	global $meo_site_id;
	if (empty($meo_site_id)) {
		$meo_site_id = mrea_get_site_id();
	}

	$sites = mrea_get_sites_for_user();
	if (count($sites) < 2) {
		return;
	}

	?><form action="<?php the_permalink(); ?>" method="get" class="site-selector"><?php
	mrea_get_site_selector($meo_site_id, $sites);
	?></form><?php
}

function mrea_get_site_selector($current_site_id, $sites) {
	?><select name="site_id"><?php

	foreach ($sites as $site) {
		?><option value="<?php echo $site['id']; ?>"<?php echo $current_site_id == $site['id'] ? ' selected="selected"' : ''; ?>><?php echo $site['name']; ?></option><?php
	}

	?></select><?php
}

function mrea_get_sites_for_user($username = null) {
	global $sites_for_user;

	if (!empty($sites_for_user) && empty($username)) {
		return $sites_for_user;
	}

	$user = null;
	if ($username) {
		$user = new WP_User(null, $username);
	}
	if (!$user) {
		$user = wp_get_current_user();
	}

	if ( in_array('administrator', (array) $user->roles) ) {
		$result = mrea_get_all_sites();
		if (empty($username)) {
			$sites_for_user = $result;
		}
		return $result;
	}

	$query = new WP_Query( array(
		'connected_type' => 'sites_to_users',
		'connected_items' => $user,
		'nopaging' => true
	) );

	$result = mrea_extract_site_details($query);

	wp_reset_postdata();

	if (empty($username)) {
		$sites_for_user = $result;
	}

	return $result;
}

/* Fred */
function mrea_get_sites_for_user_v2meo($username = null) {
	global $sites_for_user;

	if (!empty($sites_for_user) && empty($username)) {
		return $sites_for_user;
	}

	$user = null;
	if ($username) {
		$user = new WP_User(null, $username);
	}
	if (!$user) {
		$user = wp_get_current_user();
	}

	if ( in_array('administrator', (array) $user->roles) ) {
		$result = mrea_get_all_sites_v2meo();
		if (empty($username)) {
			$sites_for_user = $result;
		}
		return $result;
	}

	$query = new WP_Query( array(
		'connected_type' => 'sites_to_users',
		'connected_items' => $user,
		'nopaging' => true
	) );

	$result = mrea_extract_site_details_v2meo($query);

	wp_reset_postdata();

	if (empty($username)) {
		$sites_for_user = $result;
	}

	return $result;
}

function mrea_get_all_sites() {
	$query = new WP_Query( array(
		'post_type' => CPT_SITE,
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC'
	) );

	$result = mrea_extract_site_details($query);

	return $result;
}

function mrea_get_all_sites_v2meo() {
	$query = new WP_Query( array(
		'post_type' => CPT_SITE,
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC'
	) );

	$result = mrea_extract_site_details_v2meo($query);

	return $result;
}

function mrea_get_site_by_api_key($api_key) {
	$sites = mrea_get_all_sites();
	foreach ($sites as $site) {
		if ($site['api_key'] == $api_key) {
			return $site;
		}
	}

	return null;
}

function mrea_extract_site_details($query) {
	global $post;

	$original_post = $post;

	$result = array();

	if (!$query->have_posts()) {
		return $result;
	}

	p2p_type( 'sites_to_clients' )->each_connected( $query );

	while ( $query->have_posts() ) {
		$query->the_post();

		$site = array();

		$site['id'] = $post->ID;
		$site['name'] = $post->post_title;
		$site['slug'] = $post->post_name;

		foreach (array('enabled_in_app', 'data_location', 'site_url', 'api_key', 'api_url', 'pdf_template', 'piwik_site_id', 'piwik_token', 'email_new_contact', 'email_weekly_stats', 'new_contact_email_content', 'page_name_regex') as $field) {
			$site[$field] = get_field($field, $post->ID);
		}

		$site['page_name_exclusions'] = array();
		$page_name_exclusions = get_field('page_name_exclusions', $post->ID);
		if ($page_name_exclusions) {
			foreach ($page_name_exclusions as $page_name_exclusion) {
				$site['page_name_exclusions'][] = trim($page_name_exclusion['exclude_page_title']);
			}
		}

		$allow_direct_requests = get_field('allow_direct_requests', $post->ID);
		$site['allow_direct_requests'] = ( !empty($allow_direct_requests) && $allow_direct_requests ) ? 1 : 0;

		$update_prices = get_field('update_prices', $post->ID);
		$site['update_prices'] = ( !empty($update_prices) && $update_prices ) ? 1 : 0;

		if (!empty($post->connected)) {
			$site['client_id'] = $post->connected[0]->ID;
			$site['uses_crm'] = get_field('mini_crm', $site['client_id']) == 1 ? 1 : 0;
		}


		$result[$post->ID] = $site;
	}

	if (is_admin()) {
		$post = $original_post;
		setup_postdata( $post );
	}
	else {
		wp_reset_postdata();
	}

	return $result;
}

function mrea_extract_site_details_v2meo($query) {
	global $post;

	$original_post = $post;

	$result = array();

	if (!$query->have_posts()) {
		return $result;
	}

	p2p_type( 'sites_to_clients' )->each_connected( $query );

	while ( $query->have_posts() ) {
		$query->the_post();

		$site = array();

		$site['id'] = $post->ID;
		$site['name'] = $post->post_title;
		$site['slug'] = $post->post_name;

		$requiredFields = array('enabled_in_app', 'data_location', 'site_url', 'api_key', 'api_url', 'pdf_template', 'piwik_site_id', 'piwik_token', 'email_new_contact', 'email_weekly_stats', 'new_contact_email_content', 'page_name_regex');
		
		foreach ($requiredFields as $field) {
			$site[$field] = get_field($field, $post->ID);
		}

		$site['page_name_exclusions'] = array();
		$page_name_exclusions = get_field('page_name_exclusions', $post->ID);
		if ($page_name_exclusions) {
			foreach ($page_name_exclusions as $page_name_exclusion) {
				$site['page_name_exclusions'][] = trim($page_name_exclusion['exclude_page_title']);
			}
		}

		$allow_direct_requests = get_field('allow_direct_requests', $post->ID);
		$site['allow_direct_requests'] = ( !empty($allow_direct_requests) && $allow_direct_requests ) ? 1 : 0;

		$update_prices = get_field('update_prices', $post->ID);
		$site['update_prices'] = ( !empty($update_prices) && $update_prices ) ? 1 : 0;

		if (!empty($post->connected)) {
			$site['client_id'] = $post->connected[0]->ID;
			$site['uses_crm'] = get_field('mini_crm', $site['client_id']) == 1 ? 1 : 0;
		}


		$result[$post->ID] = $site;
	}

	if (is_admin()) {
		$post = $original_post;
		setup_postdata( $post );
	}
	else {
		wp_reset_postdata();
	}

	return $result;
}


function mrea_uses_crm($site_id) {
	$sites = mrea_get_sites_for_user();

	return ( !empty($sites[$site_id]) and $sites[$site_id]['uses_crm'] ) ? 1 : 0;
}

/* =================================================================================================================
 * Settings page functionality
 * ================================================================================================================= */

add_action( 'wp_ajax_nopriv_reset_user_pass', 'mrea_must_be_logged_in' );
add_action( 'wp_ajax_nopriv_update_profile', 'mrea_must_be_logged_in' );
add_action( 'wp_ajax_nopriv_update_site_notification_emails', 'mrea_must_be_logged_in' );

function mrea_must_be_logged_in() {
	header('Content-Type: application/json');
	echo json_encode(array(
		'success' => 0,
		'message' => __('You must be logged in', 'meo_real_estate_admin')
	));
	exit;
}

add_action( 'wp_ajax_reset_user_pass', 'mrea_reset_user_pass' );
function mrea_reset_user_pass() {
	header('Content-Type: application/json');

	$current_user = wp_get_current_user();

	if (empty($_POST['old-password']) || empty($_POST['new-password-1']) || empty($_POST['new-password-2'])) {
		echo json_encode(array(
			'success' => 0,
			'message' => __('Please fill in all fields', 'meo_real_estate_admin')
		));
		exit;
	}

	if (! wp_check_password( $_POST['old-password'], $current_user->data->user_pass, $current_user->ID)) {
		echo json_encode(array(
			'success' => 0,
			'message' => __('Old password is incorrect', 'meo_real_estate_admin')
		));
		exit;
	}

	if ($_POST['new-password-1'] != $_POST['new-password-2']) {
		echo json_encode(array(
			'success' => 0,
			'message' => __('New password mismatch', 'meo_real_estate_admin')
		));
		exit;
	}

	wp_set_password( $_POST['new-password-1'], $current_user->ID );

	// Force a reconnect, as wp_set_password will log the user out
	wp_cache_delete($current_user->data->ID, 'users');
	wp_cache_delete($current_user->data->user_login, 'userlogins');
	wp_logout();
	wp_signon(array('user_login' => $current_user->data->user_login, 'user_password' => $_POST['new-password-1']));

	echo json_encode(array(
		'success' => 1,
		'message' => __('Password changed', 'meo_real_estate_admin')
	));
	exit;
}

add_action( 'wp_ajax_update_profile', 'mrea_update_profile' );
function mrea_update_profile() {
	header('Content-Type: application/json');

	$current_user = wp_get_current_user();

	if ($current_user->data->user_email != $_POST['email-address']) {
		// User is trying to update their email address
		if (!is_email($_POST['email-address'])) {
			echo json_encode(array(
				'success' => 0,
				'message' => __('Invalid email address', 'meo_real_estate_admin')
			));
			exit;
		}

		if (email_exists( $_POST['email-address'] )){
			echo json_encode(array(
				'success' => 0,
				'message' => __('email address already in use', 'meo_real_estate_admin')
			));
			exit;
		}

        wp_update_user( array(
			'ID'         => $current_user->data->ID,
			'user_email' => esc_attr( $_POST['email-address'] )
		) );

	}

	update_user_meta( $current_user->data->ID, 'first_name', wp_slash($_POST['first-name']) );
	update_user_meta( $current_user->data->ID, 'last_name',  wp_slash($_POST['last-name'])  );

	echo json_encode(array(
		'success' => 1,
		'message' => __('Data updated', 'meo_real_estate_admin')
	));
	exit;
}


add_action( 'wp_ajax_update_site_notification_emails', 'mrea_update_site_notification_emails' );
function mrea_update_site_notification_emails() {
	header('Content-Type: application/json');

	$acf_field_ids = array(
		'email_new_contact'  => 'field_5404ee55d5aa8',
		'email_weekly_stats' => 'field_5404ee88d5aa9'
	);

	$current_user = wp_get_current_user();

	if (empty($_POST['site_id'])) {
		echo json_encode(array(
			'success' => 0,
			'message' => __('Please fill in all fields', 'meo_real_estate_admin')
		));
		exit;
	}

	$site_id = (int) $_POST['site_id'];
	$sites = mrea_get_sites_for_user();

	if (!array_key_exists($site_id, $sites)) {
		echo json_encode(array(
			'success' => 0,
			'message' => __("You don't have permissions to change that site", 'meo_real_estate_admin')
		));
		exit;
	}

	foreach (array_keys($acf_field_ids) as $field) {
		$value = $_POST[$field];
		$addresses = preg_split('/,/', $value, -1, PREG_SPLIT_NO_EMPTY);

		$result = array();
		foreach ($addresses as $address) {
			$address = trim($address);
			if (empty($address)) {
				continue;
			}
			if (!is_email($address)) {
				echo json_encode(array(
					'success' => 0,
					'message' => __("Invalid email address", 'meo_real_estate_admin')
				));
				exit;
			}
			$result[] = $address;
		}
		update_field( $acf_field_ids[$field], join(",", $result), $site_id );
	}

	echo json_encode(array(
		'success' => 1,
		'message' => __('Data updated', 'meo_real_estate_admin')
	));
	exit;
}

/* =================================================================================================================
 * Contact page functionality
 * ================================================================================================================= */
function mrea_get_piwik_base_url() {
	return "https://piwik.meoanalytics.com/"; // TODO: add dev server
}

function mrea_get_piwik_request_url($site, $raw_analytics_id) {
	$raw_analytics_id = urldecode($raw_analytics_id);
	$raw_analytics_ids = preg_split('/,/', $raw_analytics_id);

	$visitor_segment = '';
	$delimiter = '';
	foreach ($raw_analytics_ids as $analytics_id_element) {
		$analytics_id = preg_replace('/[^a-z0-9A-Z]/', '', $analytics_id_element);
		if (!empty($analytics_id)) {
			$visitor_segment .= $delimiter . "visitorId%3D%3D" . $analytics_id;
			$delimiter = ',';
		}
	}



	$url = mrea_get_piwik_base_url() . '?';
	$parameters = array(
		"module" => "API",
		"method" => "API.getBulkRequest",
		"format" => "json"
	);

	$shared_request_parameters = array(
		"idSite"     => $site['piwik_site_id'],
		"token_auth" => $site['piwik_token'],
		"period"     => "year",
		"date"       => "today"
	);

	$parameters_by_request = array(
		// Lots IDs viewed
		'0' => array(
			"segment"  => $visitor_segment,
			"method"   => "CustomVariables.getCustomVariables",
			"expanded" => "1",
		),

		// Lots viewed (page statistics; post_type = lot)
		'1' => array(
			"segment"            => $visitor_segment . ";customVariablePageName2==post_type;customVariablePageValue2==lot",
			"method"             => "Actions.getPageTitles",
			"filter_sort_column" => "sum_time_spent"
		),

		// Other pages viewed (page statistics; post_type != lot)
		'2' => array(
			"segment"            => $visitor_segment . ";customVariablePageName2==post_type;customVariablePageValue2!=lot",
			"method"             => "Actions.getPageTitles",
			"filter_sort_column" => "sum_time_spent"
		),

		'3' => array(
			"segment" => $visitor_segment,
			"method"  => "Referrers.getAll"
		)
	);

	foreach ($parameters as $key => $value) {
		$url .= $key . "=" . $value . "&";
	}
	foreach($parameters_by_request as $index => $request) {
		$request_url = '';
		foreach ($shared_request_parameters as $key => $value) {
			$request_url .= $key . "=" . $value . "&";
		}
		foreach ($request as $key => $value) {
			$request_url .= $key . "=" . $value . "&";
		}
		$request_url = rtrim($request_url, "&");

		$url .= "urls[" . $index . "]=" . urlencode($request_url) . "&";
	}

	$url = rtrim($url, "&");

	return $url;
}

function mrea_get_development_data($site) {
	$development_url = $site['site_url'] . $site['api_url'] . '?action=get_development&api-key=' . $site['api_key'];
	$development_json = @file_get_contents($development_url);
	$development_data = ($development_json === false ? array() : json_decode($development_json));
	return $development_data;
}

function mrea_get_contact_data_by_analytics_id($site, $analytics_id) {
    
	$development_data = mrea_get_development_data($site);
	$lots = $development_data->lots;

	$result = mrea_get_piwik_data($site, $analytics_id, $lots);

	$user_url = $site['site_url'] . $site['api_url'] . '?action=get_contact&api-key=' . $site['api_key'] . '&analytics_id=' . $analytics_id;

	$user_json = @file_get_contents($user_url);
	$user_data = ($user_json === false ? array() : json_decode($user_json));

	foreach(array('submit_time', 'surname', 'first_name', 'email', 'phone', 'address', 'postcode', 'city', 'country', 'language') as $field) {
		$result[$field] = $user_data[0]->{$field};;
	}

	$lots_downloaded = array();
	foreach ($user_data as $download) {
		if (!empty($download->lot_code)) {
			$lots_downloaded[] = $download->lot_code;
		}
		if ($download->submit_time < $result['submit_time']) {
			$result['submit_time'] = $download->submit_time;
		}
	}
	$result['lots_downloaded'] = $lots_downloaded;

	return $result;
}

function mrea_get_piwik_data($site, $analytics_id, $lots) {
	$result = array();

	$piwik_url = mrea_get_piwik_request_url($site, $analytics_id);
	$piwik_json = @file_get_contents($piwik_url);
	$piwik_data = ($piwik_json === false ? array() : json_decode($piwik_json));

	$result['lots_viewed'] = mrea_filter_pages_viewed($site, $piwik_data[1]);
	$result['pages_viewed'] = mrea_filter_pages_viewed($site, $piwik_data[2]);

	$lot_sizes = array();

	$total_rooms_viewed = 0;
	if (!empty($piwik_data)) {
		foreach ($piwik_data[0] as $custom_variable) {
			if ($custom_variable->label != "post_id") {
				continue;
			}
			foreach($custom_variable->subtable as $values) {
				$lot_id = $values->label;
				$rooms = $lots->{$lot_id}->pieces;
				if ($rooms) {
					if (!array_key_exists($rooms, $lot_sizes)) {
						$lot_sizes[$rooms] = 0;
					}
					$lot_sizes[$rooms] += $values->nb_actions;
					$total_rooms_viewed += $values->nb_actions;
				}
			}
		}
		arsort($lot_sizes);
	}
	$result['lot_sizes'] = $lot_sizes;
	$result['total_rooms_viewed'] = $total_rooms_viewed;

	$referrer = '';
	if (!empty($piwik_data) and isset($piwik_data[3])) {
		foreach($piwik_data[3] as $ref) {
			if ($ref->label) {
				$referrer = $ref->label;
			}
		}
	}

	$rooms_per_lots = array();
	if (!empty($lots)) {
		foreach ($lots as $lot) {
			$rooms_per_lots[$lot->name] = $lot->pieces;
		}
	}
	$result['rooms_per_lots'] = $rooms_per_lots;

	$result['referrer'] = $referrer;

	return $result;
}
function mrea_filter_pages_viewed($site, $pages) {
	$result = array();

	if (!empty($pages)) {
		foreach ($pages as $page) {
                    
                    
                    if(isset($page) && is_object($page))
                    {
			if ($site['page_name_regex']) {
                            $value = preg_replace($site['page_name_regex'], '$1', $page->label);
                            
                            $page->label = $value;
			}
			if ($page->label === "parallèle") {
				$page->label = "plan masse";
			}

			if (in_array(trim($page->label), $site['page_name_exclusions'])) {
				continue;
			}
                    }
                    else $page = false;

                    $result[] = $page;
		}
	}

	return $result;
}
/*function mrea_filter_pages_viewed($site, $pages) {
	$result = array();

	if (!empty($pages)) {
		foreach ($pages as $page) {
                        
			if ($site['page_name_regex']) {
				$page->label = preg_replace($site['page_name_regex'], '$1', $page->label);
			}
			if ($page->label === "parallèle") {
				$page->label = "plan masse";
			}

			if (in_array(trim($page->label), $site['page_name_exclusions'])) {
				continue;
			}

			$result[] = $page;
		}
	}

	return $result;
}*/


function mrea_get_last_visit_data($site, $analytics_id) {
	$url = mrea_get_piwik_base_url() .
	       "?module=API" .
	       "&method=Live.getLastVisitsDetails" .
	       "&format=json" .
	       "&idSite=" . $site['piwik_site_id'] .
	       "&period=year" .
	       "&date=today" .
		   "&token_auth=" . $site['piwik_token'] .
		   "&segment=visitorId==" . $analytics_id .
	       "&filter_limit=1";

	$curl = new WP_Http_Curl();
	$response = $curl->request($url, array(
		'headers' => array(
			'Accept'       => 'application/json',
			'Content-type' => 'application/json',
		),
		'method' => 'GET',
		'redirection' => 0
	));

	if ( is_wp_error( $response ) || empty( $response['body']) ) {
		// piwik call failed.  Notify MEO
		mrea_report_error(__FILE__, __LINE__, array(
			'error'        => 'Failed to get data from piwik (error in request or empty body)',
			'site_id'      => empty($site) ? '' : $site['id'],
			'site_name'    => empty($site) ? '' : $site['name'],
			'analytics_id' => $analytics_id,
			'url'          => $url,
			'response'     => $response
		));
		return null;
	}

	$result = json_decode($response['body']);

	if ( empty($result) || $result->result == "error" ) {
		// piwik call failed.  Notify MEO
		mrea_report_error(__FILE__, __LINE__, array(
			'error'        => 'Failed to get data from piwik (JSON object empty).  Probably analytics ID not found in piwik',
			'site_id'      => empty($site) ? '' : $site['id'],
			'site_name'    => empty($site) ? '' : $site['name'],
			'analytics_id' => $analytics_id,
			'url'          => $url,
			'response'     => $response
		));
		return null;
	}

	return $result;
}

/* =================================================================================================================
 * Proxy ajax requests for development server
 * ================================================================================================================= */

add_action( 'wp_ajax_nopriv_get_contact_list',   'mrea_must_be_logged_in' );
add_action( 'wp_ajax_nopriv_get_statuses',       'mrea_must_be_logged_in' );
add_action( 'wp_ajax_nopriv_get_apartment_list', 'mrea_must_be_logged_in' );
add_action( 'wp_ajax_nopriv_set_status',         'mrea_must_be_logged_in' );
add_action( 'wp_ajax_nopriv_set_price',          'mrea_must_be_logged_in' );
add_action( 'wp_ajax_nopriv_set_linked_lot',     'mrea_must_be_logged_in' );


add_action( 'wp_ajax_get_contact_list',   'mrea_proxy_get_contact_list'   );
add_action( 'wp_ajax_get_statuses',       'mrea_proxy_get_statuses'       );
add_action( 'wp_ajax_get_apartment_list', 'mrea_proxy_get_apartment_list' );
add_action( 'wp_ajax_set_status',         'mrea_proxy_set_status'         );
add_action( 'wp_ajax_set_price',          'mrea_proxy_set_price'         );
add_action( 'wp_ajax_set_linked_lot',     'mrea_set_linked_lot'           );

function mrea_proxy_get_contact_list() {
	return mrea_proxy_request('GET', array(
		'action'  => $_GET['action'],
		'api-key' => $_GET['api-key']
	),
	true);
}

function mrea_proxy_get_statuses() {
	return mrea_proxy_request('POST', array(
		'action'  => $_POST['action'],
		'api-key' => $_POST['api-key']
	),
	true);
}

function mrea_proxy_get_apartment_list() {
	return mrea_proxy_request('GET', array(
		'action'  => $_GET['action'],
		'api-key' => $_GET['api-key']
	),
	true);
}

function mrea_proxy_set_status() {
	return mrea_proxy_request('POST', array(
		'action'  => $_POST['action'],
		'api-key' => $_POST['api-key'],
		'lot_id'  => $_POST['lot_id'],
		'status'  => $_POST['status']
	),
	true);
}

function mrea_proxy_set_price() {
	return mrea_proxy_request('POST', array(
		'action'  => $_POST['action'],
		'api-key' => $_POST['api-key'],
		'lot_id'  => $_POST['lot_id'],
		'price'  => $_POST['price']
	),
	true);
}

function mrea_set_linked_lot() {
	global $meocrm;

	$site = mrea_get_site_by_api_key($_POST['api-key']);
	if (empty($site)) {
		header('Content-Type: application/json');
		echo json_encode(array(
			'success' => 0,
			'message' => __('Unknown API key', 'meo_real_estate_admin')
		));
		exit;
	}

	$contact_id = $_POST['contact_id'];
	if (isset($_POST['prefix']) && !empty($_POST['prefix'])) {
		$contact_id = preg_replace("/^" . preg_quote($_POST['prefix'], '/') . "/", '', $contact_id);
	}

	$rows_inserted = $meocrm->setLinkedContactForLot($site['id'], (int) $_POST['lot_id'], (int) $contact_id);

	header('Content-Type: application/json');
	echo json_encode(array(
		'success' => $rows_inserted > 0 ? 1 :0,
		'message' => $rows_inserted > 0 ? __('Success', 'meo_real_estate_admin') : __('Failed', 'meo_real_estate_admin')
	));
	exit;
}


function mrea_proxy_request($method, $parameters, $exit = true) {
	$sites = mrea_get_all_sites();
	$site = mrea_get_site_by_api_key($parameters['api-key']);
        
	if (empty($site)) {
		$result = array(
			'success' => 0,
			'message' => __('Unknown API key', 'meo_real_estate_admin')
		);
		if ($exit) {
			header('Content-Type: application/json');
			echo json_encode($result);
			exit;
		}
		return $result;
	}

	$url = $site['site_url'] . $site['api_url'];

	$curl = new WP_Http_Curl();
	$request_parameters = array(
		'headers' => array(),
		'method' => $method,
		'redirection' => 0
	);

	if ($method == 'POST') {
		$request_parameters['body'] = http_build_query($parameters);
		$request_parameters['headers']['User-Agent'] = $_SERVER['HTTP_USER_AGENT'];
	}
	else {
		$url .= '?' . http_build_query($parameters);
	}

	$response = $curl->request($url, $request_parameters);

	if ( is_wp_error( $response ) || empty( $response['body'] ) || (int )$response['response']['code'] < 200  || (int )$response['response']['code'] > 299 ) {

		$message = 'Unknown Error';
                
		/*if(!empty($response['errors']))
		{
                    $message = '';
                    foreach($response['errors'] as $errors) 
                    {
                        foreach($errors as $error)
                        {
                            $message .= $error.' | ';
                        }
                    }
		}*/
                
		$result = array(
			'success' => 0,
			'message' => __($message, 'meo_real_estate_admin')
		);

		if ($exit) {
			header('Content-Type: application/json');
			echo json_encode($result);
			exit;
		}
		return $result;
	}

	$result = $response['body'];

	if ($parameters['action'] == 'get_apartment_list' && $site['uses_crm']) {
		$result = mrea_get_lot_notes($site, $result);
	}

	if ($exit) {
		header('Content-Type: application/json');
		echo $result;
		exit;
	}
	return json_decode($result);
}

function mrea_get_lot_notes($site, $raw) {
	global $meocrm;

	$raw_lots = $meocrm->getRemoteLots($site['id']);
	$lots = array();
	foreach ($raw_lots as $raw_lot) {
		$lots[$raw_lot->remote_lot_id] = $raw_lot->note;
	}

	$obj = json_decode($raw);

	foreach ($obj->aaData as $index => $row) {
		$obj->aaData[$index]->note = isset($lots[$row->lot_id]) ? $lots[$row->lot_id] : null;
	}

	$result = json_encode($obj);

	return $result;
}
/** Enqueue admin scripts and styles */
function wpmediacategory_enqueue_media_action_theme() {

        global $pagenow;
        //if ( wp_script_is( 'media-editor' ) && 'upload.php' == $pagenow ) {

                // Default taxonomy
                $taxonomy = 'category';
                // Add filter to change the default taxonomy
                $taxonomy = apply_filters( 'wpmediacategory_taxonomy', $taxonomy );

                if ( $taxonomy != 'category' ) {
                        $dropdown_options = array(
                                'taxonomy'        => $taxonomy,
                                'hide_empty'      => false,
                                'hierarchical'    => true,
                                'orderby'         => 'name',
                                'show_count'      => true,
                                'walker'          => new wpmediacategory_walker_category_mediagridfilter(),
                                'value'           => 'id',
                                'echo'            => false
                        );
                } else {
                        $dropdown_options = array(
                                'taxonomy'        => $taxonomy,
                                'hide_empty'      => false,
                                'hierarchical'    => true,
                                'orderby'         => 'name',
                                'show_count'      => false,
                                'walker'          => new wpmediacategory_walker_category_mediagridfilter(),
                                'value'           => 'id',
                                'echo'            => false
                        );
                }
                $attachment_terms = wp_dropdown_categories( $dropdown_options );
                $attachment_terms = preg_replace( array( "/<select([^>]*)>/", "/<\/select>/" ), "", $attachment_terms );

                echo '<script type="text/javascript">';
                echo '/* <![CDATA[ */';
                echo 'var wpmediacategory_taxonomies = {"' . $taxonomy . '":{"list_title":"' . html_entity_decode( __( 'View all categories', 'wp-media-library-categories' ), ENT_QUOTES, 'UTF-8' ) . '","term_list":[' . substr( $attachment_terms, 2 ) . ']}};';
                echo '/* ]]> */';
                echo '</script>';

                wp_enqueue_script( 'wpmediacategory-media-views', get_bloginfo('home').'/wp/wp-content/plugins/wp-media-library-categories/js/wpmediacategory-media-views.min.js', array( 'media-views' ), '1.5.2', true );
        //}
        wp_enqueue_style( 'wpmediacategory', get_bloginfo('home').'/wp/wp-content/plugins/wp-media-library-categories/js/wpmediacategory-media-views.min.js', array(), '1.5.2' );
}
add_action( 'admin_enqueue_scripts', 'wpmediacategory_enqueue_media_action_theme' );

function get_contacts_coordonnee()
{
    global $wpdb;
    $tabCoordonnee = array();
    $count = 0;
    $site_id = $_GET['site_id'];   
    
    $results = $wpdb->get_results('
        SELECT *
        FROM wp_meo_contacts
        WHERE site_id='.$site_id.'
    ');
    
    echo json_encode($results);
    wp_die(); 
}
add_action( 'wp_ajax_get_contacts_coordonnee', 'get_contacts_coordonnee' );
add_action( 'wp_ajax_nopriv_get_contacts_coordonnee', 'get_contacts_coordonnee' );

function update_contact_interaction()
{
    global $wpdb;    
    $id = $_POST['id'];
    $description = $_POST['description'];
    $table = 'wp_meo_contact_interactions';
    $data = array('description' => $description);
    $where = array('id' => $id);	
    return $wpdb->update($table,$data,$where);
}
add_action( 'wp_ajax_update_contact_interaction', 'update_contact_interaction' );
add_action( 'wp_ajax_nopriv_update_contact_interaction', 'update_contact_interaction' );

function update_contact_relationstate()
{
	global $wpdb;
	$id = $_POST['id'];
	$state = $_POST['state'];
	$table = 'wp_meo_contact_relation_states';
	$data = array('state' => $state);
	$where = array('id' => $id);
	return $wpdb->update($table,$data,$where);
}
add_action( 'wp_ajax_update_contact_relationstate', 'update_contact_relationstate' );
add_action( 'wp_ajax_nopriv_update_contact_relationstate', 'update_contact_relationstate' );