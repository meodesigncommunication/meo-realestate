<div class="row">
	<div class="twelve columns">
		<aside class="meta portfolio">
		  <?php echo thb_printLikes(get_the_ID()); ?>
		  
		  <?php $image_id = get_post_thumbnail_id();
		  			$image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0]; ?>
	  	<ul class="social">
	  		<li><span class="hide-for-small"><?php _e("Share:", THB_THEME_NAME); ?></span></li>
	  		<li><a href="http://twitthis.com/twit?url=<?php the_permalink(); ?>" class="twitter" target="_blank"><i class="icon-twitter"></i></a></li>
	  		<li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>" class="facebook" target="_blank"><i class="icon-facebook"></i></a></li>
	  		<li><a href="http://google.com/bookmarks/mark?op=edit&amp;bkmk=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>" class="googleplus" target="_blank"><i class="icon-google-plus"></i></a></li>
	  		<li><a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo $image_url; ?>" class="pinterest" target="_blank"><i class="icon-pinterest"></i></a></li>
	  		<li><a href="http://linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>" class="linkedin" target="_blank"><i class="icon-linkedin"></i></a></li>
	  	</ul>
		</aside>
	</div>
</div>