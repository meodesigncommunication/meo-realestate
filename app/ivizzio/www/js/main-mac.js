var app_prefix = '%%LOCALSTORAGE_PREFIX%%', // %% values are replaced in the build (by 020_set_version_number.js and 030_set_app_properties.js)
    alert_title = "%%ALERT_TITLE%%",
    local_directory_name = '%%DIRECTORY_NAME%%',
    splash_background = 'img/bg-splash.jpg',
    main_background = 'img/bg-app.jpg',
    use_dev_splash = '%%USE_DEV_SPLASH%%', // MEO Fred adding ''
    default_project_page = '%%DEFAULT_DEV_PAGE%%',
    app_name = '%%APP_NAME%%',
    app_version = '%%VERSION%%', // replaced with the version from config.xml in the build (by 020_set_version_number.js)
    prompt_before_update = '%%PROMPT_BEFORE_UPDATE%%', // MEO Fred adding ''
    disable_submit = false,
    skip_download = false,
    piwik_app_id = '%%PIWIK_APP_ID%%',
    piwik_enabled = '%%PIWIK_ENABLED%%', // MEO Fred adding ''
    platform = '%%PLATFORM%%';

var active_development_id;
var development_data = null;

var base_url = 'http://app.meo-realestate.com';

var piwik_url = 'http://piwik.meoanalytics.com/piwik.php';

var connection_active = false,
    active_spinner = null,
    active_zoomer = null,
	lots_by_floor = {},
	lots_by_rooms = {},
	lots_by_building = {},
	gallery_scrollbars = [],
	vignette_scrollbars = [],
	keyboard_timer = null,
	screen_height = null;

var imagecache_plugin_promise = null;

// These should be both the names of the lot properties and the keys in translations.building_labels
var area_types = ['surface_interior', 'surface_balcony', 'surface_terrace', 'surface_garden', 'surface_weighted'];

// Default translations.  Can be overidden with server data
var translations = {
	"building_labels" : {
		"volume": "volume",
		"surface_interior": "appartement",
		"surface_balcony":  "surface balcon",
		"surface_terrace":  "surface terrasse",
		"surface_garden":   "surface jardin",
		"surface_weighted": "surface pondérée",
		"property_info_explanation": "Surface pondérée = surface habitable +<br>1/2 surface balcon ou + 1/3 surface terrasse"
	},
	"initialisation_messages" : {
		"no_data_or_conn" : "Merci de connecter à Internet pour le téléchargement initial",
		"error_checking_session" : "Une erreur est survenue lors de la vérification de vos coordonnées. Merci de reconnecter.",
		"close" : "fermer",
		"update" : "Télécharger les données les plus récentes?"
	},
	"login_messages" : {
		"username" : "nom d'utilisateur",
		"password" : "mot de passe",
		"connect"  : "Connexion",
		"commerr" :  "erreur de connexion au serveur",
		"invalid" :  "les détails de connexion sont invalides",
		"no_pass" :  "mot de passe est vide",
		"no_user" :  "nom d'utilisateur est vide",
		"no_devs" :  "Vous ne avez pas accès à un développement.  S'il vous plaît vous connecter comme un autre utilisateur."
	},
	"confirm_messages" : {
		"logout" :  "Êtes-vous sûr? Pour pouvoir vous logger à nouveau sur cette application, vous aurez besoin d’une connexion internet, de votre nom d’utilisateur et de votre mot de passe.",
		"reload" :  "Êtes-vous sûr ? Cette opération peut prendre quelques minutes selon la qualité de votre connexion à internet et la quantité de nouveaux contenus à télécharger."
	},
	"error_messages" : {
		"missing_lot" : "Identificateur de lot manquant",
		"lot_not_active_promo" : "Invalid identificateur de lot pour la promotion actuelle",
		"open_file_failed" : "Erreur lors de l'ouverture du fichier"
	},
	"button_labels" : {
		"list" : "liste",
		"thumbs" : "vignettes",
		"gallery" : "galerie",
		"logout"  : "logout",
		"refresh"  : "refresh",
		"last_update" : "dernière mise à jour",
		"yes" : "Oui",
		"no" : "Non"
	},
	"thumb_labels" : {
		'buildings' : 'bâtiments',
		'floors' : 'étages',
		'rooms' : 'pièces',
		'building_short' : 'Bât',
		'rooms_short' : 'p.'
	},
	'lot_page_labels' : {
		'rooms' : 'Pièces'
	},
	"listing_headers" : {
		'lot'          : 'lot',
		'building'     : 'bâtiment',
		'floor'        : 'étage',
		'rooms'        : 'pièces',
		'surface'      : 'surf. ponderée',
		'availability' : 'disponibilité'
	},
	"month_names" : [
		"janvier",
		"février",
		"mars",
		"avril",
		"mai",
		"juin",
		"juillet",
		"août",
		"septembre",
		"octobre",
		"novembre",
		"décembre"
	],
	"availability" : {
		"available" : "Libre",
		"reserved"  : "Réservé",
		"sold"      : "Vendu"
	}
};


// ------------------------------------------------------------------------------------------------------ //
// Utilities
// ------------------------------------------------------------------------------------------------------ //
if (!Object.keys) {
	Object.keys = function (obj) {
		var keys = [],
			k;
		for (k in obj) {
			if (Object.prototype.hasOwnProperty.call(obj, k)) {
				keys.push(k);
			}
		}
		return keys;
	};
}

function is_plugin_installed(plugin_name) {
	if (typeof cordova === 'undefined' || !cordova) {
		return false;
	}
	var md = cordova.require("cordova/plugin_list").metadata;

	return md.hasOwnProperty(plugin_name);
}

function get_date_as_string(d) {
	return d.getDate() + " " + translations.month_names[d.getMonth()] + " " + d.getFullYear();
}

// ------------------------------------------------------------------------------------------------------ //
// ivizzio utility functions
// ------------------------------------------------------------------------------------------------------ //
function get_all_floors_for_lot(lot, development) {
	var result = [];

	if (typeof lot === 'undefined' ||
		lot === null ||
		typeof lot.all_floors === 'undefined' ||
		lot.all_floors === null ||
		typeof development === 'undefined' ||
		development === null ||
		typeof development.floors === 'undefined' ||
		development.floors === null ) {
		return result;
	}

	$.each(lot.all_floors, function(index, floor_id) {
		var floor = development.floors[floor_id];
		if (typeof floor !== 'undefined' && !!floor) {
			result.push(floor);
		}
	});

	return result;
}

function get_floor_for_lot(lot, development) {
	var result = null;

	if (typeof lot === 'undefined' ||
		lot === null ||
		typeof lot.floor_id === 'undefined' ||
		lot.floor_id === null ||
		typeof development === 'undefined' ||
		development === null ||
		typeof development.floors === 'undefined' ||
		development.floors === null ) {
		return result;
	}
	result = development.floors[lot.floor_id];
	return result;
}

function get_building_for_lot(lot, development) {
	var result = null;

	var floor = get_floor_for_lot(lot, development);
	if (typeof floor === 'undefined' ||
		floor === null ||
		typeof floor.building_id === 'undefined' ||
		floor.building_id === null ||
		development === null ||
		typeof development.floors === 'undefined' ) {
		return result;
	}

	result = development.buildings[floor.building_id];

	return result;
}

function get_lot_availability(lot, development) {
	var availability = lot.availability;

	var persisted_value = window.localStorage.getItem(development.id + '.lot_status_' + lot.id);
	if (typeof persisted_value !== 'undefined' && !!persisted_value) {
		availability = persisted_value;
	}

	if (typeof translations.availability[availability] !== 'undefined') {
		availability = translations.availability[availability];
	}

	return availability;
}

function categorise_lots(development) {
	lots_by_floor = {};
	lots_by_rooms = {};
	lots_by_building = {};

	// Ensure floors are in the correct order
	$.each(development.floors, function( index, floor ){
		if (typeof lots_by_floor[floor.name] === 'undefined') {
			lots_by_floor[floor.name] = [];
		}
	});

	// Ensure buildings are in the correct order
	$.each(development.buildings, function( index, building ){
		if (typeof lots_by_building[building.name] === 'undefined') {
			lots_by_building[building.name] = [];
		}
	});

	$.each(development.lots, function( lot_id, lot ){
		var building = get_building_for_lot(lot, development),
		    floor = get_floor_for_lot(lot, development),
		    rooms = lot.pieces;

		if (typeof lots_by_rooms[rooms] === 'undefined') {
			lots_by_rooms[rooms] = [ lot_id ];
		}
		else {
			lots_by_rooms[rooms].push(lot_id);
		}

		lots_by_building[building.name].push(lot_id);

		lots_by_floor[floor.name].push(lot_id);
	});
}


// ------------------------------------------------------------------------------------------------------ //
// Initialisation
// ------------------------------------------------------------------------------------------------------ //
function on_ready() {
	extend_jsrender();
	add_error_handler();
}

function extend_jsrender() {
	if (typeof $.views !== 'undefined') {
		$.views.tags({
			// Halve a number (used to calcualte pixel width of a retina image)
			halve: function(value) {
				return Math.ceil(value / 2);
			},

			// Get the entry (or building) name for the lot
			entry_name: function(lot_id) {
				var development = get_active_development(),
				    lot = development.lots[lot_id];
				if (typeof lot === 'undefined' || lot === null) {
					return '';
				}
				if (typeof lot.entry_id !== 'undefined' && lot.entry_id !== null) {
					var entry = development.entries[lot.entry_id];
					if (typeof entry !== 'undefined' && entry !== null && typeof entry.name !== 'undefined' && entry.name ) {
						return entry.name;
					}
				}
				var building = get_building_for_lot(lot, development);
				if (typeof building !== 'undefined' && building !== null && typeof building.name !== 'undefined' && building.name ) {
					return building.name;
				}
				return '';
			},

			// Get the floor name for the lot
			floor_ordinal: function(lot_id) {
				var development = get_active_development(),
				    lot = development.lots[lot_id];
				if (typeof lot === 'undefined' || lot === null) {
					return '';
				}
				var floors = get_all_floors_for_lot(lot, development),
				    separator = '',
				    result = '';
				$.each(floors, function(index, floor) {
					if (typeof floor !== 'undefined' && floor !== null && typeof floor.ordinal !== 'undefined' && floor.ordinal ) {
						result += separator + floor.ordinal;
						separator = ' - ';
					}
				});
				return result;
			},

			// Lot availability
			availability: function(lot_id) {
				var development = get_active_development(),
				    lot = development.lots[lot_id];
				if (typeof lot === 'undefined' || lot === null) {
					return '';
				}
				return get_lot_availability(lot, development);
			},

			// Extract URL from media
			get_media_url : get_media_url
		});

		$.views.helpers({
			isEmptyObject: $.isEmptyObject,
			getElement : function( object, property ) {
				return object[property];
			},
			flattenLot : function(development, lot_id) {
				var lot = development.lots[lot_id],
				    plan = development.plans[lot.plan_id],
				    floor = get_floor_for_lot(lot, development),
				    building = get_building_for_lot(lot, development);

				return {
					lot_id : lot.id,
					lot_code : lot.code,
					lot_name : lot.name,
					lot_pieces : lot.pieces,
					surface_weighted : lot.surface_weighted,
					building_name : ( building ? building.name : '' ),
					building_code : ( building ? building.code : '' ),
					floor_ordinal : ( floor ? floor.ordinal : '' ),
					thumb : get_media_url(plan.thumbnail_plan_image)
				};
			}
		});
	}
}

function add_error_handler() {
	// Add some simple throttling
	var transmissions = 0;

	window.onerror = function(m,u,l){
		if (transmissions > 10) {
			return;
		}

		transmissions++;

		var data = {
			msg: m,
			url: u,
			line: l,
			window: window.location.href,
			timestamp: new Date().toISOString(),
			app_name : app_name,
			app_version : app_version,
			platform : platform,
			user: typeof window.localStorage[app_prefix + ".session_user"] === 'undefined' ? '' : window.localStorage[app_prefix + ".session_user"]
		};

		var ajax_promise = $.ajax({
			type: 'POST',
			cache: false,
			url: base_url + '/log_error.php',
			dataType: 'json',
			data: data
		});

		// Show results of logging on console, if we're running locally in the browser
		if (platform === 'browser') {
			ajax_promise.then(function(data){
				if (data.success) {
					console.log('Error logged to server');
				}
				else {
					console.log('Server side error when logging');
				}
				console.log(data);
			}, function(result){
				console.log('Server call failed when attempting to log');
				console.log(result);
				console.log(data);
			});
		}

		return true; // prevent default error handler from running
	};

}


function override_alerts() {
	if (navigator.notification && platform !== 'browser') { // Override default HTML alert with native dialog
		window.alert = function (message) {
			navigator.notification.alert(
				message,    // message
				null,       // callback
				alert_title,   // title
				'OK'        // buttonName
			);
		};
	}
}

function show_confirm(message, on_ok, on_cancel) {
	if (navigator.notification && platform !== 'browser') {
		navigator.notification.confirm(
			message,
			function(button_index) {
				if (button_index === 1) {
					on_ok();
				}
				else {
					on_cancel();
				}
			},
			alert_title,
			[translations.button_labels.yes, translations.button_labels.no]
		);
	}
	else if (window.confirm) {
		if (confirm(message)) {
			on_ok();
		}
		else {
			on_cancel();
		}
	}
	else {
		on_ok();
	}
}


function keyboard_hide_handler(e) {
	// Use timer to stop background jump when user moves from one input to another
	keyboard_timer = window.setTimeout(function() {
		$('body').removeClass("keyboard-visible");
	}, 50);
}

function keyboard_show_handler(e) {
	if (keyboard_timer) {
		// Keyboard is being shown immediately after being hidden
		window.clearTimeout(keyboard_timer);
		keyboard_timer = null;
	}

	var $css = $('#keyboard-css');
	if ($css.length === 0) {
		// Move the background up in line with the screen resize (no resize event is triggered, as the keyboard overlays the page)
		var adjustment = ( screen_height - e.keyboardHeight ) / 2;
		$('<style>.keyboard-visible .app.background-splash { background-position: 0 -' + adjustment + 'px }</style>').appendTo('head').attr({
			media: 'all',
			id: 'keyboard-css',
			type: 'text/css'
		});
	}

	$('body').addClass("keyboard-visible");

	// Stop the iPad registering a submit when keyboard is shown (caused by positions of name field when keyboard hidden and submit button when it's shown)
	disable_submit = true;
	setTimeout(function() {
	    disable_submit = false;
	}, 100);
}


function set_background_images() {
	$('.app').css({
		'background-image': 'url(' + main_background + ')'
	});
	$('.app.background-splash').css({
		'background-image': 'url(' + splash_background + ')'
	});
}

function initialise_app() {
	set_background_images();

	screen_height = $('body').height();

	$('body').addClass('platform-' + platform);

	window.addEventListener('native.keyboardhide', keyboard_hide_handler);
	window.addEventListener('native.keyboardshow', keyboard_show_handler);

	$.mobile.changePage("#loading");
	imagecache_plugin_promise = $.Deferred();
	ImgCache.options.localCacheFolder = local_directory_name;
	ImgCache.init(function(){
		imagecache_plugin_promise.resolve();
	},
	function () {
		imagecache_plugin_promise.reject();
	});

	update_app_config();
	override_alerts();

	initialise_connection_handling();

	var session_key  = window.localStorage[app_prefix + ".session_key"],
	    session_user = window.localStorage[app_prefix + ".session_user"],
	    session_date = window.localStorage[app_prefix + ".session_date"],
	    no_session = (typeof session_key  === 'undefined' || !session_key ||
	                  typeof session_user === 'undefined' || !session_user ||
	                  typeof session_date === 'undefined' || !session_date );

	if (no_session) {
		$.mobile.changePage("#login");
		if (!connection_active) {
			alert(translations.initialisation_messages.no_data_or_conn);
		}
	}
	else {
		if (connection_active) {
			var update_action = function() {
					verify_session_key({
						'key'  : session_key,
						'user' : session_user,
						'date' : session_date,
						'ver'  : app_version
					});
				},
				load_action = function() {
					skip_download = true;
					load_local_data();
			    };
			if (prompt_before_update) {
				show_confirm(translations.initialisation_messages.update, update_action, load_action);
			}
			else {
				update_action();
			}
		}
		else {
			load_local_data();
		}
	}
}

function verify_session_key(session_details) {
	var ajax_promise = $.ajax({
		type: 'POST',
		cache: false,
		url: base_url + '/check_session_key.php',
		dataType: 'json',
		data: session_details
	});

	ajax_promise.then(function(data){
		if (data.success) {
			// Session key is valid
			load_data_for_session_key(session_details);
		}
		else {
			// Session key no longer valid
			$.mobile.changePage("#login");
		}
	}, function(){
		// Some other error; try getting the user to log in again
		$.mobile.changePage("#login");
	});
}

function clear_session_data() {
	window.localStorage.removeItem(app_prefix + ".session_key");
	window.localStorage.removeItem(app_prefix + ".session_user");
	window.localStorage.removeItem(app_prefix + ".session_date");
	window.localStorage.removeItem(app_prefix + ".development_details");
	window.localStorage.removeItem(app_prefix + ".selected_development");
	// localStorage.clear();
}

function load_data_for_session_key(session_details) {
	var ajax_promise = $.ajax({
		type: 'GET',
		cache: false,
		url: base_url + '/download_development_data_test.php',
		dataType: 'json',
		data: session_details
	});

	ajax_promise.then(function(data){
		if (data.success) {
			cache_data(data);
			load_local_data();
		}
		else {
			alert(translations.initialisation_messages.error_checking_session);

			// Unexpected error; try getting the user to log in again
			$.mobile.changePage("#login");
		}
	}, function(){
		alert(translations.initialisation_messages.error_checking_session);
		// Some other error; try getting the user to log in again
		$.mobile.changePage("#login");
	});


}

function cache_data(data) {
	var last_update_date = get_date_as_string(new Date()),
	    old_data = window.localStorage[app_prefix + ".development_details"],
	    new_data = JSON.stringify(data);

	if (typeof old_data === 'undefined' || old_data !== new_data) {
		window.localStorage[app_prefix + ".development_details"] = JSON.stringify(data);

		// Clear any cached pages, in case the associated data has changed
		$('[data-role="page"]').filter('[data-development-id]').data('development-id', '-1');
		$('[data-role="page"]').filter('[data-lot-id]').data('lot-id', '-1');
	}
	window.localStorage[app_prefix + '.last_updated'] = last_update_date;
	development_data = data;
}

function get_cached_data() {
	if (typeof development_data !== 'undefined' && development_data) {
		return development_data;
	}

	var all_details_string = window.localStorage[app_prefix + ".development_details"];
	if (typeof all_details_string === 'undefined' || !all_details_string) {
		return null;
	}
	//console.log(all_details_string);
	//console.log('------------------------------------------------------------------------------------');
	//var fullstring = all_details_string.replace(/GIMG/g, 'http:\/\/app.meo-realestate.com\/get_image.php?url=http%3A%2F%2F');
	//var fullstringcomplete = fullstring.replace(/CTU/g, '%2Fwp%2Fwp-content%2Fuploads%2F');
	development_data = JSON.parse(all_details_string);
	//console.log(fullstringcomplete);
	// development_data = JSON.parse(fullstringcomplete);
	return development_data;
}

function update_app_config() {
	var all_details = get_cached_data();
	if (typeof all_details === 'undefined' || !all_details) {
		return;
	}

	if (typeof all_details.translations !== 'undefined' && all_details.translations) {
		// Use .extend so we keep any translations not provided by the server
		translations = $.extend(true, translations, all_details.translations);
	}
	show_logout_and_refresh_buttons();

	if (typeof all_details.app.alert_title !== 'undefined' && all_details.app.alert_title) {
		alert_title = all_details.app.alert_title;
	}

	if (typeof all_details.app.default_project_page !== 'undefined' && all_details.app.default_project_page) {
		default_project_page = all_details.app.default_project_page;
	}

	if (typeof all_details.app.piwik_app_id !== 'undefined' && all_details.app.piwik_app_id) {
		piwik_app_id = all_details.app.piwik_app_id;
	}

	if (typeof all_details.app.piwik_enabled !== 'undefined') {
		piwik_enabled = all_details.app.piwik_enabled;
	}
}

function show_logout_and_refresh_buttons() {
	var template = $.templates('#template_logout');
	$('.logout-wrapper').each(function(n) {
		var content = template.render({
				labels : translations.button_labels
			});

		$(this).replaceWith(content);
	});

	$('.app__reload').off('click').on('click', function(e) {
		e.preventDefault();

		show_confirm(translations.confirm_messages.reload, function() {
			var session_key  = window.localStorage[app_prefix + ".session_key"],
			    session_user = window.localStorage[app_prefix + ".session_user"],
			    session_date = window.localStorage[app_prefix + ".session_date"];

			skip_download = false;

			$.mobile.changePage("#loading");
			$('.downloader .progress').html('');

			verify_session_key({
				'key'  : session_key,
				'user' : session_user,
				'date' : session_date,
				'ver'  : app_version
			});
		}, function() {
			// Do nothing
		});
	});

	$('.app__logout').off('click').on('click', function(e) {
		e.preventDefault();
		show_confirm(translations.confirm_messages.logout, function() {
			$.mobile.changePage("#login");
		}, function() {
			// Do nothing
		});
	});
}

function update_cache_progress(counters) {
	var processed = counters.cache_hit + counters.download_success + counters.download_error,
	    percent_processed = Math.round(processed / counters.download_counter * 10000) / 100;

	$('.downloader .progress').html( percent_processed + "%");
}

function update_image_cache_key(url) {
	ImgCache.getCachedFileURL(url, function(orig, localurl) {
		window.localStorage[app_prefix + ".media." + url] = localurl;
	}, function() {
		window.localStorage.removeItem(app_prefix + ".media." + url);
	});
}

function process_queue(download_queue, cache_promise, counters) {
	if (download_queue.length === 0) {
		cache_promise.resolve();
		return;
	}

	var url = download_queue.pop();

	ImgCache.isCached(url, function(path, success) {
		if (success) {
			// Already in the cache
			counters.cache_hit++;
			update_image_cache_key(url);
			update_cache_progress(counters);
			process_queue(download_queue, cache_promise, counters);
		} else {
			// not there, need to cache the image
			ImgCache.cacheFile(url, function () {
				counters.download_success++;
				update_image_cache_key(url);
				update_cache_progress(counters);
				process_queue(download_queue, cache_promise, counters);
			},
			function(){
				counters.download_error++;
				window.localStorage.removeItem(app_prefix + ".media." + url);
				update_cache_progress(counters);
				process_queue(download_queue, cache_promise, counters);
			});
		}
	});
}


function cache_images(cache_promise) {
	if (skip_download) {
		cache_promise.resolve();
		return;
	}
	var all_details = get_cached_data(),
	    all_images = all_details.all_media,
	    download_counter = all_images.length,
	    cache_hit = 0,
	    download_success = 0,
	    download_error = 0;

	var download_queue = [];

	$('.downloader .progress').html("0%");
	$.mobile.changePage("#downloader");

	$.each(all_images, function(index, image_details) {
		// var url = image_details.url;
		
		/* Fred */
		var url = '';
		url += replace_string_in_url(image_details.url);
		
		url += (url.indexOf('?') === -1) ? '?' : '&';
		url += image_details.date;
		download_queue.push(url);
	});

	process_queue(download_queue, cache_promise, {
			'download_counter' : download_counter,
			'cache_hit'        : cache_hit,
			'download_success' : download_success,
			'download_error'   : download_error
		});
}

function load_local_data() {
	update_app_config();

	if (piwik_enabled && is_plugin_installed('com.storefront.cordova.piwik')) {
		piwik.startTracker(piwik_url, piwik_app_id, window.localStorage[app_prefix + ".session_user"]);
		$('.app').on( "pageshow", function(e) {
			piwik_track(e.target);
		});
	}

	var all_details = get_cached_data(),
	    selected_development = window.localStorage[app_prefix + ".selected_development"],
	    development_ids = Object.keys(all_details.developments),
	    number_of_developments = development_ids.length;

	if (number_of_developments === 0) {
		// User doesn't have permissions on any developments
		alert(translations.login_messages.no_devs);
		$.mobile.changePage("#login");
		return;
	}

	var cache_promise = $.Deferred();
	imagecache_plugin_promise.done(function() {
		cache_images(cache_promise);
	}
	).fail(function() {
		// We can't cache
		cache_promise.resolve();
	});

	cache_promise.done(function() {
		var $logo_links = $('.app__logo').not('.login .app__logo');
		if (number_of_developments === 1) {
			set_active_development(development_ids[0]);
			$('body').addClass('single-development');

			var project_page = 'splash-development';

			if (!use_dev_splash) {
				var development = get_active_development();
				project_page = get_development_page(development);
			}

			$logo_links.attr('href', '#' + project_page);

			return;
		}

		$('body').removeClass('single-development');

		$logo_links.attr('href', '#development-select');
		if (typeof selected_development !== 'undefined' && selected_development && $.inArray(selected_development, development_ids) !== -1) {
			set_active_development(selected_development);
			return;
		}

		// We have several developments, none of which has been selected
		window.localStorage.removeItem(app_prefix + ".selected_development");
		active_development_id = null;

		$.mobile.changePage('#development-select');
	});
}

function get_development_page(development) {
	var result = default_project_page,
	    has_lots = (typeof development.lots !== 'undefined' && !$.isEmptyObject(development.lots)),
	    has_gallery = (typeof development.gallery !== 'undefined' && !$.isEmptyObject(development.gallery));

	if (!has_lots) {
		$('#gallery').addClass('hide-footer'); // If we only have the gallery page, no point showing the footer
		if (result === 'listing' || result === 'thumbs') {
			result = 'gallery';
		}
	}
	else {
		$('#gallery').removeClass('hide-footer');
		if (result === 'gallery' && !has_gallery) {
			result = 'thumbs';
		}
	}

	return result;
}

function set_active_development(selected_development_id) {
	active_development_id = selected_development_id;
	window.localStorage[app_prefix + ".selected_development"] = selected_development_id;

	var template = $.templates('#template_footer'),
	    development = get_active_development(),
	    has_lots = (typeof development.lots !== 'undefined' && !$.isEmptyObject(development.lots)),
	    has_gallery = (typeof development.gallery !== 'undefined' && !$.isEmptyObject(development.gallery)),
	    project_page = 'splash-development';

	if (!use_dev_splash) {
		project_page = get_development_page(development);
	}

	$('.development_logo img').attr('src', get_media_url(development.logo));

	$('footer').each(function(n) {
		var data = {
				has_lots : has_lots,
				has_gallery : has_gallery,
				has_reload : $(this).hasClass('contains_reload'),
				labels : translations.button_labels
			},
			content = template.render(data);

		$(this).html(content);
	});

	$.mobile.changePage('#' + project_page);
}

function initialise_connection_handling() {
	connection_active = is_connection_active();

	document.addEventListener("offline", function() {
		$('body').removeClass('connected');
		connection_active = false;
	}, false);

	document.addEventListener("online", function() {
		$('body').addClass('connected');
		connection_active = is_connection_active();
	}, false);
}

function is_connection_active() {
	try {
		var connectionType = navigator.connection.type;

		switch(connectionType) {
			case Connection.ETHERNET :
			case Connection.WIFI :
				$('body').addClass('connected');
				return true;

			case Connection.CELL_2G :
			case Connection.CELL_3G :
			case Connection.CELL_4G :
			case Connection.CELL :
				$('body').removeClass('connected');
				return false; // Don't chew up the user's data plan

			default :
				// Fall through to the browser check
		}
	}
	catch (e) {
		// Might be in a browser; fall through
	}

	// If we're in a desktop browser, assume we have a connection
	var result = !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/);
	if (result) {
		$('body').addClass('connected');
	}
	else {
		$('body').removeClass('connected');
	}
	return result;
}

function piwik_track(page) {
	var parameters = $.mobile.pageData || null;

	var url = page.id + '?development_id=' + active_development_id;

	if (parameters) {
		url = url + '&' + $.param(parameters);
	}

	piwik.trackScreenView(url);
}

// ------------------------------------------------------------------------------------------------------ //
// jQuery mobile events
// ------------------------------------------------------------------------------------------------------ //

function pageshow_splash_main(event, ui) {
}

function pagebeforeshow_splash_development(event, ui) {
	var development = get_active_development(),
	    splash_image_url = get_media_url(development.splash_screen_image);

	set_development_details();

	$('.show-development').attr('href', '#' + get_development_page(development));

	$('.splash-development .app__main').css({
		'background-image': 'url(' + splash_image_url + ')'
	});
}

function pagebeforeshow_gallery(event, ui) {
	var development = get_active_development(),
	    gallery_development = $("#gallery").data('development-id');

	if (gallery_development === development.id) {
		// Gallery already set up for this development.  Reinitialise scrolling just in case it's hung
		initialise_gallery_scrolling();
		return;
	}

	set_development_details();

	var content = get_gallery_content();
	$('.gallery .app__main .content-wrapper').html(content);

	$('.gallery_link_pdf').click(function(e) {
		e.preventDefault();
		open_pdf($(this).data('group'), $(this).data('id'));
	});

	initialise_gallery_scrolling();

	$("#gallery").data('development-id', development.id);
}

function initialise_gallery_scrolling() {
	setTimeout(function() {
		if (platform === 'browser') {
			console.log('initialise_gallery_scrolling');
		}

		// Give the page time to render before turning scrolling on

		$.each(gallery_scrollbars, function( index, value ){
			value.destroy();
			gallery_scrollbars[index] = null;
		});

		gallery_scrollbars = [];

		$('.gallery-scroll-wrapper').filter(':visible').each(function(n) {
			var rowScrollBar = add_scrolling(this, true, false);
			gallery_scrollbars.push(rowScrollBar);
		});

		var pageScrollBar = add_scrolling('.gallery .app__main', false, true);

		gallery_scrollbars.push(pageScrollBar);

	}, 1000);
}

function pagecreate_login(event, ui) {
	var template = $.templates('#template_login_form');
	$('.login-form-wrapper').each(function(n) {
		var content = template.render({
				labels : translations.login_messages
			});

		$(this).replaceWith(content);
	});

	$('.login .login-form').submit(function(e) {
		e.preventDefault();

		if (disable_submit) {
			disable_submit = false;
			return;
		}

		var $form = $(this);

		$('button[type=submit]', $form).attr('disabled', 'disabled');

		var user = $('.name', $form).val(),
		    pass = $('.password', $form).val();

		if (user === '') {
			$('button[type=submit]', $form).removeAttr('disabled');
			alert(translations.login_messages.no_user);
			return;
		}
		if (pass === '') {
			$('button[type=submit]', $form).removeAttr('disabled');
			alert(translations.login_messages.no_pass);
			return;
		}

		$.ajax({
			type: 'POST',
			cache: false,
			url: base_url + '/connect.php',
			dataType: 'json',
			data: {
				user: user,
				pass: pass
			},
			success: function(data, status, xml){
				if (data.success) {
					$.mobile.changePage("#loading");

					window.localStorage[app_prefix + ".session_key"] = data.session_key;
					window.localStorage[app_prefix + ".session_user"] = user;
					window.localStorage[app_prefix + ".session_date"] = data.date;

					load_data_for_session_key({
						'key'  : data.session_key,
						'user' : user,
						'date' : data.date,
						'ver'  : app_version
					});
				}
				else {
					alert(translations.login_messages.invalid);
				}
			},
			error: function(xml, status, error){
				alert(translations.login_messages.commerr);
			},
			complete: function(xml, status){
				$('button[type=submit]', $form).removeAttr('disabled');
			}
		});
	});
}

function pagebeforeshow_login(event, ui) {
	clear_session_data();

	$('.login-form__text').val('');
}

function pagebeforeshow_development_select(event, ui) {
	var all_details = get_cached_data();

	var developments = [];

	$.each(all_details.developments, function(index, value) {
		developments.push({
			'id' : index,
			'selector_image' : get_media_url(value.selector_image),
			'name' : value.name,
			'location' : value.location
		});
	});

	var template = $.templates('#template_selector'),
	    data = {
	        developments : developments
	    },
	    content = template.render(data);

	$('.selector__items').html(content);

	setTimeout(function() {
		// Give the page time to render before turning scrolling on
		$('.selector__items').each(function(n) {
			add_scrolling(this, true, false);
		});
	}, 1000);


	$('.selector__link, .selector__link__label').click(function(e) {
		e.preventDefault();
		set_active_development($(this).data('development-id'));
	});
}

function add_gallery_nav() {
	$('.gallery-nav').click(function(e) {
		e.preventDefault();
		var link = $(this).attr('href');

		$.mobile.changePage(link, {
			allowSamePageTransition: true,
			transition: 'none'
		});
	});
}

function pagebeforeshow_gallery_image(event, ui) {
	var parameters = $.mobile.pageData || null;
	set_development_details();

	// Make sure we don't have a light line at the bottom of the screen
	$('body').addClass('dark');

	if (parameters === null) {
		return;
	}

	var element_id = parameters['id'],
	    element_group = get_gallery_group(parameters['group']),
	    image_details = get_gallery_element(parameters['group'], element_id, element_group),
	    content = get_gallery_item_html(image_details);

	$('.gallery_image .app__main .content-wrapper').html(content);
	set_media_size('.gallery_image', '.gallery-image-wrapper img', '.gallery-image-wrapper');

	if (typeof image_details.current !== 'undefined' && typeof image_details.current.label !== 'undefined') {
		set_header_suffix(image_details.current.label);
	}

	set_zoomer($('.gallery-image-group-scroll-wrapper .gallery-image-wrapper.active .image-zoom-wrapper'), false);

	add_gallery_nav();
}

function pagebeforeshow_gallery_spinner(event, ui) {
	var parameters = $.mobile.pageData || null;
	set_development_details();

	// Make sure we don't have a dark line at the bottom of the screen
	$('body').addClass('light');

	$('.gallery_spinner .pane--content').addClass('is-active');

	if (parameters === null) {
		return;
	}

	var all_details = get_cached_data(),
	    spinners = all_details.spinners,
	    element_id = parameters['id'],
	    element_group = get_gallery_group(parameters['group']),
	    spinner_id = element_group['elements'][element_id]['id'],
	    spinner_details = get_gallery_element(parameters['group'], element_id, element_group),
	    template = $.templates('#template_gallery_spinner'),
	    content = template.render(spinner_details);

	$('.gallery_spinner .pane--left .frame__inner').html(content);
	add_gallery_nav();

	activate_spinner(spinners[spinner_id], 1);

	// Remove the lot ID from the lot page, so the gallery spinner doesn't become associated with the lot
	$("#lot").data('lot-id', -1);
}

function pagebeforeshow_gallery_video(event, ui) {
	var parameters = $.mobile.pageData || null;
	set_development_details();

	// Make sure we don't have a light line at the bottom of the screen
	$('body').addClass('dark');

	if (parameters === null) {
		return;
	}

	var all_details = get_cached_data(),
	    videos = all_details.videos,
	    element_id = parameters['id'],
	    element_group = get_gallery_group(parameters['group']),
	    video_id = element_group['elements'][element_id]['id'],
	    element = videos[video_id],
	    formats = ['webm', 'mp4', 'ogg'],
	    video_size = get_scaled_dimensions('.gallery_video', element),
	    video_links = {};

	$.each(formats, function( index, value ){
		if (element.hasOwnProperty(value)) {
			video_links[value] = get_media_url(element[value]);
		}
	});

	var template = $.templates('#template_video'),
	    data = {
	        video_links  : video_links,
	        poster       : get_media_url(element.poster),
	        width        : video_size.width,
	        height       : video_size.height,
	        padding_top  : video_size.padding_top,
	        padding_left : video_size.padding_left
	    },
	    content = template.render(data);

	$('.gallery_video .content-wrapper').html(content);
}


function pagebeforeshow_listing(event, ui) {
	var development = get_active_development(),
	    listing_development = $("#listing").data('development-id');

	if (listing_development === development.id) {
		// Listing already set up for this development
		return;
	}

	set_development_details();

	var last_update_date = window.localStorage[app_prefix + '.last_updated'],
	    data = {},
	    content = '';
	if (typeof last_update_date !== 'undefined' && !!last_update_date) {
		data = {
		    last_update_label : translations.button_labels.last_update,
		    last_update_date  : last_update_date
		};
		content = $.templates('#template_last_update').render(data);

		$('.reload').html(content);
	}

	var template = $.templates('#template_listing');
	data = {
		header : translations.listing_headers,
		lots : development.lots
	};

    content = template.render(data);
	$('.listing .app__main').html(content);

	setTimeout(function() {
		// Allow the scrollbar details to be correctly calculated
		$('.listing-body-wrapper').height($('.listing .app__main').outerHeight());

		// Give the page time to render before turning scrolling on
		add_scrolling('.listing-body-wrapper', false, true);
	}, 1000);

	$("#listing").data('development-id', development.id);

}

function recreate_vignette_scrollbars() {
	if (platform === 'browser') {
		console.log('recreate_vignette_scrollbars');
	}

	$.each(vignette_scrollbars, function( index, value ){
		value.destroy();
		vignette_scrollbars[index] = null;
	});

	vignette_scrollbars = [];

	$('.property-scroll-wrapper').filter(':visible').each(function(n) {
		var rowScrollBar = add_scrolling(this, true, false);
		vignette_scrollbars.push(rowScrollBar);
	});

	var pageScrollBar = add_scrolling('.thumbs .app__main', false, true);

	vignette_scrollbars.push(pageScrollBar);
}

function switch_vignette_order(e, to_show) {
	e.preventDefault();
	$('.vignette-group').hide().filter(to_show).show();
	recreate_vignette_scrollbars();
}


function pagebeforeshow_thumbs(event, ui) {
	var development = get_active_development(),
	    thumbs_development = $("#thumbs").data('development-id');

	if (thumbs_development === development.id) {
		// Reinitialise scrolling just in case it's hung on this page before
		setTimeout(function() {
			recreate_vignette_scrollbars();
		}, 1000);

		// Thumbs already set up for this development
		return;
	}

	set_development_details();

	categorise_lots(development);

	var show_building_filter = (Object.keys(lots_by_building).length > 1);

	var template = $.templates('#template_vignette_filters');
	$('.thumbs .app__head .filters').html(template.render({
		labels : translations.thumb_labels,
		show_building_filter : show_building_filter
	}));

	template = $.templates('#template_thumbs');

	$('.thumbs .app__main .lots-by-rooms').html(template.render({
		lot_ids : lots_by_rooms,
		title_suffix : ' ' + 'pièces',
		display_order : Object.keys(lots_by_rooms).sort(),
		development : development,
		labels : translations.thumb_labels
	}));

	$('.thumbs .app__main .lots-by-floor').html(template.render({
		lot_ids : lots_by_floor,
		title_suffix : '',
		display_order : Object.keys(lots_by_floor),
		development : development,
		labels : translations.thumb_labels
	}));

	if (show_building_filter) {
		$('.thumbs .app__main .lots-by-building').html(template.render({
			lot_ids : lots_by_building,
			title_suffix : '',
			display_order : Object.keys(lots_by_building),
			development : development,
			labels : translations.thumb_labels
		}));
	}

	$('.filter-rooms').click(function(e){
		switch_vignette_order(e, '.lots-by-rooms');
	});

	$('.filter-floors').click(function(e){
		switch_vignette_order(e, '.lots-by-floor');
	});

	if (show_building_filter) {
		$('.filter-buildings').click(function(e){
			switch_vignette_order(e, '.lots-by-building');
		});
	}

	$('.vignette-group').hide().filter('.lots-by-rooms').show();

	setTimeout(function() {
		// Give the page time to render before turning scrolling on
		recreate_vignette_scrollbars();
	}, 1000);

	$("#thumbs").data('development-id', development.id);
}

function scale_imagemap_coords(coords, raw_image_width, scaled_image_width) {
	if (typeof raw_image_width    === "undefined" ||
		typeof scaled_image_width === "undefined" ||
		!raw_image_width                          ||
		!scaled_image_width                       ||
		raw_image_width === scaled_image_width)
	{
		return coords;
	}
	var percent = scaled_image_width / raw_image_width,
	    separate_coords = coords.split(','),
	    scaled_coords = new Array(separate_coords.length);

	for (var i = 0; i < scaled_coords.length; ++i) {
		scaled_coords[i] = Math.round(separate_coords[i] * percent);
	}
	return scaled_coords.toString();
}


function pagebeforeshow_lot(event, ui) {
	var parameters = $.mobile.pageData || null;

	if (typeof parameters.lot_id === 'undefined' || !parameters.lot_id) {
		alert(translations.error_messages.missing_lot);
		return;
	}

	var lot_id = parameters.lot_id,
	    development = get_active_development(),
	    existing_lot = $("#lot").data('lot-id');

	if (existing_lot === lot_id) {
		// Page already set up for this lot
		return;
	}

	var lot = development.lots[lot_id];

	if (typeof lot === 'undefined' || !lot) {
		alert(translations.error_messages.lot_not_active_promo);
		return;
	}

	set_development_details();

	var template = $.templates('#template_lot_left'),
	    plan = development.plans[lot.plan_id],
	    floor = get_floor_for_lot(lot, development),
	    features = plan.feature_id ? development.features[plan.feature_id] : null;

	$('.lot .app__main .pane--left').html(template.render({
		lot : lot,
		plan : plan,
		features : features,
		is_windows : ( platform === 'windows' ) ? 1 : 0
	}));

	var areas = [];
	$.each(area_types, function( index, area_type ){
		if (lot[area_type]) {
			areas.push({
				type: area_type,
				label: translations.building_labels[area_type],
				area: lot[area_type]
			});
		}
	});

	template = $.templates('#template_lot_right');
	$('.lot .app__main .pane--right').html(template.render({
		lot : lot,
		plan : plan,
		floor : floor,
		features : features,
		areas : areas,
		property_info_explanation : translations.building_labels.property_info_explanation,
		coords : floor.keyplan_lot_coords,
		labels : translations.lot_page_labels
	}));

	// Store the coordinates for user once the page has been rendered (in pageshow_lot)
	if (typeof lot.keyplan_image !== null && !!lot.keyplan_image) {
		$('#floor_image_map_' + floor.id).data('coords', floor.keyplan_lot_coords);
		$('#floor_image_map_' + floor.id).data('width', lot.keyplan_image.width);
	}

	// Activate imagemap links
	$('.lot-load-link').on('click', function(e) {
		e.preventDefault();
		$.mobile.changePage($(this).attr('href'), { allowSamePageTransition:true });
	});

	var views = ['plan', '3d', 'spinner', 'info'];
	$.each(views, function( index, value ){
		$('.button--' + value).click(function(e) {
			e.preventDefault();
			if (! $(this).hasClass('is-active')) {
				set_lot_view(value);
			}
		});
	});

	$("#lot").data('lot-id', lot_id);

	set_lot_view('plan');
}


function pageshow_lot(event) {
	var standard_width = $('.lot .pane--left').width(),
	    full_width = standard_width + $('.lot .pane--right').width(),
	    keyplan_width = $('.property__plan').width();

	$('.lot').data({
		standard_width: standard_width,
		full_width: full_width
	});

	// Add coordinates for keyplan
	$('.floor_image_map').each(function(index){
		var raw_coords = $(this).data('coords'),
		    source_width = $(this).data('width');
		if (typeof raw_coords !== 'undefined' && !!raw_coords) {
			$.each(raw_coords, function(lot_id, coords) {
				var transformed = scale_imagemap_coords(coords, source_width, keyplan_width);
				$('area.lot-load-link-' + lot_id).attr('coords', transformed);
			});
		}
	});

	// Enable navigation buttons
	$('.lot a.hide-details').click(function(e) {
		e.preventDefault();
		$('.lot .pane--left').stop(true, false).animate(
			{ width: full_width },
			600,
			'linear',
			function () {
				$('body').addClass('full');
				$('.lot a.hide-details').css({'display': ''}); // Use default value from stylesheet
			}
		);
	});
	$('.lot a.show-details').click(function(e) {
		e.preventDefault();
		$('body').removeClass('full');
		$('.lot .pane--left').stop(true, false).animate(
			{ width: standard_width },
			600,
			'linear'
		);
	});

	// Position zoomer image
	if (active_zoomer !== null) {
		$(active_zoomer).trigger('refresh');
	}
}

function pagebeforehide_all(event, ui) {
	$('body').removeClass('dark light full');
	var standard_width = $('.lot').data('standard_width');
	if (standard_width) {
		$('.lot .pane--left').width(standard_width);
	}
}



// ------------------------------------------------------------------------------------------------------ //
//
// ------------------------------------------------------------------------------------------------------ //

function get_media_url(media_properties) {
	/* Fred */
	media_properties.url = replace_string_in_url(media_properties.url);
	
	var url = media_properties.url;
	
	url += (url.indexOf('?') === -1) ? '?' : '&';
	url += media_properties.date;

	if (typeof window.localStorage[app_prefix + ".media." + url] !== 'undefined' && window.localStorage[app_prefix + ".media." + url]) {
		return window.localStorage[app_prefix + ".media." + url];
	}

	// ideally this won't happen - everything should be cached
	return media_properties.url;
}

function get_active_development() {
	var all_details = get_cached_data();

	return all_details.developments[active_development_id];
}

function get_gallery_group(group_slug) {
	var development = get_active_development(),
	    gallery = development.gallery,
	    result = null;

	$.each(gallery.rows, function(index, value) {
		if (value.slug === group_slug) {
			result = value;
		}
	});

	return result;
}


function set_development_details() {
	var development = get_active_development();

	$('.development-name').html(development.name);
	$('.development-location').html(development.location);

	var template = $.templates('#template_slogan'),
	    data = {
	        lines : development.slogan
	    },
	    content = template.render(data);

	$('.development-slogan').html(content);

}

function get_gallery_content() {
	var development = get_active_development(),
	    gallery = development.gallery,
	    all_details = get_cached_data(),
	    images = all_details.gallery_images,
	    spinners = all_details.spinners,
	    videos = all_details.videos,
	    pdfs = all_details.pdfs;

	var elements = {};

	if (gallery === null) {
		return '';
	}

	$.each(gallery.rows, function(row_index, row_content) {
		if (typeof elements[row_content.slug] === 'undefined') {
			elements[row_content.slug] = {
				content: {}
			};
		}

		elements[row_content.slug]['title'] = row_content.title;
		$.each(row_content.elements, function(element_index, element_content){
			var element = null;
			switch(element_content.type) {
				case 'spinner':
					element = spinners[element_content.id];
					break;
				case 'image':
					element = images[element_content.id];
					break;
				case 'video':
					element = videos[element_content.id];
					break;
				case 'pdf':
					element = pdfs[element_content.id];
					break;
			}
			if (element === null) {
				return;
			}

			var thumbnail = {
				width: element.thumbnail.width,
				height: element.thumbnail.height,
				name: get_media_url(element.thumbnail)
			};
			elements[row_content.slug]['content'][element_index] = {
				'type' : element_content.type,
				'label' : element.label,
				'thumbnail' : thumbnail
			};
		});
	});

	var template = $.templates('#template_gallery'),
	    data = {
	        elements : elements
	    },
	    result = template.render(data);

	return result;
}

function add_scrolling(to_scroll, scrollX, scrollY, additional_properties) {
	var properties = {
		scrollX: scrollX,
		scrollY: scrollY,
		scrollbars: false,
		interactiveScrollbars: true,
		shrinkScrollbars: 'scale',
		fadeScrollbars: true
	};

	if (typeof additional_properties !== 'undefined') {
		$.extend(properties, additional_properties);
	}

	var scrollbar = new IScroll(to_scroll, properties);

	return scrollbar;
}

function set_header_suffix(label) {
	if (label) {
		$('.header-suffix').text(label);
		$('.header-suffix-wrapper').show();
	}
	else {
		$('.header-suffix-wrapper').hide();
	}
}

function set_zoomer($wrapper, adjust_margins_container) {
	if (active_zoomer !== null) {
		active_zoomer.destroy();
		active_zoomer = null;
	}

	if (! $wrapper.length) { // Zoom wrapper doesn't exist - zoom is disabled
		return;
	}

	if (platform !== 'windows') {
		// Windows uses built in zooming.  See the -ms-content-zooming CSS in app.css
		if (!!adjust_margins_container)  {
			set_zoomed_item_margins($wrapper[0].children[0], 1, adjust_margins_container);
		}

		active_zoomer = new IScroll($wrapper[0], {
			scrollX: true,
			scrollY: true,
			scrollbars: false,
			zoom: true,
			startZoom: 1,
			zoomMin: 1,
			zoomMax: 4
		});
		if (!!adjust_margins_container)  {
			active_zoomer.on('refresh', function(){
				set_zoomed_item_margins(this.scroller, this.scale, adjust_margins_container);
			});
		}
	}
}

function set_zoomed_item_margins(zoomed_item, scale, container) {
	var zoom_area_width = $(container).width(),
	    zoom_area_height = $(container).height(),
	    zoomed_item_width  = $(zoomed_item).width() * scale,
	    zoomed_item_height = $(zoomed_item).height() * scale;

	var left_margin = Math.max((zoom_area_width  - zoomed_item_width ) / 2, 0) / zoom_area_width * 100;

	$(zoomed_item).css({
		'margin-left': left_margin + '%',
		marginTop:  Math.max((zoom_area_height - zoomed_item_height) / 2, 0)
	});
}


function get_body_size(page_selector) {
	var $header = $(page_selector + ' .app__head'),
	    header_height = $header.length ? $header.height() : 0,
	    $footer = $(page_selector + ' .app__foot'),
	    footer_height = $footer.length ? $footer.height() : 0,
	    visible_content_height = $( document ).height() - (header_height + footer_height),
	    visible_content_width  = $( document ).width();

	return {
		'height' : visible_content_height,
		'width'  : visible_content_width
	};
}

function set_media_size(page_selector, media_selector, wrapper_selector) {
	var body_size = get_body_size(page_selector);

	$(media_selector).css({
		'max-height': body_size.height + 'px'
	});
	$(wrapper_selector).css({
		'width': body_size.width + 'px',
		'height': body_size.height + 'px'
	});

	return {
		'visible_content_height': body_size.height,
		'visible_content_width':  body_size.width
	};
}

function get_gallery_element(group_id, element_id, group) {
	var all_details = get_cached_data(),
	    images = all_details.gallery_images;

	var previous = null,
	    previous_type = null,
	    next = null,
	    next_type = null,
	    current = null,
	    current_type = null;

	$.each(group.elements, function( index, element_details ){
		if (element_id === index) {
			current_type = element_details.type;
			if (current_type !== "image") {
				current = index;
			}
			else {
				current = images[element_details.id];
				current.main = get_media_url(current.main_image);
				current.zoom = (typeof current.zoom === 'undefined' || current.zoom) ? true : false;
			}
		}
		else if (current === null) {
			previous = index;
			previous_type = element_details.type;
		}
		else if (current !== null && next === null) {
			next = index;
			next_type = element_details.type;
		}

	});

	var result = {
		group_id: group_id,
		group: group,
		previous: previous,
		previous_type: previous_type,
		current: current,
		current_type: current_type,
		next: next,
		next_type: next_type,
		is_windows : ( platform === 'windows' ) ? 1 : 0
	};

	return result;
}


function get_gallery_item_html(image_details) {
	if (image_details.current === null) {
		return '';
	}

	var template = $.templates('#template_gallery_image'),
	    data = image_details,
	    content = template.render(data);

	return content;
}

function activate_spinner(spinner_properties, frame) {
	if (typeof active_spinner !== 'undefined' && active_spinner) {
		active_spinner.unreel();
	}
    active_spinner = $('.pane--spinner .frame__inner img').not('.gallery-nav img');

	var body_size = get_body_size('.gallery_spinner'),
	    pane_width  = body_size.width,
	    pane_height = body_size.height,
	    attributes = {
			'src' : get_media_url(spinner_properties.images[frame - 1]), // Mine are 0-indexed, reel's are 1-indexed
			'width'  : spinner_properties.width,
			'height' : spinner_properties.height
		};

	if (typeof spinner_properties.stretch !== 'undefined' && spinner_properties.stretch) {
		var new_height = Math.floor((pane_width / spinner_properties.width) * spinner_properties.height);
		attributes.width = pane_width;
		attributes.height = new_height;
	}

	active_spinner.css({
		'width': attributes.width + 'px',
		'height': attributes.height + 'px'
	});

	active_spinner.attr(attributes);

	var images = $.map(spinner_properties.images, get_media_url );

	active_spinner.reel({
		images: images,
		'cw': (typeof spinner_properties.clockwise !== 'undefined' && spinner_properties.clockwise),
		'brake': 1,
		'frame': frame
	});
}

function get_scaled_dimensions(page_selector, default_dimensions) {
	var body_size = get_body_size(page_selector),
	    body_aspect_ratio = body_size.width / body_size.height,
	    video_aspect_ratio = default_dimensions.width / default_dimensions.height,
	    result = {
	        padding_top: 0,
	        padding_left: 0,
	        width:  body_size.width,
	        height: body_size.height
	    };

	if (body_aspect_ratio > video_aspect_ratio) {
		result.width = Math.floor(default_dimensions.width * body_size.height / default_dimensions.height);
		result.padding_left = Math.floor( (body_size.width - result.width) / 2 );
	}
	else {
		result.height = Math.floor(default_dimensions.height * body_size.width / default_dimensions.width);
		result.padding_top = Math.floor( (body_size.height - result.height) / 2 );
	}

	return result;
}

function open_pdf(group, id) {
	var all_details = get_cached_data(),
		pdfs = all_details.pdfs,
	    element_group = get_gallery_group(group),
	    pdf_id = element_group['elements'][id]['id'],
	    pdf = pdfs[pdf_id],
	    pdf_url = get_media_url(pdf.main_image);

	if (is_plugin_installed('com.phonegap.plugins.fileopener') && platform === 'Android') {
		window.plugins.fileOpener.open(pdf_url);
	}
	else if (is_plugin_installed('cordova-plugin-inappbrowser') && platform === 'iOS' ) {
		var ref = window.open(pdf_url, '_blank', 'location=no,closebuttoncaption=' + translations.initialisation_messages.close + ',enableViewportScale=yes');
	}
	else if (platform === 'windows') {
		// Ensure we don't have a problematic double slash in the file name
		pdf_url = pdf_url.replace(new RegExp('\/\/' + local_directory_name), '/' + local_directory_name);
		var uri = new Windows.Foundation.Uri(pdf_url);

		Windows.Storage.StorageFile.getFileFromApplicationUriAsync(uri).done(function(file) {
			Windows.System.Launcher.launchFileAsync(file);
		});
	}
}


function set_lot_view(view) {
	$('.lot .button--page.is-active').removeClass('is-active');
	$('.lot .button--' + view).addClass('is-active');

	$('.lot .pane--content.is-active').removeClass('is-active');
	$('.lot .pane--' + view).addClass('is-active');

	set_zoomer($('.lot .pane--' + view + ' .zoom-wrapper'), '.lot .pane--content.is-active');

	if (view === "spinner") {
		var all_details = get_cached_data(),
		    lot_id = $("#lot").data('lot-id'),
		    development = get_active_development(),
		    lot = development.lots[lot_id],
		    plan = development.plans[lot.plan_id],
		    spinners = all_details.spinners,
		    spinner = spinners[plan.spinner_3d];
		
			//console.log('Before ' + JSON.stringify(all_details));
			// Empty it to maybe light up the execution memory
			all_details = '';
			//console.log('After ' + JSON.stringify(all_details));
			
		if (active_spinner !== null) {
			active_spinner.unreel();
			active_spinner = null;
		}

		active_spinner = $('.lot .pane--spinner .frame__inner img');

		var images = $.map(spinner.images, get_media_url );

		active_spinner.attr({
			'src'    : images[0],
			'height' : spinner.height,
			'width'  : spinner.width
		});

		active_spinner.reel({
			images: images,
			'cw': (typeof spinner.clockwise !== "undefined" && spinner.clockwise),
			'brake': 1,
			frames: images.length
		});
	}

	if (view === "info") {
		$('.hide-details').hide();

		if (typeof lot_details_scroll !== "undefined") {
			lot_details_scroll.destroy();
			lot_details_scroll = null;
		}
		var lot_details_scroll = new IScroll('.pane--info .pane__inner', { // MEO Fred adding var
			scrollX: false,
			scrollY: true,
			scrollbars: false,
			mouseWheel: false
		});
		setTimeout(function () {
			lot_details_scroll.refresh();
		}, 0);
	}
	else {
		$('.hide-details').show();
	}
}

/* Fred Update */
function replace_string_in_url(data_string){
	var fullstring = data_string.replace(/XYZ/g, 'http:\/\/app.meo-realestate.com\/get_image.php?url=http%3A%2F%2F');
	var fullstringcomplete = fullstring.replace(/ZYX/g, '%2Fwp%2Fwp-content%2Fuploads%2F');
	
	return fullstringcomplete;
}
