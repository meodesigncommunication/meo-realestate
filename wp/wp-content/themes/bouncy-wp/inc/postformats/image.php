<?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
	<div class="four columns item">
		<div class="postcontainer">
<?php } ?>
<article <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
	<div class="row">
		<div class="twelve columns">
			<?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
				<div class="masonry-date">
					<?php echo get_the_date() . ' / '; ?>
					<a href="<?php comments_link(); ?>" class="commentcount">
						 <?php comments_number('0 Comments', '1 Comment', '% Comments' ); ?> 
					</a>
				</div>
			<?php } ?>
			<?php if(!is_page_template('template-blog-masonry.php') && !is_admin()) { ?>
				<div class="row">
					<?php if(!is_single()) { ?>
					<div class="two columns"></div>
					<?php } ?>
					<header class="post-title <?php if(!is_single()) { echo 'ten'; } else { echo 'twelve'; } ?> columns cf">
					  	<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						  <?php echo thb_printLikes(get_the_ID()); ?>
					  <aside class="meta">
					    <?php _e("Posted By", THB_THEME_NAME); ?> <?php the_author_posts_link(); ?>
					    <?php if (get_the_category()) { ?>
					    <?php _e("in", THB_THEME_NAME); ?> <?php the_category(', '); ?>
					    <?php } ?>
					  </aside>
					</header>
				</div>
			<?php } ?>
		  <?php if (!is_search()) { ?>
			  <?php $image_id = get_post_thumbnail_id();
			  			$image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0]; ?>
			  <div class="post-gallery fresco">
			  	<?php if(!is_page_template('template-blog-masonry.php') && !is_admin()) { 
			  					the_post_thumbnail('twothirds'); 
			  				} else { 
			  					the_post_thumbnail('onethirds');  
			  				}?>
			    <div class="overlay">
			    	<a href="<?php the_permalink(); ?>" class="details" title="<?php the_title(); ?>"><i class="icon-share-alt"></i></a>
			    	<a href="<?php echo $image_url; ?>" class="zoom" title="<?php the_title(); ?>" rel="magnific"><i class="icon-eye-open"></i></a>
			    </div>
			  </div>
		  <?php } ?>
		  <?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
	  		<header class="post-title cf">
	  		  	<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	  		</header>
		  <?php } ?>
		  <?php if(!is_page_template('template-blog-masonry.php') && !is_admin()) { ?>
			  <div class="row">
			  	<?php if(!is_single()) { ?>
				  <aside class="two columns hide-for-small">
					  	<?php get_template_part('inc/postformats/post-date'); ?>
				  </aside>
				  <?php } ?>
				  <div class="post-content <?php if(!is_single()) { echo 'ten'; } else { echo 'twelve'; } ?> columns">
				    <?php if ( is_search() || is_archive() ) { the_excerpt(); }  else { the_content('Read More'); } ?>
				    <?php if ( is_single()) { wp_link_pages(); } ?>
				  </div>
			  </div>
		  <?php } else { ?>
			  <div class="post-content">
			    <?php if ( is_search() || is_archive() ) { the_excerpt(); }  else { the_content('Read More'); } ?>
			    <?php if ( is_single()) { wp_link_pages(); } ?>
			  </div>
		   <?php } ?>
		  <?php if ( is_single()) { get_template_part('inc/postformats/post-meta'); } ?>
	  </div>
  </div>
</article>
<?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
	</div>
</div>
<?php } ?>