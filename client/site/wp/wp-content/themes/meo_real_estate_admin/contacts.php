<?php
/**
 * Template name: Contacts
 *
 * @package MEO real estate admin
 */
global $wpdb;
$count = $wpdb->get_var($wpdb->prepare('SELECT COUNT(id) AS counter FROM wp_meo_contacts',null));

if (!current_user_can( 'read_contacts' )) {
	wp_redirect( '/settings' );
	exit;
}

$site_id = mrea_get_site_id();

$uses_crm = mrea_uses_crm($site_id);

get_header(); ?>

	<div class="portlet">
            <?php if (current_user_can( 'read_contacts' )) { ?>
                    <?php mrea_get_site_selector_form(); ?>
            <?php } ?>

            <h3 class="portlet-title">
                    <u><?php _e( 'All Contacts', 'meo_real_estate_admin' ); ?></u>
            </h3>
            <?php print_r($results); ?> 
            <p>
                Total de contacts <?php echo $count; ?>
            </p>
            
            <?php if ($uses_crm) { ?>
                    <div style="margin-bottom: 10px;">
                                    <a href="<?php echo get_permalink(PAGE_ID_CONTACT); ?>?contact_id=-1&amp;edit=1">Add a new contact</a>
                                    <a style="float: right;" href="<?php echo get_permalink(PAGE_ID_CONTACT_CSV); ?>">Export in CSV format</a>
                    </div>
            <?php } ?>

            <div class="portlet-body">

            <?php if ($site_id > 0) {

                    if ($uses_crm) {
                            get_template_part( 'contacts-table', 'crm' );
                    }
                    else {
                            get_template_part( 'contacts-table', 'nocrm' );
                    }
            }
            else {
                    _e( 'You do not have sufficient permissions to view contacts.', 'meo_real_estate_admin' );
            } ?>

            </div> <!-- /.portlet-body -->

	</div> <!-- /.portlet -->

<?php get_footer(); ?>
