(function($){

	// Document ready function
	$(function() {

		$('.refresh-lots-link').click( function(e) {
			e.preventDefault();

			var $link = $(this),
			    $loader = $link.siblings('.lots-loader'),
			    $message = $link.siblings('.refresh-message');

			$link.hide();
			$loader.show();
			$message.hide();

			$.ajax({
				type:"GET",
				url: $link.attr('href'),
				success:function (result) {
					var message_class = result.success ? 'success' : 'error';
					$message.removeClass('success error').addClass(message_class).html(result.message);
				},
				error: function(jqXHR, textStatus, errorThrown ) {
					$message.removeClass('success error').addClass('error').html('Unknown error');
				},
				complete: function( jqXHR, textStatus ) {
					$link.show();
					$loader.hide();
					$message.show();
				}
			});

		});

	});

})(jQuery);
