<?php require_once( '../../../../../wp-load.php' );
	Header("Content-type: text/css");
	error_reporting(0);
	$fontlist = array();
	function google_webfont($font, $default = false) {
			global $fontlist;
	
			$fontbase = 'http://fonts.googleapis.com/css?family=';
			$import='';	
			
			if ($font) {
				$otfont = ot_get_option($font);
				if ($otfont['font-family']) {
					$otfontfamily = $otfont['font-family'];
				} else {
					$otfontfamily = $default;
				}
				if (!in_array($otfontfamily, $fontlist)) {
					array_push($fontlist, $otfontfamily);
					if ($otfontfamily) {
						$cssfont = str_replace(' ', '+', $otfontfamily);
						$import = '@import "'.$fontbase.$cssfont .':200,300,400,600,700";';
		
						return $import;
					}
				}
			}
		}
	function typeecho($array, $important = false, $default = false) {
		
		if ($array['font-family']) { 
			echo "font-family: " . $array['font-family'] . ";\n";
		} else if ($default) {
			echo "font-family: " . $default . ";\n";
		}
		if ($array['font-color']) { 
			echo "color: " . $array['font-color'] . ";\n";
		}
		if ($array['font-style']) { 
			echo "font-style: " . $array['font-style'] . ";\n";
		}
		if ($array['font-variant']) { 
			echo "font-variant: " . $array['font-variant'] . ";\n";
		}
		if ($array['font-weight']) { 
			echo "font-weight: " . $array['font-weight'] . ";\n";
		}
		if ($array['font-size']) { 
			
			if ($important) {
				echo "font-size: " . $array['font-size'] . " !important;\n";
			} else {
				echo "font-size: " . $array['font-size'] . ";\n";
			}
		}
		if ($array['text-decoration']) { 
				echo "text-decoration: " . $array['text-decoration'] . " !important;\n";
		}
		if ($array['text-transform']) { 
				echo "text-transform: " . $array['text-transform'] . " !important;\n";
		}
		if ($array['line-height']) { 
				echo "line-height: " . $array['line-height'] . " !important;\n";
		}
		if ($array['letter-spacing']) { 
				echo "letter-spacing: " . $array['letter-spacing'] . " !important;\n";
		}
	}
	function bgecho($array) {
		if ($array['background-color']) { 
			echo "background-color: " . $array['background-color'] . ";\n";
		}
		if ($array['background-image']) { 
			echo "background-image: url(" . $array['background-image'] . ");\n";
		}
		if ($array['background-repeat']) { 
			echo "background-repeat: " . $array['background-repeat'] . ";\n";
		}
		if ($array['background-attachment']) { 
			echo "background-attachment: " . $array['background-attachment'] . ";\n";
		}
		if ($array['background-position']) { 
			echo "background-position: " . $array['background-position'] . ";\n";
		}
	}
	function measurementecho($array) {
			echo $array[0] . $array[1];
	}
	echo google_webfont('logo_type') . "\n";
	echo google_webfont('post_title_type') . "\n";
	echo google_webfont('body_type', 'Open Sans') . "\n";
	echo google_webfont('menu_type') . "\n";
	echo google_webfont('submenu_type') . "\n";
	echo google_webfont('widget_title_type') . "\n";
	echo google_webfont('footer_widget_title_type') . "\n";
	echo google_webfont('footer_type') . "\n";
	echo google_webfont('heading_h1_type') . "\n";
	echo google_webfont('heading_h2_type') . "\n";
	echo google_webfont('heading_h3_type') . "\n";
	echo google_webfont('heading_h4_type') . "\n";
	echo google_webfont('heading_h5_type') . "\n";
	echo google_webfont('heading_h6_type') . "\n";
	
	function hex2rgb($hex) {
	
	   $hex = str_replace("#", "", $hex);
	
		if(strlen($hex) == 3) {
	
	      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	
	   } else {
	
	      $r = hexdec(substr($hex,0,2));
	      $g = hexdec(substr($hex,2,2));
	      $b = hexdec(substr($hex,4,2));
	
	   }
	
	   $rgb = array($r, $g, $b);
	
	   return implode(",", $rgb); // returns the rgb values separated by commas
	
	
	}
?>
/* Options set in the admin page */

body { 
	<?php typeecho(ot_get_option('body_type'), false, 'Open Sans'); ?>
	color: <?php echo ot_get_option('text_color'); ?>;
}
.sf-menu li.current-menu-item, .pagenavi ul li.disabled a, .pagenavi ul li a:hover, .iconbox.hexagon:hover > span, .filters li a.active, .widget.widget_flickr .flickr_badge_image:hover, .gotoportfolio:hover, .widget.widget_tag_cloud li a:hover, .post .post-gallery.link:hover, .pricing .item.featured .header, .btn:hover, input[type=submit]:hover, .comment-reply-link:hover { background: <?php echo ot_get_option('accent_color'); ?>; } dl.tabs dd.active a, .clientlist .columns div:hover, .widget.widget_flickr .flickr_badge_image:hover, .pricing .item.featured .header h2 { border-color: <?php echo ot_get_option('accent_color'); ?>; } a:hover, footer a:hover, footer .subfooter a:hover, .iconbox.left:hover > span, #scrolltotop:hover i, #header .searchlink:hover, #header .searchlink.open,.single-portfolio .gotoportfolio:hover i { color: <?php echo ot_get_option('accent_color'); ?>; } ::-webkit-selection{ background-color: <?php echo ot_get_option('accent_color'); ?>; } ::-moz-selection{ background-color: <?php echo ot_get_option('accent_color'); ?>; } ::selection{ background-color: <?php echo ot_get_option('accent_color'); ?>; }

<?php if (ot_get_option('overlay_color')) { ?>
.fresco:hover .overlay {
	<?php $rgb = hex2rgb(ot_get_option('overlay_color')); ?>
	<?php if(ot_get_option('overlay_opacity')) { 
		echo "background-color: rgba(".$rgb.", ".ot_get_option('overlay_opacity').");";
		} else { 
		echo "background-color: rgb(".$rgb.");";
		}?>
}
<?php } ?>
a { 
	color: <?php echo ot_get_option('link_color'); ?>;
}
a:hover { 
	color: <?php echo ot_get_option('link_color_hover'); ?>;
}
.post .post-title h2 a{
	<?php typeecho(ot_get_option('post_title_type')); ?>
}
.post .post-content h1 {
	<?php typeecho(ot_get_option('heading_h1_type')); ?>
}
.post .post-content h2 {
	<?php typeecho(ot_get_option('heading_h2_type')); ?>
}
.post .post-content h3 {
	<?php typeecho(ot_get_option('heading_h3_type')); ?>
}
.post .post-content h4 {
	<?php typeecho(ot_get_option('heading_h4_type')); ?>
}
.post .post-content h5 {
	<?php typeecho(ot_get_option('heading_h5_type')); ?>
}
.post .post-content h6 {
	<?php typeecho(ot_get_option('heading_h6_type')); ?>
}
#header .sf-menu li:hover, .sf-menu li.sfHover,
#header .sf-menu li ul li {
	background-color: <?php echo ot_get_option('dropdown_bg'); ?>;
}
#header .sf-menu li ul li:hover {
	background-color: <?php echo ot_get_option('dropdown_hover_bg'); ?>;
}
#header .logo h1 a{
	<?php typeecho(ot_get_option('logo_type'), true); ?>
}
#header ul.sf-menu>li>a {
	<?php typeecho(ot_get_option('menu_type')); ?>
}
#header ul.sf-menu ul>li>a {
	<?php typeecho(ot_get_option('submenu_type')); ?>
}
#header ul.sf-menu>li>a {
	<?php typeecho(ot_get_option('menu_type')); ?>
}
#header ul.sf-menu ul>li>a {
	<?php typeecho(ot_get_option('submenu_type')); ?>
}
.sidebar .widget h6{
	<?php typeecho(ot_get_option('widget_title_type')); ?>
}
<?php if(ot_get_option('sidebar_position') == 'left-sidebar') { ?>
section.blog {float:right}
<?php } ?>

/* Footer */
footer {
	<?php typeecho(ot_get_option('footer_type')); ?>
	<?php bgecho(ot_get_option('footer_bg')); ?>
	color: <?php echo ot_get_option('footer_text_color'); ?>;
}
footer .widget h6 {
	<?php typeecho(ot_get_option('footer_widget_title_type')); ?>
}
footer a { 
	color: <?php echo ot_get_option('footer_link_color'); ?>;
}
footer a:hover { 
	color: <?php echo ot_get_option('footer_link_color_hover'); ?>;
}
footer .subfooter {
	<?php bgecho(ot_get_option('subfooter_bg')); ?>
}
<?php echo ot_get_option('extra_css'); ?>