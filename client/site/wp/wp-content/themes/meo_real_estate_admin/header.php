<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package MEO real estate admin
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php

add_filter('get_avatar', 'mrea_add_class_to_avatar', 10, 5);
function mrea_add_class_to_avatar($avatar, $id_or_email, $size, $default, $alt ) {
	$avatar = str_replace("class='", "class='navbar-profile-avatar ", $avatar);
	return $avatar;
}

add_filter('loginout', 'mrea_add_icon_to_logout_link');
function mrea_add_icon_to_logout_link($link) {
	$link = str_replace('">', '"><i class="fa-sign-out fa"></i> ', $link);
	return $link;
}

global $meo_site_id;
$meo_site_id = mrea_get_site_id();

wp_head();
$user_id = get_current_user_id();
$logo_url = is_user_logged_in() ? get_bloginfo("url") : 'http://www.meo-realestate.com/';
$loginoutlink = wp_loginout('', false);

?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'meo_real_estate_admin' ); ?></a>

	<header class="navbar navbar-inverse" role="banner">

		<div class="container">

			<div class="navbar-header">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only"><?php _e( 'Toggle navigation', 'meo_real_estate_admin' ); ?></span>
					<i class="fa fa-cog"></i>
				</button>

				<a href="<?php echo $logo_url; ?>" class="navbar-brand navbar-brand-img">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="MEO">
				</a>
			</div> <!-- /.navbar-header -->

			<nav class="collapse navbar-collapse" role="navigation">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="http://www.meo-realestate.com/"><?php _e( 'About', 'meo_real_estate_admin' ); ?></a>
					</li>
					<?php if (is_user_logged_in()) { ?>
						<?php $avatar = get_avatar( $user_id, 32 ); ?>
						<li class="dropdown navbar-profile">
							<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
								<?php echo $avatar; ?>
								<i class="fa fa-caret-down"></i>
							</a>

							<?php

							wp_nav_menu( array(
								'theme_location' => 'settings',
								'container'      => false,
								'items_wrap'     => '<ul class="dropdown-menu" role="menu">%3$s<li class="divider"></li><li class="nav-menu" class="menu-item">'. $loginoutlink .'</li></ul>'
							) ); ?>



						</li>

					<?php } ?>
				</ul>
			</nav>

		</div> <!-- /.container -->

	</header>

	<?php if (is_user_logged_in()) { ?>

		<div class="mainnav">

			<div class="container">

				<a class="mainnav-toggle" data-toggle="collapse" data-target=".mainnav-collapse">
					<span class="sr-only">Toggle navigation</span>
					<i class="fa fa-bars"></i>
				</a>

				<nav class="collapse mainnav-collapse" role="navigation">

					<ul class="mainnav-menu">

						<?php if (current_user_can( 'read_contacts' )) { ?>
							<li class="dropdown active">
								<a href="./index.php" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
								<?php _e( 'Reports', 'meo_real_estate_admin' ); ?>
								<i class="mainnav-caret"></i>
								</a>

								<?php wp_nav_menu( array(
									'theme_location' => 'primary',
									'container'      => false,
									'items_wrap'     => '<ul class="dropdown-menu" role="menu">%3$s</ul>'
								) ); ?>

							</li>
						<?php } ?>


						<li class="dropdown ">

							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
							<?php _e( 'Preferences', 'meo_real_estate_admin' ); ?>
							<i class="mainnav-caret"></i>
							</a>

							<?php wp_nav_menu( array(
								'theme_location' => 'settings',
								'container'      => false,
								'items_wrap'     => '<ul class="dropdown-menu" role="menu">%3$s<li class="nav-menu" class="menu-item">'. $loginoutlink .'</li></ul>'
							) ); ?>

						</li>

					</ul>

				</nav>

			</div> <!-- /.container -->

		</div> <!-- /.mainnav -->

	<?php } ?>

	<div class="content site-content">
		<div class="container">
