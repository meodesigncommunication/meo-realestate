<?php 
		$excerpt = get_post_meta($post->ID, 'portfolio_excerpt', TRUE); 
		$headercolor = get_post_meta($post->ID, 'portfolio_header', TRUE); 
		$portfolio_main = get_post_meta($post->ID, 'portfolio_main', TRUE);
		?>
<?php 
	if ($portfolio_main) {
		$portfolio_link = get_permalink($portfolio_main);
	} else {
		$portfolio_link = get_portfolio_page_link(get_the_ID()); 
	}
?>
<div class="portfolio-header" style="background: <?php echo $headercolor;?>;">
	<div class="row">
		<div class="eight columns centered">
			  <h2><?php the_title(); ?></h2>
			  
			  <?php if ($excerpt) { ?>
			  	<p><?php echo $excerpt; ?></p>
			  <?php } ?>
			  <?php next_post_link('%link', '<i class="icon-angle-left"></i>'); ?>
			  <a href="<?php echo $portfolio_link; ?>" class="gotoportfolio"><i class="icon-th-large"></i></a>
			  <?php previous_post_link('%link', '<i class="icon-angle-right"></i>'); ?>
		</div>
	</div>
</div>