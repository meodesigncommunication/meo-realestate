<?php
/*-----------------------------------------------------------------------------------*/
/*	Create a new post type called slides
/*-----------------------------------------------------------------------------------*/
function create_post_type_slides() 
{
	$labels = array(
		'name' => __( 'Slides',THB_THEME_NAME),
		'singular_name' => __( 'Slide',THB_THEME_NAME ),
		'rewrite' => array('slug' => __( 'slides',THB_THEME_NAME )),
		'add_new' => _x('Add New', 'slide',THB_THEME_NAME ),
		'add_new_item' => __('Add New Slide',THB_THEME_NAME),
		'edit_item' => __('Edit Slide',THB_THEME_NAME),
		'new_item' => __('New Slide',THB_THEME_NAME),
		'view_item' => __('View Slide',THB_THEME_NAME),
		'search_items' => __('Search Slides',THB_THEME_NAME),
		'not_found' =>  __('No slides found',THB_THEME_NAME),
		'not_found_in_trash' => __('No slides found in Trash',THB_THEME_NAME), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'can_export' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title', 'thumbnail')
	  ); 
	  
	  register_post_type(__( 'slide',THB_THEME_NAME ),$args);
	  flush_rewrite_rules();
}

function slide_edit_columns($columns){  
	$newcolumns = array(
		"cb" => "<input type=\"checkbox\" />",
		"image column-comments" => "Image",
		"title" => "Title"
	);

	$columns= array_merge($newcolumns, $columns);

	return $columns;  
}
/*-----------------------------------------------------------------------------------*/
/*	Create a new post type called portfolios
/*-----------------------------------------------------------------------------------*/

function create_post_type_portfolios() 
{
	$labels = array(
		'name' => __( 'Portfolio',THB_THEME_NAME),
		'singular_name' => __( 'Portfolio',THB_THEME_NAME ),
		'rewrite' => array('slug' => __( 'portfolios',THB_THEME_NAME )),
		'add_new' => _x('Add New', 'portfolio', THB_THEME_NAME),
		'add_new_item' => __('Add New Portfolio',THB_THEME_NAME),
		'edit_item' => __('Edit Portfolio',THB_THEME_NAME),
		'new_item' => __('New Portfolio',THB_THEME_NAME),
		'view_item' => __('View Portfolio',THB_THEME_NAME),
		'search_items' => __('Search Portfolio',THB_THEME_NAME),
		'not_found' =>  __('No portfolios found',THB_THEME_NAME),
		'not_found_in_trash' => __('No portfolios found in Trash',THB_THEME_NAME), 
		'parent_item_colon' => ''
	  );
	  
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail')
	  ); 
	  
	  register_post_type('portfolio',$args);
	  flush_rewrite_rules();
}

function build_taxonomies(){
	register_taxonomy('project-category', array(__( "portfolio",THB_THEME_NAME )), array("hierarchical" => true, "label" => __( "Categories",THB_THEME_NAME ), "singular_label" => __( "Category",THB_THEME_NAME ), "rewrite" => array('slug' => 'project-category', 'hierarchical' => true)));  
}


function portfolio_edit_columns($columns){  
	$newcolumns = array(
		"cb" => "<input type=\"checkbox\" />",
		"image column-comments" => "Image",
		"title" => "Title",
		"type" => "Categories"
	);

	$columns= array_merge($newcolumns, $columns);

	return $columns;  
}
function post_edit_columns($columns)
{
	$newcolumns = array(
		"cb" => "<input type=\"checkbox\" />",
		"image column-comments" => "Image",
	);

	$columns= array_merge($newcolumns, $columns);

	return $columns;
}
function portfolio_custom_columns($column){  
        global $post;  
        switch ($column)  
        {    
            case __( 'type', THB_THEME_NAME ):  
                echo get_the_term_list($post->ID,'project-category', '', ', ','');  
                break;
            case __( 'image column-comments',THB_THEME_NAME ):  
                echo get_the_post_thumbnail($post->ID, array(40,40));  
                break;

        }  
}  

/* Initialize post types */
add_action( 'init', 'build_taxonomies', 0 );
add_action( 'init', 'create_post_type_portfolios' );
add_filter("manage_edit-portfolio_columns", "portfolio_edit_columns");
add_filter("manage_edit-post_columns", "post_edit_columns");
add_action("manage_posts_custom_column",  "portfolio_custom_columns"); 

add_action( 'init', 'create_post_type_slides' );
add_filter("manage_edit-slide_columns", "slide_edit_columns");
?>