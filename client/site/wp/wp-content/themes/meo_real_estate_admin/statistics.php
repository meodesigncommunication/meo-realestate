<?php
/**
 * Template name: Statistics
 *
 * @package MEO real estate admin
 */

$site_id = mrea_get_site_id();

get_header(); ?>

<div class="portlet">
	<?php if (current_user_can( 'read_analytics' )) { ?>
		<?php mrea_get_site_selector_form(); ?>
	<?php } ?>

	<h3 class="portlet-title">
		<u><?php _e( 'Statistics', 'meo_real_estate_admin' ); ?></u>
	</h3>

	<div class="portlet-body">

		<?php if (current_user_can( 'read_analytics' ) and $site_id > 0 ) {
			$sites = mrea_get_sites_for_user();
			$current_site = $sites[$site_id];
			$piwik_site_id = $current_site['piwik_site_id'];
			$piwik_token = $current_site['piwik_token'];

			if (empty($piwik_site_id) or empty($piwik_token)) {
				_e( 'The selected site is missing configuration, so statistics are not available.', 'meo_real_estate_admin' );
			}
			else { ?>
				<iframe id="analytics" src="https://piwik.meoanalytics.com/index.php?module=Widgetize&amp;action=iframe&amp;moduleToWidgetize=Dashboard&amp;actionToWidgetize=index&amp;idSite=<?php echo $piwik_site_id; ?>&amp;period=week&amp;date=today&amp;token_auth=<?php echo $piwik_token; ?>" frameborder="0" marginheight="0" marginwidth="0" width="100%" height="100%"></iframe><?php
			}
		}
		else {
			_e( 'You do not have sufficient permissions to view statistics.', 'meo_real_estate_admin' );
		} ?>

	</div> <!-- /.portlet-body -->

</div> <!-- /.portlet -->

<?php get_footer(); ?>
