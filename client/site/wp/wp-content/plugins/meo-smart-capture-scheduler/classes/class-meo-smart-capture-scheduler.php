<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoSmartCaptureScheduler {

	const DB_VERSION = '1.0.0';

	public function __construct() {
		$this->addHooks();
	}

	public static function activate() {
		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$charset_collate = '';

		if ( ! empty( $wpdb->charset ) ) {
			$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
		}

		if ( ! empty( $wpdb->collate ) ) {
			$charset_collate .= " COLLATE {$wpdb->collate}";
		}


		$sql = "CREATE TABLE " . $wpdb->prefix . "email_schedule (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			analytics_id varchar(100) NOT NULL,
			site_id bigint(20) unsigned NOT NULL,
			creation_date datetime NOT NULL,
			update_date datetime NOT NULL,
			sent_date datetime,
			in_error enum('yes','no') default 'no',
			PRIMARY KEY (id),
			UNIQUE KEY analytics_and_site (analytics_id, site_id)
		) $charset_collate;";

		dbDelta( $sql );

		add_option( 'msc_scheduler_db_version', self::DB_VERSION );
	}

	public static function deactivate() {
		// Do nothing - leave the tables in case the plugin's reactivated
	}

	public function addHooks() {
		add_action( 'wp_ajax_nopriv_schedule_pdf_email', array($this, 'schedulePdfEmail') );
		add_action( 'wp_ajax_schedule_pdf_email', array($this, 'schedulePdfEmail') );
	}


	public function schedulePdfEmail() {
		global $wpdb;
		header('Content-Type: application/json');

		if (!array_key_exists('analytics_id', $_GET) or empty($_GET['analytics_id']) or
			!array_key_exists('api_key', $_GET) or empty($_GET['api_key'])) {
			echo json_encode(array(
				'success' => 0,
				'result' => __('Required parameter missing', 'meo_real_estate_admin')
			));
			exit;
		}

		$site = mrea_get_site_by_api_key($_GET['api_key']);
		if (empty($site)) {
			echo json_encode(array(
				'success' => 0,
				'result' => __('Invalid API key', 'meo_real_estate_admin')
			));
			exit;
		}

		$result = $wpdb->query( $wpdb->prepare( "
			INSERT INTO {$wpdb->prefix}email_schedule (analytics_id, site_id, creation_date, update_date)
			VALUES (%s, %d, now(), now())
			ON DUPLICATE KEY UPDATE update_date = now()",
			$_GET['analytics_id'],
			$site['id']
		) );

		if (!empty($wpdb->last_error)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __('Insert failed: ', 'meo_real_estate_admin') . $wpdb->last_error
			));
			exit;
		}

		echo json_encode(array(
			'success' => 1,
			'message' => __('PDF email scheduled', 'meo_real_estate_admin')
		));
		exit;
	}
}
