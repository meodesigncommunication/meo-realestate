jQuery(function() {
	var select_prefix = '_'; // Prepended to contact id.  Without it, the dropdown will be sorted by ID, numerically

	if (meo_settings['site_id'] < 0) {
		return;
	}

	var get_all_contacts = function(site, request_url) {
		var entityMap = {
			"&": "&amp;",
			"<": "&lt;",
			">": "&gt;",
			'"': '&quot;',
			"'": '&#39;',
			"/": '&#x2F;'
		};

		function escapeHtml(string) {
			return String(string).replace(/[&<>"'\/]/g, function (s) {
				return entityMap[s];
			});
		}

		jQuery.ajax({
			type:"GET",
			url:request_url,
			data:{
				"action" : "get_crm_list",
				"api-key" : site['api_key']
			},
			async: false,
			success:function (msg) {

				contacts_array = [];
				lot_to_contact = {};
				jQuery.each(msg.aaData, function(index, value){
					var id = parseInt(value.id, 10),
					    name = jQuery.trim(value.name);
					if (name) {
						contacts_array.push({
							"id" : id,
							"name" : escapeHtml(name)
						});
						jQuery.each(value.linked_lots, function(lot_index, lot_name) {
							lot_to_contact[lot_name] = id;
						});
					}
				});

				contacts_array.sort(function (a, b) {
					var x = a.name.toLowerCase();
					var y = b.name.toLowerCase();
					return x < y ? -1 : x > y ? 1 : 0;
				});

				contacts_lookup = {};
				contacts_lookup[select_prefix + '-1'] = '--None--';
				jQuery.each(contacts_array, function(index, value){
					contacts_lookup[select_prefix + value.id] = value.name;
				});
			},
			error:function( jqXHR, textStatus, errorThrown ) {
				// Ignore.  Show the untranslated statuses

			}
		});
	};

	function add_linked_lot_handler(site, request_url) {
		var chosen_name;
		jQuery('.linked-lot.editable').editable(request_url, {
			type: 'select',
			name: 'contact_id',
			data   : contacts_lookup,
			placeholder: meo_settings['translations']['clicktoadd'],
			indicator: '<img src="/wp/wp-content/themes/meo_real_estate_admin/images/ajax-loader-small.gif">',
			onblur: 'submit',
			submitdata: function() {
				chosen_name = jQuery(this).find('option:selected').text();
				return {
					action: 'set_linked_lot',
					"api-key" : site['api_key'],
					lot_id : jQuery(this).data('lot-id'),
					"prefix" : select_prefix
				};
			},
			callback: function (value, settings) {
				jQuery(this).html(chosen_name);
			}
		});
	}



	function add_notes_handler(site, request_url) {
		jQuery('.note.editable').editable(request_url, {
			type: 'textarea',
			rows: 5,
			placeholder: meo_settings['translations']['clicktoadd'],
			indicator: '<img src="/wp/wp-content/themes/meo_real_estate_admin/images/ajax-loader-small.gif">',
			onblur: 'submit',
			submitdata: function() {
				return {
					action: 'set_note',
					lot_id : jQuery(this).data('lot-id')
				};
			}
		});
	}

	var site = meo_settings['sites'][meo_settings['site_id']],
	    request_url = ( site['allow_direct_requests'] == 1 ? site['site_url'] + site['api_url'] : meo_settings['ajax_url']),
	    contacts_array = null,
	    contacts_lookup = null,
	    lot_to_contact = null;

	get_statuses(site, request_url);
	get_all_contacts(site, request_url);
	
	// Table columns
	var columns = [
	   			{ "mData": "lot_type" },
				{ "mData": "building" },
				{ "mData": "floor" },
				{ "mData": "lot" },
				{ "mData": "rooms" },
				{ "mData": "surface" },
				{ "mData": availability_extractor }];
				// Can update prices (adding column only if can, to match number of columns)
				if(site['update_prices'] == 1) {
					columns[columns.length] = { "mData": price_checker };
				}
				// Continue normally
				columns[columns.length] = { "mData": function( data, type, extra )
								{
									var lot = data['lot'],
									    contact_id = -1,
									    content = '';
			
									if (typeof lot_to_contact !== 'undefined' && lot_to_contact &&
									    typeof contacts_lookup !== 'undefined' && contacts_lookup &&
									    typeof lot_to_contact[lot] !== 'undefined' && lot_to_contact[lot]) {
										contact_id = lot_to_contact[lot];
										content = contacts_lookup[select_prefix + contact_id];
									}
			
									return '<div class="linked-lot editable" data-lot-id="' + data['lot_id'] + '">' + content + '</div>';
								}
							};
				columns[columns.length] = { "mData": function( data, type, extra ) {
								var content = '';
									if (data['note'] != 'undefined' && data['note']) {
										content = data['note'];
									}
									return '<div class="note editable" data-lot-id="' + data['lot_id'] + '">' + content + '</div>';
								}
							};
	
	// Creating table with columns and other params
	var table_1 = jQuery('#table-1').dataTable ({
		"sAjaxSource": request_url + "?action=get_apartment_list&api-key=" + site['api_key'],
		"aoColumns": columns,
		"iDisplayLength": -1, /* Show all rows */
		"fnInitComplete": function(oSettings, json) {
			jQuery(this).parents ('.dataTables_wrapper').find ('.dataTables_filter input').prop ('placeholder', meo_settings['search_placeholder']).addClass('form-control input-sm');
			add_availability_handler(this, site, request_url);
			add_price_handler(this, site, request_url);
			add_linked_lot_handler(site, request_url);
			add_notes_handler(site, request_url);
		},
		"oLanguage": meo_settings['datatable_trans']
	});
});
