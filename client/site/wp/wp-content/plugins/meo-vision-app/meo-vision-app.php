<?php
/*
Plugin Name: MEO Vision App
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited.
Proprietary and confidential

info@meomeo.ch
*/

if (!defined('MVA_TEXT_DOMAIN')) {
	define('MVA_TEXT_DOMAIN', 'meo_vision_app');
}

if (!defined('GALLERY_THUMB_SIZE')) {
	define('GALLERY_THUMB_SIZE', 'meo-vision-app-gallery-thumb');
}



$plugin_dir = dirname(plugin_basename(__FILE__));
load_plugin_textdomain(MVA_TEXT_DOMAIN, false, $plugin_dir . '/languages/');

$plugin_root = plugin_dir_path( __FILE__ );

require_once( $plugin_root . 'classes/class-meo-vision-app.php');

// Installation and uninstallation hooks
register_activation_hook(__FILE__, array('MeoVisionApp', 'activate'));
register_deactivation_hook(__FILE__, array('MeoVisionApp', 'deactivate'));

global $meova;
$meova = new MeoVisionApp();

add_image_size( GALLERY_THUMB_SIZE, 0, 216 );  // Max height 216px, no max width
