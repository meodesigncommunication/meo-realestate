<?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
	<div class="four columns item">
		<div class="postcontainer">
<?php } ?>
<article <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
	<div class="row">
		<div class="twelve columns">
			<?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
				<div class="masonry-date">
					<?php echo get_the_date() . ' / '; ?>
					<a href="<?php comments_link(); ?>" class="commentcount">
						 <?php comments_number('0 Comments', '1 Comment', '% Comments' ); ?> 
					</a>
				</div>
			<?php } ?>
			<?php if(!is_page_template('template-blog-masonry.php') && !is_admin()) { ?>
				<div class="row">
					<?php if(!is_single()) { ?>
					<div class="two columns"></div>
					<?php } ?>
					<header class="post-title <?php if(!is_single()) { echo 'ten'; } else { echo 'twelve'; } ?> columns cf">
					  	<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						  <?php echo thb_printLikes(get_the_ID()); ?>
					  <aside class="meta">
					    <?php _e("Posted By", THB_THEME_NAME); ?> <?php the_author_posts_link(); ?>
					    <?php if (get_the_category()) { ?>
					    <?php _e("in", THB_THEME_NAME); ?> <?php the_category(', '); ?>
					    <?php } ?>
					  </aside>
					</header>
				</div>
			<?php } ?>
	    <?php if (!is_search()) { ?>
	    <div class="post-gallery flex-start flex">
	      <ul class="slides">
	      <?php 
	          $args = array(
	              'orderby'    => 'menu_order',
	              'post_type'      => 'attachment',
	              'post_parent'    => get_the_ID(),
	              'post_mime_type' => 'image',
	              'post_status'    => null,
	              'numberposts'    => -1,
	          );
	          $attachments = get_posts($args);
	      ?>
	      <?php foreach ($attachments as $attachment) : ?>
	          
	          <?php
	          		if(!is_page_template('template-blog-masonry.php') && !is_admin()) {
	              	$src = wp_get_attachment_image_src( $attachment->ID, 'twothirds'); 
	              } else {
	              	$src = wp_get_attachment_image_src( $attachment->ID, 'onethirds'); 
	              }
	              $image_id = $attachment->ID;
	              $image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0];
	          ?>
	          
	          <li>
	              <img 
	              alt="<?php echo apply_filters('the_title', $attachment->post_title); ?>" 
	              src="<?php echo $src[0]; ?>" 
	              />
	              <a class="enlarge" href="<?php echo $image_url; ?>"><i class="icon-resize-full"></i></a>
	          </li>
	      <?php endforeach; ?>
	      </ul>
	      
	    </div>
	    <?php } ?>
	    <?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
	    	<header class="post-title cf">
	    	  	<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	    	</header>
	    <?php } ?>
	    <?php if(!is_page_template('template-blog-masonry.php') && !is_admin()) { ?>
	      <div class="row">
	      	<?php if(!is_single()) { ?>
		    	  <aside class="two columns hide-for-small">
		    		  	<?php get_template_part('inc/postformats/post-date'); ?>
		    	  </aside>
	    	  <?php } ?>
	    	  <div class="post-content <?php if(!is_single()) { echo 'ten'; } else { echo 'twelve'; } ?> columns">
	    	    <?php if ( is_search() || is_archive() ) { the_excerpt(); }  else { the_content('Read More'); } ?>
	    	    <?php if ( is_single()) { wp_link_pages(); } ?>
	    	  </div>
	      </div>
	    <?php } else { ?>
	      <div class="post-content">
	        <?php if ( is_search() || is_archive() ) { the_excerpt(); }  else { the_content('Read More'); } ?>
	        <?php if ( is_single()) { wp_link_pages(); } ?>
	      </div>
	     <?php } ?>
	    <?php if ( is_single()) { get_template_part('inc/postformats/post-meta'); } ?>
    </div>
	</div>    
</article>
<?php if(is_page_template('template-blog-masonry.php') || is_admin()) { ?>
	</div>
</div>
<?php } ?>