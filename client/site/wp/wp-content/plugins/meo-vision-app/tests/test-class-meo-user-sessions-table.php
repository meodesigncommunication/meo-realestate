<?php

class MeoUserSessionsTableTest extends WP_UnitTestCase {
	function testColumnDate() {
		global $hook_suffix;
		$hook_suffix = 'user_sessions';

		require_once(dirname( __FILE__ ) . '/../classes/class-meo-user-sessions-table.php');
		$must = new MeoUserSessionsTable();

		$this->assertNotNull( $must );


		$tests = array(
			array(
				'data' => array( 'session_key' => 'invalidstring' ),
				'expected' => 'Invalid Session Key'
			),
			array(
				'data' => array( 'session_key' => 'Jogy4ftOcqV4W4rBw/L/re0kmv2ozR9lSH9qo2w2MZqZ4DaR3AKRbOhzb2E0LFx+wNc/Rku9j4r4kZoG9R5QxA==' ),
				'expected' => '2015-04-27 13:58:11'
			)
		);

		foreach ($tests as $test) {
			$this->assertEquals($test['expected'], $must->column_default($test['data'], 'session_date'));
		}
	}
}
