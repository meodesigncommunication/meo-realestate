<?php
/*
Plugin Name: MEO Smart Capture Scheduler
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited.
Proprietary and confidential

info@allmeo.com
*/

$plugin_dir = dirname(plugin_basename(__FILE__));
load_plugin_textdomain(MSC_TEXT_DOMAIN, false, $plugin_dir . '/languages/');

$plugin_root = plugin_dir_path( __FILE__ );

require_once( $plugin_root . 'classes/class-meo-smart-capture-scheduler.php');

// Installation and uninstallation hooks
register_activation_hook(__FILE__, array('MeoSmartCaptureScheduler', 'activate'));
register_deactivation_hook(__FILE__, array('MeoSmartCaptureScheduler', 'deactivate'));

global $meoscs;
$meoscs = new MeoSmartCaptureScheduler();
