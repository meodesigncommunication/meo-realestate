<?php
/**
 * Template name: Contacts CSV export
 *
 */

global $meocrm;

$site_id = mrea_get_site_id();
$contacts = $meocrm->getContacts($site_id);
$fields = $meocrm->getCustomFieldsForUser(true);

header('Content-Type: application/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=all-contacts.csv');

$output = fopen('php://output', 'w');

$headers = array(
	'ID',
	'Surname',
	'First name',
	'contact type',
	'Address',
	'City',
	'Postcode',
	'Country',
	'email',
	'First contact',
	'Phone',
	'Note',
	'Lots Viewed',
	'Linked lots'
);

foreach ($fields as $field) {
	$headers[] = $field['name'];
}

fputcsv($output, $headers);

foreach ($contacts as $contact) {
	$data = array(
		$contact->id,
		$contact->surname,
		$contact->first_name,
		$contact->contact_type,
		$contact->address,
		$contact->city,
		$contact->postcode,
		$contact->country,
		$contact->email,
		$contact->first_contact,
		$contact->phone,
		$contact->note,
		join(", ", $contact->lots),
		join(", ", $contact->linked_lots)
	);

	foreach ($fields as $field) {
		$data[] = isset($contact->field_values[$field['slug']]) ? $contact->field_values[$field['slug']]['label'] : '';
	}

	fputcsv($output, $data, ',');

}
