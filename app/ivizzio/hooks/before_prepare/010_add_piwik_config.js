#!/usr/bin/env node

//
// This hook adds piwik-related settings if the %%PIWIK_ENABLED%% config variable is true
// Specifically, it adds the piwik plugin, and adds access to http://piwik.meoanalytics.com
//

var PIWIK_PLUGIN_URL = 'https://github.com/kriserickson/cordova-piwik-plugin';
var PIWIK_PLUGIN_NAME = 'com.storefront.cordova.piwik';
var PIWIK_SERVER = 'http://piwik.meoanalytics.com';

var fs = require('fs');
var path = require('path');
var sys = require('sys');
var Config = require('cordova-config');

var execSync = require('child_process').execSync,
    execSyncArgs = { encoding: 'utf8' };

var rootdir = process.argv[2];

var all_properties = require(path.join(rootdir, 'config/properties.js')),
    properties = all_properties.properties,
    config_file = path.join(rootdir, 'config.xml'),
    piwik_enabled = ( properties['%%PIWIK_ENABLED%%'] === 'true');

var existing_plugins = execSync("cordova plugin list", execSyncArgs),
    plugin_installed = ( existing_plugins.indexOf(PIWIK_PLUGIN_NAME) > -1 );


// Add or remove the piwik plugin depending on the project's settings
if (piwik_enabled) {
	if (!plugin_installed) {
		console.log('    Adding piwik plugin');
		execSync("cordova plugin add " + PIWIK_PLUGIN_URL, execSyncArgs);
	}
}
else {
	if (plugin_installed) {
		console.log('    Removing piwik plugin');
		execSync("cordova plugin rm " + PIWIK_PLUGIN_NAME, execSyncArgs);
	}
}


// Ensure the app has access to the piwik server, if necessary
// Remove the access if it's not needed

// Load and parse the config.xml
var config = new Config(config_file);
if (piwik_enabled) {
	console.log('    Adding access to piwik server');
	config.setAccessOrigin(PIWIK_SERVER);
}
else {
	console.log('    Removing access to piwik server');
	config.removeAccessOrigin(PIWIK_SERVER);
}

// Write the config file
config.writeSync();
