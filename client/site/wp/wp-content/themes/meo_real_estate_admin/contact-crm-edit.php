<?php
/**
 * Contact page for CRM (populated in place)
 */

global $meocrm;

if (!current_user_can( 'read_contacts' )) {
	mrea_show_error_page(__( 'You do not have sufficient permissions to view contacts.', 'meo_real_estate_admin' ));
}

$contact_id = (int) $_GET['contact_id'];
$site_id = mrea_get_site_id();

$new_contact = ($contact_id == -1);
if (!$new_contact) {
	$contacts = $meocrm->getContacts($site_id, $contact_id);
	if (empty($contacts)) {
		mrea_show_error_page(__( "Contact not found (or you don't have permissions to view it)", 'meo_real_estate_admin' ));
	}
	$contact = $contacts[$contact_id];
}
else {
	$contact = (object) array(
		'first_name' => '',
		'surname'    => '',
		'address'    => '',
		'city'       => '',
		'postcode'   => '',
		'country'    => '',
		'phone'      => '',
		'email'      => '',
		'note'       => ''
	);
}

$sites = mrea_get_sites_for_user();
$site = $sites[$site_id];


get_header();


$fields = array(
	array(
		'label'  => 'First Name',
		'class'  => 'first-name',
		'field'  => 'first_name',
		'value'  => $contact->first_name
	),
	array(
		'label'  => 'Surname *',
		'class'  => 'surname',
		'field'  => 'surname',
		'value'  => $contact->surname
	),
	array(
		'label'  => 'Address',
		'class'  => 'address',
		'field'  => 'address',
		'value'  => $contact->address
	),
	array(
		'label'  => 'City',
		'class'  => 'city',
		'field'  => 'city',
		'value'  => $contact->city
	),
	array(
		'label'  => 'Post Code',
		'class'  => 'postcode',
		'field'  => 'postcode',
		'value'  => $contact->postcode
	),
	array(
		'label'  => 'Country',
		'class'  => 'country',
		'field'  => 'country',
		'value'  => $contact->country
	),
	array(
		'label'  => 'Telephone',
		'class'  => 'phone',
		'field'  => 'phone',
		'value'  => $contact->phone
	),
	array(
		'label'  => 'email',
		'class'  => 'email',
		'field'  => 'email',
		'value'  => $contact->email
	)
);

?>

	<div class="portlet details">
		<form class="edit-contact-form" bmethod="post" action="<?php the_permalink(); ?>">
			<input type="hidden" name="action" value="save_contact">
			<input type="hidden" name="contact_id" value="<?php echo $contact_id; ?>">

			<h3 class="portlet-title">
				<?php if ($new_contact) { ?>
					<u>New Contact</u>
				<?php } else { ?>
					<u>Contact <span class="contact-name"><?php echo $contact->first_name; ?> <?php echo $contact->surname; ?></span></u>
					<a class="pdf-download" href="<?php the_permalink(); ?>?contact_id=<?php echo urlencode($contact_id); ?>&amp;pdf=1"><i class="fa fa-file-pdf-o"></i></a>
					<a class="view-contact" href="<?php the_permalink(); ?>?contact_id=<?php echo urlencode($contact_id); ?>"><i class="fa fa-info-circle"></i></a>
				<?php } ?>
			</h3>

			<div class="portlet-body">

				<table class="table">
					<tbody>
						<?php $field_index = 0;
						foreach ($fields as $field) {
							if ($field_index % 2 == 0) {
								?><tr><?php
							} ?>
							<th style="width: 16.6%"><?php echo $field['label']; ?></th>
							<td style="width: 33.3%" class="<?php echo $field['class']; ?>">
								<input type="text" name="<?php echo $field['field']; ?>" value="<?php echo $field['value']; ?>">
							</td>
							<?php if ($field_index % 2 != 0) {
								?></tr><?php
							}
							$field_index++;
						}
						if ($field_index % 2 != 0) { ?>
								<th style="width: 16.6%">&nbsp;</th>
								<td style="width: 33.3%">&nbsp;</td>
							</tr>
						<?php } ?>
						<tr>
							<th style="width: 16.6%">Note</th>
							<td colspan="3">
								<textarea name="note" rows="4"><?php echo $contact->note; ?></textarea>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<div class="edit-contact-form-message"></div>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="border-top: none; text-align: right;">
								<button type="submit" class="btn btn-primary"><?php _e('Save', 'meo_real_estate_admin'); ?></button>
							</td>
						</tr>
					</tbody>
				</table>
			</div> <!-- /.portlet-body -->

		</form>
	</div> <!-- /.portlet -->

<?php get_footer(); ?>
