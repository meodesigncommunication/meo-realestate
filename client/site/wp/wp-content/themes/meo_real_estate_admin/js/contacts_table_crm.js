var contact_type_extractor = function( data, type, extra ) {
									var contact_type = data["contact_type"];
									if (type !== "display") { // Leave unchanged for sorting
										return contact_type;
									}
									if (contact_type == "file" || contact_type == "contact" || contact_type == "alert" || contact_type == "day_info" || contact_type == "newsletter" || contact_type == "achat") {
										return '<div class="contact-type-icon contact-type-icon-' + contact_type + '" title="' + contact_type + '">&nbsp;</div>'
									}
									return '<div class="contact-type-icon contact-type-icon-manual" title="Manuel">&nbsp;</div>';
								};

var interactions_extractor = function( data, type, extra ) {
									var num_interactions = data['interactions'].length,
									    last_interaction = (num_interactions === 0 ? data["first_contact"] : data['interactions'][0]['date_added']);
									if (type !== "display") { // Leave unchanged for sorting
										return last_interaction;
									}
									else {
										return jQuery.timeago(last_interaction) + ' / '  + num_interactions;
									}
								};

var field_extractor = function(slug) {
								return function( data, type, extra ) {
									return data[slug];
								};
							};

var custom_field_extractor = function(slug) {
								return function( data, type, extra ) {
									var result = "";
									if (data['field_values'].hasOwnProperty(slug)) {
										result = data['field_values'][slug]['label'];
									}
									return result;
								};
							};

var array_extractor = function(slug) {
							return function( data, type, extra ) {
								var rows = data[slug];
								return rows.join('<br>');
							};
						};

var summary_links_extractor = function( data, type, extra ) {
									var contact_id = data['id'];
									var result = '<a href="' + meo_settings['contact_url'] + '?contact_id=' + contact_id + '"><i class="fa fa-info-circle"></i></a>' +
									             '<a href="' + meo_settings['contact_url'] + '?contact_id=' + contact_id + '&pdf=1"><i class="fa fa-file-pdf-o"></i></a>';
									if (can_delete_contacts) {
										result += '<a class="delete-contact" data-contact-id="' + contact_id + '" href="#"><i class="fa fa-trash-o"></i></a>';
									}
									if (can_add_app_users) {
										var site = meo_settings['sites'][meo_settings['site_id']];
										if (site['enabled_in_app']) {
											result += '<a class="add-app-user" data-contact-id="' + contact_id + '" href="#"><i class="fa fa-tablet"></i></a>';
										}
									}
									return result;
								};


jQuery(function() {
	if (meo_settings['site_id'] < 0) {
		return;
	}

	var site = meo_settings['sites'][meo_settings['site_id']],
	    request_url = meo_settings['ajax_url'];
	var table_1 = jQuery('#table-1').dataTable ({
		"sAjaxSource": request_url + "?action=get_crm_list&api-key=" + site['api_key'],
		"aoColumns": column_extractors,
		"iDisplayLength": -1, /* Show all rows */
		"fnInitComplete": function(oSettings, json) {
			jQuery(this).parents ('.dataTables_wrapper').find ('.dataTables_filter input').prop ('placeholder', meo_settings['search_placeholder']).addClass ('form-control input-sm');

			jQuery('.delete-contact').on('click', function(e) {
				e.preventDefault();
				var contact_id = jQuery(this).data('contact-id');
				deleteContact(contact_id);
			});

			jQuery('.add-app-user').on('click', function(e) {
				e.preventDefault();
				var contact_id = jQuery(this).data('contact-id');
				addAppUser(contact_id);
			});

		},
		"oLanguage": meo_settings['datatable_trans'],
		"aaSorting": [[0,'desc']]
	});

	function deleteContact(contact_id) {
		if (!confirm("Delete this contact?")) {
			return;
		}

		var url = meo_settings['ajax_url'],
		    site = meo_settings['sites'][meo_settings['site_id']],
		    data = {
				'action'     : 'delete_contact',
				'contact_id' : contact_id,
				'api-key'    : site['api_key']
			};

		jQuery.ajax({
			url: url,
			type: 'POST',
			data: data
		})
		.done(function(result) {
			if (result.success) {
				jQuery('#row_' + contact_id).slideUp();
			} else {
				alert(result.message);
			}
		})
		.fail(function() {
			alert('Unexpected error');
		});
	}

	function addAppUser(contact_id) {
		var site = meo_settings['sites'][meo_settings['site_id']];

		if (!confirm("Grant this user access to on " + site['name'] + " the app?")) {
			return;
		}

		var url = meo_settings['ajax_url'],
		    data = {
				'action'     : 'add_app_user',
				'contact_id' : contact_id,
				'api-key'    : site['api_key']
			};

		jQuery.ajax({
			url: url,
			type: 'POST',
			data: data
		})
		.done(function(result) {
			if (result.success) {
				alert('User added.  Password has been emailed to them.');
			} else {
				alert(result.message);
			}
		})
		.fail(function() {
			alert('Unexpected error');
		});
	}
});
