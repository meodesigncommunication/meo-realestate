<?php

class MeoRemoteLotHandler {

	public function __construct() {
		$this->addHooks();
	}

	public function addHooks() {
		add_action('wp_ajax_update_lot_list', array($this, 'synchroniseLots'));
		add_action('wp_ajax_nopriv_update_lot_list', array($this, 'mustBeLoggedIn'));

		add_action('wp_ajax_set_note', array($this, 'setNote'));
		add_action('wp_ajax_nopriv_set_note', array($this, 'mustBeLoggedIn'));
	}

	public function getRemoteLots($site_id) {
		global $wpdb;

		$result = array();

		$sites = mrea_get_sites_for_user();

		if (empty($sites[$site_id])) {
			return $result;
		}

		$rows = $wpdb->get_results( $wpdb->prepare( "select id, remote_lot_id, name, note from " . $wpdb->prefix . "meo_site_lots where site_id = %d order by name", $site_id ) );
		foreach ($rows as $row) {
			$result[$row->id] = (object) array(
				'remote_lot_id' => $row->remote_lot_id,
				'name'          => $row->name,
				'note'          => $row->note
			);
		}

		return $result;
	}

	public function mustBeLoggedIn() {
		header('Content-Type: application/json');
		echo json_encode(array(
			'success' => 0,
			'message' => __('You must be logged in', 'meo_mini_crm')
		));
		exit;
	}

	public function synchroniseLots() {
		global $wpdb;

		$site_id = (int) $_GET['site_id'];
		$sites = mrea_get_sites_for_user();

		header('Content-Type: application/json');

		if (empty($sites[$site_id]) or !$sites[$site_id]['uses_crm'] ) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Not permissioned (or site doesn't use MiniCRM)", 'meo_mini_crm')
			));
			exit;
		}

		$site = $sites[$site_id];

		$url = $site['site_url'] . $site['api_url'] .  '?' . http_build_query(array(
			'action'  => 'get_apartment_list',
			'api-key' => $site['api_key']
		));

		$curl = new WP_Http_Curl();
		$request_parameters = array(
			'headers' => array(),
			'method' => 'GET',
			'redirection' => 0
		);

		$response = $curl->request($url, $request_parameters);

		if ( is_wp_error( $response ) || empty( $response['body'] ) || (int )$response['response']['code'] < 200  || (int )$response['response']['code'] > 299 ) {
			echo json_encode(array(
				'success' => 0,
				'message' => __('Unknown Error', 'meo_mini_crm')
			));
			exit;
		}

		$lot_details = json_decode($response['body']);

		$remote_lots = array();
		foreach ($lot_details->aaData as $lot) {
			$remote_lots[(int) $lot->lot_id] = $lot->lot;
		}

		if (empty($remote_lots)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __('No lots found', 'meo_mini_crm')
			));
			exit;
		}

		// All arguments have been cast to integers, so we should be safe from SQL injection
		$delete = "delete from " . $wpdb->prefix . "meo_site_lots where site_id = " . $site_id . " and remote_lot_id not in ( " . join(", ", array_keys($remote_lots)) . " )";
		$wpdb->query($delete);

		foreach ($remote_lots as $remote_lot_id => $lot_name) {
			$result = $wpdb->query( $wpdb->prepare( "
				INSERT INTO " . $wpdb->prefix . "meo_site_lots (site_id, remote_lot_id, name)
				VALUES (%d, %d, %s)
				ON DUPLICATE KEY UPDATE name = VALUES(name)",
				$site_id,
				$remote_lot_id,
				$lot_name
			) );
		}

		echo json_encode(array(
			'success' => 1,
			'message' => __('Lots updated', 'meo_mini_crm')
		));
		exit;
	}

	public function setNote() {
		global $wpdb;

		$lot_id = (int) $_POST['lot_id'];
		$note = htmlentities(stripcslashes($_POST['value']));

		$site_id = mrea_get_site_id(false);
		$lots = $this->getRemoteLots($site_id);

		foreach ($lots as $row_id => $lot) {
			if ($lot->remote_lot_id == $lot_id) {
				$wpdb->update(
					$wpdb->prefix . "meo_site_lots",
					array( 'note' => $note ),
					array( 'id'   => $row_id ),
					array( '%s' ),
					array( '%d' )
				);
			}
		}

		echo $note;
		exit;
	}
}
