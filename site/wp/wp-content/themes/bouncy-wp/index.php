<?php get_header(); ?>
<div class="row">
<section class="blog nine columns">
  <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
  <?php
    // The following determines what the post format is and shows the correct file accordingly
    $format = get_post_format();
    if ($format) {
    get_template_part( 'inc/postformats/'.$format );
    } else {
    get_template_part( 'inc/postformats/standard' );
    }
  ?>
  <?php endwhile; ?>
      <?php theme_pagination(); ?>
  <?php else : ?>
    <p><?php _e( 'Please add posts from your WordPress admin page.', THB_THEME_NAME ); ?></p>
  <?php endif; ?> 
</section>
  <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>