<?php
/*
Template Name: Front Page - Flex Slider
*/
?>
<?php get_header(); ?>
<!-- Start Slider -->
<div class="flex slider flex-start">
	<ul class="slides">
	<?php query_posts('post_type=slide&orderby=menu_order&showposts=-1'); ?>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	 		<?php $image_id = get_post_thumbnail_id(); 
	 					$image_url = wp_get_attachment_image_src($image_id,'slider'); $image_url = $image_url[0];
	 					$content = get_post_meta($post->ID, 'slider_subtitle', TRUE);
	 					$btn_link = get_post_meta($post->ID, 'slider_btn_link', TRUE);
	 					$btn_text = get_post_meta($post->ID, 'slider_btn_text', TRUE);
	 					$btn_color = get_post_meta($post->ID, 'slider_btn_color', TRUE);
	 					$slider_title_color = get_post_meta($post->ID, 'slider_title_color', TRUE);
	 					$slider_subtitle_color = get_post_meta($post->ID, 'slider_subtitle_color', TRUE);
	 					$slide_align = get_post_meta($post->ID, 'slider_position', TRUE); 
	 					$slide_video = get_post_meta($post->ID, 'slider_video', TRUE); ?>
		<li style="background: transparent url('<?php echo $image_url; ?>') top center no-repeat;">
				
				<figure>
					<?php the_post_thumbnail('slider', array('class' => 'hidden')); ?>
					<figcaption class="flex-caption">
					<div class="row">
						<?php if ($slide_align == "slider-center") { ?>
							<div class="eight columns centered">
								<h2<?php if ($slider_title_color) { echo ' style="color:'.$slider_title_color.';"'; } ?>><?php the_title(); ?></h2>
								<?php if ($content) { echo '<h3 style="color:'.$slider_subtitle_color.';">'.$content.'</h3>'; } ?>
								<?php if ($btn_link) { echo '<a href="'.$btn_link.'" title="'.get_the_title().'" class="btn '.$btn_color.' slide_btn">'.$btn_text.'</a>'; } ?>
							</div>
						<?php } elseif ($slide_align == "slider-right") { ?>
							
								<div class="six columns">
									<div class="video">
										<?php if ($slide_video) { ?>
											<?php echo $slide_video; ?>
										<?php } ?>
									</div>
								</div>
							
							<div class="six columns <?php if ($slide_video) { ?>hide-for-small<?php } ?>  rightaligned">
								<h2<?php if ($slider_title_color) { echo ' style="color:'.$slider_title_color.';"'; } ?>><?php the_title(); ?></h2>
								<?php if ($content) { echo '<h3 style="color:'.$slider_subtitle_color.';">'.$content.'</h3>'; } ?>
								<?php if ($btn_link) { echo '<a href="'.$btn_link.'" title="'.get_the_title().'" class="btn '.$btn_color.' slide_btn">'.$btn_text.'</a>'; } ?>
							</div>
						<?php } else { ?>
							<div class="six columns <?php if ($slide_video) { ?>hide-for-small<?php } ?>">
								<h2<?php if ($slider_title_color) { echo ' style="color:'.$slider_title_color.';"'; } ?>><?php the_title(); ?></h2>
								<?php if ($content) { echo '<h3 style="color:'.$slider_subtitle_color.';">'.$content.'</h3>'; } ?>
								<?php if ($btn_link) { echo '<a href="'.$btn_link.'" title="'.get_the_title().'" class="btn '.$btn_color.' slide_btn">'.$btn_text.'</a>'; } ?>
							</div>
							<?php if ($slide_video) { ?>
								<div class="six columns">
									<div class="video">
										<?php echo $slide_video; ?>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					</figcaption>
				</figure>
		</li>
	<?php endwhile; else: ?>
		<li><?php _e( 'Please add slides from your WordPress admin page.', THB_THEME_NAME ); ?></li>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
	</ul>
</div>
<!-- End Slider -->
<div class="row">
	<section class="fullwidth twelve columns">
	  <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
	  	<article id="post-<?php the_ID(); ?>" class="post">
	  		<div class="post-content">
	      		<?php the_content(); ?>
	      	</div>
	    </article>
	  <?php endwhile; else : endif; ?> 
	</section>
</div>
<?php get_footer(); ?>