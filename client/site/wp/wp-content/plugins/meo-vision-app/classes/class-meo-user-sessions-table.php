<?php

// class-meo-user-sessions-table.php

if( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class MeoUserSessionsTable extends WP_List_Table {

	function __construct() {
		parent::__construct( array(
				'singular' => __( 'user session', MVA_TEXT_DOMAIN ),
				'plural'   => __( 'user sessions', MVA_TEXT_DOMAIN ),
				'ajax'     => false
		) );
	}

	function get_columns(){
		$columns = array(
			'cb'           => '<input type="checkbox" />',
			'session_id'   => __( 'Session ID',   MVA_TEXT_DOMAIN ),
			'user_id'      => __( 'User ID',      MVA_TEXT_DOMAIN ),
			'user_login'   => __( 'User',         MVA_TEXT_DOMAIN ),
			'session_date' => __( 'Session Date', MVA_TEXT_DOMAIN ),
			'session_key'  => __( 'Session Key',  MVA_TEXT_DOMAIN )
		);
		return $columns;
	}

	function get_sortable_columns() {
		$sortable_columns = array(
			'session_id'     => array('session_id',false)
		);
		return $sortable_columns;
	}


	function column_cb($item) {
		return sprintf(
			'<input type="checkbox" name="session_id[]" value="%s" />', $item['session_id']
		);
	}

	function column_session_id($item) {
		$nonce = wp_create_nonce( 'bulk-' . $this->_args['plural'] );

        $actions = array(
            'delete' => sprintf('<a href="?page=%s&action=delete&session_id=%d&_wpnonce=%s" onclick="return confirm(\'' . __('Are you sure?', MVA_TEXT_DOMAIN) . '\');" >' . __('Delete', MVA_TEXT_DOMAIN) . '</a>',$_REQUEST['page'], $item['session_id'], $nonce),
        );

        return sprintf('%1$s %2$s',
            $item['session_id'],
            $this->row_actions($actions)
        );
	}

	function column_default( $item, $column_name ) {
		global $meova;

		switch( $column_name ) {
			case 'user_id':
			case 'user_login':
			case 'session_key':
				return $item[ $column_name ];

			case 'session_date':
				$session_details = $meova->decode($item['session_key']);
				if ($session_details) {
					$d = DateTime::createFromFormat("YmdHis", $session_details['date']);
					return $d->format("Y-m-d H:i:s");
				}
				else {
					return 'Invalid Session Key';
				}

			default:
				return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
		}
	}

	function prepare_items() {
		global $wpdb;

		$per_page = 25;

		$this->process_bulk_action();

		$columns  = $this->get_columns();
		$hidden	  = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array( $columns, $hidden, $sortable );


		$sql = "
			select mus.id as session_id,
			       u.id as user_id,
			       u.user_login,
			       mus.session_key
			  from " . $wpdb->prefix . "users u
			       join " . $wpdb->prefix . "meo_user_sessions mus on u.id = mus.user_id
		";

		if (!empty($_REQUEST['orderby'])) {
			$sql .= " order by " . $_REQUEST['orderby'] . " " . (!empty($_REQUEST['order']) ? $_REQUEST['order'] : 'asc');
		}

		$data = $wpdb->get_results($sql, ARRAY_A );

		$current_page = $this->get_pagenum();
		$total_items = count($data);
		$this->items = array_slice($data,(($current_page-1)*$per_page),$per_page);

		$this->set_pagination_args( array(
			'total_items' => $total_items,
			'per_page'    => $per_page,
			'total_pages' => ceil($total_items/$per_page)
		) );
	}

	function get_bulk_actions() {
		$actions = array(
			'delete' => __( 'Delete', MVA_TEXT_DOMAIN )
		);
		return $actions;
	}

	function process_bulk_action() {
		$session_ids = ( is_array( $_REQUEST['session_id'] ) ) ? $_REQUEST['session_id'] : array( $_REQUEST['session_id'] );

		if ( 'delete' === $this->current_action() ) {
			global $wpdb;

			if ( ! wp_verify_nonce( $_REQUEST['_wpnonce'], 'bulk-' . $this->_args['plural'] ) ) {
				die( __( 'Not permissioned', MVA_TEXT_DOMAIN ) );
			}

			foreach ( $session_ids as $session_id ) {
				$sql = $wpdb->prepare( "DELETE FROM " . $wpdb->prefix . "meo_user_sessions WHERE id = %d", $session_id );
				$wpdb->query( $sql );
			}
		}
	}

	function no_items() {
		_e( 'No user sessions found.', MVA_TEXT_DOMAIN );
	}
}
