<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package MEO real estate admin
 */
?>

		</div><!-- .container -->
	</div><!-- .content -->

	<footer class="footer">
		<div class="container">
			<p class="pull-left"><?php _e('Copyright', 'meo_real_estate_admin'); ?> © 2013 MEO Real Estate.</p>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript"> 
    window.$ = jQuery;
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: {lat: 46.6136509, lng: 7.0592593}
        });   

        setMarkers(map);
    }
    function setMarkers(map)
    {
        $.ajax({
            url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
            data: {
                'action':'get_contacts_coordonnee',
                'site_id' : $('.site-selector select[name="site_id"]').val()
            },
            success:function(datas) { 
                var contacts = [];
                var count = 0;   
                
                //Parse mon JSON
                $.each($.parseJSON(datas), function(idx, obj) {
                    myData = [];
                    myData['title'] = obj.surname+" "+obj.first_name;
                    myData['latitude'] = obj.latitude;
                    myData['longitude'] = obj.longitude;
                    myData['zindex'] = count;                    
                    contacts.push(myData); 
                    count++;
                }); 
                for (var i = 0; i < contacts.length; i++) {
                    var contact = contacts[i];
                    var myLatLng = {lat: parseFloat(contact['latitude']), lng: parseFloat(contact['longitude'])};
                    var marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        title: contact['title']
                    });
                }
            },
            error: function(errorThrown){
                console.log(errorThrown);
            }
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhozibvh6-JYJanNI0h4JYVHropxiKUCQ&callback=initMap"></script>

</body>
</html>
