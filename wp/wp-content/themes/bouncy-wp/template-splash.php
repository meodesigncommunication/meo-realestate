<?php if(!is_404()){ ?>
	<?php if(ot_get_option('breadcrumbs') != 'no'){ ?>
		<!-- Start Breadcrumbs -->
		<div id="breadcrumb">
			<div class="row">
			  <div class="six columns breadcrumb">
			    <?php thb_breadcrumb(); ?>
			  </div>
			  <div class="six columns name hide-for-small">
			  	<?php if (single_post_title()) {
			    		single_post_title();
			    		} else {
			      		if(is_tag()){ echo __("Tag: ", THB_THEME_NAME) . single_tag_title('',FALSE); }
			      		if(is_search()){ echo __("Search results for", THB_THEME_NAME).' "'.get_search_query().'"'; }
			      		if(is_month()){ echo get_the_time('F, Y'); }
			      		if(is_category()){$category = get_the_category(); $ID = $category[0]->cat_ID; echo get_category_parents($ID, FALSE, ', ', FALSE ); }
			      		if(is_year()){ echo get_the_time('Y'); }
			    		}
			  	?>
			  </div>
			</div>
		</div>
		<!-- End Breadcrumbs -->
	<?php } ?>
<?php } ?>