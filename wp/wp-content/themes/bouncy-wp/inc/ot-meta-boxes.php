<?php
/**
 * Initialize the meta boxes. 
 */
add_action( 'admin_init', '_custom_meta_boxes' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types
 * in demo-theme-options.php.
 *
 * @return    void
 *
 * @access    private
 * @since     2.0
 */

function _custom_meta_boxes() {
  /**
   * Create a custom meta boxes array that we pass to 
   * the OptionTree Meta Box API Class.
   */
  $post_meta_box_video = array(
    
    'id'          => 'post_meta_video',
    'title'       => 'Video Settings',
    'pages'       => array( 'post' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => 'Video Embed Code',
        'id'          => 'post_video',
        'type'        => 'textarea-simple',
        'desc'        => 'Video Embed Code. Works only if this is a video post.',
        'std'         => '',
        'rows'        => '5'
      )
    )
  );
  $post_meta_box_quote = array(
    'id'          => 'post_meta_quote',
    'title'       => 'Quote Settings',
    'pages'       => array( 'post' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => 'Quote',
        'id'          => 'post_quote',
        'type'        => 'textarea-simple',
        'desc'        => 'Quote Text. Works only if this is a quote post.',
        'std'         => '',
        'rows'        => '3'
      ),
      array(
        'label'       => 'Quote Author',
        'id'          => 'post_quote_author',
        'type'        => 'text',
        'desc'        => 'Author of the quote',
        'std'         => '',
        'rows'        => '1'
      )
    )
  );
  
  $post_meta_box_link = array(
    'id'          => 'post_meta_link',
    'title'       => 'Link Settings',
    'pages'       => array( 'post' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => 'Link Text',
        'id'          => 'post_link_text',
        'type'        => 'text',
        'desc'        => 'Link Text. Works only if this is a link post.',
        'std'         => '',
        'rows'        => '1'
      ),
      array(
        'label'       => 'Link URL',
        'id'          => 'post_link_url',
        'type'        => 'text',
        'desc'        => 'Link URL. Works only if this is a link post.',
        'std'         => '',
        'rows'        => '1'
      )
    )
  );
  $slider_meta_box = array(
    'id'          => 'slider_meta',
    'title'       => 'Slide Subtitle & Button',
    'pages'       => array( 'slide' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
    	array(
    	  'label'       => 'Slider Image',
    	  'id'          => 'slider_img_desc',
    	  'type'        => 'textblock_titled',
    	  'desc'        => 'Using the Wordpress "Set Featured Image" link on the right, upload an image that is at least 1900px x 700px in size. We suggest that, you compress these images using the <a href="http://jpegmini.com/" target="_blank">JpegMini</a> service.'
    	),
    	array(
    	  'label'       => 'Slide Aligment',
    	  'id'          => 'slider_position',
    	  'type'        => 'radio-image',
    	  'desc'        => 'How should the content of this slide be aligned?',
    	  'std'         => 'slider-left'
    	),
    	array(
    	  'label'       => 'Slide Subtitle',
    	  'id'          => 'slider_subtitle',
    	  'type'        => 'textarea-simple',
    	  'desc'        => 'The subtitle visible below the slide title',
    	  'std'         => '',
    	  'rows'        => '2'
    	),
      array(
        'label'       => 'Button Link',
        'id'          => 'slider_btn_link',
        'type'        => 'text',
        'desc'        => 'Do you want a button for the slider? If so please enter the URL to link to.',
        'std'         => '',
        'rows'        => '1'
      ),
      array(
        'label'       => 'Button Text',
        'id'          => 'slider_btn_text',
        'type'        => 'text',
        'desc'        => 'Text for the slider button',
        'std'         => '',
        'rows'        => '1'
      ),
      array(
        'label'       => 'Video Embed Code',
        'id'          => 'slider_video',
        'type'        => 'textarea-simple',
        'desc'        => 'Embed code for the video inside this slide <small>Not visible on center aligned slides</small>',
        'rows'        => '5'
      )
    )
  );
  $slider_style_meta_box = array(
    
    'id'          => 'slider_style',
    'title'       => 'Slide Style',
    'pages'       => array( 'slide' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
    	array(
    	  'id'          => 'slider_btn_color',
    	  'label'       => 'Button Color',
    	  'type'        => 'select',
    	  'desc'        => 'Color of the slider button',
    	  'choices'     => array(
    	    array(
    	      'label'       => 'Red',
    	      'value'       => 'red'
    	    ),
    	    array(
    	      'label'       => 'Black',
    	      'value'       => 'black'
    	    ),
    	    array(
    	      'label'       => 'Blue',
    	      'value'       => 'blue'
    	    ),
    	    array(
    	      'label'       => 'Green',
    	      'value'       => 'green'
    	    ),
    	    array(
    	      'label'       => 'Purple',
    	      'value'       => 'purple'
    	    ),
    	    array(
    	      'label'       => 'Pink',
    	      'value'       => 'pink'
    	    ),
    	    array(
    	      'label'       => 'Grey',
    	      'value'       => 'grey'
    	    ),
    	    array(
    	      'label'       => 'Smoked',
    	      'value'       => 'smoked'
    	    ),
    	    array(
    	      'label'       => 'Brown',
    	      'value'       => 'brown'
    	    ),
    	    array(
    	      'label'       => 'Yellow',
    	      'value'       => 'yellow'
    	    ),
    	    array(
    	      'label'       => 'Dark Blue',
    	      'value'       => 'darkblue'
    	    ),
    	    array(
    	      'label'       => 'Bordeaux',
    	      'value'       => 'bordeaux'
    	    )
    	  ),
    	  'std'         => 'red'
    	),
    	array(
    	  'label'       => 'Slide Title Color',
    	  'id'          => 'slider_title_color',
    	  'type'        => 'colorpicker',
    	  'desc'        => 'Change the color of the slide title'
    	),
    	array(
    	  'label'       => 'Slide Sub-title color',
    	  'id'          => 'slider_subtitle_color',
    	  'type'        => 'colorpicker',
    	  'desc'        => 'Change the color of the slide subtitle'
    	)
    )
  );
  $page_meta_box_sidebar = array(
    'id'        => 'meta_box_sidebar',
    'title'     => 'Layout',
    'pages'     => array('page'),
    'context'   => 'side',
    'priority'  => 'high',
    'fields'    => array(
      array(
        'id'          => 'sidebar_set',
        'label'       => 'Sidebar',
        'type'        => 'sidebar_select'
        )
      )
    );
    
  $page_meta_box = array(

    'id'          => 'page_settings',
    'title'       => 'Page Settings',
    'pages'       => array( 'page' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
    	array(
    	  'id'          => 'portfolio_shapes',
    	  'label'       => 'Portfolio Shapes',
    	  'type'        => 'radio',
    	  'desc'        => 'If you are using the Portfolio - Shapes template, which shape would you like to use? <small>Uses the 4 column layout by default</small>',
    	  'choices'     => array(
    	    array(
    	      'label'       => 'Hexagon',
    	      'value'       => 'hexagon'
    	    ),
    	    array(
    	      'label'       => 'Circle',
    	      'value'       => 'circle'
    	    )
    	  ),
    	  'std'         => 'hexagon'
    	),
      array(
        'label'       => 'Portfolio Columns',
        'id'          => 'portfolio_columns',
        'type'        => 'radio-image',
        'desc'        => 'If this is a portfolio page, you can change the number of columns here. <br><small>Only works if you select the Portfolio template</small>',
        'std'         => 'three'
      ),
      array(
        'label'       => 'Portfolio Categories',
        'id'          => 'portfolio_categories',
        'type'        => 'taxonomy-checkbox',
        'desc'        => 'If this is a portfolio page, which portfolio categories would you like to display?<small>Select at least one</small>',
        'taxonomy'    => 'project-category'
      ),
      array(
        'label'       => 'Portfolio Items per Page',
        'id'          => 'portfolio_pagecount',
        'type'        => 'text',
        'desc'        => 'If this is a paginated portfolio, how man items would you like to show per page? <small>Enter -1 for unlimited</small>',
        'rows'        => '1',
        'std'					=> '-1'
      )
    )
  );
  $portfolio_meta_box = array(
    'id'          => 'portfolio_settings',
    'title'       => 'Portfolio Settings',
    'pages'       => array( 'portfolio' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => 'Header Background Color',
        'id'          => 'portfolio_header',
        'type'        => 'colorpicker',
        'desc'        => 'Background color of the header'
      ),
      array(
        'label'       => 'Portfolio Excerpt',
        'id'          => 'portfolio_excerpt',
        'type'        => 'text',
        'desc'        => 'Short text to show under the portfolio title',
        'rows'        => '1'
      ),
      array(
        'label'       => 'Portfolio Page',
        'id'          => 'portfolio_main',
        'type'        => 'portfolio-select',
        'desc'        => 'Main Portfolio Page for this portfolio item. This is useful if you have multiple portfolios and have different child portfolio items.'
      ),
      array(
        'id'          => 'portfolio_type',
        'label'       => 'Portfolio Type',
        'type'        => 'radio',
        'desc'        => 'What type of portfolio item is this? Always upload an image and set it as the featured image even if its a video portfolio.',
        'choices'     => array(
          array(
            'label'       => 'Standard',
            'value'       => 'standard'
          ),
          array(
            'label'       => 'Image',
            'value'       => 'image'
          ),
          array(
            'label'       => 'Gallery',
            'value'       => 'gallery'
          ),
          array(
            'label'       => 'Video',
            'value'       => 'video'
          )
        ),
        'std'         => 'standard'
      ),
      array(
        'label'       => 'Video Embed Code',
        'id'          => 'portfolio_video',
        'type'        => 'textarea-simple',
        'desc'        => 'Video Embed Code. Works only if this is a video portfolio item.',
        'std'         => '',
        'rows'        => '5'
      ),
      array(
        'label'       => 'Client',
        'id'          => 'portfolio_client',
        'type'        => 'text',
        'desc'        => 'Who was the client for this project?',
        'rows'        => '1'
      ),
      array(
        'label'       => 'Date',
        'id'          => 'portfolio_date',
        'type'        => 'text',
        'desc'        => 'Date the project is published',
        'rows'        => '1'
      ),
      array(
        'label'       => 'Services',
        'id'          => 'portfolio_services',
        'type'        => 'text',
        'desc'        => 'What kind of services did you provide in this project?',
        'rows'        => '1'
      ),
      array(
        'label'       => 'Link',
        'id'          => 'portfolio_link',
        'type'        => 'text',
        'desc'        => 'Link to the live project',
        'rows'        => '1'
      )
    )
  );
  /**
   * Register our meta boxes using the 
   * ot_register_meta_box() function.
   */
	ot_register_meta_box( $post_meta_box_video );
	ot_register_meta_box( $post_meta_box_quote );
	ot_register_meta_box( $post_meta_box_link );
  ot_register_meta_box( $slider_meta_box );
  ot_register_meta_box( $slider_style_meta_box );
  ot_register_meta_box( $page_meta_box );
  ot_register_meta_box( $page_meta_box_sidebar );
  ot_register_meta_box( $portfolio_meta_box );
  
  /**
   * Portfolio Select option type.
   *
   * See @ot_display_by_type to see the full list of available arguments.
   *
   * @param     array     An array of arguments.
   * @return    string
   *
   * @access    public
   * @since     2.0
   */
  if ( ! function_exists( 'ot_type_portfolio_select' ) ) {
    
    function ot_type_portfolio_select( $args = array() ) {
  
      /* turns arguments array into variables */
      extract( $args );
      
      /* verify a description */
      $has_desc = $field_desc ? true : false;
      
      /* format setting outer wrapper */
      echo '<div class="format-setting type-page-select ' . ( $has_desc ? 'has-desc' : 'no-desc' ) . '">';
        
        /* description */
        echo $has_desc ? '<div class="description">' . htmlspecialchars_decode( $field_desc ) . '</div>' : '';
        
        /* format setting inner wrapper */
        echo '<div class="format-setting-inner">';
        
          /* build page select */
          echo '<select name="' . esc_attr( $field_name ) . '" id="' . esc_attr( $field_id ) . '" class="option-tree-ui-select ' . $field_class . '">';
          
          /* query pages array */
          $query = new WP_Query( array( 'meta_query' => array(
                  array(
                      'key' => '_wp_page_template',
                      'value' => array('template-portfolio.php', 'template-portfolio-shapes.php', 'template-portfolio-paginated.php'),
                      'compare' => 'IN'
                  ),
              ), 'post_type' => array( 'page' ), 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'post_status' => 'any' ) );
          
          /* has pages */
          if ( $query->have_posts() ) {
            echo '<option value="">-- ' . __( 'Choose One', 'option-tree' ) . ' --</option>';
            while ( $query->have_posts() ) {
              $query->the_post();
              echo '<option value="' . esc_attr( get_the_ID() ) . '"' . selected( $field_value, get_the_ID(), false ) . '>' . esc_attr( get_the_title() ) . '</option>';
            }
          } else {
            echo '<option value="">' . __( 'No Pages Found', 'option-tree' ) . '</option>';
          }
          echo '</select>';
          
        echo '</div>';
  
      echo '</div>';
      
    }
    
  }
}