<?php

class MeoVisionAppTest extends WP_UnitTestCase {
	protected $meova;

	private $user_with_access    = 'valid@example.com';
	private $user_without_access = 'invalid@example.com';

	/* ---------------------------------------------------------------------------------------------- *
	 * Set up and tear down
	 * ---------------------------------------------------------------------------------------------- */

	public function setUp() {
		parent::setUp();

		global $meova;
		$this->meova = $meova;

		$meova->activate(); // Ensure tables exist
	}

	public function tearDown() {
		parent::tearDown();
	}

	/* ---------------------------------------------------------------------------------------------- *
	 * Test Cases
	 * ---------------------------------------------------------------------------------------------- */

	function testCreation() {
		$this->assertNotNull( $this->meova );
	}

	/**
	 * @depends testCreation
	 * @dataProvider encodedData
	 */
	function testEncode($encoded, $decoded) {
		$this->assertEquals($encoded, $this->meova->encode($decoded));
	}

	/**
	 * @depends testEncode
	 * @dataProvider encodedData
	 */
	function testDecode($encoded, $decoded) {
		$this->assertEquals($decoded, $this->meova->decode($encoded));
	}

	/**
	 * @depends testEncode
	 */
	function testDecodeAdditional() {
		// Tests that don't have the equivalent encode tests
		$tests = array(
			array(
				'encoded' => null,
				'decoded' => null
			),
			array(
				'encoded' => 'invalidstring',
				'decoded' => null
			)
		);

		foreach ($tests as $test) {
			$this->testDecode($test['encoded'], $test['decoded']);
		}
	}

	/**
	 * @depends testEncode
	 */
	function testValidateUser() {
		global $wpdb;

		$this->createTestUsers();

		// Test invalid login
		$result = $this->meova->validateUser('dummy', 'dummy');
		$this->assertTrue(is_array($result), 'Result of validateUser() should be an array');
		$this->assertArrayHasKey('success', $result, 'Result of validateUser() should have correct structure (missing success)');
		$this->assertEquals(0, $result['success'], 'validateUser() should fail for an invalid login');

		// Test valid login without appropriate permissions
		$result = $this->meova->validateUser($this->user_without_access, 'password');
		$this->assertTrue(is_array($result), 'Result of validateUser() should be an array');
		$this->assertArrayHasKey('success', $result, 'Result of validateUser() should have correct structure (missing success)');
		$this->assertEquals(0, $result['success'], 'validateUser() should fail for users with incorrect permissions');

		// Test valid login with appropriate permissions
		$result = $this->meova->validateUser($this->user_with_access, 'password');
		$this->assertTrue(is_array($result), 'Result of validateUser() should be an array');
		$this->assertArrayHasKey('success', $result, 'Result of validateUser() should have correct structure (missing success)');
		$this->assertEquals(1, $result['success'], 'validateUser() should succeed for users with incorrect permissions');
		$this->assertArrayHasKey('session_key', $result, 'Result of validateUser() should have correct structure (missing session_key)');

		// Check the session has been inserted into the database
		$sql = $wpdb->prepare("
			select count(*)
			  from " . $wpdb->prefix . "meo_user_sessions mus
			       join " . $wpdb->prefix . "users u on mus.user_id = u.ID
			 where u.user_login = %s
			   and mus.session_key = %s",
			$this->user_with_access,
			$result['session_key']);

		$rowcount = $wpdb->get_var($sql);
		$this->assertEquals(1, $rowcount, 'validateUser() should insert a session key into the database on successful login');
	}

	/**
	 * @depends testDecode
	 * @depends testEncode
	 */
	function testValidateKey() {
		global $wpdb;

		$this->createTestUsers();

		// Test missing user, date, or key
		$this->assertFalse( $this->meova->validateKey(null,   'date', 'key'), 'Validation should fail on missing user' );
		$this->assertFalse( $this->meova->validateKey('user', null,   'key'), 'Validation should fail on missing date'  );
		$this->assertFalse( $this->meova->validateKey('user', 'date', null ), 'Validation should fail on missing session key' );

		// Test invalid key
		$this->assertFalse( $this->meova->validateKey('user',   'date', 'key'), 'Validation should fail with undecodable session key' );

		// Decodable key, but with the wrong content
		$this->assertFalse( $this->meova->validateKey('user',   'date', $this->meova->encode(null)), 'Validation should fail with session key that decodes to null' );
		$this->assertFalse( $this->meova->validateKey('user',   'date', $this->meova->encode('dummy')), "Validation should fail with session key that doesn't decode to an array" );
		$this->assertFalse( $this->meova->validateKey('user',   'date', $this->meova->encode(array('dummy' => 'dummy'))), 'Validation should fail with session key that decodes to an array with the wrong structure' );

		// Valid key, but for the wrong user
		$this->assertFalse( $this->meova->validateKey('user',   'date', $this->meova->encode(array('user' => 'other', 'date' => 'date'))), 'Validation should fail on wrong user');

		// Valid key, but for the wrong date
		$this->assertFalse( $this->meova->validateKey('user',   'date', $this->meova->encode(array('user' => 'user', 'date' => 'other'))), 'Validation should fail on wrong date');

		// Valid key, but not in the database
		$this->assertFalse( $this->meova->validateKey('user',   'date', $this->meova->encode(array('user' => 'user', 'date' => 'date'))), 'Validation should fail on valid key missing from database');

		// Valid key, in the database, but user not permissioned (presumably revoked)
		$key = $this->meova->encode(array('user' => $this->user_without_access, 'date' => '20150501'));
		$this->insertTestSessionIntoDatabase($this->user_without_access, $key);
		$this->assertFalse( $this->meova->validateKey($this->user_without_access, '20150501', $key), "Validation should fail when key is valid and in database, but user isn't permissioned");

		// Valid key, in the database, and user permissioned
		$key = $this->meova->encode(array('user' => $this->user_with_access, 'date' => '20150501'));
		$this->insertTestSessionIntoDatabase($this->user_with_access, $key);
		$this->assertTrue( $this->meova->validateKey($this->user_with_access, '20150501', $key), "Validation should pass when key is valid, in database, and user is permissioned");
	}

	function testGetDevelopmentDataForApp() {
		// TODO
		$this->assertTrue( true );
	}


	/* ---------------------------------------------------------------------------------------------- *
	 * Data providers
	 * ---------------------------------------------------------------------------------------------- */

	// Valid encodes/decoded pairs.
	public function encodedData() {
		return array(
			array(
				'encoded' => 'hnFnq1ubAVl8ewNGwVWUnYBFDSBRaK2rHH4L/epPHjo=', // Valid string, but actually encoded null
				'decoded' => null
			),
			array(
				'encoded' => 'q99LZUUHksBMq4Lj0+3xB4g5+EUNF4SYSEzqNtAmMaw=',
				'decoded' => 1
			),
			array(
				'encoded' => 'RonHySi9fga5tEwBkqFiEAcPDyha+erUg+3OqftnKSg=',
				'decoded' => ''
			),
			array(
				'encoded' => 'WDISt85LDfwn1Gpk+k6OvskI6K10M/m9Az0vVTeUL28=',
				'decoded' => 'test'
			),
			array(
				'encoded' => 'LxqtQ1xaeyjs09GTD66ZZ/rNgzdmnxHdXZGRhobQLWQNNRR2bPbjINOSNw+hYCNHy85Yapk0ZP+eoO66cmDapw==',
				'decoded' => array('user' => 'test', 'date' => '20150501')
			)
		);
	}

	/* ---------------------------------------------------------------------------------------------- *
	 * Utilities
	 * ---------------------------------------------------------------------------------------------- */

	// Create test users with appropriate permissions
	private function createTestUsers() {
		// Create the App User role
		add_role('app_user', 'App User', array(
			'read' => true,
			'read_app_data' => true
		));


		// Create a user with the App User role
		$user = $this->factory->user->create_and_get(array(
			'user_login' => $this->user_with_access,
			'user_email' => $this->user_with_access
		));
		$user->set_role( 'app_user' );


		// Create a user without the App User role (contributor by default)
		$this->factory->user->create(array(
			'user_login' => $this->user_without_access,
			'user_email' => $this->user_without_access
		));
	}

	// Insert a user session into the database
	private function insertTestSessionIntoDatabase($email, $encoded_key) {
		global $wpdb;
		$sql = $wpdb->prepare("insert into " . $wpdb->prefix . "meo_user_sessions (user_id, session_key) select u.id, %s from " . $wpdb->prefix . "users u where u.user_login = %s", $encoded_key, $email);
		$wpdb->query($sql);
	}
}
