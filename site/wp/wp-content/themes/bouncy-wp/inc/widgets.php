<?php
// thb Tag Cloud Size
function tag_cloud_filter($args = array()) {
   $args['smallest'] = 11;
   $args['largest'] = 11;
   $args['unit'] = 'px';
   $args['format']= 'list';
   return $args;
}

add_filter('widget_tag_cloud_args', 'tag_cloud_filter', 90);

require_once("widgets/dribbble.php");
require_once("widgets/flickr.php");
require_once("widgets/post-formats.php");
require_once("widgets/subscribe.php");
require_once("widgets/popular-likes.php");
?>