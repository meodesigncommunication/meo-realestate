<?php

require_once(dirname(__FILE__) . '/class-abstract-app-development-dao.php');

class DevelopmentDaoRemote extends AbstractAppDevelopmentDao {
	protected $spinners;

	public function __construct($development) {
		$this->development = $development;
		$this->spinners = array();

		if (!defined('ID_ADJUSTMENT_FACTOR')) {
			define('ID_ADJUSTMENT_FACTOR', 1000000);
		}
	}

	public function getDevelopment() {
		$devdata = $this->getApartmentList();
		if (is_array($devdata) && array_key_exists('success', $devdata) && !$devdata['success']) {
			return array();
		}

		return $this->getDevelopmentData($devdata);
	}
	
	public function getDevelopment_v2meo() {
		
		//$return_temp = 'Landing getDevelopment_v2meo '."\n\n";
		//$time_start = microtime(true);
		$devdata = $this->getApartmentList_v2meo();
		//$time_elapsed_secs = microtime(true) - $time_start;
		//$return_temp .= ' End after '.$time_elapsed_secs."\n\n";
		//echo '!!!!!!!!!!!!!!!!!!!!!!!! '.$return_temp.' !!!!!!!!!!!!!!!!!!!!!!!!'."\n\n";
		if (is_array($devdata) && array_key_exists('success', $devdata) && !$devdata['success']) {
			return array();
		}

		return $this->getDevelopmentData_v2meo($devdata);
	}

	private function getApartmentList() {
		$url = $this->development['site_url'] . $this->development['api_url'];

		$curl = new WP_Http_Curl();
		$request_parameters = array(
			'headers' => array(),
			'method' => $method,
			'redirection' => 0,
			'timeout' => 60 // Though if it's not _much_ quicker than this there's a problem
		);

		$parameters = array(
			'action'  => 'get_app_development_data',
			'api-key' => $this->development['api_key']
		);

		$url .= '?' . http_build_query($parameters);

		$response = $curl->request($url, $request_parameters);

		if ( is_wp_error( $response ) || empty( $response['body'] ) || (int )$response['response']['code'] < 200  || (int )$response['response']['code'] > 299 ) {
			$result = array(
				'success' => 0,
				'message' => __('Unknown Error', 'meo_real_estate_admin')
			);
			return $result;
		}

		$result = $response['body'];

		return json_decode($result);
	}
	
	private function getApartmentList_v2meo() {
		$url = $this->development['site_url'] . $this->development['api_url'];

		$curl = new WP_Http_Curl();
		$request_parameters = array(
			'headers' => array(),
			'method' => 'GET', // wasn t defined, so, set to default value: GET ( was $method )
			'redirection' => 0,
			'timeout' => 60 // Though if it's not _much_ quicker than this there's a problem
		);

		$parameters = array(
			'action'  => 'get_app_development_data',
			'api-key' => $this->development['api_key']
		);

		$url .= '?' . http_build_query($parameters);

		$response = $curl->request($url, $request_parameters);

		if ( is_wp_error( $response ) || empty( $response['body'] ) || (int )$response['response']['code'] < 200  || (int )$response['response']['code'] > 299 ) {
			$result = array(
				'success' => 0,
				'message' => __('Unknown Error', 'meo_real_estate_admin')
			);
			return $result;
		}

		$result = $response['body'];

		return json_decode($result);
	}

	private function getDevelopmentData($devdata) {
		$result = array(
			'development' => $this->formatDevelopmentData($devdata)
		);

		return $result;
	}
	
	private function getDevelopmentData_v2meo($devdata) {
		//$return_temp = 'Landing getDevelopmentData_v2meo DAO Remote l.116'."\n\n";
		//$time_start = microtime(true);
		$result = array(
			'development' => $this->formatDevelopmentData_v2meo($devdata)
		);
		//$time_elapsed_secs = microtime(true) - $time_start;
		//$return_temp .= ' End after '.$time_elapsed_secs."\n\n";
		//return '!!!!!!!!!!!!!!!!!!!!!!!! '.$return_temp.' !!!!!!!!!!!!!!!!!!!!!!!!'."\n\n";
		return $result;
	}

	protected function formatLotData($devdata) {
		$result = array();

		if (!is_object($devdata) || !property_exists($devdata, 'lots') || empty($devdata->lots)) {
			return $result;
		}

		foreach ($devdata->lots as $source_lot) {
			$id = $this->getAdjustedId($source_lot->id);

			$all_floors = array();
			foreach ($source_lot->all_floors as $floor) {
				$all_floors[] = $this->getAdjustedId($floor);
			}

			$lot = array(
				'id'               => $id,
				'plan_id'          => $this->getAdjustedId($source_lot->plan_id),
				'floor_id'         => $this->getAdjustedId($source_lot->floor_id),
				'entry_id'         => $this->getAdjustedId($source_lot->entry_id),
				'surface_interior' => $source_lot->surface_interior,
				'surface_balcony'  => $source_lot->surface_balcony,
				'surface_terrace'  => $source_lot->surface_terrace,
				'surface_garden'   => $source_lot->surface_garden,
				'surface_weighted' => $source_lot->surface_weighted,
				'availability'     => $source_lot->availability,
				'pieces'           => $source_lot->pieces,
				'code'             => $source_lot->code,
				'name'             => $source_lot->name,
				'keyplan_image'    => $this->getImageWithMetadata($source_lot, 'keyplan_image'),
				'all_floors'       => $all_floors
			);

			$result[$id] = $lot;

		}

		return $result;
	}
	
	protected function formatLotData_v2meo($devdata) {
		$result = array();

		if (!is_object($devdata) || !property_exists($devdata, 'lots') || empty($devdata->lots)) {
			return $result;
		}

		foreach ($devdata->lots as $source_lot) {
			$id = $this->getAdjustedId($source_lot->id);

			$all_floors = array();
			foreach ($source_lot->all_floors as $floor) {
				$all_floors[] = $this->getAdjustedId($floor);
			}

			$lot = array(
				'id'               => $id,
				'plan_id'          => $this->getAdjustedId($source_lot->plan_id),
				'floor_id'         => $this->getAdjustedId($source_lot->floor_id),
				'entry_id'         => $this->getAdjustedId($source_lot->entry_id),
				'surf_interior' => $source_lot->surface_interior,
				'surf_balcony'  => $source_lot->surface_balcony,
				'surf_terrace'  => $source_lot->surface_terrace,
				'surf_garden'   => $source_lot->surface_garden,
				'surf_weighted' => $source_lot->surface_weighted,
				'availability'     => $source_lot->availability,
				'pieces'           => $source_lot->pieces,
				'code'             => $source_lot->code,
				'name'             => $source_lot->name,
				'keyplan_image'    => $this->getImageWithMetadata($source_lot, 'keyplan_image'),
				'all_floors'       => $all_floors
			);

			$result[$id] = $lot;

		}

		return $result;
	}

	protected function formatPlanData($devdata) {
		global $meova;

		$result = array();

		if (!is_object($devdata) || !property_exists($devdata, 'plans') || empty($devdata->plans)) {
			return $result;
		}

		// remote key => app key
		$image_keymap = array(
			'detailed_plan_image'  => 'detailed_plan_image',
			'image_3d'             => 'image_3d',
			'thumbnail_plan_image' => 'thumbnail_plan_image'
		);

		foreach ($devdata->plans as $source_plan) {
			$id = $this->getAdjustedId($source_plan->id);

			$plan = array(
				'id'               => $id,
				'code'             => $source_plan->code,
				'name'             => $source_plan->name,
				'feature_id'       => $this->getAdjustedId($source_plan->feature_id)
			);

			foreach($image_keymap as $remote_key => $app_key) {
				$image = $this->getImageWithMetadata($source_plan, $remote_key);

				if (!empty($image)) {
					$plan[$app_key] = $image;
				}
			}

			if (!empty($source_plan->spinner_3d)) {
				$raw_spinner = $source_plan->spinner_3d;
				$spinner_id = $this->getAdjustedId($raw_spinner->id);

				$spinner = array();
				foreach (array('width', 'height', 'clockwise', 'stretch') as $field) {
					$spinner[$field] = $raw_spinner->{$field};
				}

				$spinner['images'] = array();
				foreach ($raw_spinner->images as $image) {
					$spinner['images'][] = $meova->getMediaDetails($image);
				}

				$plan['spinner_3d'] = $spinner_id;
				$this->spinners[$spinner_id] = $spinner;
			}

			$result[$id] = $plan;
		}

		return $result;
	}

	protected function formatFloorData($devdata) {
		$result = array();

		if (!is_object($devdata) || !property_exists($devdata, 'floors') || empty($devdata->floors)) {
			return $result;
		}

		foreach ($devdata->floors as $source_floor) {
			$id = $this->getAdjustedId($source_floor->id);

			$keyplan_lot_coords = array();
			if (isset($source_floor->keyplan_lot_coords) && is_object($source_floor->keyplan_lot_coords)) {
				foreach ($source_floor->keyplan_lot_coords as $lot_id => $coords) {
					$keyplan_lot_coords[$this->getAdjustedId($lot_id)] = $coords;
				}
			}

			$plan = array(
				'id'               => $id,
				'code'             => $source_floor->code,
				'name'             => $source_floor->name,
				'order'            => $source_floor->order,
				'ordinal'          => $source_floor->ordinal,
				'building_id'      => $this->getAdjustedId($source_floor->building_id),
				'keyplan_lot_coords' => $keyplan_lot_coords
			);

			$result[$id] = $plan;
		}

		return $result;
	}


	protected function formatBuildingData($devdata) {
		$result = array();

		if (!is_object($devdata) || !property_exists($devdata, 'buildings') || empty($devdata->buildings)) {
			return $result;
		}

		foreach ($devdata->buildings as $source_building) {
			$id = $this->getAdjustedId($source_building->id);
			$name = preg_replace('/^(b(a|â)t(iment)?s? *|buildings? *|blds? *)/i', '', $source_building->name);

			$building = array(
				'id'               => $id,
				'code'             => $source_building->code,
				'name'             => $name,
				'development_id'   => $this->development['id'] // don't need to use getAdjustedId() because this is the local ID, and already unique
			);

			$result[$id] = $building;
		}

		return $result;
	}


	protected function formatEntryData($devdata) {
		$result = array();

		if (!is_object($devdata) || !property_exists($devdata, 'entries') || empty($devdata->entries)) {
			return $result;
		}

		foreach ($devdata->entries as $source_entry) {
			$id = $this->getAdjustedId($source_entry->id);

			$entry = array(
				'id'               => $id,
				'code'             => $source_entry->code,
				'name'             => $source_entry->name
			);

			$result[$id] = $entry;
		}

		return $result;
	}

	protected function formatFeatureData($devdata) {
		$result = array();

		if (!is_object($devdata) || !property_exists($devdata, 'features') || empty($devdata->features)) {
			return $result;
		}

		foreach ($devdata->features as $source_feature) {
			$id = $this->getAdjustedId($source_feature->id);

			$feature = array(
				'id'               => $id,
				'name'             => $source_feature->name,
				'content'          => $source_feature->content
			);

			$result[$id] = $feature;
		}

		return $result;
	}

	protected function formatSpinnerData($devdata) {
		// Populated in formatPlanData()
		return $this->spinners;
	}

	protected function getAdjustedId($incoming) {
		if (empty($incoming)) {
			return null;
		}
		return $this->development['id'] * ID_ADJUSTMENT_FACTOR + $incoming;
	}
}
