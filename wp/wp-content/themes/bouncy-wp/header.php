<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta http-equiv="cleartype" content="on">
	<meta name="HandheldFriendly" content="True">
	<?php if(ot_get_option('favicon')){ ?>
	<link rel="shortcut icon" href="<?php echo ot_get_option('favicon'); ?>">
	<?php } ?>
	<?php 
		/* Always have wp_head() just before the closing </head>
		 * tag of your theme, or you will break many plugins, which
		 * generally use this hook to add elements to <head> such
		 * as styles, scripts, and meta tags.
		 */
		wp_head(); 
	?>
<?php if (defined('IS_PRODUCTION') and IS_PRODUCTION) { ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51183558-1', 'meo-realestate.com');
  ga('send', 'pageview');

</script>
<?php } ?>
</head>
<?php 
	if(ot_get_option('boxed') == 'yes') { 
		$class[0] = 'boxed';
 	} else { $class[1] = ''; }
 	
	if(ot_get_option('fixed_header') != 'yes') { 
		$class[1] = 'notfixed';
 	} else { $class[1] = ''; }
?>
<body <?php body_class($class); ?> data-url="<?php echo home_url(); ?>">
<div id="wrapper">
<!-- Start Header and Nav -->
<header id="header" class="clearfix <?php if (ot_get_option('fixed_header') != 'no') { echo 'fixed'; } ?>">
<?php if (ot_get_option('logo')) { $logo = ot_get_option('logo'); } else { $logo = get_template_directory_uri(). '/assets/img/logo.png'; } ?>
<div class="row">
	<div class="twelve columns">
		<nav class="row">
			 <div class="four mobile-two columns logo">
		    	<?php if (ot_get_option('logo_text') == 'yes') { ?>
		    	<h1><a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1></li>
		    	<?php } else { ?>
		      <a href="<?php echo home_url(); ?>" <?php if(ot_get_option('logo_mobile')) { ?>class="hide-logo"<?php } ?>><img src="<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?>" /></a>
		      	<?php if(ot_get_option('logo_mobile')) { ?>
		      		<a href="<?php echo home_url(); ?>" class="show-logo"><img src="<?php echo ot_get_option('logo_mobile'); ?>" alt="<?php bloginfo('name'); ?>" /></a>
		      	<?php } ?>
		      <?php } ?>
		   </div>
		   <div class="eight mobile-two columns mobile-menu-toggle">
		   	<i class="icon-reorder" id="mobile-toggle"></i>
		   </div>
		   <div class="eight columns desktop-menu">
		   		<?php if(ot_get_option('headersearch') != 'no'){ ?>
					<div class="searchlink">
						<i class="icon-search"></i>
					</div>
					<?php } ?>
		    	<?php if(has_nav_menu('nav-menu')) { ?>
		        <?php wp_nav_menu( array( 'theme_location' => 'nav-menu', 'depth' => 3, 'container' => false, 'menu_class' => 'sf-menu') ); ?>
		      <?php } else { ?>
		      	<ul class="sf-menu">
		      		<li><a href="">No menu assigned!</a></li>
		      	</ul>
		      <?php } ?>
		      
		   </div>
		</nav>
	</div>
</div>
<!-- Start Mobile Menu -->
<div class="mobile-menu">
	<div class="row">
		<div class="twelve columns">
			<?php if(ot_get_option('headersearch') != 'no'){ ?>
				<?php get_search_form(); ?>
			<?php } ?>
			<?php if(has_nav_menu('nav-menu')) { ?>
			  <?php wp_nav_menu( array( 'theme_location' => 'nav-menu', 'depth' => 3, 'container' => false, 'menu_class' => 'open-mobile-menu') ); ?>
			<?php } else { ?>
				<ul class="sf-menu">
					<li><a href="">No menu assigned!</a></li>
				</ul>
			<?php } ?>
		</div>
	</div>
</div>
<!-- End Mobile Menu -->
</header>
<!-- End Header and Nav -->
<!-- Start Search -->
<aside class="headersearch fixed">
	<div class="row">
		<div class="twelve columns">
			<?php get_search_form(); ?>
		</div>
	</div>
</aside>
<!-- End Search -->
<!-- Start Content -->
<div role="main">
<?php if (!is_page_template('template-home-flex.php')) { get_template_part('template-splash'); }?>