-- to be run against development's database (eg parallele)

insert into wp_cf7dbplugin_submits
select *
  from (
        select x.submit_time
             , x.form_name
             , 'analytics_id'
             , 'ea43e5651209c62c' -- change this
             , 12
             , null
          from wp_cf7dbplugin_submits x
         where x.field_name = 'email'
           and submit_time in (
                select s.submit_time
                  from wp_cf7dbplugin_submits s
                 where s.field_value = 'laetitia.versel@gmail.com' -- change this
               )
           and submit_time not in (
                select submit_time
                  from wp_cf7dbplugin_submits s
                 where s.field_name = 'analytics_id'
               )
) q