<?php
/* Columns Shortcode */
function shortcode_column_row($atts, $content = null, $code) {
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));

	return '<div class="row">' .  do_shortcode($content) . '</div>';
}
function shortcode_column($atts, $content = null, $code) {

	if ($code == 'two-columns') {
		return '<div class="six columns">' .  do_shortcode($content) . '</div>';
	}
	if ($code == 'three-columns') {
		return '<div class="four columns">' .  do_shortcode($content) . '</div>';
	}
	if ($code == 'four-columns') {
		return '<div class="three columns">' .  do_shortcode($content) . '</div>';
	}
	if ($code == 'six-columns') {
		return '<div class="two columns">' .  do_shortcode($content) . '</div>';
	}
	if ($code == 'two-three-columns') {
		return '<div class="eight columns">' .  do_shortcode($content) . '</div>';
	}
	if ($code == 'three-four-columns') {
		return '<div class="nine columns">' .  do_shortcode($content) . '</div>';
	}
}
add_shortcode('columns', 'shortcode_column_row');
add_shortcode('two-columns', 'shortcode_column');
add_shortcode('three-columns', 'shortcode_column');
add_shortcode('four-columns', 'shortcode_column');
add_shortcode('six-columns', 'shortcode_column');
add_shortcode('two-three-columns', 'shortcode_column');
add_shortcode('three-four-columns', 'shortcode_column');

/* Inline Label Shortcodes */
function highlight($atts, $content = null ) {
    extract(shortcode_atts(array(
       	'color'      => 'red'
    ), $atts));
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));
	$out = '<span class="label '.$color.'">' .$content. '</span>';
	
    return $out;
}
add_shortcode('highlight', 'highlight');

/* CTA Shortcodes */
function panels( $atts, $content = null ) {
    extract(shortcode_atts(array(
       	'color'      => 'grey'
    ), $atts));
    $content = remove_invalid_tags($content, array('p'));
    $content = remove_invalid_tags($content, array('br'));
	$out = '<div class="panel radius '.$color.'">' .do_shortcode($content) . '</div>';
    return $out;
}
add_shortcode('cta', 'panels');

/* Notification Shortcodes */
function notification( $atts, $content = null ) {
    extract(shortcode_atts(array(
       	'type'      => 'success',
       	'title'			=> 'Success Message'
    ), $atts));
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));
	$icon = '';
	switch($type) {
	 case 'success':
	 	$icon = 'ok';
	 break;
	 case 'error':
	 	$icon = 'remove';
	 break;
	 case 'information':
	 	$icon = 'info-sign';
	 break;
	 case 'warning':
	 	$icon = 'warning-sign';
	 break;
	 case 'note':
	 	$icon = 'file-alt';
	 break;
	}
	$out = '<div class="notification-box '.$type.'"><div class="icon-holder"><i class="icon-'.$icon.'"></i></div><h6>'.$title.'</h6><div class="text cf"><p>' .$content. '</p></div><a href="" class="close">×</a></div>';
    return $out;
}
add_shortcode('notification', 'notification');


/* Pricing Column */
function pricing($atts, $content = null, $code) {
	extract(shortcode_atts(array(
	   	'columns'			=> '4'
	), $atts));
	switch ($columns) {
		case '2' :
			$column_class = 'two-cols';
			break;
		case '3' :
			$column_class = 'three-cols';
			break;
		case '4' :
			$column_class = 'four-cols';
			break;	
		case '5' :
			$column_class = 'five-cols';
			break;
	}

	return '<div class="pricing row '.$column_class.'">' .  do_shortcode($content) . '</div>';
}
function pricingcol( $atts, $content = null ) {
    extract(shortcode_atts(array(
    	'featured'		=> 'false',
       	'packagetitle'      => '',
       	'price'    => '',
       	'currency' => '$',
       	'currencytext'    => '',
       	'caption'    => '',
       	'buttontext'    => '',
       	'buttonlink'    => '',
       	'columns'			=> '4'
    ), $atts));
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));
	$feature = '';
	if ($featured == 'true') {
		$feature = 'featured';
	} else {
		$caption = '';
	}
	if ($columns == '4') {
		$col = 'three';
	} else if ($columns == '3') {
		$col = 'four';
	} else if ($columns == '2') {
		$col = 'six';
	} 
	
	$out = '<div class="columns">
						<div class="item '.$feature.'">
							<div class="header">
								<h2>' . $packagetitle . '';
								if ($caption) {
								$out .= '<span>'.$caption.'</span>';
								}
								$out .= '</h2>
							</div>
							<div class="price">
								<h4><span><b>'.$currency.'</b>' . $price . '</span>' . $currencytext . '</h4>
							</div>
							' .$content. '
							<div class="footer">
								<a href="' . $buttonlink . '" class="btn">' . $buttontext . '</a>
							</div>
						</div>
					</div>';
    return $out;
}
add_shortcode('pricing_table', 'pricing');
add_shortcode('pricing_column', 'pricingcol');

/* Image with Icon Link */
function images( $atts, $content = null, $code ) {
    extract(shortcode_atts(array(
       	'link'      => '#',
       	'lightbox'    => 'on',
       	'title'      => '',
       	'icon'      => 'icon-zoom-in'
    ), $atts));
    if ($lightbox == 'on') {
    	$rel = ' rel="prettyPhoto"';
    }
	$out = '<a href="'.$link.'" class="fresco" title="'.$title.'"'.$rel.'><img src="' .$content. '" /><div class="zoom"><i class="'.$icon.'"></i></div></a>';
    return $out;
}
add_shortcode('image', 'images');

/* Icon box */
function iconbox( $atts, $content = null, $code ) {
    extract(shortcode_atts(array(
       	'type'      => 'left',
       	'icon'      => 'zoom-in',
       	'heading'		=> 'Iconbox Heading'
    ), $atts));
		$content = remove_invalid_tags($content, array('p'));
		$content = remove_invalid_tags($content, array('br'));
		$out = '<div class="iconbox '.$type.'"><span><i class="icon-'.$icon.'"></i></span><div class="content"><strong>'.$heading.'</strong><div>'.do_shortcode($content).'</div></div></div>';
    return $out;
}
add_shortcode('iconbox', 'iconbox');

/* Blockquote */
function blockquotes( $atts, $content = null ) {
    extract(shortcode_atts(array(
       	'pull'      => '',
       	'author'    => ''
    ), $atts));
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));
	if ($author) {
		$authorhtml = '<cite>'. $author. '</cite>';
	}
	$out = '<blockquote class="'.$pull.'"><p>' .$content. $authorhtml. '</p></blockquote>';
    return $out;
}
add_shortcode('blockquote', 'blockquotes');

/* Buttons */
function buttons( $atts, $content = null ) {
    extract(shortcode_atts(array(
       	'color'      => '',
       	'link'       => '#',
       	'rounded'		 => '',
       	'size'			 => 'small',
       	'icon'			 => ''
    ), $atts));
	
	if($icon) { $content = '<i class="icon-'.$icon.'"></i> '.$content; }
	if($rounded == 'true') { $round = 'rounded'; }
	
	$out = '<a class="btn '.$color.' '.$round.' '.$size.'" href="'.$link.'">' .$content. '</a>';
  
  return $out;
}
add_shortcode('button', 'buttons');

/* Dial */
function dial( $atts, $content = null ) {
    extract(shortcode_atts(array(
       	'value'      => '50',
       	'width'			 => '170',
       	'center'		 => true
    ), $atts));
  wp_enqueue_script('dial');
  $content = remove_invalid_tags($content, array('p'));
  $content = remove_invalid_tags($content, array('br'));
  
  $out = '';
  if ($center) {
  	$out .= '<div style="text-align: center">';
  }
	$out .= '<figure class="knob" style="width:'.$width.'px; min-height:'.$width.'px;"><input class="dial" data-value="'.$value.'" data-width="'.$width.'" value="'.$value.'" data-linecap="round" /><strong>'.$content.'</strong></figure>';
	
	if ($center) {
		$out .= '</div>';
	}
  return $out;
}
add_shortcode('dial', 'dial');

/* Team Member */
function team_member( $atts, $content = null ) {
    extract(shortcode_atts(array(
       	'name'      => '',
       	'email'			 => '',
       	'job'		 => '',
       	'description'		 => '',
       	'image'		 => '',
    ), $atts));
  
  if($image!=''){
  	$output .= '<div class="fresco">';
  	$output .= '<img src="'.$image.'" />';
  	if($email!=''){
  		$output .= '<div class="overlay"><a href="mailto:'.$email.'" class="email" title="'.$name.'"><i class="icon-envelope"></i></a></div>';
  	}
  	$output .= '</div>';
  }
  $output .= '<h3>'.$name.'</h3>';
  $output .= '<h4>'.$job.'</h4>';
  $output .= '<p>'.$description.'</p>';
  $out = '<div class="team_member">'.$output.'</div>';
  return $out;
}
add_shortcode('team_member', 'team_member');

/* Icons */
function icons( $atts, $content = null ) {
    extract(shortcode_atts(array(
       	'type'      => 'ok',
       	'url'				=> '',
       	'box'				=> '',
       	'size'			=> 'icon-large'
    ), $atts));
 
		$out = '<i class="icon-'.$type.' '. $size.'"></i>';
  
  	if ($box) {
  		if ($type == 'facebook' || $type == 'twitter' || $type == 'google-plus' || $type == 'pinterest' || $type == 'linkedin') {
  			$class = $type;
  		}
  		$out = '<a href="'.$url.'" class="boxed-icon '.$class.'">'.$out.'</a>';
  	}
  	
  	return $out;
}
add_shortcode('icon', 'icons');

/* Clients */
function clients($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		'count'      => '6'
	), $atts));

	$output = '';
	if (!preg_match_all("/(.?)\[(client)\b(.*?)(?:(\/))?\](?:(.+?)\[\/client\])?(.?)/s", $content, $matches)) {
		return do_shortcode($content);
	} else {
		for($i = 0; $i < count($matches[0]); $i++) {
			$matches[3][$i] = shortcode_parse_atts($matches[3][$i]);
		}
		if ($count == '6') {
			$col = 'two';
		} else if ($count == '4') {
			$col = 'three';
		} else if ($count == '3') {
			$col = 'four';
		} else if ($count == '2') {
			$col = 'six';
		} else {
			$col = 'two';
		}
		$output .='<div class="row clientlist">';
		for($i = 0; $i < count($matches[0]); $i++) {
			$output .= '<div class="'.$col.' mobile-two columns"><div>';
			if ($matches[3][$i]['url'] != '') {
				$output .= '<a href="'.$matches[3][$i]["url"].'" target="_blank">';
			}
			$output .= '<img src="'.$matches[3][$i]["image"] .'" />';
			if ($matches[3][$i]['url'] != '') {
				$output .= '</a>';
			}
			$output .='</div></div>';
		}
		$output .='</div>';
		return $output;
	}
}
add_shortcode('clients', 'clients');

/* Icon Styled Lists */
function icon_list($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		'icon'      => 'ok'
	), $atts));
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));
	$output = '';
	if (!preg_match_all("/(.?)\[(item)\b(.*?)(?:(\/))?\](?:(.+?)\[\/item\])?(.?)/s", $content, $matches)) {
		return do_shortcode($content);
	} else {
		for($i = 0; $i < count($matches[0]); $i++) {
			$matches[3][$i] = shortcode_parse_atts($matches[3][$i]);
		}
		
		$output .='<ul class="iconlist">';
		for($i = 0; $i < count($matches[0]); $i++) {
			$output .= '<li><i class="icon-'.$icon.'"></i>' . do_shortcode(trim($matches[5][$i])) .'</li>';
		}
		$output .='</ul>';
		return $output;
	}
}
add_shortcode('icon-list', 'icon_list');

/* Tabs Shortcode */
function shortcode_tabs($atts, $content = null, $code) {
	extract(shortcode_atts(array(
	), $atts));
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));
	$output = '';
	if (!preg_match_all("/(.?)\[(tab)\b(.*?)(?:(\/))?\](?:(.+?)\[\/tab\])?(.?)/s", $content, $matches)) {
		return do_shortcode($content);
	} else {
		$numRand = rand('10','100');
		for($i = 0; $i < count($matches[0]); $i++) {
			$matches[3][$i] = shortcode_parse_atts($matches[3][$i]);
		}
		
		$output .='<dl class="tabs">';
		for($i = 0; $i < count($matches[0]); $i++) {
			if ($i == "0") {
			$output .= '<dd class="active"><a title="' . $matches[3][$i]['title'] .  '" href="#tab'.$numRand.'-'. $i . '">';
				if($matches[3][$i]['icon'] != '') {
					$output .= '<i class="icon-'.$matches[3][$i]["icon"].'"></i>';
				}
			$output .= $matches[3][$i]['title'] . '</a></dd>';
			} else {
			$output .= '<dd><a title="' . $matches[3][$i]['title'] .  '" href="#tab' .$numRand.'-'. $i . '">';
				if($matches[3][$i]['icon'] != '') {
					$output .= '<i class="icon-'.$matches[3][$i]["icon"].'"></i>';
				}
			$output .= $matches[3][$i]['title'] . '</a></dd>';
			}
			
		}
		$output .='</dl>';
		$output .='<ul class="tabs-content">';
		for($i = 0; $i < count($matches[0]); $i++) {
			if ($i == "0") {
			$output .= '<li id="tab' .$numRand.'-'. $i . 'Tab" class="active">' . do_shortcode(trim($matches[5][$i])) .'</li>';
			} else {
			$output .= '<li id="tab' .$numRand.'-'. $i . 'Tab" style="display:none;">' . do_shortcode(trim($matches[5][$i])) .'</li>';
			}
					}
		$output .='</ul>';
		return $output;
	}
}
add_shortcode('tabs', 'shortcode_tabs');

/* Testimonials Shortcode */
function testimonials($atts, $content = null, $code) {
	extract(shortcode_atts(array(
	), $atts));
	
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));
	$output = '';
	
	if (!preg_match_all("/(.?)\[(testimonial)\b(.*?)(?:(\/))?\](?:(.+?)\[\/testimonial\])?(.?)/s", $content, $matches)) {
		return do_shortcode($content);
	} else {
	
		$output = '<div class="testimonials flex" data-controls="false"><ul class="slides">';
		
		for($i = 0; $i < count($matches[0]); $i++) {
			$matches[3][$i] = shortcode_parse_atts($matches[3][$i]);
			$output .= '<li><blockquote><p>'.do_shortcode(trim($matches[5][$i])).'<cite>'.$matches[3][$i]['author'] .'</cite></p></blockquote></li>';
		}

		$output .= '</ul></div>';
		
		return $output;
	}
	
}
add_shortcode('testimonials', 'testimonials');

/* Carousel Shortcode */
function shortcode_carousel($atts) {
	extract(shortcode_atts(array(
		'count' => '4',
		'type' => 'posts',
		'autoplay' => 'true',
		'columns' => '3column',
		'blog_url' => '',
		'portfolio_url' => ''
	), $atts));
	wp_enqueue_script('carousel');
	global $post;
	if ($columns == '4column') {
		$thumbsize = 'onefourth';
		$col = '4';
	} else if ($columns == '3column') {
		$thumbsize = 'onethirds';
		$col = '3';
	} else if ($columns == '2column') {
		$thumbsize = 'onehalf';
		$col = '2';
	} 
	if ($type == 'posts') {
		$query = array(
			'showposts' => $count, 
			'nopaging' => 0, 
			'post_type'=>'post', 
			'post_status' => 'publish', 
			'ignore_sticky_posts' => 1, 
			'tax_query' => array(
					array(
			    'taxonomy' => 'post_format',
			    'field' => 'slug',
			    'terms' => array("post-format-link", "post-format-quote"),
			    'operator' => 'NOT IN'
			   )
			 )
		);
		$meta = get_the_category();
		$separator = ', ';
		if($meta){
			$categories = '';
			foreach($meta as $category) {
				$categories .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", THB_THEME_NAME ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
			}
			$meta = trim($categories, $separator);
		}
	} else if ($type == 'portfolio') {
		$query = array(
			'showposts' => $count,
			'post_type' => 'portfolio',
			'orderby'=>'menu_order',
			'post_status' => 'publish',
			'order'     => 'ASC',
			'posts_per_page' => '-1'
		);
		$meta = get_the_term_list($post->ID, 'project-category', '', ', ');
	}
	
	$r = new WP_Query($query);
	$rand = rand(0, 100);

	if ($r->have_posts()){
		global $post;
		
		$output = '<div class="carousel row" data-columns="'.$col.'" data-next="#next-'.$rand.'" data-prev="#prev-'.$rand.'">';
				while ($r->have_posts()){
					$r->the_post();
					
					// Meta
					if ($type == 'posts') {
						$format = get_post_format();
						$meta = get_the_category();
						$separator = ', ';
						if($meta){
							$categories = '';
							foreach($meta as $category) {
								$categories .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", THB_THEME_NAME ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
							}
						$meta = trim($categories, $separator);
						}
						
					} else if ($type == 'portfolio') {
						$meta = strip_tags( get_the_term_list( $post->ID, 'project-category', '', ', ', '' ) );
						$format = get_post_meta($post->ID, 'portfolio_type', true);
					}
					
					// Image URL
					$image_id = get_post_thumbnail_id();
					$image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0];
					 
					$output .= '<div class="post"><div class="post-gallery fresco">';
					
					$output .= get_the_post_thumbnail($post->ID,$thumbsize);
					$output .= '<div class="overlay">
												<a href="'.get_permalink().'" class="details" title="'.get_the_title().'"><i class="icon-share-alt"></i></a>
												<a href="'.$image_url.'" class="zoom" title="'.get_the_title().'" rel="magnific"><i class="icon-eye-open"></i></a>
											</div>';
					$output .= '</div>';
					$output .= '<div class="post-title"><h3><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3></div>';
					
					// Content
					if ($type == 'posts') {
						$content = get_the_excerpt();
					} else if ($type == 'portfolio') {
						$excerpt = get_post_meta($post->ID, 'portfolio_excerpt', TRUE); 
						if($excerpt != '') {
							$content = $excerpt;
						} else {
							$content = apply_filters('the_content',get_the_content());
						}
					}
					if ($columns == '4column') {
						$output .= '<div class="excerpts">'.ShortenText($content, '40').'</div>';
					} else if ($columns == '3column') {
						$output .= '<div class="excerpts">'.ShortenText($content, '80').'</div>';
					} else if ($columns == '2column') {
						$output .= '<div class="excerpts">'.ShortenText($content, '100').'</div>';
					}
					$output .= '<div class="meta">'.thb_printLikes(get_the_ID()).$meta.'</div>';
					$output .= '</div>';
				}
		$output .= '</div>';
		$output .= '<div class="carousel-controls">';
		$output .= '<a href="#" id="prev-'.$rand.'" class="smallarrow"><i class="icon-angle-left"></i></a>';
		
		if ($type == 'posts') {
			if ($blog_url) {
			$output .= '<a href="'.$blog_url.'" class="gotoportfolio"><i class="icon-th-large"></i> '.__( "View Blog", THB_THEME_NAME ). '</a>';
			}
		} else if ($type == 'portfolio') {
			if ($portfolio_url) {
			$output .= '<a href="'.$portfolio_url.'" class="gotoportfolio"><i class="icon-th-large"></i> '.__( "View Full Portfolio", THB_THEME_NAME ). '</a>';
			}
		}
		
		$output .= '<a href="#" id="next-'.$rand.'" class="smallarrow"><i class="icon-angle-right"></i></a>';
		$output .= '</div>';
		return $output;
	} 
	wp_reset_query();
	
}
add_shortcode('carousel', 'shortcode_carousel');

/* Blog Post Shortcode */
function shortcode_blog_posts($atts) {
	extract(shortcode_atts(array(
		'type' => '3column',
		'count' => '3'
	), $atts));
	
	$query = array(
		'showposts' => $count, 
		'nopaging' => 0, 
		'post_type'=>'post', 
		'post_status' => 'publish', 
		'ignore_sticky_posts' => 1, 
		'tax_query' => array(
				array(
		    'taxonomy' => 'post_format',
		    'field' => 'slug',
		    'terms' => array("post-format-link", "post-format-quote"),
		    'operator' => 'NOT IN'
		   )
		 )
	);
	
	$r = new WP_Query($query);

	if ($r->have_posts()){
		global $post; 
		$output = '<div class="posts '.$type.'">';
				$output .= '<div class="row">';
				while ($r->have_posts()){
					$r->the_post();
					
					if($type == '4column') {
						$columns = 'three';
						$thumbsize = 'onefourth';
					} else if($type == '3column') {
						$columns = 'four';
						$thumbsize = 'onethirds';
					} else if($type == '2column') {
						$columns = 'six';
						$thumbsize = 'onehalf';
					}
					
					$meta = get_the_category();
					$separator = ', ';
					if($meta){
						$categories = '';
						foreach($meta as $category) {
							$categories .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", THB_THEME_NAME ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
						}
					$meta = trim($categories, $separator);
					}
					
					$image_id = get_post_thumbnail_id();
					$image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0];
					
					$output .= '<div class="'.$columns.' columns post">';
					if($image!='false'){
						$output .= '<div class="post-gallery fresco">';
						$output .= get_the_post_thumbnail($post->ID,$thumbsize);
						$output .= '<div class="overlay">
													<a href="'.get_permalink().'" class="details" title="'.get_the_title().'"><i class="icon-share-alt"></i></a>
													<a href="'.$image_url.'" class="zoom" title="'.get_the_title().'" rel="magnific"><i class="icon-eye-open"></i></a>
												</div></div>';
					}
					$output .= '<div class="post-title"><h3><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3></div>';
					$output .= '<div class="meta">'.$meta. thb_printLikes(get_the_ID()).'</div>';
					$output .= '</div>';
				}
				$output .= '</div>';
			
		$output .= '</div>';
		return $output;
	} 
	wp_reset_query();
}
add_shortcode('blog_posts', 'shortcode_blog_posts');

/* Portfolio Post Shortcode */
function shortcode_portfolio_posts($atts) {
	extract(shortcode_atts(array(
		'type' => '3column',
		'count' => '3'
	), $atts));

	$query = array(
	   'post_type' => 'portfolio',
	   'orderby'=>'menu_order',
	   'order'     => 'ASC',
	   'showposts' => $count,
  	);
	$r = new WP_Query($query);

	if ($r->have_posts()){
		global $post; 
		$output = '<div class="posts '.$type.'">';
	
			$output .= '<div class="row">';
				while ($r->have_posts()){
					$r->the_post();
					
					if($type == '4column') {
						$columns = 'three';
						$thumbsize = 'onefourth';
					} else if($type == '3column') {
						$columns = 'four';
						$thumbsize = 'onethirds';
					} else if($type == '2column') {
						$columns = 'six';
						$thumbsize = 'onehalf';
					}
					
					$meta = get_the_term_list($post->ID, 'project-category', '', ', ');
					
					$image_id = get_post_thumbnail_id();
					$image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0];
					
					$output .= '<div class="'.$columns.' columns post">';
						$output .= '<div class="post-gallery fresco">';
						$output .= get_the_post_thumbnail($post->ID,$thumbsize);
						$output .= '<div class="overlay">
													<a href="'.get_permalink().'" class="details" title="'.get_the_title().'"><i class="icon-share-alt"></i></a>
													<a href="'.$image_url.'" class="zoom" title="'.get_the_title().'" rel="magnific"><i class="icon-eye-open"></i></a>
												</div></div>';
						$output .= '<div class="post-title"><h3><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3></div>';
						$output .= '<div class="meta">'.$meta. thb_printLikes(get_the_ID()).'</div>';
					$output .= '</div>';
				}
			$output .= '</div>';
		$output .= '</div>';
		return $output;
	} 
	wp_reset_query();
}
add_shortcode('portfolio_posts', 'shortcode_portfolio_posts');

/* Accordion Shortcode */
function shortcode_accordion($atts, $content, $code) {
	extract(shortcode_atts(array(
	), $atts));
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));
	$output = '';
	if (!preg_match_all("/(.?)\[(tab)\b(.*?)(?:(\/))?\](?:(.+?)\[\/tab\])?(.?)/s", $content, $matches)) {
		return do_shortcode($content);
	} else {
		$numRand = rand('10','100');
		for($i = 0; $i < count($matches[0]); $i++) {
			$matches[3][$i] = shortcode_parse_atts($matches[3][$i]);
		}
		
		for($i = 0; $i < count($matches[0]); $i++) {
			if ($i == "0") {
			$output .= '
			<li class="active">
				<div class="title">
					<h5>' . $matches[3][$i]['title'] .  '</h5>
				</div>
				<div class="content">
					' . do_shortcode(trim($matches[5][$i])) .'
				</div>
			</li>';
			} else {
			$output .= '
			<li>
				<div class="title">
					<h5>' . $matches[3][$i]['title'] .  '</h5>
				</div>
				<div class="content">
					' . do_shortcode(trim($matches[5][$i])) .'
				</div>
			</li>';
			}
			
		}
		
		$output ='<ul class="accordion" id="accordion-' .$numRand.'">' . $output . '</ul>';
		return $output;
	}
}
add_shortcode('accordion', 'shortcode_accordion');

/* Toggle Shortcode */
function shortcode_toggle( $atts, $content = null)
{
 extract(shortcode_atts(array(
        'title'      => '',
        ), $atts));
		$content = remove_invalid_tags($content, array('p'));
		$content = remove_invalid_tags($content, array('br'));
   return '<div class="toggle"><div class="title">'.$title.'</div><div class="inner">'.do_shortcode($content).'</div></div>';
}
add_shortcode('toggle', 'shortcode_toggle');


/* Video Shortcode */
function ff_video($atts, $content=null) {
	extract(shortcode_atts(array(
		'type'		=> 'youtube',
		'id'			=> '',
		'format'	=> 'widescreen'
	), $atts));
	$content = remove_invalid_tags($content, array('p'));
	$content = remove_invalid_tags($content, array('br'));
	
	if($type == "vimeo") { $return = '<iframe src="http://player.vimeo.com/video/'.$id.'" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'; }
	else if($type == "youtube") { $return = '<iframe src="http://www.youtube.com/embed/'.$id.'" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'; }
	else if($type == "dailymotion") { $return ='<iframe src="http://www.dailymotion.com/embed/video/'.$id.'?related=true&amp;chromeless=true&amp;theme=none&amp;foreground=%23F7FFFD&amp;highlight=%23FFC300&amp;background=%23171D1B&amp;iframe=1&amp;wmode=transparent"></iframe>'; }
	
	return '<div class="flex-video '.$format.'">'.$return.'</div>';
}
add_shortcode('video', 'ff_video');

/* Divider Shortcodes */
function divider( $atts, $content = null ) {
   return '<div class="divider cf"></div>';
}
add_shortcode('divider', 'divider');


/* Google Chart Shortcodes */
function theme_shortcode_chart( $atts ) {
	extract(shortcode_atts(array(
	    'data' => '',
	    'colors' => '',
		'size' => '720x300',
	    'bg' => 'bg,s,65432100',
	    'title' => '',
	    'labels' => '',
	    'advanced' => '',
	    'type' => 'pie'
	), $atts));
 
	switch ($type) {
		case 'line' :
			$charttype = 'lc'; break;
		case 'xyline' :
			$charttype = 'lxy'; break;
		case 'sparkline' :
			$charttype = 'ls'; break;
		case 'meter' :
			$charttype = 'gom'; break;
		case 'scatter' :
			$charttype = 's'; break;
		case 'venn' :
			$charttype = 'v'; break;
		case 'pie' :
			$charttype = 'p3'; break;
		case 'pie2d' :
			$charttype = 'p'; break;
		default :
			$charttype = $type;
		break;
	}
  $string = ''; 
	if ($title) $string .= '&chtt='.$title.'';
	if ($labels) $string .= '&chl='.$labels.'';
	if ($colors) $string .= '&chco='.$colors.'';
	$string .= '&chs='.$size.'';
	$string .= '&chd=t:'.$data.'';
	$string .= '&chf='.$bg.'';
 
	return '<img title="'.$title.'" src="http://chart.apis.google.com/chart?cht='.$charttype.''.$string.$advanced.'" alt="'.$title.'" />';
}
add_shortcode('chart', 'theme_shortcode_chart');


/* Google Map Shortcodes */
function theme_shortcode_googlemap($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		"width" => false,
		"height" => '400',
		"address" => '',
		"latitude" => 0,
		"longitude" => 0,
		"zoom" => 8,
		"html" => '',
		"controls" => 'true',
		"maptype" => 'TERRAIN', // 'HYBRID', 'SATELLITE', 'ROADMAP' or 'TERRAIN'
		"marker" => 'true',
		'align' => false,
	), $atts));
	if($width && is_numeric($width)){
		$width = 'width:'.$width.'px;';
	}else{
		$width = '';
	}
	if($height && is_numeric($height)){
		$height = 'height:'.$height.'px';
	}else{
		$height = '';
	}
	
	$align = $align?' align'.$align:'';
	$id = rand(100,1000);
	wp_enqueue_script('gmapdep');
	wp_enqueue_script('gmap');
	if($marker != 'false'){
		return <<<HTML
<div id="google_map_{$id}" class="google_map" style="{$width}{$height}"></div>
<script type="text/javascript">
jQuery(document).ready(function($) {
		jQuery("#google_map_{$id}").gMap({
			latitude: {$latitude},
			longitude: {$longitude},
			maptype: '{$maptype}', // 'HYBRID', 'SATELLITE', 'ROADMAP' or 'TERRAIN'
			zoom: {$zoom},
			markers: [
				{
					latitude: {$latitude},
					longitude: {$longitude},
					address: "{$address}",
					popup: true,
					html: "{$html}"
				}
			],
			controls: {
				panControl: true,
				zoomControl: {$controls},
				mapTypeControl: {$controls},
				scaleControl: {$controls},
				streetViewControl: false,
				overviewMapControl: false
			}
		});
});
</script>
HTML;
	}else{
return <<<HTML
<div id="google_map_{$id}" class="google_map{$align}" style="{$width}{$height}"></div>
<script type="text/javascript">
jQuery(document).ready(function($) {
	var tabs = jQuery("#google_map_{$id}").parents('.tabs_container,.mini_tabs_container,.accordion');
	jQuery("#google_map_{$id}").bind('initGmap',function(){
		jQuery("#google_map_{$id}").gMap({
			zoom: {$zoom},
			latitude: {$latitude},
			longitude: {$longitude},
			address: "{$address}",
			controls: {$controls},
			maptype: {$maptype},
			scrollwheel:{$scrollwheel}
		});
	});
});
</script>
HTML;
	}
}

add_shortcode('gmap','theme_shortcode_googlemap');

?>