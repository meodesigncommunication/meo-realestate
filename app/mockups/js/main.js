window.addEventListener('load', function() {
  FastClick.attach(document.body);
}, false);

(function($){
	$(function() {
		var standard_width = $('.pane--left').width(),
		    full_width = standard_width + $('.pane--right').width();

		$('.lot a.hide-details').click(function(e) {
			e.preventDefault();

			$('.pane--left').stop(true, false).animate(
				{ width: full_width },
				500,
				'swing',
				function () {
					$('body').addClass('full');
				}
			);
		});
		$('.lot a.show-details').click(function(e) {
			e.preventDefault();
			$('body').removeClass('full');
			$('.pane--left').stop(true, false).animate(
				{ width: standard_width },
				500
			);
		});
	});
})(jQuery);
