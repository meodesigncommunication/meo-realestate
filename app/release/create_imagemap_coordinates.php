<?php

// execute from the command line:
// php --php-ini d:/conf/php/php.ini E:/Development/meo-realestate/app/release/create_imagemap_coordinates.php


define('GILAMONT_REPO', 'E:/Development/pierre-etoile/gilamont');

require_once GILAMONT_REPO . "/site/wp/wp-content/plugins/meo-real-estate-developments/classes/svglib/svglib.php";

// Force WordPress to use local config
$_SERVER['SERVER_ADDR'] = "127.0.0.1";

define( 'SHORTINIT', false ); // false to load plugin code - mred_get_svg_element_points()
global $wpdb;
require(GILAMONT_REPO . "/site/wp/wp-load.php");

define('IMAGE_WIDTH',  500);
define('IMAGE_HEIGHT', 200);

// define('TARGET_DIRECTORY', 'E:/Development/meo-realestate/app/release/out/keyplans/');

$lots = $wpdb->get_results( "
		select f.id as floor_id
		     , pf.meta_value as plan_file
		     , l.id as lot_id
		     , if (ifnull(sv.meta_value, '') = '', ps.meta_value, sv.meta_value) as svg_id
		  from wp_postmeta pm
		       join wp_posts f on pm.post_id = f.id
			   join wp_postmeta pf on pm.meta_value = pf.post_id
		       join wp_p2p p2pf on f.id = p2pf.p2p_to and p2pf.p2p_type = 'lots_to_floors'
		       join wp_posts l on p2pf.p2p_from = l.id
		       left join wp_postmeta sv on l.id = sv.post_id and sv.meta_key = 'svg_id'
		       join wp_p2p p2pp on l.id = p2pp.p2p_from and p2pp.p2p_type = 'lots_to_plans'
		       join wp_posts p on p2pp.p2p_to = p.id
		       left join wp_postmeta ps on p.ID = ps.post_id and ps.meta_key = 'svg_id'
		 where f.post_type = 'floor'
		   and pm.meta_key = 'plan'
		   and pf.meta_key = '_wp_attached_file'
		   and l.post_type = 'lot'
		order by f.id");

$result = array();

foreach ($lots as $lot) {
	if (!array_key_exists($lot->floor_id, $result)) {
		$result[$lot->floor_id] = array();
	}
	$svg_file = WP_CONTENT_DIR . '/uploads/' . $lot->plan_file;
	$svg_obj = SVGDocument::getInstance( $svg_file );

	$lot_element = $svg_obj->getElementById( $lot->svg_id );
	$unscaled_plan_points = mred_get_svg_element_points($lot_element);

	$svg_dimension = split(' ', $svg_obj->getAttribute('viewBox'));

	$left = $svg_dimension[0];
	$top = $svg_dimension[1];

	$width = $svg_dimension[2] - $left;
	$height = $svg_dimension[3] - $top;

	$plan_points = array();
	foreach ($unscaled_plan_points as $point) {
		$new_point = array(
			'x' => intval(($point['x'] - $left) / $width * IMAGE_WIDTH),
			'y' => intval(($point['y'] - $top) / $height * IMAGE_HEIGHT)
		);

		$plan_points[] = $new_point;
	}

	$result[$lot->floor_id][$lot->lot_id] = $plan_points;
}

foreach ($result as $floor_id => $floor) {
	echo $floor_id . " : {\n";
	foreach ($floor as $lot_id => $coords) {
		echo '  "' . $lot_id . '": "';
		$coord_string = '';
		foreach ($coords as $pair) {
			$coord_string .= $pair['x'] . ',' . $pair['y'] . ',';
		}
		echo trim($coord_string, ',');
		echo '",' . "\n";
	}
	echo "}\n\n";
}
