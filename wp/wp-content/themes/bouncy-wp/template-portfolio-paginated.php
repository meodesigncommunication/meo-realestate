<?php
/*
Template Name: Portfolio - Paginated
*/
?>
<?php get_header(); ?>
<div class="row">
<section class="portfolio-container twelve columns">
	<div class="row">
		<?php $activevar = get_query_var('project-category'); ?>
		<?php $mainpage = get_permalink();?>
		<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
		<?php $itemlimit 	=	get_post_meta($post->ID, 'portfolio_pagecount', TRUE); ?>
		<?php $cols = get_post_meta($post->ID, 'portfolio_columns', TRUE); ?>
		<?php $catorg = get_post_meta($post->ID, 'portfolio_categories', TRUE); ?>
		<?php $cat = implode(',', $catorg); ?>
		<div class="twelve columns">
			<ul class="filters hide-for-small">
				<?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; ?>
				<li><span><?php _e( 'Sort By:', THB_THEME_NAME ); ?></span></li>
			  <li><a href="<?php echo add_query_arg(array ( 'project-category' => ''));?>" <?php if ( $activevar == "") { echo 'class="active"'; } ?>><?php _e( 'show all', THB_THEME_NAME ); ?></a></li>
			  <?php 
				$portfolio_categories = get_categories(array('taxonomy'=>'project-category', 'include' => $cat));
				foreach($portfolio_categories as $portfolio_category) {
					$thelink = remove_query_arg( 'paged', $mainpage );
					$thelink = add_query_arg(array ('project-category' => $portfolio_category->category_nicename), $thelink); 
				?>
					<li><a href="<?php echo $thelink; ?>" title="<?php echo $portfolio_category->name; ?>" <?php if ( $activevar == $portfolio_category->category_nicename) { echo 'class="active"'; } ?>><?php echo $portfolio_category->name ?></a></li>
					
				<?php if ( $activevar == $portfolio_category->category_nicename) {$catorg = $portfolio_category->term_id;} ?>
				<?php } ?>
			</ul>
			<div id="portfolioselect" class="show-for-small">
				<a href="#" id="sortportfolio"><?php _e( 'Sort By:', THB_THEME_NAME ); ?></a>
				<ul>
				   <li><a href="<?php echo add_query_arg(array ( 'project-category' => ''));?>" class="<?php if ( $activevar == "") { echo 'active'; } ?>"><?php echo __('All', THB_THEME_NAME); ?></a></li>
			     <?php 
			     $portfolio_categories = get_categories(array('taxonomy'=>'project-category', 'include' => $cat));
			     foreach ($portfolio_categories as $category){ ?>
			       <?php $thelink = remove_query_arg( 'paged', $mainpage ); ?> 
			       <?php $thelink = add_query_arg(array ('project-category' => $category->category_nicename), $thelink); ?>
			       <li><a href="<?php echo $thelink; ?>" title="<?php echo $category->name;?>" class="<?php if ( $activevar == $category->category_nicename) { echo 'active'; } ?>"><?php echo $category->name;?></a></li>
			       <?php if ( $activevar == $category->category_nicename) {
			       	$catorg = $category->term_id;
			       	} ?>
			     <?php } ?>
				</ul>
			</div>
		</div>
		<?php endwhile; else : endif; ?> 
	</div>
	<?php if ($cols == 'three') {
					$columns = '4';
				} else if ($cols == 'four') {
					$columns = '3';
				} else if ($cols == 'six') {
					$columns = '2';
				} ?>	
	<div class="thbportfolio paginated row" data-columns="<?php echo $columns; ?>">
		<?php $args = array(
	    	   'post_type' => 'portfolio',
	    	   'orderby'=>'menu_order',
	    	   'order'     => 'ASC',
	    	   'posts_per_page' => $itemlimit,
	    	   'paged' => $paged,
	    	   'skill-type' => get_query_var('project-category'),
	    	   'tax_query' => array(
	    	   		array(
		           'taxonomy' => 'project-category',
		           'field' => 'id',
		           'terms' => $catorg,
		           'operator' => 'IN'
	    	      )
	    	    ) // end of tax_query
    	  	);
		?>
		<?php $query = new WP_Query($args); ?>
            <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                  $terms = get_the_terms( get_the_ID(), 'project-category' );  ?>
            <div class="item <?php echo $cols; ?> columns <?php foreach ($terms as $term) { echo strtolower($term->slug). ' '; } ?>">
	            <article id="post-<?php the_ID(); ?>" class="post">
                <?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
                <div class="post-gallery fresco">
                    	<?php if ($cols == 'three') { ?>
                    		<?php the_post_thumbnail('onefourth'); ?>
                    	<?php } else if ($cols == 'four') { ?>
                    		<?php the_post_thumbnail('onethirds'); ?>
                    	<?php } else if ($cols == 'six') { ?>
                    		<?php the_post_thumbnail('onehalf'); ?>
                    	<?php } ?>
                    	<?php $image_id = get_post_thumbnail_id();
                    				$image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0]; ?>
                    	<div class="overlay">
                    		<a href="<?php the_permalink(); ?>" class="details"><i class="icon-share-alt"></i></a>
                    		<a href="<?php echo $image_url; ?>" class="zoom" rel="magnific" title="<?php the_title(); ?>"><i class="icon-eye-open"></i></a>
                    	</div>
                </div>
                <?php } ?>
                <div class="post-title cf">
                	<h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                	<?php echo thb_printLikes(get_the_ID()); ?>
                </div> 
	            </article>
            </div>
            <?php endwhile; endif; ?>
        <?php wp_reset_query(); ?>
	</div>
	
	<?php theme_pagination($query->max_num_pages,1,true); ?>
</section>
</div>
<?php get_footer(); ?>