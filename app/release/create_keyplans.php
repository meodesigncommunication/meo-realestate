<?php

// execute from the command line:
// php --php-ini d:/conf/php/php.ini E:/Development/meo-realestate/app/release/create_keyplans.php


define('GILAMONT_REPO', 'E:/Development/pierre-etoile/gilamont');

require_once GILAMONT_REPO . "/site/wp/wp-content/plugins/meo-real-estate-developments/classes/svglib/svglib.php";

// Force WordPress to use local config
$_SERVER['SERVER_ADDR'] = "127.0.0.1";

define( 'SHORTINIT', true );
global $wpdb;
require(GILAMONT_REPO . "/site/wp/wp-load.php");

define('IMAGE_WIDTH',  500);
define('IMAGE_HEIGHT', 200);

define('COLOUR_HIGHLIGHT',     '#cfaf64');

define('TARGET_DIRECTORY', 'E:/Development/meo-realestate/app/release/out/keyplans/');

$lots = $wpdb->get_results( "select p.ID as lot_id,
       p.post_title as lot_name,
       p.post_name as lot_slug,
       av.meta_value as availability,
       if (ifnull(sv.meta_value, '') = '', ps.meta_value, sv.meta_value) as svg_id,
       pl.ID as plan_id,
       pl.post_title as plan_name,
       pl.post_name as plan_slug,
       fl.ID as floor_id,
       substring(fl.post_title,LOCATE('<!--:fr-->', fl.post_title) + 10,LOCATE('<!--:-->', fl.post_title, LOCATE('<!--:fr-->', fl.post_title)) - ( LOCATE('<!--:fr-->', fl.post_title) + 10 )) as floor_name,
       fl.post_name as floor_slug,
       b.ID as building_id,
       b.post_title as building_name,
       b.post_name as building_slug
  from wp_posts p
       left join wp_postmeta av on p.id = av.post_id and av.meta_key = 'availability'
       left join wp_postmeta sv on p.id = sv.post_id and sv.meta_key = 'svg_id'
       left join wp_p2p p2pp on p.id = p2pp.p2p_from and p2pp.p2p_type = 'lots_to_plans'
       left join wp_posts pl on p2pp.p2p_to = pl.ID
       left join wp_postmeta ps on pl.ID = ps.post_id and ps.meta_key = 'svg_id'
       left join wp_p2p p2pf on p.id = p2pf.p2p_from and p2pf.p2p_type = 'lots_to_floors'
       left join wp_posts fl on p2pf.p2p_to = fl.ID
       left join wp_p2p p2pb on fl.id = p2pb.p2p_from and p2pb.p2p_type = 'floors_to_buildings'
       left join wp_posts b on p2pb.p2p_to = b.ID
 where p.post_type = 'lot'
   and p.post_status = 'publish'
order by b.ID,
         fl.ID,
         p.ID" );

$raw_files = $wpdb->get_results( "select p.ID as floor_id , substring(p.post_title,LOCATE('<!--:fr-->', p.post_title) + 10,LOCATE('<!--:-->', p.post_title, LOCATE('<!--:fr-->', p.post_title)) - ( LOCATE('<!--:fr-->', p.post_title) + 10 )) as floor_name, am.meta_value as plan_file , o.meta_value as sort_order from wp_posts p  left join wp_postmeta pm on p.id = pm.post_id and pm.meta_key = 'plan' left join wp_postmeta am on pm.meta_value = am.post_id left join wp_postmeta o on p.id = o.post_id and o.meta_key = 'order' where p.post_type = 'floor' and p.post_status = 'publish' order by o.meta_value" );

$files = array();
foreach ($raw_files as $raw_file) {
	$files[$raw_file->floor_id] = $raw_file;
}

function svg_to_file($svg, $name) {
	$fh = fopen(TARGET_DIRECTORY . $name, 'w') or die("can't open file: " . TARGET_DIRECTORY . $name);
	fwrite($fh,  $svg->asXML(null, false));
	fclose($fh);
}

function svg_to_png($svg, $name) {
	$xml = $svg->asXML(null, false);

	$imageMagick = new Imagick();
	$imageMagick->setBackgroundColor(new ImagickPixel('transparent'));
	$imageMagick->readImageBlob($xml);
	$imageMagick->setImageFormat("png24");
	$imageMagick->resizeImage(IMAGE_WIDTH, IMAGE_HEIGHT, imagick::FILTER_LANCZOS, 1);

	$imageMagick->writeImage(TARGET_DIRECTORY . $name);

	$imageMagick->clear();
	$imageMagick->destroy();
}

foreach ($lots as $lot) {
	echo "Processing " . $lot->lot_name . "\n";

	$file = WP_CONTENT_DIR . '/uploads/' . $files[$lot->floor_id]->plan_file;

	$svg = SVGDocument::getInstance( $file );

	$lot_element = $svg->getElementById( $lot->svg_id );
	$lot_element->setAttribute("fill",   COLOUR_HIGHLIGHT);

	svg_to_file($svg, "svg/lot-situation-" . $lot->svg_id . ".svg");
	svg_to_png($svg, "png/lot-situation-" . $lot->svg_id . ".png");
}
