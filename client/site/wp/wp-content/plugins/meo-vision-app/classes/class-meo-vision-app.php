<?php
/*
Copyright (C) MEO design et communication S�rl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

require_once(dirname( __FILE__ ) . '/class-meo-media-utils.php');
require_once(dirname( __FILE__ ) . '/class-meo-gallery-utils.php');

if (!defined('ID_ADJUSTMENT_FACTOR')) {
	define('ID_ADJUSTMENT_FACTOR', 1000000);
}


class MeoVisionApp {

	const DB_VERSION = '1.2.0';

	const ENCRYPTION_HASH = 'b84ca89f934a064a5478f7a35e933b26';

	private $media_utils = null;
	private $gallery_utils = null;

	// DAOs corresponding to development data_location field.
	private $daos = array(
		'local' => array(
			'file' => 'class-development-dao-local.php',
			'class' => 'DevelopmentDaoLocal'
		),
		'remote' => array(
			'file' => 'class-development-dao-remote.php',
			'class' => 'DevelopmentDaoRemote'
		)
	);

	public function __construct() {
		$this->addHooks();
		$this->media_utils = new MeoMediaUtils();
		$this->gallery_utils = new MeoGalleryUtils($this->media_utils);
	}

	public static function activate() {
		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		// For this table, we don't need utf8mb4, and using it will make the index on session_key too many bytes
		$charset_collate = "DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";

		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_user_sessions (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			user_id bigint(20) unsigned NOT NULL,
			session_key varchar(255) NOT NULL,
			PRIMARY KEY (id),
			UNIQUE KEY session_key (session_key),
			INDEX user (user_id)
		) $charset_collate;";

		dbDelta( $sql );

		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_log (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			app_name varchar(255) NOT NULL,
			app_version varchar(255) NOT NULL,
			message varchar(255) NOT NULL,
			script varchar(255),
			line_number varchar(255),
			page varchar(255),
			client_timestamp varchar(40),
			platform varchar(40),
			user_name varchar(60),
			PRIMARY KEY (id)
		) $charset_collate;";

		dbDelta( $sql );

		$sql = "CREATE TABLE " . $wpdb->prefix . "meo_user_connections (
			id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			user_id bigint(20) unsigned NOT NULL,
			connection_date datetime NULL,
			PRIMARY KEY (id),
			INDEX user (user_id)
		) $charset_collate;";

		dbDelta( $sql );


		add_option( 'mva_scheduler_db_version', self::DB_VERSION );
	}

	public static function deactivate() {
		// Do nothing at this stage.  Leave stub for future expansion
	}

	public function addHooks() {
		add_action( 'admin_menu', array($this, 'addUserSessionsMenu') );
	}

	public function validateUser($username, $password) {
		global $wpdb;

		$date = date('YmdHis');
		$creds = array(
			'user_login'    => $username,
			'user_password' => $password,
			'remember'      => false
		);

		$user = @wp_signon( $creds, false );
		$success = !is_wp_error($user) && user_can($user, 'read_app_data');

		$result = array(
			'success' => $success ? 1 : 0,
			'date'    => $date
		);

		if ($success) {
			$result['session_key'] = $this->encode(array(
				'user' => $user->user_login,
				'date' => $date
			));

			$wpdb->insert(
				$wpdb->prefix . "meo_user_sessions",
				array(
					'user_id'     => $user->ID,
					'session_key' => $result['session_key']
				),
				array(
					'%d',
					'%s'
				)
			);

			$this->updateConnectionCounter($user->ID);
		}

		return $result;
	}

	public function getDevelopmentData($development) {
		$data_location = $development['data_location'];

		if (!array_key_exists($data_location, $this->daos)) {
			$data_location = 'remote';
		}

		$dao_details = $this->daos[$data_location];

		require_once(dirname( __FILE__ ) . '/dao/' . $dao_details['file']);

		$dao = new $dao_details['class']($development);

		$development_data = $dao->getDevelopment();

		$gallery_data = $this->gallery_utils->getGalleryForDevelopment($development['id']);
		if (!empty($gallery_data)) {
			// Reuse the gallery data object to return the results
			$result = $gallery_data;

			// Move the gallery itself into the development data where it belongs
			$development_data['development'][$development['id']]['gallery'] = $result['gallery'];
			unset($result['gallery']);

			// Add the development data to the result object
			$result['development_data'] = $development_data;
		}
		else {
			$result = array(
				'development_data' => $development_data
			);
		}
		return $result;
	}
	
	public function getDevelopmentData_v2meo($development) {
		$data_location = $development['data_location'];

		if (!array_key_exists($data_location, $this->daos)) {
			$data_location = 'remote';
		}

		$dao_details = $this->daos[$data_location];

		require_once(dirname( __FILE__ ) . '/dao/' . $dao_details['file']);

		$dao = new $dao_details['class']($development);

		//$time_start = microtime(true);
		$development_data = $dao->getDevelopment_v2meo();
		// echo "\n\n".'[getDevelopmentData_v2meo meo-vision-app l. 194] ---------------- '.$development_data.' ------------------'."\n\n";
		//$time_elapsed_secs = microtime(true) - $time_start;
		//echo "\n"."\n".'Time for getDevelopment_v2meo: '.$time_elapsed_secs."\n"."\n";
		
		//$time_start = microtime(true);
		$gallery_data = $this->gallery_utils->getGalleryForDevelopment($development['id']);
		//$time_elapsed_secs = microtime(true) - $time_start;
		//echo "\n"."\n".'Time for getGalleryForDevelopment: '.$time_elapsed_secs."\n"."\n";
		
		
		if (!empty($gallery_data)) {
			// Reuse the gallery data object to return the results
			$result = $gallery_data;

			// Move the gallery itself into the development data where it belongs
			$development_data['development'][$development['id']]['gallery'] = $result['gallery'];
			unset($result['gallery']);

			// Add the development data to the result object
			$result['development_data'] = $development_data;
		}
		else {
			$result = array(
				'development_data' => $development_data
			);
		}
		return $result;
	}


	public function validateKey($user, $date, $key, $log = false) {
		if (empty($user) || empty($date) || empty($key)) {
			return false;
		}

		$session_creation_data = $this->decode($key);
		if ($session_creation_data == null || !is_array($session_creation_data) || !array_key_exists('user', $session_creation_data) || !array_key_exists('date', $session_creation_data)) {
			// Key is not in valid format
			return false;

		}

		$success = false;
		if ($session_creation_data['user'] == $user && $session_creation_data['date'] == $date) { // Key is in valid format.

			// Check session is still in the database.
			$user_id = $this->getUserIdForSessionKey($user, $key);

			if ($user_id) {
				// Session key is valid.  Check user still has read_app_data permissions
				$wp_user = get_user_by( 'id', $user_id );
				if ($wp_user) {
					$success = user_can( $wp_user, 'read_app_data' );

					if ($success && $log) {
						$this->updateConnectionCounter($user_id);
					}
				}
			}
		}

		return $success;
	}


	public function encode($data) {
		return base64_encode( mcrypt_encrypt(MCRYPT_RIJNDAEL_256, self::ENCRYPTION_HASH, serialize($data), MCRYPT_MODE_ECB) );
	}

	public function decode($encrypted_data) {
		$result = null;

		try {
			$crypttext = base64_decode( $encrypted_data );
			$decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, self::ENCRYPTION_HASH, $crypttext, MCRYPT_MODE_ECB);
			$serialized = rtrim($decrypted, "\0");
			$result = unserialize ( $serialized );
		}
		catch (Exception $e) {
			$result = null;
		}

		return $result;
	}

	public function addUserSessionsMenu() {
		add_menu_page( 'App User Sessions', 'App User Sessions', 'read_user_sessions', 'user_sessions', array( $this, 'renderUserSessions' ), 'dashicons-admin-users' );
	}

	public function renderUserSessions() {
		require_once(dirname( __FILE__ ) . '/class-meo-user-sessions-table.php');
		$userSessionsTable = new MeoUserSessionsTable();
		$userSessionsTable->prepare_items();

		?>

		<div class="wrap">

			<div id="icon-users" class="icon32"><br/></div>
			<h2><?php _e('User Sessions', MVA_TEXT_DOMAIN); ?></h2>

			<form id="user-sessions-filter" method="get">
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
				<?php $userSessionsTable->display(); ?>
			</form>

		</div><?php
	}

	public function getMediaDetails($raw_url, $additional_properties = array(), $local_url = false) {
		return $this->media_utils->getMediaDetails($raw_url, $additional_properties, $local_url);
	}

	private function getUserIdForSessionKey($user, $key) {
		global $wpdb;

		// wp_meo_user_sessions.session_key has a unique index, so this query will return at most one row
		return $wpdb->get_var($wpdb->prepare("
			select u.id
			  from " . $wpdb->prefix . "users u
			       join " . $wpdb->prefix . "meo_user_sessions mus on u.id = mus.user_id
			 where u.user_login = %s
			   and mus.session_key = %s
			 ", $user, $key));
	}

	public function getGalleryForDevelopment($development_id) {
		return $this->gallery_utils->getGalleryForDevelopment($development_id);
	}

	public function logError($data) {
		global $wpdb;

		$result = $wpdb->insert(
			$wpdb->prefix . "meo_log",
			array(
				'app_name'         => $data['app_name'],
				'app_version'      => $data['app_version'],
				'message'          => $data['msg'],
				'script'           => $data['url'],
				'line_number'      => $data['line'],
				'page'             => $data['window'],
				'client_timestamp' => $data['timestamp'],
				'platform'         => $data['platform'],
				'user_name'        => $data['user']
			),
			array(
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s'
			)
		);

		return $result;
	}

	private function updateConnectionCounter($user_id) {
		global $wpdb;

		$wpdb->insert(
			$wpdb->prefix . "meo_user_connections",
			array(
				'user_id' => $user_id,
				'connection_date' => current_time('mysql', 1)
			),
			array(
				'%d',
				'%s'
			)
		);
	}

	public function summarizeUserConnections() {
		global $wpdb;

		$rows = $wpdb->get_results(
			" select user_id
			       , count(*) as total_connections
			       , max(connection_date) as last_connection
			    from " . $wpdb->prefix . "meo_user_connections
			group by user_id");

		$result = array();

		if (!empty($rows)) {
			foreach ($rows as $row) {
				$result[$row->user_id] = array(
					'total_connections' => $row->total_connections,
					'last_connection'   => $row->last_connection
				);
			}
		}

		return $result;
	}
}
