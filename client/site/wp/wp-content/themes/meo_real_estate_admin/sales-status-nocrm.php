<?php

$site_id = mrea_get_site_id();
$update_prices = get_field('update_prices', $site_id);
get_header(); ?>

<div class="portlet">
	<?php if (current_user_can( 'read_sales' )) { ?>
		<?php mrea_get_site_selector_form(); ?>
	<?php } ?>

	<h3 class="portlet-title">
		<u><?php _e( 'Sales Status', 'meo_real_estate_admin' ); ?></u>
	</h3>

	<div class="portlet-body">

		<?php if (current_user_can( 'read_sales' ) and $site_id > 0 ) { ?>

			<table class="table table-striped table-bordered" id="table-1">
				<thead>
					<tr>
						<th><?php _e( 'Lot type', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Building', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Floor', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Lot', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Rooms', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Surface', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Availability', 'meo_real_estate_admin' ); ?></th>
						<?php if($update_prices) { ?>
						<th><?php _e( 'Price', 'meo_real_estate_admin' ); ?></th>
						<?php } ?>
					</tr>
				</thead>

				<tfoot>
					<tr>
						<th><?php _e( 'Lot type', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Building', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Floor', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Lot', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Rooms', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Surface', 'meo_real_estate_admin' ); ?></th>
						<th><?php _e( 'Availability', 'meo_real_estate_admin' ); ?></th>
						<?php if($update_prices) { ?>
						<th><?php _e( 'Price', 'meo_real_estate_admin' ); ?></th>
						<?php } ?>
					</tr>
				</tfoot>
			</table>

		<?php }
		else {
			_e( 'You do not have sufficient permissions to view sales.', 'meo_real_estate_admin' );
		} ?>

	</div> <!-- /.portlet-body -->

</div> <!-- /.portlet -->

<?php get_footer(); ?>
