<?php

class MeoContactHandler {

	private $customFieldHandler = null;
	private $remoteLotHandler = null;

	public function __construct($customFieldHandler, $remoteLotHandler) {
		$this->customFieldHandler = $customFieldHandler;
		$this->remoteLotHandler = $remoteLotHandler;
		$this->addHooks();
	}

	public function addHooks() {
		add_action('wp_ajax_get_crm_list',        array($this, 'getContactList'));
		add_action('wp_ajax_nopriv_get_crm_list', array($this, 'mustBeLoggedIn'));

		add_action('wp_ajax_update_contact_custom_field', array($this, 'updateContactCustomField') );
		add_action('wp_ajax_nopriv_update_contact_custom_field', array($this, 'mustBeLoggedIn'));

		add_action('wp_ajax_update_contact_linked_lots', array($this, 'updateContactLinkedLots') );
		add_action('wp_ajax_nopriv_update_contact_linked_lots', array($this, 'mustBeLoggedIn'));

		add_action('wp_ajax_add_new_interaction', array($this, 'addNewInteraction') );
		add_action('wp_ajax_nopriv_add_new_interaction', array($this, 'mustBeLoggedIn'));
		
		/* Exception Estavayer */
		add_action('wp_ajax_add_new_relationstate', array($this, 'addNewRelationstate') );
		add_action('wp_ajax_nopriv_add_new_relationstate', array($this, 'mustBeLoggedIn'));
		/* /Exception */

		add_action('wp_ajax_save_contact', array($this, 'saveContact') );
		add_action('wp_ajax_nopriv_save_contact', array($this, 'mustBeLoggedIn'));

		add_action('wp_ajax_delete_contact', array($this, 'deleteContact') );
		add_action('wp_ajax_nopriv_delete_contact', array($this, 'mustBeLoggedIn'));

		add_action('wp_ajax_add_app_user', array($this, 'makeContactAppUser') );
		add_action('wp_ajax_nopriv_add_app_user', array($this, 'mustBeLoggedIn'));

		add_action('wp_ajax_add_app_user_by_email', array($this, 'makeAppUserFromEmail') );
		add_action('wp_ajax_nopriv_add_app_user_by_email', array($this, 'mustBeLoggedIn'));

		add_action('wp_ajax_get_app_users_for_site', array($this, 'getUsersForSite') );
		add_action('wp_ajax_nopriv_get_app_users_for_site', array($this, 'mustBeLoggedIn'));

		add_action('wp_ajax_revoke_user_access_for_site', array($this, 'revokeUserAccessForSite') );
		add_action('wp_ajax_nopriv_revoke_user_access_for_site', array($this, 'mustBeLoggedIn'));



		add_action( 'show_user_profile', array($this, 'showUserFields') );
		add_action( 'edit_user_profile', array($this, 'showUserFields') );

		add_action( 'personal_options_update',  array($this, 'saveUserDetails') );
		add_action( 'edit_user_profile_update', array($this, 'saveUserDetails') );

	}

	public function getContactList() {
		$site = mrea_get_site_by_api_key($_GET['api-key']);
                
		$site_id = $site['id'];

		$sites = mrea_get_sites_for_user();

		header('Content-Type: application/json');

		$result = array(
			"id" => -1,
			"fieldErrors" => array(),
			"sError" => "",
			"aaData" => array()
		);

		if (empty($sites[$site_id]) or !$sites[$site_id]['uses_crm'] ) {
			$result["sError"] = __('Not permissioned', 'meo_mini_crm');
			echo json_encode($result);
			exit;
		}

		$this->updateLatestDataFromSite($sites[$site_id]);

		$contacts = $this->getContactDetails($sites[$site_id]);

		$result['aaData'] = array_values($contacts); // reindex from 0.  Required by the receiving javascript

		echo json_encode($result);

		exit;
	}

	public function mustBeLoggedIn() {
		header('Content-Type: application/json');
		echo json_encode(array(
			'success' => 0,
			'message' => __('You must be logged in', 'meo_mini_crm')
		));
		exit;
	}

	private function updateLatestDataFromSite($site) {                
                global $wpdb;

		if (empty($site['api_key'])) {
			// Standalone CRM - doesn't fetch data from another site
			return;
		}
                
		// This code assumes the local data is the correct version, as it could have been updated
		// by the user.  As such, it only inserts missing data - hence the use of the ignore keyword
		// on the inserts

		$url = $site['site_url'] . $site['api_url'] . "?action=get_contact_list&api-key=" . $site['api_key'];

		$curl = new WP_Http_Curl();
		$request_parameters = array(
			'headers' => array(),
			'method' => 'GET',
			'redirection' => 0,
			'timeout' => 60
		);

		$response = $curl->request($url, $request_parameters);

		if ( is_wp_error( $response ) || empty( $response['body'] ) || (int )$response['response']['code'] < 200  || (int )$response['response']['code'] > 299 ) {
			if (function_exists('mrea_report_error')) {
				mrea_report_error(__FILE__, __LINE__, array(
					'site_id'      => empty($site) ? '' : $site['id'],
					'site_name'    => empty($site) ? '' : $site['name'],
					'function'     => 'updateLatestDataFromSite',
					'url'          => $url,
					'parameters'   => $request_parameters,
					'response'     => $response
				));
			}
			return;
		}

		$contact_details = json_decode($response['body']);

		foreach ($contact_details->aaData as $contact) {
                    
			$contact_type = 'manual';
                        
			if ($contact->contact_type == 'file' || $contact->contact_type == 'contact' || $contact->contact_type == 'alert'  || $contact->contact_type == 'day_info' || $contact->contact_type == 'newsletter' || $contact->contact_type == 'achat') 
                        {
				$contact_type = $contact->contact_type;
			}
                        
                        // Test pour le champ courtier 
                        
                        // End test
                        
			$result = $wpdb->query( $wpdb->prepare(
				"insert ignore into " . $wpdb->prefix . "meo_contacts (client_id, site_id, surname, first_name, contact_type, email, phone, address, postcode, city, country, language, date_added)
				values (%d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
				$site['client_id'],
				$site['id'],
				$contact->surname,
				$contact->first_name,
				$contact_type,
				$contact->email,
				$contact->phone,
				$contact->address,
				$contact->postcode,
				$contact->city,
				$contact->country,
				$contact->language,
				$contact->first_contact
                                //Ajout du save courtier
			));
                        
                        if(!empty($contact->lots))
                        {
                            foreach ($contact->lots as $download) {
                                $result = $wpdb->query( $wpdb->prepare(
                                    "insert ignore into " . $wpdb->prefix . "meo_contact_download_names (contact_id, filename)
                                    select c.id, %s
                                      from " . $wpdb->prefix . "meo_contacts c
                                     where c.email = %s",
                                    $download,
                                    $contact->email
                                ));
                            }
                        }

			foreach ($contact->analytics_id as $analytics_id) {
                            $result = $wpdb->query( $wpdb->prepare(
                                "insert ignore into " . $wpdb->prefix . "meo_contact_analytics_codes (contact_id, analytics_code)
                                select c.id, %s
                                  from " . $wpdb->prefix . "meo_contacts c
                                 where c.email = %s",
                                $analytics_id,
                                $contact->email
                            ));
			}

		}
	}

	public function getContactDetails($site, $contact_id = null) {
		global $wpdb;

		$result = array();

		$sql = " select c.id,
				         c.surname,
				         c.first_name,
				         c.contact_type,
				         c.address,
				         c.city,
				         c.postcode,
				         c.country,
				         c.email,
				         concat( c.surname, ', ', c.first_name ) as name,
				         c.language,
				         c.date_added as first_contact,
				         c.phone,
				         c.note,
				         concat( 'row_', c.id ) as `DT_RowId`,
				         concat(
				                    if ( c.address is null,  '', concat(c.address,  '<br>')),
				                    if ( c.city is null,     '', concat(c.city,     '<br>')),
				                    if ( c.postcode is null, '', concat(c.postcode, '<br>')),
				                    if ( c.country is null,  '', concat(c.country,  '<br>')),
				                    if ( c.email is null,    '', concat(c.email,    '<br>'))
				                ) as full_address
				    from " . $wpdb->prefix . "meo_contacts c
				   where c.site_id = %d ";

		$args = array($site['id']);

		if ((int) $contact_id) {
			$sql .= " and c.id = %d ";
			$args[] = (int) $contact_id;
		}
		else {
			$sql .= " and c.date_deleted is null ";
		}

		$sql .= " order by c.id ";


		$all_contacts = $wpdb->get_results(
			$wpdb->prepare(
				$sql,
				$args
			)
		);

		$result = array();
		foreach ($all_contacts as $contact) {
			$result[$contact->id] = $contact;
			$result[$contact->id]->analytics_id = array();
			$result[$contact->id]->lots = array();
			$result[$contact->id]->linked_lots = array();
			$result[$contact->id]->interactions = array();
			$result[$contact->id]->relationstates = array();
			$result[$contact->id]->field_values = array();
		}

		$all_contact_custom_field_values = $this->customFieldHandler->getContactCustomFieldValues($site['id'], $contact_id);
		foreach ($all_contact_custom_field_values as $contact_id => $contact_custom_field_values) {
			if (array_key_exists($contact_id, $result)) {
				$result[$contact_id]->field_values = $contact_custom_field_values;
			}
		}

		// User's analytics IDs
		$all_analytics_ids = $wpdb->get_results(
			$wpdb->prepare(
				" select ac.contact_id,
				         ac.analytics_code
				    from " . $wpdb->prefix . "meo_contact_analytics_codes ac
				         join " . $wpdb->prefix . "meo_contacts c on ac.contact_id = c.id
				   where c.site_id = %d
				order by ac.contact_id,
				         ac.analytics_code",
				$site['id']
			)
		);

		foreach ($all_analytics_ids as $analytics_row) {
			if (!array_key_exists($analytics_row->contact_id, $result)) {
				continue;
			}
			$result[$analytics_row->contact_id]->analytics_id[] = $analytics_row->analytics_code;
		}


		// Downloaded files
		$all_downloads = $wpdb->get_results(
			$wpdb->prepare(
				" select dn.contact_id,
				         dn.filename
				    from " . $wpdb->prefix . "meo_contact_download_names dn
				         join " . $wpdb->prefix . "meo_contacts c on dn.contact_id = c.id
				   where c.site_id = %d
				order by dn.contact_id,
				         dn.filename",
				$site['id']
			)
		);

		foreach ($all_downloads as $download_row) {
			if (!array_key_exists($download_row->contact_id, $result)) {
				continue;
			}
			$result[$download_row->contact_id]->lots[] = $download_row->filename;
		}


		// Lots linked in the CRM (as opposed to downloads)

		$all_linked_lots = $wpdb->get_results(
			$wpdb->prepare(
				" select cl.contact_id,
				         sl.name as lot_name
				    from " . $wpdb->prefix . "meo_contact_lots cl
				         join " . $wpdb->prefix . "meo_contacts c on cl.contact_id = c.id
				         join " . $wpdb->prefix . "meo_site_lots sl on cl.site_lot_id = sl.id
				   where c.site_id = %d
				order by cl.contact_id,
				         sl.name ",
				$site['id']
			)
		);

		foreach ($all_linked_lots as $linked_lot_row) {
			if (!array_key_exists($linked_lot_row->contact_id, $result)) {
				continue;
			}
			$result[$linked_lot_row->contact_id]->linked_lots[] = $linked_lot_row->lot_name;
		}

		// Interactions

		$all_interactions = $wpdb->get_results(
			$wpdb->prepare(
				" select ci.id,
                                         ci.contact_id,
				         ci.date_added,
				         ci.description
				    from " . $wpdb->prefix . "meo_contact_interactions ci
				         join " . $wpdb->prefix . "meo_contacts c on ci.contact_id = c.id
				   where c.site_id = %d
				order by ci.contact_id,
				         ci.date_added desc ",
				$site['id']
			)
		);

		foreach ($all_interactions as $interaction_row) {
			if (!array_key_exists($interaction_row->contact_id, $result)) {
				continue;
			}
			$result[$interaction_row->contact_id]->interactions[] = array(
                                'id'  => $interaction_row->id,
				'date_added'  => $interaction_row->date_added,
				'description' => $interaction_row->description,
			);
		}
		
		// Relation States
		
		$all_relationstates = $wpdb->get_results(
				$wpdb->prepare(
						" select ci.id,
                                         ci.contact_id,
				         ci.date_added,
				         ci.state
				    from " . $wpdb->prefix . "meo_contact_relation_states ci
				         join " . $wpdb->prefix . "meo_contacts c on ci.contact_id = c.id
				   where c.site_id = %d
				order by ci.contact_id,
				         ci.date_added desc ",
						$site['id']
				)
		);
		
		foreach ($all_relationstates as $relationstate_row) {
			if (!array_key_exists($relationstate_row->contact_id, $result)) {
				continue;
			}
			$result[$relationstate_row->contact_id]->relationstates[] = array(
					'id'  => $relationstate_row->id,
					'date_added'  => $relationstate_row->date_added,
					'state' => $relationstate_row->state,
			);
		}

		return $result;
	}

	public function updateContactCustomField() {
		global $wpdb;

		$site = mrea_get_site_by_api_key($_POST['api-key']);
		$site_id = $site['id'];

		$sites = mrea_get_sites_for_user();


		header('Content-Type: application/json');


		if (empty($sites[$site_id]) or !$sites[$site_id]['uses_crm'] ) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to update that field", 'meo_mini_crm')
			));
			exit;
		}

		$custom_field_id = (int) $_POST['field_id'];
		$fields = $this->customFieldHandler->getCustomFieldsForUser(false);

		if (!array_key_exists($custom_field_id, $fields)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to update that field", 'meo_mini_crm')
			));
			exit;
		}

		$field_value = (int) $_POST['field_value'];
		if ($field_value != -1 && !array_key_exists($field_value, $fields[$custom_field_id]['fields'])) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Invalid value", 'meo_mini_crm')
			));
			exit;
		}

		$contact_id = (int) $_POST['contact_id'];

		$contacts = $this->getContactDetails($site, $contact_id);
		if (empty($contacts)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Invalid contact", 'meo_mini_crm')
			));
			exit;
		}

		$rows_deleted = $wpdb->query(
			$wpdb->prepare(
				"delete
				   from " . $wpdb->prefix . "meo_contact_custom_field_values
				  where contact_id = %d
				    and custom_field_value_id in (
					      select id
						    from " . $wpdb->prefix . "meo_custom_field_values cfv
						   where cfv.custom_field_id = %d
				)",
				$contact_id,
				$custom_field_id
			)
		);

		if ($field_value != -1) {
			$wpdb->insert(
				$wpdb->prefix . "meo_contact_custom_field_values",
				array(
					'contact_id' => $contact_id,
					'custom_field_value_id' => $field_value
				),
				array(
					'%d',
					'%d'
				)
			);
		}

		// Audit table details
		$user_ID = get_current_user_id();
		if ($rows_deleted == 0) {
			$action = 'I';  // insert
		}
		elseif ($field_value == -1) {
			$action = 'D';  // delete
		}
		else {
			$action = 'U';  // update
		}

		$wpdb->insert(
			$wpdb->prefix . "meo_contact_custom_field_values_h",
			array(
				'contact_id' => $contact_id,
				'custom_field_value_id' => $field_value,
				'date_added' => current_time('mysql', 1),
				'change_type' => $action,
				'change_by_user_id' => $user_id
			),
			array(
				'%d',
				'%d',
				'%s',
				'%s',
				'%d'
			)
		);


		echo json_encode(array(
			'success' => 1,
			'message' => __("Saved", 'meo_mini_crm')
		));
		exit;

	}

	public function updateContactLinkedLots() {
		global $wpdb;

		$site = mrea_get_site_by_api_key($_POST['api-key']);
		$site_id = $site['id'];

		$sites = mrea_get_sites_for_user();


		header('Content-Type: application/json');


		if (empty($sites[$site_id]) or !$sites[$site_id]['uses_crm'] ) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to update that field", 'meo_mini_crm')
			));
			exit;
		}

		$contact_id = (int) $_POST['contact_id'];

		$contacts = $this->getContactDetails($site, $contact_id);
		if (empty($contacts)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Invalid contact", 'meo_mini_crm')
			));
			exit;
		}

		$remote_lots = $this->remoteLotHandler->getRemoteLots((int) $site_id);
		$lots_by_remote_id = array();
		foreach ($remote_lots as $lot_id => $remote_lot) {
			$lots_by_remote_id[$remote_lot->remote_lot_id] = array(
				'id' => $lot_id,
				'code' => $remote_lot->name
			);
		}

		$int_contact_linked_lots = array();
		foreach($_POST['contact-linked-lot'] as $contact_linked_lot) {
			if (!array_key_exists($contact_linked_lot, $lots_by_remote_id)) {
				echo json_encode(array(
					'success' => 0,
					'message' => __("Unknown lot ID", 'meo_mini_crm')
				));
				exit;
			}
			$int_contact_linked_lots[] = (int) $contact_linked_lot;
		}

		$already_assigned = $wpdb->get_results(
			$wpdb->prepare(
				"select sl.name, cl.contact_id
				   from " . $wpdb->prefix . "meo_contact_lots cl
				        join " . $wpdb->prefix . "meo_site_lots sl on cl.site_lot_id = sl.id
				  where contact_id != %d
				    and sl.remote_lot_id in (" . join(", ", $int_contact_linked_lots) . ")",
				$contact_id
			)
		);

		if (!empty($already_assigned)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Lot " . $already_assigned[0]->name . " is already assigned to contact ID " . $already_assigned[0]->contact_id, 'meo_mini_crm')
			));
			exit;
		}

		$wpdb->delete(
			$wpdb->prefix . "meo_contact_lots",
			array('contact_id' => $contact_id),
			array('%d')
		);

		$wpdb->query(
			"insert into " . $wpdb->prefix . "meo_contact_lots (contact_id, site_lot_id)
			select " . $contact_id . " as contact_id,
			       sl.id
			  from " . $wpdb->prefix . "meo_site_lots sl
			 where sl.remote_lot_id in (" . join(", ", $int_contact_linked_lots) . ")"
		);

		echo json_encode(array(
			'success' => 1,
			'message' => __("Saved", 'meo_mini_crm')
		));
		exit;
	}

	public function addNewInteraction() {
		global $wpdb;

		$site = mrea_get_site_by_api_key($_POST['api-key']);
		$site_id = $site['id'];

		$sites = mrea_get_sites_for_user();


		header('Content-Type: application/json');


		if (empty($sites[$site_id]) or !$sites[$site_id]['uses_crm'] ) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to update that field", 'meo_mini_crm')
			));
			exit;
		}

		$contact_id = (int) $_POST['contact_id'];

		$contacts = $this->getContactDetails($site, $contact_id);
		if (empty($contacts)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Invalid contact", 'meo_mini_crm')
			));
			exit;
		}

		if (empty($_POST['interaction-description'])) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Missing description", 'meo_mini_crm')
			));
			exit;
		}

		$date = strtotime($_POST['interaction-date']);
		if ($date <= 0) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Invalid date", 'meo_mini_crm')
			));
			exit;
		}

		$wpdb->insert(
			$wpdb->prefix . "meo_contact_interactions",
			array(
				'contact_id' => $contact_id,
				'date_added' => date('Y-m-d', $date),
				'description' => $_POST['interaction-description']
			)
		);

		echo json_encode(array(
			'success' => 1,
			'message' => __("Added", 'meo_mini_crm')
		));
		exit;
	}
	
	/* Exception Estavayer */
	public function addNewRelationstate() {
		global $wpdb;

		$site = mrea_get_site_by_api_key($_POST['api-key']);
		$site_id = $site['id'];

		$sites = mrea_get_sites_for_user();


		header('Content-Type: application/json');


		if (empty($sites[$site_id]) or !$sites[$site_id]['uses_crm'] ) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to update that field", 'meo_mini_crm')
			));
			exit;
		}

		$contact_id = (int) $_POST['contact_id'];

		$contacts = $this->getContactDetails($site, $contact_id);
		if (empty($contacts)) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Invalid contact", 'meo_mini_crm')
			));
			exit;
		}

		if (empty($_POST['relationstate-state'])) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Missing state", 'meo_mini_crm')
			));
			exit;
		}

		$date = strtotime($_POST['relationstate-date']);
		if ($date <= 0) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Invalid date", 'meo_mini_crm')
			));
			exit;
		}

		$wpdb->insert(
			$wpdb->prefix . "meo_contact_relation_states",
			array(
				'contact_id' => $contact_id,
				'date_added' => date('Y-m-d', $date),
				'state' => $_POST['relationstate-state']
			)
		);

		echo json_encode(array(
			'success' => 1,
			'message' => __("Added", 'meo_mini_crm')
		));
		exit;
	}
	/* /Exception */

	public function saveContact() {
		global $wpdb;

		$site = mrea_get_site_by_api_key($_POST['api-key']);
		$site_id = $site['id'];

		$sites = mrea_get_sites_for_user();


		header('Content-Type: application/json');


		if (empty($sites[$site_id]) or !$sites[$site_id]['uses_crm'] ) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to edit contacts", 'meo_mini_crm')
			));
			exit;
		}

		$required_fields = array( 'surname' );
		foreach($required_fields as $required_field) {
			if (empty($_POST[$required_field])) {
				echo json_encode(array(
					'success' => 0,
					'message' => __("Please fill in all required fields", 'meo_mini_crm')
				));
				exit;
			}
		}

		if (!empty($_POST['email']) && !is_email($_POST['email']) ) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("email address seems invalid", 'meo_mini_crm')
			));
			exit;
		}


		$contact_id = (int) $_POST['contact_id'];
		$new_contact = ($contact_id == -1);
		if ($new_contact) {
			$wpdb->insert(
				$wpdb->prefix . 'meo_contacts',
				array(
					'client_id'     => $site['client_id'],
					'site_id'       => $site_id,
					'surname'       => $_POST['surname'],
					'first_name'    => $_POST['first_name'],
					'email'         => $_POST['email'],
					'phone'         => $_POST['phone'],
					'address'       => $_POST['address'],
					'postcode'      => $_POST['postcode'],
					'city'          => $_POST['city'],
					'country'       => $_POST['country'],
					'note'          => $_POST['note'],
					'language'      => 'fr',
					'date_added'    => current_time('mysql', 1)
				),
				array(
					'%d',
					'%d',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s'
				)
			);

			$contact_id = $wpdb->insert_id;
		}
		else {
			$contacts = $this->getContactDetails($site, $contact_id);
			if (empty($contacts)) {
				echo json_encode(array(
					'success' => 0,
					'message' => __("Invalid contact", 'meo_mini_crm')
				));
				exit;
			}

			$wpdb->update(
				$wpdb->prefix . 'meo_contacts',
				array(
					'surname'       => $_POST['surname'],
					'first_name'    => $_POST['first_name'],
					'email'         => $_POST['email'],
					'phone'         => $_POST['phone'],
					'address'       => $_POST['address'],
					'postcode'      => $_POST['postcode'],
					'city'          => $_POST['city'],
					'country'       => $_POST['country'],
					'note'          => $_POST['note']
				),
				array(
					'id' => $contact_id
				),
				array(
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s'
				),
				array(
					'%d'
				)
			);
		}


		echo json_encode(array(
			'success' => 1,
			'message' => $new_contact ? __("Contact added", 'meo_mini_crm') : __("Contact updated", 'meo_mini_crm'),
			'contact_id' => $contact_id
		));
		exit;
	}

	public function setLinkedContactForLot($site_id, $remote_lot_id, $contact_id) {
		global $wpdb;

		$result = 0;

		// Delete any previous record for this lot
		$result = $wpdb->query(
			$wpdb->prepare(
				"delete
				   from " . $wpdb->prefix . "meo_contact_lots
				  where site_lot_id in (
				          select id from " . $wpdb->prefix . "meo_site_lots
				           where remote_lot_id = %d
				             and site_id = %d
				)",
				$remote_lot_id,
				$site_id
			)
		);

		// If a lot's been selected, insert it
		if ($contact_id > 0) {
			$result = $wpdb->query(
				$wpdb->prepare(
					"insert into " . $wpdb->prefix . "meo_contact_lots (contact_id, site_lot_id)
					 select c.id as contact_id
					      , sl.id as site_lot_id
					   from wp_meo_contacts c
					        join wp_meo_site_lots sl on c.site_id = sl.site_id
					  where c.id = %d
					    and sl.remote_lot_id = %d
					    and c.site_id = %d ",
					 $contact_id,
					 $remote_lot_id,
					 $site_id
				)
			);
		}

		return $result;
	}

	public function deleteContact() {
		global $wpdb;

		$site = mrea_get_site_by_api_key($_POST['api-key']);
		$site_id = $site['id'];

		$sites = mrea_get_sites_for_user();


		header('Content-Type: application/json');

		if (empty($sites[$site_id]) or !$sites[$site_id]['uses_crm'] or !$this->canDeleteContacts()) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to delete this contact", 'meo_mini_crm')
			));
			exit;
		}

		$user_id = get_current_user_id();
		$contact_id = (int) $_POST['contact_id'];

		$rows_updated = $wpdb->update(
			$wpdb->prefix . "meo_contacts",
			array(
				'date_deleted'     => current_time('mysql', 1),
				'deleting_user_id' => $user_id
			),
			array(
				'id'      => $contact_id,
				'site_id' => $site_id
			),
			array(
				'%s',
				'%d'
			),
			array(
				'%d',
				'%d'
			)
		);

		if (!$rows_updated) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("Delete failed", 'meo_mini_crm')
			));
			exit;
		}

		echo json_encode(array(
			'success' => 1,
			'message' => __("Deleted", 'meo_mini_crm')
		));
		exit;
	}

	public function makeContactAppUser() {
		global $wpdb;

		$site = mrea_get_site_by_api_key($_POST['api-key']);
		$site_id = $site['id'];

		$sites = mrea_get_sites_for_user();

		header('Content-Type: application/json');

		if (empty($sites[$site_id]) or !$sites[$site_id]['enabled_in_app'] or !$this->canAddAppUsers()) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to add this user to the app for this development", 'meo_mini_crm')
			));
			exit;
		}

		$contact_id = (int) $_POST['contact_id'];
		$contact = $this->getContactDetails($site, $contact_id)[$contact_id];

		$result = $this->createAppUser($site, $contact->email);

		echo json_encode($result);
		exit;

	}


	public function makeAppUserFromEmail() {
		$site_id = (int) $_POST['site_id'];
		$sites = mrea_get_sites_for_user();

		header('Content-Type: application/json');

		if (empty($sites[$site_id]) or !$sites[$site_id]['enabled_in_app'] or !$this->canAddAppUsers()) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to add this user to the app for this development", 'meo_mini_crm')
			));
			exit;
		}

		$result = $this->createAppUser($sites[$site_id], $_POST['email_new_app_user']);

		echo json_encode($result);
		exit;
	}

	private function createAppUser($site, $email) {
		if (empty($email) or !is_email($email)) {
			return array(
				'success' => 0,
				'message' => __("Error - the contact must have a valid email address", 'meo_mini_crm')
			);
		}

		$new_user = false;
		$password = '';
		$user = get_user_by('email', $email);
		if (!$user) {
			$new_user = true;
			$password = wp_generate_password( 6, false );
			$user_id = wp_create_user( $email, $password, $email );
			if( is_wp_error($user_id) ) {
				return array(
					'success' => 0,
					'message' => __("Error creating the user", 'meo_mini_crm')
				);
			}

			wp_update_user( array(
				'ID'       => $user_id,
				'nickname' => $email
			) );

			// Hide admin bar when user logged in
			update_user_meta( $user_id, 'show_admin_bar_front', 'false' );

			$user = new WP_User( $user_id );
			$user->set_role( 'app_user' );
		}

		$connection_id = p2p_type( 'sites_to_users' )->connect( $site['id'], $user, array(
			'date' => current_time('mysql')
		) );

		if( is_wp_error($connection_id) ) {
			$error = $connection_id->get_error_codes()[0];
			if ($error != 'duplicate_connection') {
				return array(
					'success' => 0,
					'message' => __("User created, could not link to project", 'meo_mini_crm')
				);
			}
		}

		$sender_name    = $this->getEmailField('email_sender_name',    $site['id']);
		$sender_address = $this->getEmailField('email_sender_address', $site['id']);
		$subject        = $this->getEmailField('email_subject',        $site['id']);
		$content        = $this->getEmailField($new_user ? 'email_content_new_user' : 'email_content_existing_user', $site['id']);

		$replacements = array(
			'[user_email]' => $email,
			'[password]'   => $password,
			'[site]'       => $site['name']
		);

		foreach ($replacements as $key => $value) {
			$subject = str_replace($key, $value, $subject);
			$content = str_replace($key, $value, $content);
		}

		$send_to = IS_PRODUCTION ? $email : 'digital@meomeo.ch';

		$content = $this->escapeHtmlForEmail($content);

		$headers = array(
			'From: ' . $sender_name . ' <' . $sender_address . '>',
			'Content-type: text/html; charset="utf-8"'
		);

		wp_mail($send_to, $subject, $content, $headers);

		return array(
			'success' => 1,
			'message' => __("User created", 'meo_mini_crm')
		);

	}

	// Works up the hierarchy to find the field.  If it's not set on the site,
	// tries the associated client.  Failing that, the value on the MEO Settings page
	private function getEmailField($field, $site_id) {
		global $wpdb;

		$result = get_field($field, $site_id);
		if ($result) {
			return $result;
		}

		$sql = $wpdb->prepare(
				"select p.p2p_to
				  from wp_p2p p
				 where p.p2p_type = 'sites_to_clients'
				   and p.p2p_from = %d", $site_id);

		$client_id = $wpdb->get_var($sql);
		if ($client_id) {
			$result = get_field($field, $client_id);
			if ($result) {
				return $result;
			}
		}

		$result = get_field($field, 'option');

		return $result;
	}

	private function escapeHtmlForEmail($in) {
		$translation_table = get_html_translation_table( HTML_ENTITIES, ENT_NOQUOTES );
		$translation_table[chr(38)] = '&';
		unset($translation_table['<']);
		unset($translation_table['>']);

		return strtr($in, $translation_table);
	}

	public function getUsersForSite() {
		$site_id = (int) $_POST['site_id'];
		$sites = mrea_get_sites_for_user();

		header('Content-Type: application/json');

		if (empty($sites[$site_id]) or !$sites[$site_id]['enabled_in_app'] or !$this->canAddAppUsers()) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to see app users for this development", 'meo_mini_crm')
			));
			exit;
		}

		$users = get_users( array(
			'connected_type' => 'sites_to_users',
			'connected_items' => $site_id
		) );

		$user_connections = $this->summarizeUserConnections();

		$result = array();
		foreach ($users as $user) {
			$result[] = array(
				'id' => $user->ID,
				'login' => $user->user_login,
				'total_connections' => empty($user_connections[$user->ID]) ?  0 : $user_connections[$user->ID]['total_connections'],
				'last_connection'   => empty($user_connections[$user->ID]) ? '' : $user_connections[$user->ID]['last_connection']
			);
		}

		echo json_encode(array(
			'success' => 1,
			'users' => $result
		));
		exit;
	}

	private function summarizeUserConnections() {
		global $meova;

		$result = array();

		if ($meova) {
			$result = $meova->summarizeUserConnections();
		}

		return $result;
	}

	public function revokeUserAccessForSite() {
		global $wpdb;

		$site_id = (int) $_POST['site_id'];
		$user_id = (int) $_POST['user_id'];
		$sites = mrea_get_sites_for_user();

		header('Content-Type: application/json');

		if (empty($sites[$site_id]) or !$sites[$site_id]['enabled_in_app'] or !$this->canAddAppUsers()) {
			echo json_encode(array(
				'success' => 0,
				'message' => __("You don't have permissions to change access for this development", 'meo_mini_crm')
			));
			exit;
		}

		p2p_type( 'sites_to_users' )->disconnect( $site_id, $user_id );

		echo json_encode( array(
			'success' => 1,
			'message' => __("User's access revoked", 'meo_mini_crm')
		));
		exit;
	}

	/* =================================================================================================================
	 * Add permissions settings to the user admin page
	 * ================================================================================================================= */

	public function showUserFields( $user ) {
		$fields = array(
			'delete_contacts' => __('Delete Contacts', 'meo_mini_crm'),
			'add_app_users'   => __('Add Ivizzio users', 'meo_mini_crm')
		);

		?>
		<h3><?php _e('Extra permissions', 'meo_mini_crm'); ?></h3>

		<table class="form-table">
			<?php foreach ($fields as $field => $label) { ?>
				<tr>
					<th><?php echo $label; ?></th>
					<td><input type="checkbox" name="<?php echo $field; ?>" value="1"<?php echo (get_user_meta($user->ID, $field, true) ? ' checked="checked"' : ''); ?> class="checkbox"></td>
				</tr>
			<?php } ?>
		</table>
	<?php
	}

	public function saveUserDetails($user_id) {
		global $wpdb;
		if ( !current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}

		foreach (array('delete_contacts', 'add_app_users') as $field) {
			if (!empty($_POST[$field])) {
				update_user_meta( $user_id, $field, $wpdb->prepare("%d", $_POST[$field]) );
			}
			else {
				delete_user_meta( $user_id, $field );
			}
		}
	}

	public function canDeleteContacts() {
		return $this->hasUserPermission('delete_contacts');
	}

	public function canAddAppUsers() {
		return $this->hasUserPermission('add_app_users');
	}

	private function hasUserPermission($permission_name) {
		$user = wp_get_current_user();
		if ( in_array('administrator', (array) $user->roles) ) {
			return true;
		}

		$has_permission = get_user_meta($user->ID, $permission_name, true);

		return (isset($has_permission) && (intval($has_permission) == 1));
	}
}
