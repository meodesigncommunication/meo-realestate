jQuery(document).ready(function($){
   
    //init Thickbox
    
    ////stop the flash from happening
	$('#TB_window').css('opacity',0);
	
	function calcTB_Pos() {
		$('#TB_window').css({
	   	   'height': ($('#TB_ajaxContent').outerHeight() + 30) + 'px',
	   	   'top' : (($(window).height() + $(window).scrollTop())/2 - (($('#TB_ajaxContent').outerHeight()-$(window).scrollTop()) + 30)/2) + 'px',
	   	   'opacity' : 1
		});
	}
	
	setTimeout(calcTB_Pos,100);
	
	$(window).resize(calcTB_Pos);
	
	
  //Upload function
  initUpload();
			
	function initUpload(clone){
		var itemToInit = null;
		itemToInit = typeof clone !== 'undefined' ? clone : $('.shortcode-dynamic-item');
		
		itemToInit.find('.redux-opts-upload').on('click',function(e) {
		
		    var activeFileUploadContext = jQuery(this).parent();
		    var relid = jQuery(this).attr('rel-id');
		
		    e.preventDefault();
		
		    // if its not null, its broking custom_file_frame's onselect "activeFileUploadContext"
		    custom_file_frame = null;
		
		    // Create the media frame.
		    custom_file_frame = wp.media.frames.customHeader = wp.media({
		        // Set the title of the modal.
		        title: jQuery(this).data("choose"),
		
		        // Tell the modal to show only images. Ignore if want ALL
		        library: {
		            type: 'image'
		        },
		        // Customize the submit button.
		        button: {
		            // Set the text of the button.
		            text: jQuery(this).data("update")
		        }
		    });
		
		    custom_file_frame.on( "select", function() {
		        // Grab the selected attachment.
		        var attachment = custom_file_frame.state().get("selection").first();
		
		        // Update value of the targetfield input with the attachment url.
		        jQuery('.redux-opts-screenshot',activeFileUploadContext).attr('src', attachment.attributes.url);
		        jQuery('#' + relid ).val(attachment.attributes.url).trigger('change');
		
		        jQuery('.redux-opts-upload',activeFileUploadContext).hide();
		        jQuery('.redux-opts-screenshot',activeFileUploadContext).show();
		        jQuery('.redux-opts-upload-remove',activeFileUploadContext).show();
				});
		
				custom_file_frame.open();
		});
	
	 	itemToInit.find('.redux-opts-upload-remove').on('click', function( event ) {
	      var activeFileUploadContext = jQuery(this).parent();
	      var relid = jQuery(this).attr('rel-id');
	
	      event.preventDefault();
	
	      jQuery('#' + relid).val('');
	      jQuery(this).prev().fadeIn('slow');
	      jQuery('.redux-opts-screenshot',activeFileUploadContext).fadeOut('slow');
	      jQuery(this).fadeOut('slow');
	  });
	}
  


	//The chosen one
	$("select#thb-shortcodes").chosen();
	
    var ed = tinyMCE.activeEditor;
    
    function dynamic_items(){
    	
    	var name = $('#thb-shortcodes').val(),
    			dataType = $('#options-'+name).attr('data-type'),
    			content = '';
	    
	    switch(name) {
	    
	    	//tabs
	    	case 'tabs':
			    if( $('.shortcode-options[data-name='+name+']').is(':visible') ){
			    	$('.shortcode-options[data-name='+name+'] .shortcode-dynamic-item-input').each(function(){
			    	   if( $(this).val() != '' ) {
			    	   	i = $(this).parent().parent().find('.shortcode-dynamic-item-input-icon').val();
			    	   		if(i) { var icon = 'icon="'+i+'"' } else { var icon = '' }
			    			tabContent = $(this).parents('.shortcode-dynamic-item').find('.shortcode-dynamic-item-text').val();
			    			content += ' [tab title="'+$(this).val()+'" '+icon+'] '+tabContent+' [/tab] '; 
			    		}
			    	});
			    }   
	    		$('#shortcode-storage-o').html('[tabs]'+content+'[/tabs]');
	    	break;
	    
	    	//tabs
	    	case 'accordion':
	    		if( $('.shortcode-options[data-name='+name+']').is(':visible') ){
	    			$('.shortcode-options[data-name='+name+'] .shortcode-dynamic-item-input').each(function(){
	    			   if( $(this).val() != '' ) {
	    					tabContent = $(this).parents('.shortcode-dynamic-item').find('.shortcode-dynamic-item-text').val();
	    					content += ' [tab title="'+$(this).val()+'"] '+tabContent+' [/tab] '; 
	    				}
	    			});
	    		}   
	    		$('#shortcode-storage-o').html('[accordion]'+content+'[/accordion]');
	    	break;
	    	
	    	//icon-list
	    	case 'icon_list':
	    		if( $('.shortcode-options[data-name='+name+']').is(':visible') ){
	    			var listicon = $('.shortcode-options[data-name='+name+']').find('input[id='+name+'-icon]').val() || 'ok';
	    			$('.shortcode-options[data-name='+name+'] .shortcode-dynamic-item-input').each(function(){
	    			   if( $(this).val() != '' ) {
	    					tabContent = $(this).parents('.shortcode-dynamic-item').find('.shortcode-dynamic-item-input').val();
	    					content += ' [item] '+tabContent+' [/item] '; 
	    				}
	    			});
	    		}
	    		$('#shortcode-storage-o').html('[icon-list icon="'+listicon+'"]'+content+'[/icon-list]');
	    	break;
	    	
	    	//Clients
	    	case 'clients':
	    		if( $('#options-item.clients').is(':visible') ) {
	    			var count = $('.shortcode-options[data-name='+name+']').find('input[id='+name+'-count]').val() || '6';
	    			$('#options-item.'+name+' .shortcode-dynamic-item').each(function(){
	    				
	    				clientImage = $(this).find('.redux-opts-screenshot:first').attr('src');
	    				clientURL = ($(this).find('.shortcode-dynamic-item-input').val().length != 0) ? $(this).find('.shortcode-dynamic-item-input').val() : '';
	    				
	    				content += '[client image="'+clientImage+'" url="' + clientURL + '"]'; 

	    			});
	    		}
	    		$('#shortcode-storage-o').html('[clients count="'+count+'"]'+content+'[/clients]');
	    	break;
	    	
	    	//testimonials
	    	case 'testimonials':
	    		console.log('1');
	    	  if( $('.shortcode-options[data-name='+name+']').is(':visible') ){
	    	  		console.log('2');
	    	  	$('.shortcode-options[data-name='+name+'] .shortcode-dynamic-item').each(function(){
	    	  		    					console.log('3');
	    				author = $(this).find('.shortcode-dynamic-item-input').val();
	    				text = $(this).find('.shortcode-dynamic-item-text').val();
	    				
	    				content += ' [testimonial author="'+author+'"] '+text+' [/testimonial] '; 

	    			});
	    	  }   
	    		$('#shortcode-storage-o').html('[testimonials]'+content+'[/testimonials]');
	    	break;
	    }
    }
    
    function update_shortcode(){
		
			var name = $('#thb-shortcodes').val(),
					dataType = $('#options-'+name).data('type'),
					content = '';
			
			if(dataType == 'dynamic') {
				dynamic_items();
				return false;
			}
			
			switch(name) {
			
				// Columns
				case 'two_columns':
					content = '[columns]' + 
					'[two-columns] content here [/two-columns]' + 
					'[two-columns] content here [/two-columns]' + 
					'[/columns]';
				break;
				
				case 'three_columns':
					content = '[columns]' + 
					'[three-columns] content here [/three-columns]' + 
					'[three-columns] content here [/three-columns]' + 
					'[three-columns] content here [/three-columns]' + 
					'[/columns]';
				break;
				
				case 'four_columns':
					content = '[columns]' + 
					'[four-columns] content here [/four-columns]' + 
					'[four-columns] content here [/four-columns]' + 
					'[four-columns] content here [/four-columns]' + 
					'[four-columns] content here [/four-columns]' + 
					'[/columns]';
				break;
				
				case 'six_columns':
					content = '[columns]' + 
					'[six-columns] content here [/six-columns]' + 
					'[six-columns] content here [/six-columns]' + 
					'[six-columns] content here [/six-columns]' + 
					'[six-columns] content here [/six-columns]' +
					'[six-columns] content here [/six-columns]' + 
					'[six-columns] content here [/six-columns]' + 
					'[/columns]';
				break;
				
				case 'two_third_one_third':
					content = '[columns]' + 
					'[two-three-columns] content here [/two-three-columns]' + 
					'[three-columns] content here [/three-columns]' + 
					'[/columns]';
				break;
				
				case 'one_third_two_third':
					content = '[columns]' + 
					'[three-columns] content here [/three-columns]' + 
					'[two-three-columns] content here [/two-three-columns]' + 
					'[/columns]';
				break;
				
				case 'one_forth_three_forth':
					content = '[columns]' + 
					'[four-columns] content here [/four-columns]' + 
					'[three-four-columns] content here [/three-four-columns]' + 
					'[/columns]';
				break;
				
				case 'three_forth_one_forth':
					content = '[columns]' + 
					'[three-four-columns] content here [/three-four-columns]' + 
					'[four-columns] content here [/four-columns]' + 
					'[/columns]';
				break;
				
				// Elements
				
				case 'headingtag':
						var htag = $('input[name='+name+'-heading_tag]:checked').val() || 'h4'
								text = $('input[id='+name+'-text]').val(),
								center = $('input[id='+name+'-centered]:checked').length,
				
						content = '<'+htag+'>'+text+'</'+htag+'>';
						if (center) { content = '<div style="text-align:center;" class="cf">'+content+'</div><br>';}
					break;
					
				case 'quote':
					var align = $('input[name='+name+'-align]:checked').val() || '',
							author = $('input[id='+name+'-author]').val(),
							text = $('textarea[id='+name+'-content]').val();
					
					if (align != '') { var alignment = 'pull="'+align+'"'; } else { var alignment = '';}
					
					content = '[blockquote author="'+author+'" '+alignment+']'+text+'[/blockquote]';
				break;
				
				case 'btn':
					var size = $('input[name='+name+'-size]:checked').val() || 'small',
							color = $('input[name='+name+'-color]:checked').val() || 'black',
							rounded = $('input[id='+name+'-rounded]:checked').length,
							icon = $('input[id='+name+'-icon]').val(),
							title = $('input[id='+name+'-title]').val(),
							link = $('input[id='+name+'-link]').val();
					
					if (rounded) { var round = 'true'; } else { var round = 'false';}
					
					content = '[button link="'+link+'" color="'+color+'" rounded="'+round+'" icon="'+icon+'" size="'+size+'"]'+title+'[/button]';
					
				break;
				
				case 'pricing_table':
					var columns = $('input[name='+name+'-columns]:checked').val() || '4';
							for(var i=1;i<=columns;i++) {
								content += '[pricing_column featured="false" packagetitle="Package" price="300" currency="$" currencytext="per month" caption="caption" buttontext="Buy Now" buttonlink="#"]<ul><li>20 Website Cap</li><li>50TB Bandwidth Cap</li><li>200GB Storage Space</li><li>Domain Name Included</li><li>24/7 Unlimited Support</li><li>Custom Email</li></ul>[/pricing_column]';
							}
					content = '[pricing_table columns="'+columns+'"]'+content+'[/pricing_table]';
					
				break;
				
				case 'team_member':
					var pname = $('input[id='+name+'-pname]').val(),
							description = $('textarea[id='+name+'-description]').val(),
							email = $('#'+name+'-email').val(),
							job = $('input[id='+name+'-job_pos]').val(),
							image = $('#image_url').attr('src');
					
					content = '[team_member name="'+pname+'" email="'+email+'" job="'+job+'" description="'+description+'" image="'+image+'"]';
					
				break;
				
				case 'divider':
					content = '[divider]<br>';
				break;
				
				case 'video':
					var id = $('input[id='+name+'-id]').val(),
							type = $('input[id='+name+'-type]').val() || 'youtube',
							format = $('input[name='+name+'-format]:checked').val() || 'widescreen';
					
					content = '[video id="'+id+'" type="'+type+'" format="'+format+'"]';
					
				break;
				
				case 'highlight':
					var color = $('input[name='+name+'-color]:checked').val() || 'red',
							text = $('input[id='+name+'-text]').val();
					
					content = '[highlight color="'+color+'"]'+text+'[/highlight]';
					
				break;
				
				case 'notifications':
					var type = $('input[name='+name+'-type]:checked').val() || 'success',
							title = $('input[id='+name+'-title]').val(),
							content = $('textarea[id='+name+'-content]').val();
					
					content = '[notification type="'+type+'" title="'+title+'"]'+content+'[/notification]';
					
				break;
				
				// Interface
				case 'toggle':
					var title = $('input[id='+name+'-title]').val(),
							text = $('textarea[id='+name+'-content]').val();

					content = '[toggle title="'+title+'"]'+text+'[/toggle]';
				break;
				
				// Recent Posts / Portfolios
				case 'recent_posts':
					var columns = $('input[name='+name+'-columns]:checked').val(),
							count = $('input[id='+name+'-count]').val();
					content = '[blog_posts type="'+columns+'" count="'+count+'"]';
				break;
				
				case 'recent_portfolio':
					var columns = $('input[name='+name+'-columns]:checked').val(),
							count = $('input[id='+name+'-count]').val();
					content = '[portfolio_posts type="'+columns+'" count="'+count+'"]';
				break;
				
				case 'recent_posts_carousel':
					var columns = $('input[name='+name+'-columns]:checked').val() || '4',
							count = $('input[id='+name+'-count]').val() || '4',
							display_link = $('input[id='+name+'-display_link]:checked').length,
							link_url = $('input[id='+name+'-link_url]').val();
					
					if(display_link) { 	var blog_url = 'blog_url="'+link_url+'"' } else { var blog_url = '' }
					
					content = '[carousel type="posts" count="'+count+'" columns="'+columns+'" '+blog_url+']';
				break;
				
				case 'portfolio_posts_carousel':
					var columns = $('input[name='+name+'-columns]:checked').val() || '4',
							count = $('input[id='+name+'-count]').val() || '4',
							display_link = $('input[id='+name+'-display_link]:checked').length,
							link_url = $('input[id='+name+'-link_url]').val();
					
					if(display_link) { 	var portfolio_url = 'portfolio_url="'+link_url+'"' } else { var portfolio_url = '' }
					
					content = '[carousel type="portfolio" count="'+count+'" columns="'+columns+'" '+portfolio_url+']';
				break;
				
				// Icons
				case 'single_icon':
					var link = $('input[id='+name+'-icon_link]').val(),
							icon = $('input[id='+name+'-icon]').val() || 'leaf',
							size = $('input[name='+name+'-size]:checked').val() || 'icon-smallsize',
							boxed = $('input[id='+name+'-boxed]:checked').length;
					
					if(boxed) { var box = 'box="true"' } else { var box = '' }
					if(link) { var url = 'url="'+link+'"' } else { var url = '' }
					
					content = '[icon type="'+icon+'" size="'+size+'" '+url+' '+box+']';	
				break;
				
				case 'icon_box':
					var type = $('input[name='+name+'-type]:checked').val() || 'left',
							icon = $('input[id='+name+'-icon]').val() || 'rocket',
							title = $('input[id='+name+'-title]').val() || 'Heading',
							text = $('textarea[id='+name+'-content]').val();
					
					content = '[iconbox type="'+type+'" icon="'+icon+'" heading="'+title+'"]'+text+'[/iconbox]';	
				break;
				
				// Google
				case 'google_map':
					var latitude = $('input[id='+name+'-latitude]').val() || '41',
							longtitude = $('input[id='+name+'-longtitude]').val() || '29',
							html = $('textarea[id='+name+'-content]').val(),
							zoom = $('input[id='+name+'-zoom]').val() || '9',
							control = $('input[id='+name+'-control]:checked').length,
							maptype = $('input[name='+name+'-maptype]:checked').val() || 'HYBRID';
					
					if(control) { 	var controls = 'true' } else { var controls = 'false' }
					
					content = '[gmap latitude="'+latitude+'" longitude="'+longtitude+'" html="'+html+'" maptype="'+maptype+'"  zoom="'+zoom+'" controls="'+controls+'"]';	
				break;
				
				case 'google_chart':
					var charttype = $('input[name='+name+'-charttype]:checked').val() || 'pie';
					
					if (charttype == 'pie'){
						content = '[chart data="70,25,20.01,4.99" labels="Reffering+sites|Google|Yahoo|Other" colors="058DC7,50B432,ED561B,EDEF00" bg="bg,s,65432100" size="460x250" title="3D Pie Chart Title" type="pie"]';	
					}
					if (charttype == 'line'){
						content = '[chart data="70,25,20.01,4.99" labels="2010|2011|2012|2013" colors="058DC7,50B432,ED561B,EDEF00" bg="bg,s,65432100" size="460x250" title="Line Chart Title" type="line"]';	
					}
					if (charttype == 'xyline'){
						content = '[chart data="0,25,50,75,100|2,33,43,17,25|0,25,50,75,100|0,20,25,40,75" labels="Begin|25|50|75|End" colors="058DC7,50B432" bg="bg,s,65432100" size="460x250" title="Line Chart Title 2" type="xyline"]';	
					}
					if (charttype == 'scatter'){
						content = '[chart data="0,10,20,30,40,50,60,70,80,90,100|50,52,56,63,70,80,92,85,75,60,43" labels="1|2|3|4|5|6|7|8|9|10" colors="058DC7" bg="bg,s,65432100" size="460x250" title="Scatter Chart Title" type="scatter"]';	
					}
					if (charttype == 'pie2d'){
						content = '[chart data="70,25,20.01,4.99" labels="Reffering+sites|Google|Yahoo|Other" colors="058DC7,50B432,ED561B,EDEF00" bg="bg,s,65432100" size="460x250" title="Pie Chart Title" type="pie2d"]';	
					}
					
				break;
			}
			
			$('#shortcode-storage-o').html(content);
	 	}
   
  ///// EVENTS /////
	
		// Main Select Change
    $('#thb-shortcodes').change(function(){
			$('.shortcode-options').hide();
			$('#options-'+$(this).val()).show();
			update_shortcode();
    });
		    
		// Radio Change
    $('#add-shortcode').click(function(){
    	var name = $('#thb-shortcodes').val(),
    			dataType = $('#options-'+name).attr('data-type'),
    			code = $('#shortcode-storage-o').html();
    			
    	update_shortcode();
			ed.selection.setContent($('#shortcode-storage-o').html());
			
			tb_remove();
		
			return false;
    });
		
		// Radio Change
		$('[id^=shortcode-option]').change(function(){
			update_shortcode();
    });
    
    

 	
 		// Add Item    
 		
 		$('.add-list-item').click(function(){
    	
    	if(!$(this).parent().find('.remove-list-item').is(':visible')) $(this).parent().find('.remove-list-item').show();
    	
    	//clone item 
    	var $clone = $(this).parent().find('.shortcode-dynamic-item:first').clone();
    	$clone.find('input[type=text],textarea').attr('value','');
    	
    	
    	//init new upload button and clear image if it's an upload
    	if( $clone.find('.redux-opts-upload').length > 0 ) {
    		$clone.find('.redux-opts-screenshot').attr('src','');
    		$clone.find('.redux-opts-upload-remove').hide();
    		$clone.find('.redux-opts-upload').css('display','inline-block');
    		setTimeout(function(){ initUpload($clone) },200);
    	}
    	
    	//append clone
			$(this).parent().find('.shortcode-dynamic-items').append($clone);
			return false;
    });
		
		// Remove Item
    $('.remove-list-item').hide().live('click', function(){
    	if($(this).parent().find('.shortcode-dynamic-item').length > 1){
    		$(this).parent().find('#options-item .shortcode-dynamic-item:last').remove();
			dynamic_items();	
    	}
    	if($(this).parent().find('.shortcode-dynamic-item').length == 1) $(this).hide();	
			return false;
    });
    
});