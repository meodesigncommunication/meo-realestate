<?php

$client_base = '../client/wp';

if ($_SERVER['SERVER_ADDR'] == "192.168.67.103") {
	$_SERVER['SERVER_ADDR'] = "127.0.0.1";
}

if ($_SERVER['SERVER_ADDR'] == "127.0.0.1" ) {
	$client_base = '../../client/site/wp';
}

## Load the client site
include_once $client_base . '/wp-load.php';

global $meova; // MeoVisionApp Object, instantiated in the plugin in clients

$success = $meova->logError($_POST);

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

$result = array(
	'success' => $success ? 1 : 0
);

echo json_encode($result);

exit;
