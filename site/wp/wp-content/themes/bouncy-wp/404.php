<?php get_header(); ?>
<?php $home = get_home_url(); ?>
<div class="row">
<section class="seven columns centered notfound">
  <div class="errorimage"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/404.png" alt="404" /></div>
  <h2><?php _e( "We are sorry, but the page you are looking for can not be found.", THB_THEME_NAME ); ?></h2>
  <p><?php _e( 'You might try searching our site or visit the homepage.', THB_THEME_NAME ); ?></p>
  <p><?php get_search_form(); ?> </p>
</section>
</div>
<?php get_footer(); ?>