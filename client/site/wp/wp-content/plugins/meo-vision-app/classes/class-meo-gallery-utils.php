<?php
/*
Copyright (C) MEO design et communication S�rl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/


class MeoGalleryUtils {

	// Custom post types
	const CPT_GALLERY = 'gallery';

	// Relationships
	const REL_GALLERY_DEV = 'galleries_to_developments';

	const ROW_ELEMENT_TYPE_IMAGE = 'image';
	const ROW_ELEMENT_TYPE_VIDEO = 'video';
	const ROW_ELEMENT_TYPE_PDF = 'pdf';
	const ROW_ELEMENT_TYPE_SPINNER = 'spinner';


	private $media_utils = null;

	public function __construct($media_utils) {
		$this->media_utils = $media_utils;
		add_action('init', array(&$this, 'createCustomPostTypes'), 5);
		add_action('p2p_init', array(&$this, 'createRelationships'));
	}


	public function getGalleryForDevelopment($development_id) {
		$raw_data = $this->retrieveGallery($development_id);
		$result = $this->formatGallery($raw_data);
		return $result;
	}

	private function retrieveGallery($development_id) {
		global $post;

		$result = array();

		$args = array(
		 	'connected_type' => self::REL_GALLERY_DEV,
			'connected_items' => $development_id,
			'nopaging' => true
		);

		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) {
			$loop->the_post();

			$result['id'] = $post->ID;
			$result['rows'] = get_field('gallery_row', $post->ID);
		}
		wp_reset_postdata();

		return $result;
	}

	private function formatGallery($raw_data) {
		$result = array();

		if (empty($raw_data)) {
			return $result;
		}

		$image = array();
		$video = array();
		$pdf = array();
		$spinner = array();

		$rows = array();

		foreach ($raw_data['rows'] as $raw_row) {
			$row = array(
				'title' => $raw_row['gallery_row_title'],
				'slug' => $raw_row['gallery_row_slug'],
				'elements' => array()
			);

			$element_index = 1;

			foreach ($raw_row['gallery_row_elements'] as $raw_element) {
				$element_type = $raw_element['gallery_row_element_type'];
				$element_id = is_array($raw_element['gallery_row_element_' . $element_type ]) ? $raw_element['gallery_row_element_' . $element_type ]['id'] : $raw_element['gallery_row_element_' . $element_type ]->ID;

				$element = array(
					'type' => $element_type,
					'id' => $element_id
				);

				$row['elements'][$element_index++] = $element;

				${$element_type}[$element_id] = $this->{"extractElement" . ucfirst($element_type)}($raw_element);
			}

			$rows[] = $row;
		}

		$result = array(
			'gallery' => array(
				'rows' => $rows
			),
			'gallery_images' => $image,
			'videos' => $video,
			'pdfs' => $pdf,
			'spinners' => $spinner
		);

		return $result;
	}

	private function extractElementImage($element) {
		$result = $this->extractCommonElementData($element);
		$result['main_image'] = $this->media_utils->getMediaDetails($element['gallery_row_element_image']['url']);
		$result['zoom'] = ( isset($element['gallery_row_element_image_zoom']) && $element['gallery_row_element_image_zoom'] ) ? 1 : 0;

		return $result;
	}

	private function extractElementPdf($element) {
		$result = $this->extractCommonElementData($element);
		$result['main_image'] = $this->media_utils->getMediaDetails($element['gallery_row_element_pdf']['url']);
		return $result;
	}

	private function extractElementVideo($element) {
		$result = $this->extractCommonElementData($element);
		$result['mp4'] = $this->media_utils->getMediaDetails($element['gallery_row_element_video']['url']);
		$result['poster'] = $this->media_utils->getMediaDetails($element['gallery_row_element_video_poster']);

		$result['width'] = $element['gallery_row_element_video_width'];
		$result['height'] = $element['gallery_row_element_video_height'];

		return $result;
	}

	private function extractElementSpinner($element) {
		global $mvas;
		global $meova;

		$result = $this->extractCommonElementData($element);

		$spinner = $mvas->getSpinner($element['gallery_row_element_spinner']->ID);

		$result['width']     = $spinner['width'];
		$result['height']    = $spinner['height'];
		$result['clockwise'] = $spinner['clockwise'];
		$result['stretch']   = $spinner['stretch'];
		$result['images']    = array();

		foreach ($spinner['images'] as $image) {
			$result['images'][] = $meova->getMediaDetails($image);
		}

		return $result;
	}

	private function extractCommonElementData($element) {
		// All gallery elements must have a thumbnail and an optional label
		$thumb = $element['gallery_row_element_thumb'];

		$result = array(
			'thumbnail' => $this->media_utils->getMediaDetails($thumb['sizes'][GALLERY_THUMB_SIZE], array(
				'width' => $thumb['sizes'][GALLERY_THUMB_SIZE . '-width'],
				'height' => $thumb['sizes'][GALLERY_THUMB_SIZE . '-height']
			))
		);

		if (!empty($element['gallery_row_element_label'])) {
			$result['label'] = $element['gallery_row_element_label'];
		}

		return $result;
	}


	public function createCustomPostTypes() {
		$this->createGalleryCustomPostType();
	}

	public function createRelationships() {
		if (! function_exists('p2p_register_connection_type')) {
			error_log("p2p_register_connection_type function doesn't exist");
			return;
		}

		$relationships = array(
			self::REL_GALLERY_DEV => array(
				'from' => self::CPT_GALLERY,
				'to' => CPT_SITE,
				'cardinality' => 'one-to-one'
			)
		);

		foreach ($relationships as $relationship_name => $relationship_details) {
			$args =  array(
				'name' => $relationship_name,
				'admin_column' => 'from',
				'cardinality' => 'many-to-one'
			);
			foreach ($relationship_details as $relationship_field_name => $relationship_field_value) {
				$args[$relationship_field_name] = $relationship_field_value;
			}
			p2p_register_connection_type($args);
		}
	}


	private function createGalleryCustomPostType() {
		register_post_type(self::CPT_GALLERY, array(
			'labels' => array(
				'name' => __('Galleries', MVA_TEXT_DOMAIN),
				'singular_name' => __('Gallery', MVA_TEXT_DOMAIN),
				'add_new_item' => __('Add a gallery', MVA_TEXT_DOMAIN),
				'edit_item' => __('Edit gallery', MVA_TEXT_DOMAIN),
				'new_item' => __('Add a gallery', MVA_TEXT_DOMAIN),
				'search_items' => __('Find a gallery', MVA_TEXT_DOMAIN),
				'not_found' => __('No gallery found', MVA_TEXT_DOMAIN),
				'not_found_in_trash' => __('No gallery found in the trash', MVA_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => array('title')
		));

		if (function_exists('register_field_group')) {
			register_field_group(array (
				'id' => 'acf_gallery',
				'title' => 'Gallery',
				'fields' => $this->getGalleryFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_GALLERY,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getGalleryFieldDefinitions() {
		$element_fields = array (
			array (
				'key' => 'field_55963ca0d1594',
				'label' => 'Type',
				'name' => 'gallery_row_element_type',
				'type' => 'radio',
				'required' => 1,
				'column_width' => '',
				'choices' => array (
					self::ROW_ELEMENT_TYPE_IMAGE => 'Image',
					self::ROW_ELEMENT_TYPE_VIDEO => 'Video',
					self::ROW_ELEMENT_TYPE_PDF => 'PDF',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => self::ROW_ELEMENT_TYPE_IMAGE,
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_559676172040d',
				'label' => 'Thumbnail',
				'name' => 'gallery_row_element_thumb',
				'type' => 'image',
				'instructions' => 'Height should be 216 pixels',
				'save_format' => 'object',
				'preview_size' => GALLERY_THUMB_SIZE,
				'library' => 'all',
			),
			array (
				'key' => 'field_559678ba8b73a',
				'label' => 'Label',
				'name' => 'gallery_row_element_label',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),

			// Image-specific fields
			array (
				'key' => 'field_55963d73d1596',
				'label' => 'Image',
				'name' => 'gallery_row_element_image',
				'type' => 'image',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_55963ca0d1594',
							'operator' => '==',
							'value' => self::ROW_ELEMENT_TYPE_IMAGE,
						),
					),
					'allorany' => 'all',
				),
				'column_width' => '',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_55968351d3989',
				'label' => 'Enable zoom?',
				'name' => 'gallery_row_element_image_zoom',
				'type' => 'true_false',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_55963ca0d1594',
							'operator' => '==',
							'value' => self::ROW_ELEMENT_TYPE_IMAGE,
						),
					),
					'allorany' => 'all',
				),
				'message' => '',
				'default_value' => 1,
			),

			// Video-specific fields
			array (
				'key' => 'field_55967203568fd',
				'label' => 'Video',
				'name' => 'gallery_row_element_video',
				'type' => 'file',
				'instructions' => 'Format should .mp4 for iPad (and widespread support on other platforms)',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_55963ca0d1594',
							'operator' => '==',
							'value' => self::ROW_ELEMENT_TYPE_VIDEO,
						),
					),
					'allorany' => 'all',
				),
				'column_width' => '',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_55968428d46b6',
				'label' => 'Width',
				'name' => 'gallery_row_element_video_width',
				'type' => 'number',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_55963ca0d1594',
							'operator' => '==',
							'value' => self::ROW_ELEMENT_TYPE_VIDEO,
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_5596843ed46b7',
				'label' => 'Height',
				'name' => 'gallery_row_element_video_height',
				'type' => 'number',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_55963ca0d1594',
							'operator' => '==',
							'value' => self::ROW_ELEMENT_TYPE_VIDEO,
						),
					),
					'allorany' => 'all',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_559685c442dcf',
				'label' => 'Poster',
				'name' => 'gallery_row_element_video_poster',
				'type' => 'image',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_55963ca0d1594',
							'operator' => '==',
							'value' => self::ROW_ELEMENT_TYPE_VIDEO,
						),
					),
					'allorany' => 'all',
				),
				'instructions' => 'Placeholder image while video is loading',
				'save_format' => 'url',
				'preview_size' => 'medium',
				'library' => 'all',
			),

			// PDF-specific fields
			array (
				'key' => 'field_5596721c568fe',
				'label' => 'PDF',
				'name' => 'gallery_row_element_pdf',
				'type' => 'file',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_55963ca0d1594',
							'operator' => '==',
							'value' => self::ROW_ELEMENT_TYPE_PDF,
						),
					),
					'allorany' => 'all',
				),
				'column_width' => '',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		);

		if (class_exists('MeoVisionAppSpinner', false)) {
			$element_fields[0]['choices'][self::ROW_ELEMENT_TYPE_SPINNER] = 'Spinner';

			$element_fields[] = 
				// Spinner-specific fields
				array (
					'key' => 'field_5599388463c8b',
					'label' => 'Spinner',
					'name' => 'gallery_row_element_spinner',
					'type' => 'post_object',
					'post_type' => array (
						0 => MeoVisionAppSpinner::CPT_SPINNER,
					),
					'required' => 1,
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_55963ca0d1594',
								'operator' => '==',
								'value' => self::ROW_ELEMENT_TYPE_SPINNER,
							),
						),
						'allorany' => 'all',
					),
					'allow_null' => 0,
					'multiple' => 0,
				);
		}

		$result = array (
			array (
				'key' => 'field_55963c42d1590',
				'label' => 'Row',
				'name' => 'gallery_row',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_55963c5ed1591',
						'label' => 'Title',
						'name' => 'gallery_row_title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55963c76d1592',
						'label' => 'Slug',
						'name' => 'gallery_row_slug',
						'type' => 'text',
						'instructions' => 'Lower case, digits and underscores only (no spaces).  Used for next/previous navigation in the app',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55963c85d1593',
						'label' => 'Elements',
						'name' => 'gallery_row_elements',
						'type' => 'repeater',
						'column_width' => '',
						'sub_fields' => $element_fields,
						'row_min' => '',
						'row_limit' => '',
						'layout' => 'row',
						'button_label' => 'Add Row',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Row',
			),
		);

		return $result;
	}
}
