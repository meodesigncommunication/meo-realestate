INSERT INTO `wp_meo_custom_fields` (`name`, `slug`, `client_id`, `is_active`, `is_required`)
VALUES ('Status', 'meo-status', (select p.id from wp_posts p where p.post_type = 'meo-client' and p.post_title = 'MEO'), 'yes', 'no');


INSERT INTO `wp_meo_custom_field_values` (`custom_field_id`, `label`, `is_default`)
VALUES ((select id from wp_meo_custom_fields where slug = 'meo-status' and client_id = (select p.id from wp_posts p where p.post_type = 'meo-client' and p.post_title = 'MEO')), 'New', NULL);


/*

select *
  from wp_meo_custom_fields cf
       left join wp_meo_custom_field_values cfv on cfv.custom_field_id = cf.id

*/