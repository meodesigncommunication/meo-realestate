<?php

global $meocrm;

// TODO: add this to the admin console
$columns_by_client = array(
	50 => array( // Parallèle
		array(
			'label'      => __( 'First contact', 'meo_real_estate_admin' ),
			'width'      =>  8.87,
			'javascript' => "first_contact_extractor"
		),
		array(
			'label'      => __( 'Type', 'meo_real_estate_admin' ),
			'width'      =>  4.87,
			'javascript' => "contact_type_extractor"
		),
		array(
			'label'      => __( 'Name', 'meo_real_estate_admin' ),
			'width'      => 12.49,
			'javascript' => "field_extractor('name')"
		),
		array(
			'label'      => __( 'Phone', 'meo_real_estate_admin' ),
			'width'      =>  9.65,
			'javascript' => "field_extractor('phone')"
		),
		array(
			'label'      => __( 'City', 'meo_real_estate_admin' ),
			'width'      => 10.74,
			'javascript' => "field_extractor('city')"
		),
		array(
			'label'      => __( 'Last interaction / no', 'meo_real_estate_admin' ),
			'width'      => 15.58,
			'javascript' => "interactions_extractor"
		),
		array(
			'label'      => __( 'Relationship Status', 'meo_real_estate_admin' ),
			'width'      => 13.15,
			'javascript' => "custom_field_extractor('relationship-status')"
		),
		array(
			'label'      => __( 'Linked lots', 'meo_real_estate_admin' ),
			'width'      => 14.55,
			'javascript' => "array_extractor('linked_lots')"
		),
		array(
			'label'      => __( 'Summary', 'meo_real_estate_admin' ),
			'width'      =>  9.43,
			'javascript' => "summary_links_extractor"
		)
	),
	55 => array( // MEO
		array(
			'label'      => __( 'First contact', 'meo_real_estate_admin' ),
			'width'      =>  8.87,
			'javascript' => "first_contact_extractor"
		),
		array(
			'label'      => __( 'Type', 'meo_real_estate_admin' ),
			'width'      =>  4.87,
			'javascript' => "contact_type_extractor"
		),
		array(
			'label'      => __( 'Name', 'meo_real_estate_admin' ),
			'width'      => 12.49,
			'javascript' => "field_extractor('name')"
		),
		array(
			'label'      => __( 'Phone', 'meo_real_estate_admin' ),
			'width'      =>  9.65,
			'javascript' => "field_extractor('phone')"
		),
		array(
			'label'      => __( 'City', 'meo_real_estate_admin' ),
			'width'      => 10.74,
			'javascript' => "field_extractor('city')"
		),
		array(
			'label'      => __( 'Last interaction / no', 'meo_real_estate_admin' ),
			'width'      => 15.58,
			'javascript' => "interactions_extractor"
		),
		array(
			'label'      => __( 'Status', 'meo_real_estate_admin' ),
			'width'      => 13.15,
			'javascript' => "custom_field_extractor('meo-status')"
		),
		array(
			'label'      => __( 'Summary', 'meo_real_estate_admin' ),
			'width'      =>  9.43,
			'javascript' => "summary_links_extractor"
		)
	)
);

$sites = mrea_get_sites_for_user();
$site_id = mrea_get_site_id(false);
$site = $sites[$site_id];
$client_id = $site['client_id'];

// Exception For cliend Gefiswiss 63 and Estavayer site 71
if($client_id == 63 && $site_id == 71) {
	$columns_by_client = array(
		63 => array( // Gefiswiss
			array(
					'label'      => __( 'First contact', 'meo_real_estate_admin' ),
					'width'      =>  8.87,
					'javascript' => "first_contact_extractor"
			),
			array(
					'label'      => __( 'Type', 'meo_real_estate_admin' ),
					'width'      =>  4.87,
					'javascript' => "contact_type_extractor"
			),
			array(
					'label'      => __( 'Name', 'meo_real_estate_admin' ),
					'width'      => 12.49,
					'javascript' => "field_extractor('name')"
			),
			array(
					'label'      => __( 'Phone', 'meo_real_estate_admin' ),
					'width'      =>  9.65,
					'javascript' => "field_extractor('phone')"
			),
			array(
					'label'      => __( 'City', 'meo_real_estate_admin' ),
					'width'      => 10.74,
					'javascript' => "field_extractor('city')"
			),
			array(
					'label'      => __( 'Courtier', 'meo_real_estate_admin' ),
					'width'      => 10.74,
					'javascript' => "custom_field_extractor('courtier')"
			),
			array(
					'label'      => __( 'Last interaction / no', 'meo_real_estate_admin' ),
					'width'      => 15.58,
					'javascript' => "interactions_extractor"
			),
			array(
					'label'      => __( 'Summary', 'meo_real_estate_admin' ),
					'width'      =>  9.43,
					'javascript' => "summary_links_extractor"
			)
		)
	);
}
// /Exception

$columns = $columns_by_client[$client_id];

if (empty($columns)) {
	$columns = array(
		array(
			'label'      => __( 'First contact', 'meo_real_estate_admin' ),
			'width'      =>  8.87,
			'javascript' => "first_contact_extractor"
		),
		array(
			'label'      => __( 'Type', 'meo_real_estate_admin' ),
			'width'      =>  4.87,
			'javascript' => "contact_type_extractor"
		),
		array(
			'label'      => __( 'Name', 'meo_real_estate_admin' ),
			'width'      => 12.49,
			'javascript' => "field_extractor('name')"
		),
		array(
			'label'      => __( 'Phone', 'meo_real_estate_admin' ),
			'width'      =>  9.65,
			'javascript' => "field_extractor('phone')"
		),
		array(
			'label'      => __( 'City', 'meo_real_estate_admin' ),
			'width'      => 10.74,
			'javascript' => "field_extractor('city')"
		),
		array(
			'label'      => __( 'Last interaction / no', 'meo_real_estate_admin' ),
			'width'      => 15.58,
			'javascript' => "interactions_extractor"
		),
		array(
			'label'      => __( 'Summary', 'meo_real_estate_admin' ),
			'width'      =>  9.43,
			'javascript' => "summary_links_extractor"
		)
	);
}

?>

<script type="text/javascript">
jQuery(function() {
	can_delete_contacts = <?php echo ($meocrm->canDeleteContacts() ? 'true' : 'false');  ?>;
	can_add_app_users   = <?php echo ($meocrm->canAddAppUsers()    ? 'true' : 'false');  ?>;
	column_extractors = [
		<?php
		$prepend = '';
		foreach ($columns as $column) {
			echo $prepend . '{ "mData": ' . $column['javascript'] . ' }';
			$prepend = ', ';
		} ?>
	];
});
</script>
<div id="google-map">
    <div id="map"></div>
</div>
<table class="table table-striped table-bordered" id="table-1">
	<thead>
		<tr>
			<?php foreach ($columns as $column) {
				?><th style="width: <?php echo $column['width']; ?>%"><?php echo $column['label']; ?></th><?php
			} ?>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<?php foreach ($columns as $column) {
				?><th><?php echo $column['label']; ?></th><?php
			} ?>
		</tr>
	</tfoot>
</table>