<?php
/**
 * Contact page without CRM (populated via ajax)
 */

$site_id = mrea_get_site_id();

if ($_GET['pdf'] and current_user_can( 'read_contacts' ) and isset($_GET['analytics_id'])) {
	$sites = mrea_get_sites_for_user();

	$site = $sites[$site_id];
	$template = get_attached_file($site['pdf_template']['id']);

	require_once('classes/class-contact-pdf.php');

	$data = mrea_get_contact_data_by_analytics_id($site, $_GET['analytics_id']);

	$pdf = new ContactPdf('en', $template);
	$pdf->generate($data);
	$pdf->Output();

	exit;
}

get_header(); ?>

	<?php if (current_user_can( 'read_contacts' ) and isset($_GET['analytics_id'])) { ?>

		<div class="portlet loader">
			<img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" />
		</div>

		<div class="portlet details" style="display:none;">

			<h3 class="portlet-title">
				<u>Contact <span class="contact-name">&nbsp;</span></u>
				<a class="pdf-download" href="<?php the_permalink(); ?>?analytics_id=<?php echo urlencode($_GET['analytics_id']); ?>&amp;site_id=<?php echo $site_id; ?>&amp;pdf=1"><i class="fa fa-file-pdf-o"></i></a>
			</h3>

			<div class="portlet-body">

				<h4>First contact: <span class="first-contact">&nbsp;</span></h4>

				<table class="table">
					<tbody>
						<tr>
							<th style="width: 16.6%">First Name</th>
							<td style="width: 33.3%" class="first-name">&nbsp;</td>
							<th style="width: 16.6%">Surname</th>
							<td style="width: 33.3%" class="surname">&nbsp;</td>
						</tr>
						<tr>
							<th style="width: 16.6%">Address</th>
							<td style="width: 33.3%" class="address">&nbsp;</td>
							<th style="width: 16.6%">City</th>
							<td style="width: 33.3%" class="city">&nbsp;</td>
						</tr>
						<tr>
							<th style="width: 16.6%">Post Code</th>
							<td style="width: 33.3%" class="postcode">&nbsp;</td>
							<th style="width: 16.6%">Country</th>
							<td style="width: 33.3%" class="country">&nbsp;</td>
						</tr>
						<tr>
							<th style="width: 16.6%">Telephone</th>
							<td style="width: 33.3%" class="phone">&nbsp;</td>
							<th style="width: 16.6%">email</th>
							<td style="width: 33.3%" class="email">&nbsp;</td>
						</tr>
						<tr>
							<th style="width: 16.6%">Source</th>
							<td style="width: 33.3%" class="source">&nbsp;</td>
							<th style="width: 16.6%">&nbsp;</th>
							<td style="width: 33.3%">&nbsp;</td>
						</tr>
					</tbody>
				</table>

				<a class="map-wrapper" href="#" target="_blank">
					<img class="dummy" src="<?php echo get_template_directory_uri(); ?>/images/transparent.gif">
				</a>

				<table class="table analytics">
					<tbody>
						<tr>
							<th style="width: 50%">Lots viewed</th>
							<th style="width: 50%">Level of interest</th>
						</tr>
						<tr>
							<td><span class="lots-viewed"></span></td>
							<td><span class="sizes-viewed"></span></td>
						</tr>

						<tr>
							<th>Items downloaded</th>
							<th>Pages viewed</th>
						</tr>
						<tr>
							<td><span class="lots-downloaded"></span></td>
							<td><span class="pages-viewed"></span></td>
						</tr>


					</tbody>
				</table>

			</div> <!-- /.portlet-body -->

		</div> <!-- /.portlet -->

	<?php }
	else { ?>
		<div class="portlet details" style="display:none;">

			<h3 class="portlet-title">
				<u>Contact <span class="contact-name">&nbsp;</span></u>
			</h3>

			<?php if (!current_user_can( 'read_contacts' )) {
				_e( 'You do not have sufficient permissions to view contacts.', 'meo_real_estate_admin' );
			}
			else {
				_e( "'analytics_id' parameter missing", 'meo_real_estate_admin' );
			} ?>

		</div> <!-- /.portlet -->

	<?php } ?>

<?php get_footer(); ?>
