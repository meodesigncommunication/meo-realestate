<?php

/* Custom GRAvatar */
function thb_gravatar ($avatar_defaults) {
	$myavatar = get_template_directory_uri() . '/assets/img/avatar.png';
	$avatar_defaults[$myavatar] = 'THB Gravatar';
	return $avatar_defaults;
}
add_filter('avatar_defaults', 'thb_gravatar');

/* Custom Comment Styling */
function enable_threaded_comments(){
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1))
			wp_enqueue_script('comment-reply');
		}
}
add_action('get_header', 'enable_threaded_comments');

function mytheme_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
	<div id="comment-<?php comment_ID(); ?>" class="comment-inner">
  	<figure class="vcard">
    	<?php echo get_avatar($comment,60); ?>
    </figure>
    <?php comment_reply_link(array_merge( $args, array('reply_text' => __('<i class="icon-reply"></i> Reply', THB_THEME_NAME), 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    <div class="commentmeta">
        <strong><?php comment_author_link(); ?></strong>
        <br>
        <span class="authorname">
            <?php comment_date('M d, Y'); ?> - <?php comment_time('h:i A'); ?>
            <?php edit_comment_link( "Edit", ' &mdash;  <i class="icon-edit"></i> '); ?>
        </span>
        
    </div>
    <div class="comment-text">
        <?php if ($comment->comment_approved == '0') : ?>
            <em class="awaiting_moderation"><?php _e('Your comment is awaiting moderation.', THB_THEME_NAME) ?></em>
        <?php endif; ?>
        <?php comment_text() ?>
    </div>
        
	</div>
<?php } 
function list_pings($comment, $args, $depth) {
$GLOBALS['comment'] = $comment;
?>
<li id="comment-<?php comment_ID(); ?>"><?php comment_author_link(); ?>
<?php } ?>