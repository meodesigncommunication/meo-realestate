#!/bin/sh

BIN_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

STEINER_ROOT=${BIN_DIR}/..
IVIZZIO_ROOT=${STEINER_ROOT}/../ivizzio

PLATFORMS=(ios android browser windows windows8)

echo "Clearing old build"
# Make certain the dest directory exists
mkdir -p ${STEINER_ROOT}/dest
touch ${STEINER_ROOT}/dest/dummy.txt
rm -rf ${STEINER_ROOT}/dest/*

# Copy in the ivizzio build
echo "Copying latest ivizzio code"
cp -r ${IVIZZIO_ROOT}/* ${STEINER_ROOT}/dest

echo "Removing ivizzio platform-specific builds"
cd ${STEINER_ROOT}/dest

for i in "${PLATFORMS[@]}"
do
	echo "    Removing $i"
	cordova platform rm $i
done

echo "Copying assets"
# Includes images and build hooks to overwrite ivizzio properties
cp -r ${STEINER_ROOT}/config/resources/* ${STEINER_ROOT}/dest

echo "Updating config.xml"
node ${BIN_DIR}/update-config.js ${STEINER_ROOT}/dest/config.xml ${STEINER_ROOT}/dest/config/properties.js ${IVIZZIO_ROOT}

echo "Please add the platforms manually, eg"
echo "    cordova platform add ios"

