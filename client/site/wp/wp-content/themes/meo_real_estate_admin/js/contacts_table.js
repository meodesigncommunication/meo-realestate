jQuery(function() {
        
	if (meo_settings['site_id'] < 0) {
		return;
	}
        
	function get_contact_view_link(analytics_id) {
		return '<a href="' + meo_settings['contact_url'] + '?analytics_id=' + analytics_id + '&site_id=' + meo_settings['site_id'] + '"><i class="fa fa-info-circle"></i></a>';
	}

	function get_contact_pdf_link(analytics_id) {
		return '<a href="' + meo_settings['contact_url'] + '?analytics_id=' + analytics_id + '&site_id=' + meo_settings['site_id'] + '&pdf=1"><i class="fa fa-file-pdf-o"></i></a>';
	}

	var site = meo_settings['sites'][meo_settings['site_id']],
	    request_url = ( site['allow_direct_requests'] == 1 ? site['site_url'] + site['api_url'] : meo_settings['ajax_url']);
            
	var table_1 = jQuery('#table-1').dataTable ({
		"sAjaxSource": request_url + "?action=get_contact_list&api-key=" + site['api_key'],
		"aoColumns": [
			{ "mData": first_contact_extractor },
			{ "mData": "name" },
			{ "mData": "phone" },
			{ "mData": "full_address" },
			{ "mData": "language" },
			{ "mData": function( data, type, extra ) {
									 var lots = data['lots'];
									 return lots.join('<br>');
								}
			},
			{ "mData": function( data, type, extra ) {
									 var analytics_id = data['analytics_id'];
									 return get_contact_view_link(analytics_id) +
									        get_contact_pdf_link(analytics_id);
								 }
			}
		],
		"iDisplayLength": -1, /* Show all rows */
		"fnInitComplete": function(oSettings, json) {
			jQuery(this).parents ('.dataTables_wrapper').find ('.dataTables_filter input').prop ('placeholder', meo_settings['search_placeholder']).addClass ('form-control input-sm');
		},
		"oLanguage": meo_settings['datatable_trans'],
		"aaSorting": [[0,'desc']]
	});
});
