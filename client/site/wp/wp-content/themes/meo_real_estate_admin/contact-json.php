<?php

/**
 * Template name: JSON Contact Details
 *
 * @package MEO real estate admin
 */

header('Content-Type: application/json');

$site_id = mrea_get_site_id();
$analytics_id = $_GET['analytics_id'];
$sites = mrea_get_sites_for_user();

if (!array_key_exists($site_id, $sites)) {
	echo json_encode(array(
		'success' => 0,
		'message' => __("You don't have permissions to view statistics for that site", 'meo_real_estate_admin')
	));
	exit;
}

if (empty($analytics_id)) {
	echo json_encode(array(
		'success' => 0,
		'message' => __("Missing analytics ID", 'meo_real_estate_admin')
	));
	exit;
}

$site = $sites[$site_id];

$contact_data = mrea_get_contact_data_by_analytics_id($site, $analytics_id);

echo json_encode($contact_data);
