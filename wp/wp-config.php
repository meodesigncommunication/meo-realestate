<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

if ($_SERVER['SERVER_ADDR'] == "127.0.0.1") {
	define('DB_NAME', 'meorealestatecom');
	define('DB_USER', 'meorealestatecom');
	define('DB_PASSWORD', 'password');
	define('DB_HOST', 'localhost');
}
else {
	define('DB_NAME', 'meorealestatecom');
	define('DB_USER', 'meo');
	define('DB_PASSWORD', 'thousoapqueenscreen');
	define('DB_HOST', 'mysql.meo-realestate.com');
	define('IS_PRODUCTION', true);
}

if (!defined('IS_PRODUCTION')) {
	define('IS_PRODUCTION', false);
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hC<V+$kPe+RPa$68I>wmQN&~*C/2U5/x1YZ7og-63R-@4rJ!2Y-*a5Gq9L+I-~0q');
define('SECURE_AUTH_KEY',  '(NIc3$dV&HT0YebdFQD-.YZER.L0zZDrh1`VY7=jL5&al!=zxl^.Og->(/ryD:WV');
define('LOGGED_IN_KEY',    '-v$P6E)-&~XgVk3.:Z->-].&c?lw3|L,s:h0w5/-ab8`Av]VDH&tHXVD$35Jbi,L');
define('NONCE_KEY',        '4RraWVNnmPw&K|/&-j>E%^/ZU=dV>OO+L}tfD<yOmU-INu?`U:rw-FV+.GK^Oq{/');
define('AUTH_SALT',        'c  ^b&S9e|XCdoA*|WP+3r1KmXPTiJ#-dNni<_`Ox_tR~eg UUjV|Y=?+,U7hTvS');
define('SECURE_AUTH_SALT', 'o| vj`P)w?,iZqQZ-NF:Uv|Z[>W:b1uR-z&H+M8CCX](y|+?Q3?i+AOo/+bK4hAj');
define('LOGGED_IN_SALT',   '/-;_.l<L[5y(za<:HGYVHo#1YZvfZ+BX`<EGZ)hd*#_q}gZa4#tuB{IHiji]Ni<h');
define('NONCE_SALT',       'NQ!Q5Gv!5W.yNc5FA.|,;_~Bd<K++?PL8wztvs-Ztc~l[0,^s{JV1[*!sP$KK%kT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
