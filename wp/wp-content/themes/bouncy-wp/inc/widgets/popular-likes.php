<?php
// thb Popular Posts by Likes
class widget_thbpopular extends WP_Widget {
       function widget_thbpopular() {
               /* Widget settings. */
               $widget_ops = array( 'description' => __('Display popular posts by number of likes',THB_THEME_NAME) );

               /* Widget control settings. */
               $control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'thbpopular' );

               /* Create the widget. */
               $this->WP_Widget( 'thbpopular', __('Popular Posts', THB_THEME_NAME), $widget_ops, $control_ops );
       }

       function widget($args, $instance) {
               extract($args);
               $title = apply_filters('widget_title', $instance['title']);
               $show = $instance['show'];
               global $post, $wpdb;
               $pop = new WP_Query( array ( 'post_type' => 'post', 'order' => 'DESC', 'orderby' => 'meta_value_num', 'meta_key' => '_likes', 'posts_per_page' => $show ) );
               echo $before_widget;
               echo $before_title;
               echo $title;
               echo $after_title;
               echo '<ul>';
               while  ($pop->have_posts()) : $pop->the_post(); ?>
			           <li>
			               <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
			               <span><?php echo thb_likeThis(get_the_id()); ?> <?php _e('likes', THB_THEME_NAME); ?></span>
			           </li>
	           	 <?php 
	           	 endwhile;
               echo '</ul>';
               echo $after_widget;
       }
       function update( $new_instance, $old_instance ) {
               $instance = $old_instance;

               /* Strip tags (if needed) and update the widget settings. */
               $instance['title'] = strip_tags( $new_instance['title'] );
               $instance['show'] = strip_tags( $new_instance['show'] );

               return $instance;
       }
       function form($instance) {
               $defaults = array( 'title' => 'Popular Posts', 'show' => '3' );
               $instance = wp_parse_args( (array) $instance, $defaults ); ?>

               <p>
                       <label for="<?php echo $this->get_field_id( 'title' ); ?>">Widget Title:</label>
                       <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
               </p>

               <p>
                       <label for="<?php echo $this->get_field_id( 'name' ); ?>">Number of Posts:</label>
                       <input id="<?php echo $this->get_field_id( 'name' ); ?>" name="<?php echo $this->get_field_name( 'show' ); ?>" value="<?php echo $instance['show']; ?>" style="width:100%;" />
               </p>
   <?php
       }
}
function widget_thbpopular_init()
{
       register_widget('widget_thbpopular');
}
add_action('widgets_init', 'widget_thbpopular_init');

?>