;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
  		win = $(window),
      Modernizr = window.Modernizr;

  var SITE = SITE || {};
  
  SITE = {
    init: function() {
      var self = this,
      obj;
      
      for (obj in self) {
        if ( self.hasOwnProperty(obj)) {
          var _method =  self[obj];
          if ( _method.selector !== undefined && _method.init !== undefined ) {
            if ( $(_method.selector).length > 0 ) {
              if ( _method.dependencies !== undefined ) {
                (function(_async) {
                  Modernizr.load([
                  {
                    load: _async.dependencies,
                    complete: function () {
                      _async.init();
                    }
                  }]);
                })(_method);             
              } else {
                _method.init();
              }
            }
          }
        }
      }
    },
    navResize: function() {
    	var base = this,
    			offset = $(window).scrollTop(),
    			h = $('#header'),
    			s = $('.headersearch'),
    			l = $('.logo>a'),
    			src = l.find('img').attr('src'),
    			mobilelogo = l.find('img').data('mobile-src'),
    			m = $('.sf-menu, .searchlink');
    	
    	
    		
    	$(h,l).removeAttr("height");
    	if( $(window).width() > 940){ 
    		if(!$(document.body).hasClass('notfixed')) {
	    		if (offset > 100) {
	    			
	    			if(!h.hasClass('small')) {
		    			h.animate({ 
		    	      height: "50px",
		    	    }, {queue:false, duration:250, easing: 'easeOutCubic', complete: function() {
		    	      h.addClass('small');
		    	    }});
		    	    s.animate({ 
		    	      top: "50px",
		    	    }, {queue:false, duration:250, easing: 'easeOutCubic'});
		    	    l.animate({ 
		    	      height: "40px",
		    	      margin: "5px 0"
		    	    }, {queue:false, duration:250, easing: 'easeOutCubic'});
		    	    m.animate({ 
		    	      margin: "0"
		    	    }, {queue:false, duration:250, easing: 'easeOutCubic'});
	    	    }
	    		} else if (offset > -1 && offset < 100){
	    		
	    			h.animate({ 
	    			  height: "100px",
	    			}, {queue:false, duration:250, easing: 'easeOutCubic', complete: function() {
	    			  h.removeClass('small');
	    			}});
	    			s.animate({ 
	    			  top: "100px",
	    			}, {queue:false, duration:250, easing: 'easeOutCubic'});
	    			
	    			l.stop().animate({ 
	    			  height: "80px",
	    			  margin: "10px 0"
	    			}, {queue:false, duration:250, easing: 'easeOutCubic'});
	    			m.animate({ 
	    			  margin: "25px 0"
	    			}, {queue:false, duration:250, easing: 'easeOutCubic'});
	    		}
    		}
    	}
    	
    },
    stickyFooter: function() {
    	var el = $("footer"),
    			b = $("#wrapper"),
    			h = el.outerHeight(true);
    		el.removeAttr('style');
    		b.css('margin-bottom', h * -1).css('padding-bottom', h);
    },
    responsiveNav: {
    	selector: '#mobile-toggle',
    	target: '.mobile-menu',
    	init: function() {
    	  var base = this,
    	  container = $(this.selector),
    	  target = $(this.target);

    	  container.click(function(){
    	  	target.stop(true,true).slideToggle(500);
    	  	return false;
    	  });
    	  
    	  target.find('ul li').each(function(){
    	  	if($(this).find('> ul').length > 0) {
    	  		 $(this).find('> a').append('<span><i class="icon-angle-down"></i></span>');
    	  	}
    	  });
    	  
    	  target.find('ul li:has(">ul") > a').click(function(){
    	  	$(this).toggleClass('active');
    	  	$(this).parent().find('> ul').stop(true,true).slideToggle();
    	  	return false;
    	  });
    	},
    	toggle: function() {
    		if( $(window).width() > 767 ){
    			var base = this,
    			target = $(this.target);
    			
    			target.hide();
    		} 
    	}
    },
    superfish: {
    	selector: '.sf-menu',
    	init: function() {
    	  var base = this,
    	  container = $(this.selector);
    	  container.superfish({ 
    	      delay:       100,                            // one second delay on mouseout 
    	      animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
    	      speed:       'fast',                          // faster animation speed 
    	      autoArrows:  false                           // disable generation of arrow mark-up 
    	  });
    	}
    },
    searchtoggle: {
    	selector: '.searchlink',
    	target: '.headersearch',
    	init: function() {
    		var base = this,
    		container = $(this.selector),
    		target = $(this.target);
    		
    		container.on('click', function(){
    			
    			if (target.height() == '0') {
	    		  target.animate({ 
	    		    height: "80px"
	    		  }, { queue:false, duration:250, easing: 'easeOutCubic', complete: function() {
	    		    target.add(container).addClass('open');
	    		  }});
	    		  
    		  } else {
    		  	target.animate({ 
    		  	  height: "0px"
    		  	}, { queue:false, duration:250, easing: 'easeOutCubic', complete: function() {
    		  	  target.add(container).removeClass('open');
    		  	}});
    		  }
    		  return false;
    		});


    		$(document.body).bind('click', function(e){
					if(!$.contains(target[0],e.target)) {
					  target.animate({ 
					    height: "0px"
					  }, { queue:false, duration:250, easing: 'easeOutCubic', complete: function() {
					    target.removeClass('open');
					  }});
					}
    		});
    	}
    },
    flex: {
      selector: '.flex',
      init: function() {
        var base = this,
        		container = $(this.selector);
        container.each(function() {
        	var that = $(this),
        			controls = (that.data('controls') == false ? false : true);
        	
        	that.imagesLoaded(function() {
        		that.flexslider({
        		  animation: "fade",
        		  directionNav: controls,
        		  animationSpeed: 800,
        		  useCSS: false,
        		  prevText: '<i class="icon-angle-left"></i>',
        		  nextText: '<i class="icon-angle-right"></i>',
        		  start: function(slider) {
        		  	that.removeClass('flex-start');
        				if (slider.hasClass('slider')) {
        		  	  var cs = slider.slides.eq(slider.currentSlide),
        		  	  		content = cs.find('.flex-caption');
        		  		cs.addClass('animated');
        		  		
        		  		content.css('margin-top', function() {
        		  			var h = parseInt(content.height() / -2);
        		  			return h;
        		  		});
        				}
        				
        				that.magnificPopup({
        		  	  delegate: 'a.enlarge',
        		  	  type: 'image',
        		  	  fixedContentPos: false,
        					closeOnContentClick: true,
        					closeBtnInside: false,
        					removalDelay: 300,
        					mainClass: 'my-mfp-slide-bottom',
        		  	  gallery: {
        		  	    enabled: true,
        		  	    navigateByImgClick: false,
        		  	    preload: [0,1]
        		  	  },
        		  	  image: {
        		  	  	verticalFit: true
        		  	  }
        		  	});
        		  },
        		  before: function(slider) {
        			  if (slider.hasClass('slider')) {
        		  	  var cs = slider.slides.eq(slider.currentSlide),
        		  	  		el = cs.find('.flex-caption h2, .flex-caption h3, .flex-caption a, .flex-caption .video').length;
        		  	  		cs.removeClass('animated');
        		  	  
        		      cs.delay((el * 400) + 500);
        				}
        		  },
        		  after: function(slider) {
        		  	if (slider.hasClass('slider')) {
        			  	var cs = slider.slides.eq(slider.currentSlide),
        			  			content = cs.find('.flex-caption'),
        			  			el = cs.find('.flex-caption h2, .flex-caption h3, .flex-caption a, .flex-caption .video').length;
        			  			
        			  	slider.slides.eq(slider.currentSlide).addClass('animated');
        			  	content.css('margin-top', function() {
        			  		var h = parseInt(content.height() / -2);
        			  		return h;
        			  	});
        			  	
        			  	cs.delay((el * 400) + 500);
        		  	}
        		  }
        		});
        		
        		if ($.isFunction($.fn.isotope) && $('.blog.masonry').hasClass('isotope')) { 
        			$('.blog.masonry').isotope( 'reLayout'); 
        		}
        	});     	
        });
      }
    },
    scrollEffect: {
    	selector: '.slider',
    	portfolio: '.portfolio-header',
    	init: function() {
	    	var base = this,
	    			container = $(this.selector),
	    			portfolio = $(this.portfolio),
	    			t = win.scrollTop();
	    			
	    			if ($(window).width() > 767 ) {
		    		  if (Modernizr.csstransitions) {
		    		  	container.find('.hidden').css({
		    		  		'-webkit-transform': 'translateY(' +  t / 2 + 'px)',
		    		  		'-moz-transform': 'translateY(' +  t / 2 + 'px)',
		    		  		'transform': 'translateY(' +  t / 2 + 'px)'
		    		  	});	
		    		  }
		    		  container.find('.flex-caption').css({'opacity' : 1 - (t/600), 'top' : 50 + (t/20) + '%'});
		    		  container.find('.flex-direction-nav a').css({'top' : 50 + (t/20) + '%'});
	    			}
    	}
    },
    toggle: {
      selector: '.toggle .title',
      init: function() {
        var base = this,
        container = $(this.selector);
        container.each(function() {
        	$(this).toggle(function(){
        		$(this).addClass("toggled").find('i').removeClass('icon-plus').addClass('icon-minus').end().closest('.toggle').find('.inner').slideDown(400);
        		}, function () {
        		$(this).removeClass("toggled").find('i').removeClass('icon-minus').addClass('icon-plus').end().closest('.toggle').find('.inner').slideUp(400);
        	});
        });
      }
    },
    blogMasonry: {
    	selector: '.blog.masonry',
    	loadmore: '#loadmore',
    	init: function() {
    		var base = this,
    		container = $(base.selector),
    		loadmore = $(this.loadmore),
    		page = 1;
    		
    		$(window).load(function() {
    			container.isotope({
    				itemSelector : '.item',
    				layoutMode : 'masonry',
    				resizable: false,
    				animationOptions: {
    					duration: 1000,
    					easing: 'linear',
    					queue: false
    				}
    			});
    		});
    		
    		loadmore.live('click', function(){
    			var text = loadmore.text(),
    					icon = loadmore.children(),
    					loading = loadmore.data('loading'),
    					nomore = loadmore.data('nomore'),
    					count = loadmore.data('count');
    			
    			loadmore.text(loading).append('<i class="icon-spinner icon-spin"></i>');
    			
    			$.post( themeajax.url, { 
    			
    					action: 'load_more_posts', 
    					page : page++ 
    					
    			}, function(data){
    			
    				var d = $.parseHTML(data),
    						l = ($(d).length - 1) / 2;
    						
    				if( data === '' || data === 'undefined' || data === 'No More Posts' || data === 'No $args array created') {
    					data = '';
    					loadmore.text(nomore).append('<i class="icon-remove"></i>');
    				} else if (l < count){
    					loadmore.text(nomore).append('<i class="icon-remove"></i>');
    					
    					$(d).appendTo(container).hide().imagesLoaded(function() {
    						$(d).show();
    						
    						container.isotope( 'appended', $(d), function() {
    							SITE.flex.init();
    							SITE.magnific.init();
    							SITE.jplayer.init();
    						});
    					});
    				} else{
    					
    					loadmore.text(text).append(icon);
    					
    					$(d).appendTo(container).hide().imagesLoaded(function() {
    						$(d).show();
    						
    						container.isotope( 'appended', $(d), function() {
    							SITE.flex.init();
    							SITE.magnific.init();
    							SITE.jplayer.init();
    						});
    					});
    				}
    			});
    			return false;
    		});
    		
    		base.resize(container);
    	},
    	resize: function(container) {
    		$(window).smartresize(function(){
    				container.isotope({
    				masonry: { columnWidth: container.width() / 3 }
    			});	
    		});
    	}
    },
    portfolio: {
      selector: '.thbportfolio.ajax',
      init: function() {
        var base = this,
        container = $(this.selector);
        $(window).load(function() {
	        container.isotope({
	        	itemSelector : '.item',
	        	layoutMode : 'fitRows',
	        	resizable: false,
	        	animationOptions: {
	        		duration: 1000,
	        		easing: 'linear',
	        		queue: false
	        	}
	        });
        });
        $('.filters a').click(function(){
        	$('.filters a').removeClass('active');
        	$(this).addClass('active');
        	var selector = $(this).attr('data-filter');
        	container.isotope({ filter: selector }, SITE.stickyFooter());
        	return false;
        });
        
        $('#portfolioselect a').click(function(){
        	$('#portfolioselect a').removeClass('active');
        	$(this).addClass('active');
        	var selector = $(this).attr('data-filter');
        	$('#portfolioselect').toggleClass('open');
        	$('#portfolioselect').find('ul').stop(true,true).slideToggle(600,'easeOutExpo', function() {
        		container.isotope({ filter: selector }, SITE.stickyFooter());
        	});
        	return false;
        });

       
        base.resize(container);
      },
      resize: function(container) {
      	var cols = container.data('columns');
      	
      	$(window).smartresize(function(){
      	    container.isotope({
      			masonry: { columnWidth: container.width() / cols }
      		});	
      	});
      }
    },
    portfolioPaginated: {
    	selector: '.thbportfolio.paginated',
    	init: function() {
    	  var base = this,
    	  container = $(this.selector),
    	  cols = container.find('.item'),
    	  tallest = 0;
    	  
    	  cols.removeAttr('style');
    	  cols.imagesLoaded(function() {
	  	  	cols.each(function() {
	  	  		var thisHeight = $(this).height();
	  	  		if(thisHeight > tallest) {
	  	  			tallest = thisHeight;
	  	  		}
	  	  	});
	  	  	cols.height(tallest);
  	  	});
  	  	
  	  	
  	  	$('#portfolioselect a').on('click', function(){
  	  	  	$(this).parent().toggleClass('open');
  	  	  	$(this).parent().find('ul').stop(true,true).slideToggle(600,'easeOutExpo');
  	  	});
    	}
    },
    likethis: {
    	selector: '.likeThis',
    	init: function() {
    		var base = this,
    		container = $(this.selector);
    		
    		container.on('click', function() {
    			var that = $(this),
    					classes = that.attr('class').split(' '),
    					id = that.data('id'),
    					blogurl = $('body').data('url');
    			
    			if (that.hasClass('active')) {
    				return false;
    			} else {
	    			$.ajax({
	    			  type: "POST",
	    			  url: blogurl + "/index.php",
	    			  data: "likepost=" + id,
	    			  success: function() {
	    			  	var text = $('.likeThis[data-id='+id+']').html(),
	    			  			patt= /(\d)+/,
	    			  			num = patt.exec(text);
	    			  			
	    			  	num[0]++;
	    			  	text = text.replace(patt,num[0]);
	    			  	$('.likeThis[data-id='+id+']').html(text);
	    			  	that.addClass("active");
	    			  }
	    			});
	    		}
    			return false;
    		});
    	}
    },
    magnific: {
    	selector: '[rel=magnific]',
    	galleryselector: '.gallery',
    	flex: '.flex',
    	init: function() {
    		var base = this,
		    		container = $(this.selector),
		    		gallerycontainer = $(this.galleryselector),
		    		flex = $(this.flex);
    		
    		container.magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          fixedContentPos: false,
          removalDelay: 300,
          mainClass: 'my-mfp-slide-bottom',
          image: {
          	verticalFit: true
          }
        });
        
        gallerycontainer.magnificPopup({
          delegate: 'a',
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          fixedContentPos: false,
          removalDelay: 300,
          mainClass: 'my-mfp-slide-bottom',
          image: {
          	verticalFit: true
          }
        });
  
    	}
    },
    carousel: {
    	selector: '.carousel',
    	init: function() {
    		var base = this,
    				container = $(base.selector);
    				
    		container.each(function() {
    			var that = $(this),
    					columns = that.data('columns'),
    					prev = $(that.data('prev')),
    					next = $(that.data('next')),
    					autoplay = (that.data('autoplay') === false ? false : true);
    			
    			that.owlCarousel({
            //Basic Speeds
            slideSpeed : 1200,
            
            //Autoplay
            autoPlay : autoplay,
            goToFirst : true,
            stopOnHover: true,
            
            // Navigation
            navigation : false,
            pagination : false,
            
            // Responsive
            responsive: true,
            items : columns,
            itemsDesktop: false,
            itemsDesktopSmall : [980,(columns < 3 ? columns : 3)],
            itemsTablet: [768,(columns < 2 ? columns : 2)],
            itemsMobile : [479,1]
    			});
    			
    			var owl = that.data('owlCarousel');
    			
    			prev.on('click', function() {
    				owl.prev();
    				return false;
    			});
    			
    			next.on('click', function() {
    				owl.next();
    				return false;
    			});
    		});
    	}
    },
    dial: {
    	selector: '.dial',
    	init: function() {
    		var base = this,
    		container = $(this.selector),
    		p = container.parent('.knob');
    		
    		container.each(function(i) {
    			var that = $(this),
    					ao = Math.round(Math.random() * 360),
    					w = container.data('width'),
    					v = that.data('value');
    			that.addClass('visible').knob({
    				readOnly: true,
    				bgColor: '#ebebeb',
    				fgColor: '#e24f3d',
    				thickness: 0.25,
    				angleOffset: ao,
    				width: w
    			});
    			$({value: 0}).animate({value: v}, {
  			    duration: 2000,
  			    easing:'easeOutQuad',
  			    step: function() {
  			    	that.val(Math.ceil(this.value)).trigger('change');
  			    }
    			})
    		});
    	}
    },
    parsley: {
    	selector: 'form',
    	init: function() {
    		var base = this,
    		container = $(this.selector);
    		
    		if ($.fn.parsley) {
    			container.parsley();
    		}
    	}
    },
    toTop: {
      selector: '#scrolltotop',
      init: function() {
      	var base = this,
      	container = $(this.selector);
        container.on('click', function(){
            $("html, body").animate({ scrollTop: 0 }, 800, 'easeOutCubic');
            return false;
        });
      },
      scroll: function() {
      	var base = this,
      	container = $(this.selector);
      	if (($doc.height() - (win.scrollTop() + win.height())) < 550) {
      	    container.fadeIn('600');
      	} else {
      	    container.fadeOut('300');
      	}
      }
    },
    twitterBar: {
      selector: '.tweets ul',
      init: function() {
      	var base = this,
      	container = $(this.selector);

      	container.totemticker({
					row_height	:	'50px',
					next		:	'#twitter-next',
					previous	:	'#twitter-prev',
					mousestop	:	true,
					max_items : 1
				});
      }
    },
    contact: {
      selector: '#contact-map',
      init: function() {
      	var base = this,
      	container = $(this.selector),
      	mapzoom = container.data('map-zoom'),
      	maplat = container.data('map-center-lat'),
      	maplong = container.data('map-center-long'),
      	mapinfo = container.data('pin-info'),
      	pinimage = container.data('pin-image');
      	var styles=[{"featureType":"landscape","stylers":[{"hue":"#FFC600"},{"saturation":50.599999999999994},{"lightness":37.79999999999998},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#89CD6C"},{"saturation":-0.9174311926605583},{"lightness":-15.799999999999997},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF9F00"},{"saturation":14.999999999999986},{"lightness":1.5999999999999943},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#00FFFD"},{"saturation":0},{"lightness":0},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#00A9FF"},{"saturation":0},{"lightness":6.800000000000011},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#9FFF00"},{"saturation":0},{"lightness":0},{"gamma":1}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]}      	];
      	    
      	
      	var latLng = new google.maps.LatLng(maplat,maplong);
      	
				var mapOptions = {
				    center: latLng,
				    styles: styles,
				    zoom: mapzoom,
				    mapTypeId: google.maps.MapTypeId.ROADMAP,
				    scrollwheel: false,
				    panControl: false,
				  	zoomControl: 1,	  
				  	zoomControlOptions: {
				      style: google.maps.ZoomControlStyle.LARGE,
				      position: google.maps.ControlPosition.LEFT_CENTER
				 	  },
				  	mapTypeControl: false,
				  	scaleControl: false,
				  	streetViewControl: false
				  };
				
				var map = new google.maps.Map(document.getElementById("contact-map"), mapOptions);
				
				google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
						var venuemarker = new google.maps.Marker({
							  position: latLng,
							  map: map,
							  icon: pinimage,
							  animation: google.maps.Animation.DROP
						});
						map.setCenter(latLng);
						
					if (mapinfo) { 
						var infowindow = new google.maps.InfoWindow({
						    content: mapinfo
						});
						
						infowindow.open(map,venuemarker);
						
						map.setCenter(latLng);
						map.panBy(40, -80);
					
					}
				});
				
      }
    },
    styleSwitcher: {
      selector: '#style-switcher',
      init: function() {
      	var base = this,
      	container = $(this.selector),
      	toggle = container.find('.style-toggle'),
      	onoffswitch = container.find('.switch');
      	
      	toggle.on('click', function() {
      		container.add($(this)).toggleClass('active');
					return false;
      	});
      	
      	onoffswitch.each(function() {
      		var that = $(this);
      		$(this).find('a').on('click', function() {
      			that.find('a').removeClass('active');
      			$(this).addClass('active');
      			
      			if ($(this).parents('ul').data('name') == 'boxed') {
	      			$(document.body).removeClass('boxed');
	      			$(document.body).addClass($(this).data('class'));
      			}
      			if ($(this).parents('ul').data('name') == 'header') {
      				$(document.body).removeClass('notfixed');
      				$(document.body).addClass($(this).data('class'));
      				
      				$('#header, #header .logo a, #header .desktop-menu ul, #header .desktop-menu .searchlink, .headersearch').attr( "style", "" );
      				$('#header').removeClass('fixed').removeClass('small');
      				$('#header').addClass($(this).data('class2'));
      			}
      			return false;
      		});
      	});
      	
      	var style = $('<style type="text/css" id="theme_color" />').appendTo('head');
      	$('.miniColors').each(function(){
	      	$(this).minicolors({
	      		defaultValue: $(this).data('default'),
	  				change: function(hex, rgba) {
	  					style.html(' .sf-menu li.current-menu-item, .pagenavi ul li.disabled a, .pagenavi ul li a:hover, .iconbox.hexagon:hover > span, .filters li a.active, .widget.widget_flickr .flickr_badge_image:hover, .gotoportfolio:hover, .widget.widget_tag_cloud li a:hover, .post .post-gallery.link:hover, .pricing .item.featured .header, .btn:hover, input[type=submit]:hover, .comment-reply-link:hover { background:'+hex+'; } dl.tabs dd.active a, .clientlist .columns div:hover, .widget.widget_flickr .flickr_badge_image:hover, .pricing .item.featured .header h2 { border-color: '+hex+'; } a:hover, footer a:hover, footer .subfooter a:hover, .iconbox.left:hover > span, #scrolltotop:hover i, #header .searchlink:hover, #header .searchlink.open,.single-portfolio .gotoportfolio:hover i { color: '+hex+'; } ::-webkit-selection{ background-color: '+hex+'; } ::-moz-selection{ background-color: '+hex+'; } ::selection{ background-color: '+hex+'; }');	
	  				}
	  			});
  			});
      }
    }
  };
  
  // on Resize & Scroll
	$(window).resize(function() {
		SITE.stickyFooter();
		SITE.responsiveNav.toggle();
		SITE.portfolioPaginated.init();
	});
  $(window).scroll(function(){
  	SITE.navResize();
  	SITE.toTop.scroll();
  	SITE.scrollEffect.init();
  });
  
  $doc.ready(function() {
  	FastClick.attach(document.body);
  	
    $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
    $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
    $.fn.foundationTabs             ? $doc.foundationTabs() : null;
    
    SITE.init();
  });

})(jQuery, this);

