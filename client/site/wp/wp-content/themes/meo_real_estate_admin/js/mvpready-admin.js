/* ========================================================
*
* MVP Ready - Lightweight & Responsive Admin Template
*
* ========================================================
*
* File: mvpready-admin.js
* Version: 1.0.0
* Author: Jumpstart Themes
* Website: http://mvpready.com
*
* ======================================================== */

var mvpready_admin = function () {

	"use strict";

	var initLayoutToggles = function () {
		jQuery('.navbar-toggle, .mainnav-toggle').click (function (e) {
			jQuery(this).toggleClass ('is-open');
		});
	};

	var initNoticeBar = function () {
		jQuery('.noticebar > li > a').click (function (e) {
			if (mvpready_core.isLayoutCollapsed ()) {
				window.location = jQuery(this).prop ('href');
			}
		});
	};

	return {
		init: function () {
			// Layouts
			mvpready_core.navEnhancedInit ();
			mvpready_core.navHoverInit ({ delay: { show: 250, hide: 350 } });
			initLayoutToggles ();
			initNoticeBar ();

			// Components
			mvpready_core.initAccordions ();
			mvpready_core.initFormValidation ();
			mvpready_core.initTooltips ();
			mvpready_core.initBackToTop ();
			mvpready_core.initLightbox ();
		}
	};

}();

jQuery(function () {
	mvpready_admin.init ();
});
