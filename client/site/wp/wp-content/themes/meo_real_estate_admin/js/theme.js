var new_field_id = -1; // Keep them negative to distinguish from existing fields

var settings_form_handler = function(form_selector, message_selector, success_callback) {
	jQuery(form_selector).submit(function(e) {

		e.preventDefault();

		jQuery(message_selector).slideUp();

		var form = jQuery(this),
		    formData= form.serialize();

		jQuery.ajax({
			url: meo_settings['ajax_url'],
			type: 'POST',
			data: formData,
		})
		.done(function(result) {
			var message_class = result.success ? 'alert-success' : 'alert-danger';

			var message_html = '<div class="alert ' + message_class + '">' +
			                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
			                       result.message +
			                   '</div>';

			jQuery(message_selector).html(message_html).slideDown();

			if (result.success && typeof success_callback === "function") {
				success_callback();
			}
		})
		.fail(function() {
			var message_html = '<div class="alert alert-warning">' +
			                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
			                       'Unexpected error' +
			                   '</div>';

			jQuery(message_selector).html(message_html).slideDown();
		});
	});
};

(function($){

	function handle_site_change() {
		$('.site-selector select').change(function(){
			$(this).closest('form').submit();
		});
	}

	function handle_notification_site_change() {
		$('#messaging form select').change(function(){
			var site_id = $(this).val(),
			    site = meo_settings['sites'][site_id],
			    fields = ["email_new_contact","email_weekly_stats"],
			    numfields = fields.length;

			for (var i = 0; i < numfields; i++) {
				var field = fields[i];
				if (site[field]) {
					$('#messaging form .' + field).val(site[field]);
				}
				else {
					$('#messaging form .' + field).val('');
				}
			}
		});
	}

	function handle_settings_forms() {
		settings_form_handler('#profile-tab form',  '.profile-message',   false);
		settings_form_handler('#password-tab form', '.password-message',  function() {
			$('#password-tab form')[0].reset();
		} );
		settings_form_handler('#messaging form',    '.messaging-message', function() {
			var site_id = $('#messaging form select').val(),
			    fields = ["email_new_contact","email_weekly_stats"],
			    numfields = fields.length;

			for (var i = 0; i < numfields; i++) {
				var field = fields[i];
				meo_settings['sites'][site_id][field] = $('#messaging form .' + field).val();
			}
		});
		settings_form_handler('#app_users form', '.new-app-user-message',  function() {
			$('#app_users form')[0].reset();
			get_app_users_for_site();
		} );
	}

	function get_app_users_site_id() {
		return parseInt($('#app_users form [name="site_id"]').val(), 10);
	}

	function get_app_users_for_site() {
		var $target = $('.user-list');
		if ($target.length === 0) {
			return;
		}

		$target.html(meo_settings['datatable_trans']['sLoadingRecords']);

		var site_id = get_app_users_site_id(),
			data = {
				'action' : 'get_app_users_for_site',
				'site_id' : site_id
			};

		$.ajax({
			url: meo_settings['ajax_url'],
			type: 'POST',
			data: data,
		})
		.done(function(result) {
			var message_html = '';
			if (result.success) {
				message_html = '<table class="app-users table table-striped table-bordered"><thead><tr><th>&nbsp;</th><th>Login</th><th>Connections</th><th>Last Connection</th></tr></thead><tbody>';
				$.each(result.users, function(index, value){
					message_html += '<tr><td><a class="delete-user" href="#" data-user-id="' + value['id'] + '"><i class="fa fa-minus-circle"></i></a></td><td class="user-login">' + value['login'] + '</td> <td class="user-total-connections">' + value['total_connections'] + '</td> <td class="user-last-connection">' + value['last_connection'] + '</td></tr>';
				});
				message_html += '</tbody></table>';
			}
			else {
				message_html = result.message;
			}

			$target.html(message_html).slideDown();

			add_app_user_delete_handler();
		})
		.fail(function() {
			var message_html = '<div class="alert alert-warning">' +
			                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
			                       'Unexpected error' +
			                   '</div>';

			$target.html(message_html).slideDown();
		});
	}

	function add_custom_field_handlers() {
		$('.custom-field-edit').on({
				click: function(e) {
					e.preventDefault();
					$(this).closest('.form-group').remove();
				}
			},
			'.delete-field'
		);

		$('.custom-field-add').click( function(e) {
			e.preventDefault();

			var insert_before = $(this).closest('.form-group'),
			    field_id = new_field_id--;

			var to_insert = '<div class="form-group">' +
			                    '<div class="col-md-7">' +
			                        '<input type="text" name="field_value[' + field_id + ']" value="" class="form-control" />' +
			                    '</div>' +
			                    '<div class="col-md-3"><a class="close delete-field" href="#" aria-hidden="true">×</a></div>' +
			                '</div>';

			$(to_insert).insertBefore(insert_before);

		});
	}

	function add_app_user_delete_handler() {
		$('.delete-user').off('click').on('click', function(e) {
			e.preventDefault();

			if (!confirm("Are you sure you want to remove this user's access?")) {
				return;
			}

			var $target = $('.user-list'),
			    site_id = get_app_users_site_id(),
			    user_id = $(this).data('user-id');

			$target.html(meo_settings['datatable_trans']['sProcessing']);

			var data = {
					'action' : 'revoke_user_access_for_site',
					'site_id' : site_id,
					'user_id' : user_id
				};

			$.ajax({
				url: meo_settings['ajax_url'],
				type: 'POST',
				data: data,
			})
			.done(function(result) {
				if (result.success) {
					get_app_users_for_site();
				}
				else {
					$target.html(result.message).slideDown();
				}
			})
			.fail(function() {
				var message_html = '<div class="alert alert-warning">' +
				                       '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>' +
				                       'Unexpected error' +
				                   '</div>';

				$target.html(message_html).slideDown();
			});
		});
	}

	function add_app_user_site_select_handler() {
		$('#app_users select[name="site_id"]').change(function() {
			get_app_users_for_site();
		});
	}

	// Document ready function
	$(function() {
		handle_site_change();
		handle_notification_site_change();
		handle_settings_forms();
		add_custom_field_handlers();
		get_app_users_for_site();
		add_app_user_site_select_handler();
	});

})(jQuery);
