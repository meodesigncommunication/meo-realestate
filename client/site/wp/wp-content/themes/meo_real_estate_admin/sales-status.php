<?php
/**
 * Template name: Sales Status
 *
 * @package MEO real estate admin
 */

$site_id = mrea_get_site_id();
$uses_crm = mrea_uses_crm($site_id);

if ($uses_crm) {
	get_template_part( 'sales-status', 'crm' );
}
else {
	get_template_part( 'sales-status', 'nocrm' );
}
