<?php
/**
 * Template name: Settings
 *
 * @package MEO real estate admin
 */

global $meocrm;

$current_user = wp_get_current_user();
$site_id = mrea_get_site_id();
$sites = mrea_get_sites_for_user();

$custom_fields = array();
$can_add_app_users = false;
if (!empty($meocrm)) {
	$custom_fields = $meocrm->getCustomFieldsForUser(true);
	$can_add_app_users = $meocrm->canAddAppUsers();
}

get_header();

?>

	<div class="layout layout-main-right layout-stack-sm">

		<div class="col-md-3 col-sm-4 layout-sidebar">

			<ul id="myTab" class="nav nav-layout-sidebar nav-stacked">
					<li class="active">
					<a href="#profile-tab" data-toggle="tab">
					<i class="fa fa-user"></i>
					&nbsp;&nbsp;<?php _e( 'Profile Settings', 'meo_real_estate_admin' ); ?>
					</a>
				</li>

				<li>
					<a href="#password-tab" data-toggle="tab">
					<i class="fa fa-lock"></i>
					&nbsp;&nbsp;<?php _e( 'Change Password', 'meo_real_estate_admin' ); ?>
					</a>
				</li>

				<?php if (current_user_can( 'read_contacts' ) && count($sites) >= 1 ) { ?>
					<li>
						<a href="#messaging" data-toggle="tab">
						<i class="fa fa-bullhorn"></i>
						&nbsp;&nbsp;<?php _e( 'Notifications', 'meo_real_estate_admin' ); ?>
						</a>
					</li>
				<?php } ?>

				<?php
				if (!empty($custom_fields)) {
					foreach ($custom_fields as $custom_field) { ?>
						<li>
							<a href="#custom-field-<?php echo $custom_field['id']; ?>-tab" data-toggle="tab">
							<i class="fa fa-bars"></i>
							&nbsp;&nbsp;<?php echo $custom_field['name']; ?>
							</a>
						</li>
					<?php }
				}

				if (!empty($can_add_app_users) && $can_add_app_users) { ?>
					<li>
						<a href="#app_users" data-toggle="tab">
						<i class="fa fa-tablet"></i>
						&nbsp;&nbsp;<?php _e( 'App Users', 'meo_real_estate_admin' ); ?>
						</a>
					</li>
				<?php } ?>

			</ul>

		</div> <!-- /.col -->



		<div class="col-md-9 col-sm-8 layout-main">

			<div id="settings-content" class="tab-content stacked-content">

				<div class="tab-pane fade in active" id="profile-tab">

					<h3 class="content-title"><u><?php _e( 'Edit Profile', 'meo_real_estate_admin' ); ?></u></h3>

					<form action="<?php echo admin_url('admin-ajax.php'); ?>" class="form-horizontal">

						<input type="hidden" name="action" value="update_profile" />

						<div class="form-group">

							<label class="col-md-3"><?php _e( 'Username', 'meo_real_estate_admin' ); ?></label>

							<div class="col-md-7">
								<input type="text" name="user-name" value="<?php echo $current_user->user_login; ?>" class="form-control" disabled />
							</div> <!-- /.col -->

						</div> <!-- /.form-group -->



						<div class="form-group">

							<label class="col-md-3"><?php _e( 'First Name', 'meo_real_estate_admin' ); ?></label>

							<div class="col-md-7">
								<input type="text" name="first-name" value="<?php echo $current_user->user_firstname; ?>" class="form-control" />
							</div> <!-- /.col -->

						</div> <!-- /.form-group -->



						<div class="form-group">

							<label class="col-md-3"><?php _e( 'Last Name', 'meo_real_estate_admin' ); ?></label>

							<div class="col-md-7">
								<input type="text" name="last-name" value="<?php echo $current_user->user_lastname; ?>" class="form-control" />
							</div> <!-- /.col -->

						</div> <!-- /.form-group -->



						<div class="form-group">

							<label class="col-md-3"><?php _e( 'Email Address', 'meo_real_estate_admin' ); ?></label>

							<div class="col-md-7">
								<input type="text" name="email-address" value="<?php echo $current_user->user_email; ?>" class="form-control" />
							</div> <!-- /.col -->

						</div> <!-- /.form-group -->

						<div class="form-group">

							<div class="profile-message"></div>

						</div> <!-- /.form-group -->

						<div class="form-group">
							<div class="col-md-7 col-md-push-3">
								<button type="submit" class="btn btn-primary"><?php _e( 'Save Changes', 'meo_real_estate_admin' ); ?></button>
								&nbsp;
								<button type="reset" class="btn btn-default"><?php _e( 'Cancel', 'meo_real_estate_admin' ); ?></button>
							</div> <!-- /.col -->
						</div> <!-- /.form-group -->

					</form>


				</div> <!-- /.tab-pane -->



				<div class="tab-pane fade" id="password-tab">

					<h3 class="content-title"><u><?php _e( 'Change Password', 'meo_real_estate_admin' ); ?></u></h3>

					<form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="form-horizontal">

						<input type="hidden" name="action" value="reset_user_pass" />

						<div class="form-group">

							<label class="col-md-3"><?php _e( 'Old Password', 'meo_real_estate_admin' ); ?></label>

							<div class="col-md-7">
								<input type="password" name="old-password" class="form-control" />
							</div> <!-- /.col -->

						</div> <!-- /.form-group -->

						<hr>

						<div class="form-group">

							<label class="col-md-3"><?php _e( 'New Password', 'meo_real_estate_admin' ); ?></label>

							<div class="col-md-7">
								<input type="password" name="new-password-1" class="form-control" />
							</div> <!-- /.col -->

						</div> <!-- /.form-group -->


						<div class="form-group">

							<label class="col-md-3"><?php _e( 'New Password Confirm', 'meo_real_estate_admin' ); ?></label>

							<div class="col-md-7">
								<input type="password" name="new-password-2" class="form-control" />
							</div> <!-- /.col -->

						</div> <!-- /.form-group -->

						<div class="form-group">

							<div class="password-message"></div>

						</div> <!-- /.form-group -->

						<div class="form-group">

							<div class="col-md-7 col-md-push-3">
								<button type="submit" class="btn btn-primary"><?php _e( 'Save Changes', 'meo_real_estate_admin' ); ?></button>
								&nbsp;
								<button type="reset" class="btn btn-default"><?php _e( 'Cancel', 'meo_real_estate_admin' ); ?></button>
							</div> <!-- /.col -->

						</div> <!-- /.form-group -->

					</form>

				</div> <!-- /.tab-pane -->


				<?php if (current_user_can( 'read_contacts' ) && count($sites) >= 1 ) { ?>
					<div class="tab-pane fade" id="messaging">

						<h3 class="content-title"><u><?php _e( 'Notification Settings', 'meo_real_estate_admin' ); ?></u></h3>
						<?php if (count($sites) < 1 ) { ?>
							<p>
								<?php _e( "You don't have access to any sites", 'meo_real_estate_admin' ); ?>
							</p>
						<?php } else { ?>
							<p>
								<?php _e( 'Set the email addresses which will receive notifications', 'meo_real_estate_admin' ); ?><br><br>
								<?php _e( 'Separate multiple addresses with commas (,), or leave the field blank to disable the notifications.', 'meo_real_estate_admin' ); ?><br>
							</p>

							<br><br>

							<form action="<?php echo admin_url('admin-ajax.php'); ?>" class="form form-horizontal">

								<input type="hidden" name="action" value="update_site_notification_emails" />

								<?php if (count($sites) == 1) { ?>
									<input type="hidden" name="site_id" value="<?php echo $site_id; ?>" />
								<?php }
								else { ?>

									<div class="form-group">

										<label class="col-md-3"><?php _e( 'Site', 'meo_real_estate_admin' ); ?></label>

										<div class="col-md-7">
											<?php mrea_get_site_selector($site_id, $sites); ?>
										</div> <!-- /.col -->

									</div> <!-- /.form-group -->

								<?php } ?>

								<div class="form-group">

									<label class="col-md-3"><?php _e( 'Address to receive new contact emails', 'meo_real_estate_admin' ); ?></label>

									<div class="col-md-7">
										<input type="text" name="email_new_contact" class="email_new_contact form-control" placeholder="adresse e-mail" value="<?php echo $sites[$site_id]['email_new_contact']; ?>" >
									</div> <!-- /.col -->

								</div> <!-- /.form-group -->

								<div class="form-group">

									<label class="col-md-3"><?php _e( 'Address to receive site statistics (weekly)', 'meo_real_estate_admin' ); ?></label>

									<div class="col-md-7">
										<input type="text" name="email_weekly_stats" class="email_weekly_stats form-control" placeholder="adresse e-mail" value="<?php echo $sites[$site_id]['email_weekly_stats']; ?>" >
									</div> <!-- /.col -->

								</div> <!-- /.form-group -->

								<div class="form-group">

									<div class="messaging-message"></div>

								</div> <!-- /.form-group -->

								<div class="form-group">

									<div class="col-md-7 col-md-push-3">
										<button type="submit" class="btn btn-primary"><?php _e( 'Save Changes', 'meo_real_estate_admin' ); ?></button>
										&nbsp;
										<button type="reset" class="btn btn-default"><?php _e( 'Cancel', 'meo_real_estate_admin' ); ?></button>
									</div> <!-- /.col -->

								</div> <!-- /.form-group -->

							</form>

						<?php } ?>

					</div> <!-- /.tab-pane -->
				<?php } ?>

				<?php
				if (!empty($custom_fields)) {
					foreach ($custom_fields as $custom_field) { ?>
						<div class="tab-pane fade custom-field-edit" id="custom-field-<?php echo $custom_field['id']; ?>-tab">

							<h3 class="content-title"><u><?php echo $custom_field['name']; ?></u></h3>

							<form action="<?php echo admin_url('admin-ajax.php'); ?>" class="form-horizontal">

								<input type="hidden" name="action" value="update_custom_field" />
								<input type="hidden" name="custom_field_id" value="<?php echo $custom_field['id']; ?>" />

								<?php foreach ($custom_field['fields'] as $field_value) { ?>
									<div class="form-group">

										<div class="col-md-7">
											<input type="text" name="field_value[<?php echo ($field_value['id'] == null ? -9999 : $field_value['id']); ?>]" value="<?php echo $field_value['label']; ?>" class="form-control" />
										</div> <!-- /.col -->

										<div class="col-md-3"><a class="close delete-field" href="#" aria-hidden="true">×</a></div>

									</div> <!-- /.form-group -->
								<?php } ?>

								<div class="form-group">

									<div class="col-md-7">
										<a class="custom-field-add" id="custom-field-add-<?php echo $custom_field['id']; ?>" href="">ajouter</a>
									</div> <!-- /.col -->

									<div class="col-md-3">&nbsp;</div>
								</div> <!-- /.form-group -->


								<div class="form-group">

									<div class="custom-field-message custom-field-message-<?php echo $custom_field['id']; ?>"></div>

								</div> <!-- /.form-group -->

								<div class="form-group">
									<div class="col-md-7 col-md-push-3">
										<button type="submit" class="btn btn-primary"><?php _e( 'Save Changes', 'meo_real_estate_admin' ); ?></button>
										&nbsp;
										<button type="reset" class="btn btn-default"><?php _e( 'Cancel', 'meo_real_estate_admin' ); ?></button>
									</div> <!-- /.col -->
								</div> <!-- /.form-group -->


							</form>

						</div> <!-- /.tab-pane -->
						<script type='text/javascript'>
							settings_form_handler('#custom-field-<?php echo $custom_field['id']; ?>-tab form',  '.custom-field-message-<?php echo $custom_field['id']; ?>',   false);
						</script>
					<?php }
				}

				if (!empty($can_add_app_users) && $can_add_app_users) { ?>
					<div class="tab-pane fade" id="app_users">

						<h3 class="content-title"><u><?php _e( 'App Users', 'meo_real_estate_admin' ); ?></u></h3>

						<?php if (count($sites) < 1 ) { ?>
							<p>
								<?php _e( "You don't have access to any sites", 'meo_real_estate_admin' ); ?>
							</p>
						<?php } else { ?>

							<form action="<?php echo admin_url('admin-ajax.php'); ?>" class="form form-horizontal">

								<input type="hidden" name="action" value="add_app_user_by_email" />

								<?php if (count($sites) == 1) { ?>
									<input type="hidden" name="site_id" value="<?php echo $site_id; ?>" />
								<?php }
								else { ?>

									<div class="form-group">

										<label class="col-md-3"><?php _e( 'Site', 'meo_real_estate_admin' ); ?></label>

										<div class="col-md-7">
											<?php mrea_get_site_selector($site_id, $sites); ?>
										</div> <!-- /.col -->

									</div> <!-- /.form-group -->

								<?php } ?>

								<div class="form-group">

									<label class="col-md-3"><?php _e( "New user's email address", 'meo_real_estate_admin' ); ?></label>

									<div class="col-md-7">
										<input type="text" name="email_new_app_user" class="email_new_app_user form-control" placeholder="adresse e-mail" value="" >
									</div> <!-- /.col -->

								</div> <!-- /.form-group -->


								<div class="form-group">
									<div class="new-app-user-message"></div>
								</div> <!-- /.form-group -->

								<div class="form-group">
									<div class="col-md-7 col-md-push-3">
										<button type="submit" class="btn btn-primary"><?php _e( 'Add User', 'meo_real_estate_admin' ); ?></button>
										&nbsp;
										<button type="reset" class="btn btn-default"><?php _e( 'Cancel', 'meo_real_estate_admin' ); ?></button>
									</div> <!-- /.col -->
								</div> <!-- /.form-group -->

								<div class="form-group user-list-wrapper">

									<label class="col-md-3"><?php _e( 'Existing users', 'meo_real_estate_admin' ); ?></label>

									<div class="col-md-7 user-list">
									</div> <!-- /.col -->

								</div> <!-- /.form-group -->


							</form>

						<?php } ?>

					</div> <!-- /.tab-pane -->

				<?php } ?>

			</div> <!-- /.tab-content -->

		</div> <!-- /.col -->

	</div> <!-- /.row -->

<?php get_footer(); ?>
