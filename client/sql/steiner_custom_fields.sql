INSERT INTO `wp_meo_custom_fields` (`name`, `slug`, `client_id`, `is_active`, `is_required`)
VALUES ('Stade de la relation', 'relationship-status', 50, 'yes', 'no');


INSERT INTO `wp_meo_custom_field_values` (`custom_field_id`, `label`, `is_default`)
VALUES ((select id from wp_meo_custom_fields where slug = 'relationship-status'), '1_Promesse de vente', NULL);

INSERT INTO `wp_meo_custom_field_values` (`custom_field_id`, `label`, `is_default`)
VALUES ((select id from wp_meo_custom_fields where slug = 'relationship-status'), '2_Réservation', NULL);

INSERT INTO `wp_meo_custom_field_values` (`custom_field_id`, `label`, `is_default`)
VALUES ((select id from wp_meo_custom_fields where slug = 'relationship-status'), '3_Client suivi', NULL);

INSERT INTO `wp_meo_custom_field_values` (`custom_field_id`, `label`, `is_default`)
VALUES ((select id from wp_meo_custom_fields where slug = 'relationship-status'), '4_Client perdu', NULL);



INSERT INTO `wp_meo_custom_fields` (`name`, `slug`, `client_id`, `is_active`, `is_required`)
VALUES ('Documentation envoyé', 'documentation-sent', 50, 'yes', 'no');

INSERT INTO `wp_meo_custom_field_values` (`custom_field_id`, `label`, `is_default`)
VALUES ((select id from wp_meo_custom_fields where slug = 'documentation-sent'), 'No', NULL);

INSERT INTO `wp_meo_custom_field_values` (`custom_field_id`, `label`, `is_default`)
VALUES ((select id from wp_meo_custom_fields where slug = 'documentation-sent'), 'Yes', NULL);



/*

select *
  from wp_meo_custom_fields cf
       left join wp_meo_custom_field_values cfv on cfv.custom_field_id = cf.id

*/