<?php

function map_content_to_previous_structure($data, $version) {
	if (!preg_match('/^1\.0/', $version)) {
		return $data;
	}
	if (empty($data['developments'])) {
		return $data;
	}

	// Unused in v1.0
	$data['sectors'] = array();

	// These fields are now under development; in v1.0 (currently used by the Grisoni app) they were under the root
	$fields = array( 'buildings', 'floors', 'plans', 'features', 'lots', 'galleries' );

	foreach ($fields as $field) {
		$data[$field] = array();
	}

	foreach ($data['developments'] as $development) {
		foreach ($fields as $field) {
			switch ($field) {
				case 'galleries':
					if (isset($development['gallery'])) {
						$development['gallery']['development_id'] = $development['id'];
						$data['galleries'][$development['id']] = $development['gallery'];
					}
					break;

				default:
					if (isset($development[$field])) {
						$data[$field] += $development[$field];
					}
					break;
			}
		}
	}

	return $data;
}


$client_base = '../client/wp';

if ($_SERVER['SERVER_ADDR'] == "192.168.67.103") {
	$_SERVER['SERVER_ADDR'] = "127.0.0.1";
}

if ($_SERVER['SERVER_ADDR'] == "127.0.0.1") {
	$client_base = '../../client/site/wp';
	define('APP_BASE_URL', 'http://' . $_SERVER['HTTP_HOST']);
}
else {
	define('APP_BASE_URL', 'http://app.meo-realestate.com'); // probably could get this from $_SERVER, but better safe than sorry
}

## Load the client site
include_once $client_base . '/wp-load.php';


global $meova; // MeoVisionApp Object, instantiated in the plugin in clients
global $all_media; // Cache for media requests - save the app from searching through all the JSON
$all_media = array();

$user = $_POST['user'];
$date = $_POST['date'];
$key  = $_POST['key'];

$client_version = $_POST['ver'];

$valid_connection = $meova->validateKey($user, $date, $key);

if (!$valid_connection) {
	header('HTTP/1.0 403 Forbidden');
	echo 'Access denied';

	exit;
}

$user_developments = mrea_get_sites_for_user($user);

if (empty($user_developments)) {
	header('HTTP/1.0 403 Forbidden');
	echo 'Access denied';

	exit;
}

$app = array(
	'default_project_page'    => 'thumbs',
	'piwik_app_id'            => '32670457',
	'piwik_enabled'           => false
);

$gallery_element_types = array(
	'gallery_images',
	'videos',
	'pdfs',
	'spinners'
);


$developments = array();
foreach ($gallery_element_types as $gallery_element_type) {
	${$gallery_element_type} = array();
}

foreach ($user_developments as $user_development) {
	if (!$user_development['enabled_in_app']) {
		continue;
	}
	$development_data = $meova->getDevelopmentData($user_development);

	if (empty($development_data['development_data'])) {
		continue;
	}

	$developments += $development_data['development_data']['development'];
	foreach ($gallery_element_types as $gallery_element_type) {
		if (array_key_exists($gallery_element_type, $development_data)) {
			${$gallery_element_type} += $development_data[$gallery_element_type];
		}
	}

	// Move plan spinners into the common declaration
	if (!empty($development_data['development_data']['development'][$user_development['id']]['spinners'])) {
		$spinners += $development_data['development_data']['development'][$user_development['id']]['spinners'];
	}
}

$translations = array(
	'building_labels' => array(
		'volume'           => 'volume',
		'surface_interior' => 'appartement',
		'surface_balcony'  => 'surface balcon',
		'surface_terrace'  => 'surface terrasse',
		'surface_garden'   => 'surface jardin',
		'surface_weighted' => 'surface pondérée'
	),
	'initialisation_messages' => array(
		'no_data_or_conn'        => 'Merci de connecter à Internet pour le téléchargement initial',
		'error_checking_session' => 'Une erreur est survenue lors de la vérification de vos coordonnées. Merci de reconnecter.',
		'fserror'                => "Impossible d'obtenir le dossier. Les images ne peuvent être mis en cache",
		'close'                  => "fermer"
	),
	'login_messages' => array(
		// Some or all of these will have no effect, as the login form is presented before these are downloaded
		"username" => "nom d'utilisateur",
		"password" => "mot de passe",
		"connect"  => "Connexion",
		'commerr'  => 'erreur de connexion au serveur',
		'invalid'  => 'les détails de connexion sont invalides',
		'no_pass'  => 'mot de passe est vide',
		'no_user'  => "nom d'utilisateur est vide",
		'no_devs'  => "Vous ne avez pas accès à un développement.  S'il vous plaît vous connecter comme un autre utilisateur."
	),
	"button_labels" => array(
		"gallery" => "galerie",
		"logout"  => "logout",
		"last_update" => "dernière mise à jour"
	),
	"thumb_labels" => array(
		'buildings' => 'bâtiments',
		'floors' => 'étages',
		'rooms' => 'pièces',
		'building_short' => 'Bât',
		'rooms_short' => 'p.'
	),
	'lot_page_labels' => array(
		'rooms' => 'Pièces'
	),
	"month_names" => array(
		"janvier",
		"février",
		"mars",
		"avril",
		"mai",
		"juin",
		"juillet",
		"août",
		"septembre",
		"octobre",
		"novembre",
		"décembre"
	),
	"availability" => array(
		"available" => "Libre",
		"reserved"  => "Réservé",
		"sold"      => "Vendu"
	)
);


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

$result = array(
	'success'        => 1,
	'app'            => $app,
	'developments'   => $developments,
	'translations'   => $translations,
	'all_media'      => $all_media
);

foreach ($gallery_element_types as $gallery_element_type) {
	$result[$gallery_element_type] = ${$gallery_element_type};
}

$result = map_content_to_previous_structure($result, $client_version);

echo json_encode($result);
