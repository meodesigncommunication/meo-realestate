<?php global $post; 
        $postId = $post->ID;
        $query = get_posts_related_by_taxonomy($post->ID, 'project-category'); ?>
<?php if ($query->have_posts()) : ?>
	<div class="row relatedposts">
		<div class="twelve columns">
			<div style="text-align:center;"><h6><?php _e( 'Related Posts:', THB_THEME_NAME ); ?></h6></div>
		</div>
	  <?php while ($query->have_posts()) : $query->the_post();
	          $terms = get_the_terms( get_the_ID(), 'project-category' ); ?>
	                 
	    <?php if(get_the_ID() != $postId) : ?>
	    <div class="three mobile-two columns">
	      <article class="post" id="post-<?php the_ID(); ?>">
	        <?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
	        <div class="post-gallery fresco">
	        		<?php the_post_thumbnail('onefourth'); ?>
	          	<?php $image_id = get_post_thumbnail_id();
	          				$image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0]; ?>
	          	<div class="overlay">
	          		<a href="<?php the_permalink() ?>" class="details"><i class="icon-share-alt"></i></a>
	          		<a href="<?php echo $image_url; ?>" class="zoom" rel="magnific"><i class="icon-eye-open"></i></a>
	          	</div>
	        </div>
	        <?php } ?>
	        <div class="post-title cf">
	        	<h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
	        	<?php echo thb_printLikes(get_the_ID()); ?>
	        </div> 
	      </article>
	    </div>
	    <?php endif; endwhile; ?>
	</div>
<?php endif; ?>