<?php
// Enable WP Post Thumbnails
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 40, 40, true );
	add_image_size('slider', 1920, 700, true );
	add_image_size('portfolio', 1140, 650, true );
	add_image_size('twothirds', 850, 400, true );
	add_image_size('onehalf', 555, 255, true );
	add_image_size('onethirds', 360, 215, true );
	add_image_size('onefourth', 260, 185, true );
	add_image_size('portfolio', 1140, 650, true );
	add_image_size('portfolio-shapes-hex', 261, 284, true );
	add_image_size('portfolio-shapes-cir', 261, 261, true );
}
?>