(function() {
	tinymce.create('tinymce.plugins.thbtiny', {
		init : function(ed, url) {
		 ed.addCommand('shortcodeGenerator', function() {
		 	
		 		tb_show("Shortcodes", url + '/shortcode_generator/shortcode-generator.php?&width=630&height=600');

				
		 });
			//Add button
			ed.addButton('scgenerator', {	title : 'Shortcodes', cmd : 'shortcodeGenerator', image : url + '/shortcode_generator/icons/shortcode-generator.png' });
        },
        createControl : function(n, cm) {
			  return null;
        },
		  getInfo : function() {
			return {
				longname : 'thb TinyMCE',
				author : 'turkhitbox',
				authorurl : 'http://turkhitbox.com',
				infourl : 'http://turkhitbox.com',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
    });
    tinymce.PluginManager.add('thb_buttons', tinymce.plugins.thbtiny);
})();