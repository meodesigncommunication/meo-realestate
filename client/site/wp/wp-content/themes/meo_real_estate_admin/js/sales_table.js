jQuery(function() {
	if (meo_settings['site_id'] < 0) {
		return;
	}

	var site = meo_settings['sites'][meo_settings['site_id']],
	    request_url = ( site['allow_direct_requests'] == 1 ? site['site_url'] + site['api_url'] : meo_settings['ajax_url']);

	get_statuses(site, request_url);

	var table_1 = jQuery('#table-1').dataTable ({
		"sAjaxSource": request_url + "?action=get_apartment_list&api-key=" + site['api_key'],
		"aoColumns": [
			{ "mData": "lot_type" },
			{ "mData": "building" },
			{ "mData": "floor" },
			{ "mData": "lot" },
			{ "mData": "rooms" },
			{ "mData": "surface" },
			{ "mData": availability_extractor },
			{ "mData": price_checker },
		],
		"iDisplayLength": -1, /* Show all rows */
		"fnInitComplete": function(oSettings, json) {
			jQuery(this).parents ('.dataTables_wrapper').find ('.dataTables_filter input').prop ('placeholder', meo_settings['search_placeholder']).addClass('form-control input-sm');
			add_availability_handler(this, site, request_url);
			add_price_handler(this, site, request_url);
		},
		"oLanguage": meo_settings['datatable_trans']
	});
});
