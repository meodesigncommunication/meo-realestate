<?php
/**
 * Template name: Email PDFs
 *
 * @package MEO real estate admin
 */

function mrea_generate_pdf($output_dir, $data, $template, $analytics_id) {
	require_once('classes/class-contact-pdf.php');

	$surname    = preg_replace('/[^A-Za-z0-9]/', '', $data['surname']);
	$first_name = preg_replace('/[^A-Za-z0-9]/', '', $data['first_name']);

	$output_file = $output_dir . $surname . $first_name . '.' . $analytics_id . '.pdf';
	if (file_exists($output_file)) {
		unlink($output_file);
	}
	try {
		$pdf = new ContactPdf('en', $template);
		$pdf->generate($data);
		$pdf->Output($output_file, 'F');
	}
	catch (Exception $e) {
		return false;
	}

	return $output_file;
}

function mrea_check_recipients($recipient_string) {
	$raw_recipients = split(',', $recipient_string);
	$recipients = array();
	foreach ($raw_recipients as $raw_recipient) {
		if (is_email($raw_recipient)) {
			$recipients[] = $raw_recipient;
		}
	}
	return $recipients;
}

function mrea_send_pdf($data, $site, $pdf, $recipients, $extracontent = "") {
	$content = $site['new_contact_email_content'];

	$replacements = array(
		'{CLIENT_FIRST_NAME}' => $data['first_name'],
		'{CLIENT_SURNAME}'    => $data['surname']
	);

	$content = str_replace(array_keys($replacements), array_values($replacements), $content);

	$headers = array(
		'From: MEO Real Estate <' . MAIL_USER . '>',
		'Content-type: text/html; charset="utf-8"'
	);
	return wp_mail( $recipients,
	                $site['name'] . ' :: ' . __( 'new contact', 'meo_real_estate_admin' ),
	                $content.$extracontent,
	                $headers,
	                array($pdf) );
}

/* --------------------------------------------------------------------------------------------- */

global $wpdb;

$pdf_email_api_key = get_field('pdf_email_api_key', 'option');

// Shouldn't really matter, but we don't want this URL to be hit without permission
if ($_GET['api-key'] != $pdf_email_api_key) {
	header("HTTP/1.0 403 Forbidden"); ?>
	<h1>Access Forbidden!</h1>
    <?php

	exit;
}

date_default_timezone_set("UTC"); // piwik uses UTC, so we need to do likewise
$activity_cutoff = time() - 30 * 60; // Half an hour ago

date_default_timezone_set('Europe/Zurich');

$upload_dir = wp_upload_dir();
$output_dir = $upload_dir['basedir'] . '/pdfs/';

$sql = "select * from {$wpdb->prefix}email_schedule where update_date <= date_add(now(), interval -30 minute) and sent_date is null and in_error = 'no'";

$potential_emails = $wpdb->get_results($sql);

if (! empty($potential_emails)) {
	$sites = mrea_get_all_sites();

	foreach ($potential_emails as $potential_email) {
		$site = $sites[$potential_email->site_id];
		if (empty($site)) {
			mrea_report_error(__FILE__, __LINE__, "No site found for " . print_r($potential_email, true));
			continue;
		}
		$recipients = mrea_check_recipients($site['email_new_contact']);

		if (empty($recipients) || empty($site['pdf_template'])) {
			mrea_report_error(__FILE__, __LINE__, "No recipient or PDF template for " . print_r($potential_email, true));
			continue;
		}


		$last_visit_data = mrea_get_last_visit_data($site, $potential_email->analytics_id);
		
		if (empty($last_visit_data)) {
			$wpdb->update(
				"{$wpdb->prefix}email_schedule",
				array(
					'in_error' => 'yes'
				),
				array( 'id' => $potential_email->id ),
				array(
					'%s'
				),
				array( '%d' )
			);
			// An error email should have been sent
			continue;
		}
		
		$last_visit_timestamp = (int) $last_visit_data[0]->lastActionTimestamp;
		
		if ( $last_visit_timestamp <= $activity_cutoff ) {echo 'before';
			$data = mrea_get_contact_data_by_analytics_id($site, $potential_email->analytics_id);
			if (empty($data)) {
				echo 'No data found for ' . $potential_email->analytics_id . '<br>';
				mrea_report_error(__FILE__, __LINE__, "No data found for " . print_r($potential_email, true));
				continue;
			}

			$template = get_attached_file($site['pdf_template']['id']);

			$output_file = mrea_generate_pdf($output_dir, $data, $template, $potential_email->analytics_id);
			if (!$output_file) {
				echo 'Error generating PDF for ' . $potential_email->analytics_id . '<br>';
				mrea_report_error(__FILE__, __LINE__, "Error generating PDF for " . print_r($potential_email, true));
				continue;
			}
			
			// New: param to add extra content to the email
			$extracontent = "";
			
			// Exception site 71 Estavayer
			if($potential_email->site_id == 71){

				// We will send alternatively an email to the Courtiers (non administrable list, so email adresses directly inserted into db)
				// Insert the courtier in the acf fields first, in order to auto assign the courtier after email sent
				
				// Select available courtier, to get the email address // check availabitlity
				$query = "SELECT * FROM {$wpdb->prefix}meo_courtier_availability WHERE availability = 1 LIMIT 1";
				$potential_courtier = $wpdb->get_results($query);
				
				$courtier_email = "";
				
				if (! empty($potential_courtier)) {
					// one available, get the email
					$courtier_email = $potential_courtier[0]->email_address;
				}
				else {
					// no one is available. reset them all, requery, get the email
					$table = $wpdb->prefix.'meo_courtier_availability';
					$data = array('availability' => '1');
					$where = array('availability' => '0');
					$wpdb->update( $table, $data, $where);
					
					// Normaly we have one now
					$potential_courtier = $wpdb->get_results($query);
					$courtier_email = $potential_courtier[0]->email_address;
				}
				
				// send email to courtier
				mrea_send_pdf($data, $site, $output_file, $courtier_email);

				/* TEST
				$content = $site['new_contact_email_content'];
				
				$replacements = array(
						'{CLIENT_FIRST_NAME}' => $data['first_name'],
						'{CLIENT_SURNAME}'    => $data['surname']
				);
				
				$content = str_replace(array_keys($replacements), array_values($replacements), $content);
				
				$headers = array(
						'From: MEO Real Estate <' . MAIL_USER . '>',
						'Content-type: text/html; charset="utf-8"'
				);
				wp_mail( 'frederic.work.emails@gmail.com',
						$site['name'] . ' :: ' . __( 'new contact', 'meo_real_estate_admin' ),
						$content,
						$headers);
				*/
				
				// mark as unavailable
				$table = $wpdb->prefix.'meo_courtier_availability';
				$data = array('availability' => '0', 'last_send' => date('Y-m-d H:i:s'));
				$where = array('id' => $potential_courtier[0]->id);
				$wpdb->update( $table, $data, $where);
				
				// send copy to fix email (ventes@gefiswiss.ch), which is default behavior, so simply add the courtier in the email
				$extracontent = '<br/>Courtier '.$courtier_email;
			}

			// /Exception
			
			$sent = mrea_send_pdf($data, $site, $output_file, $recipients, $extracontent);

			if ($sent) {
				echo basename($output_file) . ' has been generated and sent<br>';
				$wpdb->update(
					"{$wpdb->prefix}email_schedule",
					array(
						'sent_date' => current_time('mysql', 1)
					),
					array( 'id' => $potential_email->id ),
					array(
						'%s'
					),
					array( '%d' )
				);
			}
			else {
				mrea_report_error(__FILE__, __LINE__, "Failed to send " . basename($output_file) . ' for ' . print_r($potential_email, true));
				echo 'Failed to send ' . basename($output_file) . '<br>';
			}
		}
		else {
			echo $potential_email->analytics_id . ' has been active in the last half hour, not sending<br>';
		}
	}
}

echo 'Done<br>';
