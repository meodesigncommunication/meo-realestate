jQuery(function() {
    function to_duration(duration) {
		var hours = parseInt( duration / 3600, 10 ) % 24,
		    minutes = parseInt(duration / 60, 10 ) % 60,
		    seconds = parseInt(duration % 60, 10);

		var result = '';
		if (hours > 0) {
			result = (hours < 10 ? "0" + hours : hours) + ":";
		}

		result = result + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

		return result;
    }

    function show_page_stats(data, selector) {
		var first_width = null;
		jQuery.each(data, function( index, value ) {
			if ( value['label'] ) {
				var label = value['label'];

				if (first_width === null) {
					first_width = (value['sum_time_spent'] ? value['sum_time_spent'] : 1);
				}
				// / 2.5 to make it fit in less than half the column
				jQuery(selector).append('<div class="page-name-wrapper"><span class="page-name">' + label + '</span> <span class="bar" style="width: ' + ( value['sum_time_spent']/ first_width * 100 / 2.5).toFixed(2) + '%;">&nbsp;</span>&nbsp;' + to_duration(value['sum_time_spent']) + '</div>');
			}
		});
    }

	function show_lots_viewed(data, selector, rooms_per_lot) {
		var regex = /^ *Parallèle – (.*)$/;
		jQuery.each(data, function( index, value ) {
			var seen = {};
			if ( value['label'] && !seen[value['label']]) {
				seen[value['label']] = 1;
				var label = value['label'];
				if (label.match(regex)) {
					label = label.replace(regex, "$1");
				}
				var rooms = rooms_per_lot[label];
				if (rooms == 1) {
					rooms = "atelier/arcade";
				}
				else {
					rooms += " rooms";
				}

				jQuery(selector).append('<div class="page-name-wrapper"><span class="page-name">' + label + '</span>&nbsp;' + rooms + '</div>');
			}
		});
    }

	jQuery.ajax({
		type: "GET",
		url: contact_details['data_url'],
		data: {
			"analytics_id" : contact_details['analytics_id'],
			"site_id"      : meo_settings ['site_id']
		},
		success: function(result, textStatus, jqXHR) {

			jQuery(result).each(function( index, value ) {
				jQuery('.contact-name').html(value['first_name'] + " " + value['surname']);
				jQuery('.first-name').html(value['first_name']);
				jQuery('.surname').html(value['surname']);
				jQuery('.address').html(value['address']);
				jQuery('.city').html(value['city']);
				jQuery('.postcode').html(value['postcode']);
				jQuery('.country').html(value['country']);
				jQuery('.phone').html(value['phone']);
				jQuery('.email').html('<a href="mailto:' + value['email'] + '">' + value['email'] + '</a>');
				jQuery('.first-contact').html(value['submit_time']);
				jQuery('.source').html(value['referrer']);

				var seen = {};
				jQuery(value['lots_downloaded']).each(function( lot_index, lot_code ) {
					if (typeof seen[lot_code] === "undefined") {
						jQuery('.lots-downloaded').append(lot_code + '<br>');
						seen[lot_code] = 1;
					}
				});

				jQuery.each(value['lot_sizes'], function( rooms, room_view_count ) {
					if (typeof room_view_count !== "undefined") {
						var label = (rooms == 1 ? 'atelier/arcade' : rooms + ' rooms' );
						var percent = ( value['total_rooms_viewed'] ? Math.round(room_view_count / value['total_rooms_viewed'] * 100) : 0);
						// / 2.5 to make it fit in less than half the column
						jQuery('.sizes-viewed').append('<div class="page-name-wrapper"><span class="page-name">' + label + '</span> <span class="bar" style="width: ' + ( percent / 2.5) + '%;">&nbsp;</span>&nbsp;' + percent + '%</div>');
					}
				});

				show_lots_viewed(value['lots_viewed'], '.lots-viewed', result['rooms_per_lots']);

				show_page_stats(value['pages_viewed'], '.pages-viewed');

				jQuery('.map-wrapper').attr('href', 'http://maps.google.com/maps?q=' + value['address'] + '+' + value['postcode'] + '+' + value['city'] + ',' + value['country'] + '&t=m&z=12');
				jQuery('.map-wrapper img').removeClass('dummy').attr('src', 'http://maps.googleapis.com/maps/api/staticmap?center=' + value['address'] + '+' + value['postcode'] + '+' + value['city'] + ',' + value['country'] + '&markers=' + value['address'] + '+' + value['postcode'] + '+' + value['city'] + ',' + value['country'] + '&zoom=13&size=1140x150&maptype=roadmap&scale=2');
			});

			jQuery('.loader').hide();
			jQuery('.details').show();
		}
	});
});
