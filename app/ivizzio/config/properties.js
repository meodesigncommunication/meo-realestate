var properties = {
	// These values are (mainly) used in the config.xml
	'%%APP_NAME%%'             : 'ivizzio',
	'%%APP_DESC%%'             : 'Property development viewer',
	'%%APP_AUTH_EMAIL%%'       : 'development@ivizzio.com',
	'%%APP_AUTH_SITE%%'        : 'http://www.ivizzio.com',
	'%%APP_AUTH_NAME%%'        : 'ivizzio',
	'%%APP_PACKAGE%%'          : 'com.ivizzio.app',

	// These values are used in the javascript
	'%%LOCALSTORAGE_PREFIX%%'  : 'ivizzio',
	'%%ALERT_TITLE%%'          : 'ivizzio',
	'%%DIRECTORY_NAME%%'       : 'com.ivizzio.app',
	'%%DEFAULT_DEV_PAGE%%'     : 'thumbs',
	'%%USE_DEV_SPLASH%%'       : 'false',
	'%%PIWIK_APP_ID%%'         : '32670456', // test system
	'%%PIWIK_ENABLED%%'        : 'false',
	'%%PROMPT_BEFORE_UPDATE%%' : 'true'
};

// Overwrite properties above for individual platforms
var platform_properties = {
	'windows' : {
		'%%PROMPT_BEFORE_UPDATE%%' : 'true'
	},
	'browser' : {
		'%%PROMPT_BEFORE_UPDATE%%' : 'true'
	}
};

module.exports = {
    properties: properties,
    platform_properties: platform_properties
};
