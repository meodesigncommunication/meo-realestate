<?php
function load_more_posts() {

  $args = array(
      'paged'          => $_POST['page'] + 1,
      'orderby'        => 'post_date',
      'order'          => 'DESC'
  );

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
	
	    while ( $query->have_posts() ) { $query->the_post(); 
	       $format = get_post_format();
	       get_template_part( 'inc/postformats/'.$format );
	    }
	
	}
	
	die();
}