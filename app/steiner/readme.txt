WARNING
=======

Do *NOT* edit the code in the dest folder.  That code is built from the ivizzio source, and only differs in a few subtle ways - mainly branding (icons, splash screens, etc) and the use of a intermediate development splash screen in the app itself.

The code is generated using the bin/generate.sh, which should be run in a bash shell (e.g. git bash on Windows).

Any bugs in this app should be fixed in the master (ivizzio) app, and this app regenerated.

