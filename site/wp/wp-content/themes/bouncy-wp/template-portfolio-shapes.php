<?php
/*
Template Name: Portfolio - Shapes
*/
?>
<?php get_header(); ?>
<div class="row">
<section class="portfolio-container twelve columns">
	<div class="row">
		<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
		<?php $shape = get_post_meta($post->ID, 'portfolio_shapes', TRUE); ?>
		<?php $catorg = get_post_meta($post->ID, 'portfolio_categories', TRUE); ?>
		<?php $cat = implode(',', $catorg); ?>
		<div class="twelve columns">
			<ul class="filters hide-for-small">
				<li><span><?php _e( 'Sort By:', THB_THEME_NAME ); ?></span></li>
			  <li><a href="#" data-filter="*" class="active"><?php _e( 'show all', THB_THEME_NAME ); ?></a></li>
			  <?php 
				$portfolio_categories = get_categories(array('taxonomy'=>'project-category', 'include' => $cat));
				foreach($portfolio_categories as $portfolio_category)
					echo '<li><a href="#" data-filter=".' . $portfolio_category->slug . '">' . $portfolio_category->name . '</a></li>';
				?>
			</ul>

			<div id="portfolioselect" class="show-for-small">
				<a href="#" id="sortportfolio"><?php _e( 'Sort By:', THB_THEME_NAME ); ?></a>
				<ul>
				   <li><a href="#" data-filter="*" class="active"><?php echo __('All', THB_THEME_NAME); ?></a></li>
			     <?php 
			     $portfolio_categories = get_categories(array('taxonomy'=>'project-category', 'include' => $cat));
			     foreach($portfolio_categories as $portfolio_category)
			     	echo '<li><a href="#" data-filter=".'.$portfolio_category->slug.'">' .$portfolio_category->name. '</a></li>';
			     ?>
				</ul>
			</div>
		</div>
		<?php endwhile; else : endif; ?> 
	</div>
	<div class="thbportfolio ajax row" data-columns="4">
		<?php $args = array(
	    	   'post_type' => 'portfolio',
	    	   'orderby'=>'menu_order',
	    	   'order'     => 'ASC',
	    	   'posts_per_page' => '-1',
	    	   'tax_query' => array(
	    	   		array(
		           'taxonomy' => 'project-category',
		           'field' => 'id',
		           'terms' => $catorg,
		           'operator' => 'IN'
	    	      )
	    	    ) // end of tax_query
    	  	);
		?>
		<?php $query = new WP_Query($args); ?>
            <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                  $terms = get_the_terms( get_the_ID(), 'project-category' );  ?>
            <div class="item three columns <?php foreach ($terms as $term) { echo strtolower($term->slug). ' '; } ?>">
	            <article id="post-<?php the_ID(); ?>" class="post">
                <?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
                <div class="post-gallery fresco shapes <?php echo $shape; ?>">
                	<?php if ($shape == 'circle') {
                    		the_post_thumbnail('portfolio-shapes-cir');
                  		} else {
                  			the_post_thumbnail('portfolio-shapes-hex');
                  		} ?>
                    	<?php $image_id = get_post_thumbnail_id();
                    				$image_url = wp_get_attachment_image_src($image_id,'full'); $image_url = $image_url[0]; ?>
                    	<div class="overlay">
                    		<div class="shape"><img src="<?php echo get_template_directory_uri(). '/assets/img/overlay-portfolio-'.$shape.'.png'; ?>" /></div>
                    		<h3><?php the_title(); ?></h3>
                    		
                    		<a href="<?php the_permalink(); ?>" class="details"><i class="icon-share-alt"></i></a>
                    		<a href="<?php echo $image_url; ?>" class="zoom" rel="magnific" title="<?php the_title(); ?>"><i class="icon-eye-open"></i></a>
                    	</div>
                    	
                </div>
                <?php } ?>
	            </article>
            </div>
            <?php endwhile; endif; ?>
        <?php wp_reset_query(); ?>
	</div>
</section>
</div>
<?php get_footer(); ?>