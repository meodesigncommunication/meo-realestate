<?php
/*
Copyright (C) MEO design et communication S�rl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/


class MeoMediaUtils {

	private $image_proxy_url = null;
	private $bypass_proxy = false;

	public function __construct() {
		$this->image_proxy_url = 'http' . ( $_SERVER['REQUEST_SCHEME'] == 'https' ? 's' : '' ) . '://' . preg_replace('/[^-\.a-zA-Z0-9]/', '', $_SERVER['HTTP_HOST']) . '/get_image.php';
		$this->bypass_proxy = isset($_GET['no_proxy']) && $_GET['no_proxy'] == '1';
	}

	public function getMediaDetails($raw_url, $additional_properties = array(), $local_url = false) {
		global $all_media;

		$url = $raw_url;
		if (!$local_url && !$this->bypass_proxy) {
			$url = $this->image_proxy_url . '?url=' . urlencode($url);

		}
		$result = $additional_properties;
		$result['url'] = $url;
		$result['date'] = $this->getUrlModificationDate($raw_url);

		$all_media[] = $result;

		return $result;
	}

	private function getUrlModificationDate($url) {
		$last_modified = date('YmdHis');

		if(filter_var($url, FILTER_VALIDATE_URL)){ // $url is a valid URL
			$headers = get_headers($url, 1);

			if (isset($headers['Last-Modified'])) {
				$header_time = date('YmdHis', strtotime($headers['Last-Modified']));
				if ($header_time) {
					$last_modified = $header_time;
				}
			}
		}

		return $last_modified;
	}
}
