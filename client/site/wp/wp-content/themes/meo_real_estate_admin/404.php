<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package MEO real estate admin
 */

get_header(); ?>

	<div class="error-container">

		<div class="error-code">404</div>

		<div class="error-details">

			<h4><?php _e( "Not found", 'meo_real_estate_admin' ); ?>.</h4>

			<p><?php _e( "We can not find the page you're looking for.", 'meo_real_estate_admin' ); ?></p>
			<p><?php _e( "Please check the URL", 'meo_real_estate_admin' ); ?></p>

		</div><!-- .error-details -->

	</div><!-- .error-container -->

<?php get_footer(); ?>
