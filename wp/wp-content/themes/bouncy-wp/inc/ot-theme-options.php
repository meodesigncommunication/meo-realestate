<?php
/**
 * Initialize the options before anything else. 
 */
add_action( 'admin_init', '_custom_theme_options', 1 );

/**
 * Theme Mode demo code of all the available option types.
 *
 * @return    void
 *
 * @access    private
 * @since     2.0
 */
function _custom_theme_options() {
  
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Create a custom settings array that we pass to 
   * the OptionTree Settings API Class.
   */
  $custom_settings = array(
    'sections'        => array(
      array(
        'title'       => 'General',
        'id'          => 'general'
      ),
      array(
        'title'       => 'Easy Styling',
        'id'          => 'easy'
      ),
      array(
        'title'       => 'Header Styling',
        'id'          => 'header'
      ),
      array(
        'title'       => 'Body Styling',
        'id'          => 'body'
      ),
      array(
        'title'       => 'Footer Styling',
        'id'          => 'footer'
      ),
      array(
        'title'       => 'Typography',
        'id'          => 'typography'
      ),
      array(
        'title'       => 'Contact',
        'id'          => 'contact'
      ),
      array(
        'title'       => 'Twitter Bar',
        'id'          => 'twitter'
      ),
      array(
        'title'       => 'Sidebars',
        'id'          => 'sidebars'
      ),
      array(
        'title'       => 'Misc',
        'id'          => 'misc'
      ),
      array(
        'title'       => 'Theme Updates',
        'id'          => 'update'
      )
    ),
    'settings'        => array(
    	array(
    	  'label'       => 'Boxed Layout',
    	  'id'          => 'boxed',
    	  'type'        => 'radio',
    	  'desc'        => 'The content is contained and the body background is visible from the sides.',
    	  'choices'     => array(
    	    array(
    	      'label'       => 'Yes',
    	      'value'       => 'yes'
    	    ),
    	    array(
    	      'label'       => 'No',
    	      'value'       => 'no'
    	    )
    	  ),
    	  'std'         => 'no',
    	  'section'     => 'general'
    	),
    	array(
    	  'label'       => 'Fixed Header',
    	  'id'          => 'fixed_header',
    	  'type'        => 'radio',
    	  'desc'        => 'Would you like a fixed header?',
    	  'choices'     => array(
    	    array(
    	      'label'       => 'Yes',
    	      'value'       => 'yes'
    	    ),
    	    array(
    	      'label'       => 'No',
    	      'value'       => 'no'
    	    )
    	  ),
    	  'std'         => 'yes',
    	  'section'     => 'general'
    	),
    	array(
    	  'label'       => 'Display Header Search',
    	  'id'          => 'headersearch',
    	  'type'        => 'radio',
    	  'desc'        => 'Would you like to display the search icon on the header?',
    	  'choices'     => array(
    	    array(
    	      'label'       => 'Yes',
    	      'value'       => 'yes'
    	    ),
    	    array(
    	      'label'       => 'No',
    	      'value'       => 'no'
    	    )
    	  ),
    	  'std'         => 'yes',
    	  'section'     => 'general'
    	),
    	
    	array(
    	  'label'       => 'Display Breadcrumbs',
    	  'id'          => 'breadcrumbs',
    	  'type'        => 'radio',
    	  'desc'        => 'Would you like to display the Breadcrumbs?',
    	  'choices'     => array(
    	    array(
    	      'label'       => 'Yes',
    	      'value'       => 'yes'
    	    ),
    	    array(
    	      'label'       => 'No',
    	      'value'       => 'no'
    	    )
    	  ),
    	  'std'         => 'yes',
    	  'section'     => 'general'
    	),
      array(
        'label'       => 'Display Footer',
        'id'          => 'footer',
        'type'        => 'radio',
        'desc'        => 'Would you like to display the Footer?',
        'choices'     => array(
          array(
            'label'       => 'Yes',
            'value'       => 'yes'
          ),
          array(
            'label'       => 'No',
            'value'       => 'no'
          )
        ),
        'std'         => 'yes',
        'section'     => 'general'
      ),
      array(
        'label'       => 'Display Sub-Footer',
        'id'          => 'subfooter',
        'type'        => 'radio',
        'desc'        => 'Would you like to display the Sub-Footer?',
        'choices'     => array(
          array(
            'label'       => 'Yes',
            'value'       => 'yes'
          ),
          array(
            'label'       => 'No',
            'value'       => 'no'
          )
        ),
        'std'         => 'yes',
        'section'     => 'general'
      ),
      array(
        'label'       => 'Footer Columns',
        'id'          => 'footer_columns',
        'type'        => 'radio-image',
        'desc'        => 'You can change the layout of footer columns here',
        'std'         => 'fourcolumns',
        'section'     => 'general'
      ),
      array(
        'label'       => 'Sidebar Position',
        'id'          => 'sidebar_position',
        'type'        => 'radio-image',
        'desc'        => 'You can change the position of the sidebar here',
        'std'         => 'right-sidebar',
        'section'     => 'general'
      ),
      array(
        'label'       => 'Theme Accent',
        'id'          => 'accent_color',
        'type'        => 'colorpicker',
        'desc'        => 'This changes the theme accent color. Its the same effect you see on the style switcher. This value will be overriden if you change the individual elements colors.',
        'section'     => 'easy'
      ),
      array(
        'label'       => 'Overlay Color',
        'id'          => 'overlay_color',
        'type'        => 'colorpicker',
        'desc'        => 'This changes the overlay color that you see when you hover over the images.',
        'section'     => 'easy'
      ),
      array(
        'label'       => 'Overlay Opacity',
        'id'          => 'overlay_opacity',
        'type'        => 'text',
        'desc'        => 'Opacity of the overlay color. You must set a value between 0.0 and 1.0 <small>For example, 0.75</small>',
        'section'     => 'easy'
      ),
      
      array(
        'label'       => 'Login Logo Upload',
        'id'          => 'loginlogo',
        'type'        => 'upload',
        'desc'        => 'You can upload a custom logo for your wp-admin login page here',
        'section'     => 'misc'
      ),
      array(
        'label'       => 'Favicon Upload',
        'id'          => 'favicon',
        'type'        => 'upload',
        'desc'        => 'You can upload your own favicon here',
        'section'     => 'misc'
      ),
      array(
        'label'       => 'Copyright Text',
        'id'          => 'copyright',
        'type'        => 'text',
        'desc'        => 'Copyright Text at the bottom left',
        'section'     => 'misc'
      ),
      array(
        'label'       => 'Scroll To Top',
        'id'          => 'scrolltotop',
        'type'        => 'radio',
        'desc'        => 'Would you like to display the scroll to top arrow?',
        'choices'     => array(
          array(
            'label'       => 'Yes',
            'value'       => 'yes'
          ),
          array(
            'label'       => 'No',
            'value'       => 'no'
          )
        ),
        'std'         => 'yes',
        'section'     => 'misc'
      ),
      array(
        'label'       => 'Disable Like System',
        'id'          => 'disablelike',
        'type'        => 'radio',
        'desc'        => 'Would you like to remove the like functionality?',
        'choices'     => array(
          array(
            'label'       => 'Yes',
            'value'       => 'yes'
          ),
          array(
            'label'       => 'No',
            'value'       => 'no'
          )
        ),
        'std'         => 'no',
        'section'     => 'misc'
      ),
      array(
        'label'       => 'Extra CSS',
        'id'          => 'extra_css',
        'type'        => 'textarea-simple',
        'desc'        => 'Any CSS that you would like to add to the theme',
        'rows'        => '5',
        'section'     => 'misc'
      ),
      array(
        'label'       => 'Google Analytics',
        'id'          => 'ga',
        'type'        => 'textarea-simple',
        'desc'        => 'Google analytics field. Your GA code will be entered at the bottom of the theme',
        'rows'        => '5',
        'section'     => 'misc'
      ),
      
      array(
        'label'       => 'Logo Upload',
        'id'          => 'logo',
        'type'        => 'upload',
        'desc'        => 'You can upload your own logo here. Since this theme is retina-ready, <strong>please upload a double size image.</strong> The image should be maximum 160 pixels in height.',
        'section'     => 'header'
      ),
      
      array(
        'label'       => 'Mobile Logo Upload',
        'id'          => 'logo_mobile',
        'type'        => 'upload',
        'desc'        => 'You can upload your own mobile logo here.  The image should be maximum 80 pixels in height. <small>Smaller version of your logo for mobile screens</small>',
        'section'     => 'header'
      ),
      array(
        'label'       => 'Use Text as Logo?',
        'id'          => 'logo_text',
        'type'        => 'radio',
        'desc'        => 'Would you like to use text instead of image for your logo? <small>You can adjust text options from Typography page</small> ',
        'choices'     => array(
          array(
            'label'       => 'Yes',
            'value'       => 'yes'
          ),
          array(
            'label'       => 'No',
            'value'       => 'no'
          )
        ),
        'std'         => 'no',
        'section'     => 'header'
      ),
      array(
        'label'       => 'Menu Hover & Dropdown Menu color',
        'id'          => 'dropdown_bg',
        'type'        => 'colorpicker',
        'desc'        => 'Background color for the dropdown menu',
        'section'     => 'header'
      ),
      array(
        'label'       => 'Dropdown Menu hover color',
        'id'          => 'dropdown_hover_bg',
        'type'        => 'colorpicker',
        'desc'        => 'Background color for links when you hover over them.',
        'section'     => 'header'
      ),
      array(
        'label'       => 'Text Color',
        'id'          => 'text_color',
        'type'        => 'colorpicker',
        'desc'        => 'Body Text Color',
        'section'     => 'body'
      ),
      array(
        'label'       => 'General Link Color',
        'id'          => 'link_color',
        'type'        => 'colorpicker',
        'desc'        => 'General link color',
        'section'     => 'body'
      ),
      array(
        'label'       => 'General Link Hover Color',
        'id'          => 'link_color_hover',
        'type'        => 'colorpicker',
        'desc'        => 'General link hover color',
        'section'     => 'body'
      ),
      array(
        'label'       => 'Footer Background',
        'id'          => 'footer_bg',
        'type'        => 'background',
        'desc'        => 'Background color for the footer',
        'section'     => 'footer'
      ),
      array(
        'label'       => 'Sub-Footer Background',
        'id'          => 'subfooter_bg',
        'type'        => 'background',
        'desc'        => 'Background color for the subfooter',
        'section'     => 'footer'
      ),
      array(
        'label'       => 'Footer Text Color',
        'id'          => 'footer_text_color',
        'type'        => 'colorpicker',
        'desc'        => 'Footer Text Color',
        'section'     => 'footer'
      ),
      array(
        'label'       => 'Footer Link Color',
        'id'          => 'footer_link_color',
        'type'        => 'colorpicker',
        'desc'        => 'Footer link color',
        'section'     => 'footer'
      ),
      array(
        'label'       => 'Footer Link Hover Color',
        'id'          => 'footer_link_color_hover',
        'type'        => 'colorpicker',
        'desc'        => 'Footer link hover color',
        'section'     => 'footer'
      ),
      array(
        'label'       => 'Logo Typography',
        'id'          => 'logo_type',
        'type'        => 'typography',
        'desc'        => 'If you are using text instead of image, select font options for the logo.',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Body Text Typography',
        'id'          => 'body_type',
        'type'        => 'typography',
        'desc'        => 'Font Settings for general body font',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Post Title Typography',
        'id'          => 'post_title_type',
        'type'        => 'typography',
        'desc'        => 'Font Settings for Post Titles',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Menu Typography',
        'id'          => 'menu_type',
        'type'        => 'typography',
        'desc'        => 'Font Settings for Navigation Menu',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Sub-Menu Typography',
        'id'          => 'submenu_type',
        'type'        => 'typography',
        'desc'        => 'Font Settings for Navigation Sub-Menu',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Sidebar Widget Title Typography',
        'id'          => 'widget_title_type',
        'type'        => 'typography',
        'desc'        => 'Font Settings for the widget titles on sidebar',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Footer Widget Title Typography',
        'id'          => 'footer_widget_title_type',
        'type'        => 'typography',
        'desc'        => 'Font Settings for the widget titles on footer',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Footer Typography',
        'id'          => 'footer_type',
        'type'        => 'typography',
        'desc'        => 'Font Settings for the footer',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Content h1 Headings',
        'id'          => 'heading_h1_type',
        'type'        => 'typography',
        'desc'        => 'Typography for all h1 elements inside content',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Content h2 Headings',
        'id'          => 'heading_h2_type',
        'type'        => 'typography',
        'desc'        => 'Typography for all h2 elements inside content',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Content h3 Headings',
        'id'          => 'heading_h3_type',
        'type'        => 'typography',
        'desc'        => 'Typography for all h3 elements inside content',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Content h4 Headings',
        'id'          => 'heading_h4_type',
        'type'        => 'typography',
        'desc'        => 'Typography for all h4 elements inside content',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Content h5 Headings',
        'id'          => 'heading_h5_type',
        'type'        => 'typography',
        'desc'        => 'Typography for all h5 elements inside content',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Content h6 Headings',
        'id'          => 'heading_h6_type',
        'type'        => 'typography',
        'desc'        => 'Typography for all h6 elements inside content',
        'section'     => 'typography'
      ),
      array(
        'label'       => 'Display Twitter Bar',
        'id'          => 'twitter_bar',
        'type'        => 'radio',
        'desc'        => 'Would you like to display the twitter bar above the footer?',
        'choices'     => array(
          array(
            'label'       => 'Yes',
            'value'       => 'yes'
          ),
          array(
            'label'       => 'No',
            'value'       => 'no'
          )
        ),
        'std'         => 'yes',
        'section'     => 'twitter'
      ),
      array(
        'label'       => 'Twitter Bar username',
        'id'          => 'twitter_bar_username',
        'type'        => 'text',
        'desc'        => 'Username to pull tweets for',
        'section'     => 'twitter'
      ),
      array(
        'label'       => 'Consumer Key',
        'id'          => 'twitter_bar_consumerkey',
        'type'        => 'text',
        'desc'        => 'Visit <a href="https://dev.twitter.com/apps">this link</a> in a new tab, sign in with your account, click on Create a new application and create your own keys in case you dont have already',
        'section'     => 'twitter'
      ),
      array(
        'label'       => 'Consumer Secret',
        'id'          => 'twitter_bar_consumersecret',
        'type'        => 'text',
        'desc'        => 'Visit <a href="https://dev.twitter.com/apps">this link</a> in a new tab, sign in with your account, click on Create a new application and create your own keys in case you dont have already',
        'section'     => 'twitter'
      ),
      array(
        'label'       => 'Access Token',
        'id'          => 'twitter_bar_accesstoken',
        'type'        => 'text',
        'desc'        => 'Visit <a href="https://dev.twitter.com/apps">this link</a> in a new tab, sign in with your account, click on Create a new application and create your own keys in case you dont have already',
        'section'     => 'twitter'
      ),
      array(
        'label'       => 'Access Token Secret',
        'id'          => 'twitter_bar_accesstokensecret',
        'type'        => 'text',
        'desc'        => 'Visit <a href="https://dev.twitter.com/apps">this link</a> in a new tab, sign in with your account, click on Create a new application and create your own keys in case you dont have already',
        'section'     => 'twitter'
      ),
		  array(
		    'label'       => 'Themeforest Username',
		    'id'          => 'buyer_username',
		    'type'        => 'text',
		    'desc'        => 'Your themeforest username',
		    'section'     => 'update'
		  ),
		  array(
		    'label'       => 'Themeforest API Key',
		    'id'          => 'buyer_apikey',
		    'type'        => 'text',
		    'desc'        => 'Your themeforest API key <small>You can find your API key in profile settings</small>',
		    'section'     => 'update'
		  ),
		  array(
		    'label'       => 'Map Zoom Amount',
		    'id'          => 'contact_zoom',
		    'type'        => 'text',
		    'desc'        => 'Value should be between 1-18, 1 being the entire earth and 18 being right at street level. <small>You can get lat-long coordinates using <a href="http://www.latlong.net/convert-address-to-lat-long.html" target="_blank">Latlong.net</a></small>',
		    'section'     => 'contact'
		  ),
		  array(
		    'label'       => 'Map Center Latitude',
		    'id'          => 'map_center_lat',
		    'type'        => 'text',
		    'desc'        => 'Please enter the latitude for the maps center point. <small>You can get lat-long coordinates using <a href="http://www.latlong.net/convert-address-to-lat-long.html" target="_blank">Latlong.net</a></small>',
		    'section'     => 'contact'
		  ),
		  array(
		    'label'       => 'Map Center Longtitude',
		    'id'          => 'map_center_long',
		    'type'        => 'text',
		    'desc'        => 'Please enter the longitude for the maps center point.',
		    'section'     => 'contact'
		  ),
		  array(
		    'label'       => 'Map Infowindow Text',
		    'id'          => 'map_pin_info',
		    'type'        => 'text',
		    'desc'        => 'If you would like to display any text in an info window for your pin, please enter it here.',
		    'section'     => 'contact'
		  ),
		  array(
		    'label'       => 'Map Pin Image',
		    'id'          => 'map_pin_image',
		    'type'        => 'upload',
		    'desc'        => 'If you would like to use your own pin, you can upload it here',
		    'section'     => 'contact'
		  ),
		  array(
		    'id'          => 'sidebars_text',
		    'label'       => 'About the sidebars',
		    'desc'        => 'All sidebars that you create here will appear both in the Widgets Page(Appearance > Widgets), from where you will have to configure them, and in the pages, where you will be able to choose a sidebar for each page',
		    'std'         => '',
		    'type'        => 'textblock',
		    'section'     => 'sidebars'
		  ),
		  array(
		    'label'       => 'Create Sidebars',
		    'id'          => 'sidebars',
		    'type'        => 'list-item',
		    'desc'        => 'Please choose a unique title for each sidebar!',
		    'section'     => 'sidebars',
		    'settings'    => array(
		      array(
		        'label'       => 'ID',
		        'id'          => 'id',
		        'type'        => 'text',
		        'desc'        => 'Please write a lowercase id, with <strong>no spaces</strong>'
		      )
		    )
		  )
    )
  );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
}