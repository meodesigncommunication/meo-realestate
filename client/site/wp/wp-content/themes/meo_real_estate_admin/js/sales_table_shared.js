var statuses = null;

var generate_status_dropdown = function (statuses, availability) {
	var result = jQuery('<select class="availability-update" name="availability" />');

	jQuery.each(statuses, function(code, value){
		var option = jQuery('<option>').text(value).val(code);
		if (code == availability) {
			option.attr("selected", "selected");
		}
		result.append(option);
	});

	var select = result[0].outerHTML,
	    spinner = '<img class="availability-spinner" src="' + meo_settings['stylesheet_url'] + '/images/ajax-loader-small.gif" alt="loading" />',
	    tick = '<i class="availability-result-icon tick fa fa-check-square-o"></i>',
	    cross = '<i class="availability-result-icon cross fa fa-times"></i>';
	return select + spinner + tick + cross;
};

var generate_price_input = function (price) {

	var input = '<input class="price-update" name="price" type="text" value="'+price+'" size="6" /><button class="price-updater" type="button"></button>',
	    spinner = '<img class="price-spinner" src="' + meo_settings['stylesheet_url'] + '/images/ajax-loader-small.gif" alt="loading" />',
	    tick = '<i class="price-result-icon tick fa fa-check-square-o"></i>',
	    cross = '<i class="price-result-icon cross fa fa-times"></i>';
	return input + spinner + tick + cross;
};

var availability_extractor = function( data, type, extra ) {
	var availability = data["availability"];
	if (statuses !== null) {
		if (meo_settings['can_edit'] === "1" && type === "display") {
			availability = generate_status_dropdown(statuses, availability);
		}
		else {
			availability = statuses[availability];
		}
	}

	return availability;
};

var price_checker = function( data, type, extra ) {
	var price = data["price"];
	if (price == null){
		price = 'NA';
	}
	if (meo_settings['can_edit'] === "1" && type === "display") {
		price = generate_price_input(price);
	}

	return price;
};

var get_statuses = function(site, request_url) {
	jQuery.ajax({
		type:"POST",
		url:request_url,
		data:{
			"action" : "get_statuses",
			"api-key" : site['api_key']
		},
		async: false,
		success:function (msg) {
			statuses = msg.statuses;
		},
		error:function( jqXHR, textStatus, errorThrown ) {
			// Ignore.  Show the untranslated statuses
		}
	});
};

var add_availability_handler = function(el, site, request_url) {
	jQuery(el).find('.availability-update').change(function() {
		var row = jQuery(this).closest('tr'),
		    row_id = row.attr('id'),
		    lot_id = row_id.substring(4),
		    status = jQuery(this).val(),
		    spinner = row.find('.availability-spinner');

		spinner.show();
		jQuery('.availability-result-icon').hide();

		jQuery.ajax({
			type:"POST",
			url:request_url,
			data:{
				"action" : "set_status",
				"api-key" : site['api_key'],
				"lot_id" : lot_id,
				"status" : status
			},
			success:function (msg) {
				spinner.hide();
				if (msg.success) {
					row.find('.availability-result-icon.tick').show();
				}
				else {
					row.find('.availability-result-icon.cross').show();
				}
			},
			error:function( jqXHR, textStatus, errorThrown ) {
				spinner.hide();
				row.find('.availability-result-icon.cross').show();
			}
		});
	});
};

var add_price_handler = function(el, site, request_url) {
	jQuery(el).find('.price-updater').click(function() {
		var row = jQuery(this).closest('tr'),
		    row_id = row.attr('id'),
		    lot_id = row_id.substring(4),
		    price = jQuery(this).siblings('input').val(),
		    spinner = row.find('.price-spinner');

		spinner.show();
		jQuery('.price-result-icon').hide();

		jQuery.ajax({
			type:"POST",
			url:request_url,
			data:{
				"action" : "set_price",
				"api-key" : site['api_key'],
				"lot_id" : lot_id,
				"price" : price
			},
			success:function (msg) {
				spinner.hide();
				if (msg.success) {
					row.find('.price-result-icon.tick').show();
				}
				else {
					row.find('.price-result-icon.cross').show();
				}
			},
			error:function( jqXHR, textStatus, errorThrown ) {
				spinner.hide();
				row.find('.price-result-icon.cross').show();
			}
		});
	});
};
