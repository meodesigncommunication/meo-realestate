<?php

require_once(dirname(__FILE__) . '/../../interfaces/interface-app-development-dao.php');

if (!defined('APP_BASE_URL')) {
	define('APP_BASE_URL', 'http://app.meo-realestate.com');
}


abstract class AbstractAppDevelopmentDao implements AppDevelopmentDao {

	protected $development = null;

	abstract public function getDevelopment();
	abstract protected function formatLotData($devdata);
	abstract protected function formatPlanData($devdata);
	abstract protected function formatFloorData($devdata);
	abstract protected function formatBuildingData($devdata);
	abstract protected function formatEntryData($devdata);
	abstract protected function formatFeatureData($devdata);
	abstract protected function formatSpinnerData($devdata);

	protected function formatDevelopmentData($devdata) {
		$slogan_lines = array();
		$slogan   = get_field('app_dev_slogan', $this->development['id']);
		if (is_array($slogan)) {
			foreach ($slogan as $line) {
				$slogan_lines[] = $line[app_dev_slogan_line];
			}
		}

		return array(
			$this->development['id'] => array(
				'id'                  => $this->development['id'],
				'name'                => get_field('app_dev_name',     $this->development['id']),
				'location'            => get_field('app_dev_location', $this->development['id']),
				'slogan'              => $slogan_lines,
				'selector_image'      => $this->getImageWithDefault('app_dev_selector_image', $this->development['id'], 'default-selector.jpg'),
				'splash_screen_image' => $this->getImageWithDefault('app_dev_splash_image',   $this->development['id'], 'default-development-splash.png'),
				'logo'                => $this->getImageWithDefault('app_dev_logo',           $this->development['id'], 'ivizzio.logo.png'),
				'lots'                => $this->formatLotData($devdata),
				'plans'               => $this->formatPlanData($devdata),
				'floors'              => $this->formatFloorData($devdata),
				'buildings'           => $this->formatBuildingData($devdata),
				'entries'             => $this->formatEntryData($devdata),
				'features'            => $this->formatFeatureData($devdata),
				'spinners'            => $this->formatSpinnerData($devdata)
			)
		);
	}
	
	protected function formatDevelopmentData_v2meo($devdata) {
		$slogan_lines = array();
		$slogan   = get_field('app_dev_slogan', $this->development['id']);
		if (is_array($slogan)) {
			foreach ($slogan as $line) {
				$slogan_lines[] = $line[app_dev_slogan_line];
			}
		}
		
		/* V2MEO : Reduce domain name size */
		$reducedomain = get_field('reduce-domain', $this->development['id']);
		
		return array(
			$this->development['id'] => array(
				'id'                  => $this->development['id'],
				'name'                => get_field('app_dev_name',     $this->development['id']),
				'location'            => get_field('app_dev_location', $this->development['id']),
				'slogan'              => $slogan_lines,
				'reduce'			  => $reducedomain,
				'selector_image'      => $this->getImageWithDefault('app_dev_selector_image', $this->development['id'], 'default-selector.jpg'),
				'splash_screen_image' => $this->getImageWithDefault('app_dev_splash_image',   $this->development['id'], 'default-development-splash.png'),
				'logo'                => $this->getImageWithDefault('app_dev_logo',           $this->development['id'], 'ivizzio.logo.png'),
				'lots'                => $this->formatLotData_v2meo($devdata),
				'plans'               => $this->formatPlanData($devdata),
				'floors'              => $this->formatFloorData($devdata),
				'buildings'           => $this->formatBuildingData($devdata),
				'entries'             => $this->formatEntryData($devdata),
				'features'            => $this->formatFeatureData($devdata),
				'spinners'            => $this->formatSpinnerData($devdata)
			)
		);
	}

	protected function getImage($source_object, $key) {
		global $meova;

		$result = null;
		if (isset($source_object->{$key}) && !preg_match('/svg/', $source_object->{$key}->mime_type)) {
			$result = $meova->getMediaDetails($source_object->{$key}->url);
		}
		elseif (isset($source_object->{$key . '_fallback'}) && !preg_match('/svg/', $source_object->{$key . '_fallback'}->mime_type)) {
			$result = $meova->getMediaDetails($source_object->{$key . '_fallback'}->url);
		}
		return $result;
	}


	protected function getImageWithDefault($field_name, $object_id, $default_image) {
		global $meova;

		$url = get_field($field_name, $object_id);
		$local_url = false;
		if (empty($url)) {
			$url = APP_BASE_URL . '/images/' . $default_image;
			$local_url = true;
		}
		return $meova->getMediaDetails($url, array(), $local_url);
	}

	protected function getImageWithMetadata($source_object, $key) {
		global $meova;

		$result = null;

		if (isset($source_object->{$key})) {
			$properties = array();
			if (isset($source_object->{$key}->id)) {
				$properties['id'] = $this->getAdjustedId($source_object->{$key}->id);
			}
			foreach (array('mime_type', 'width', 'height') as $property) {
				if (isset($source_object->{$key}->{$property})) {
					$properties[$property] = $source_object->{$key}->{$property};
				}
			}

			$result = $meova->getMediaDetails($source_object->{$key}->url, $properties);
		}

		return $result;
	}

	protected function getAdjustedId($incoming) {
		return $incoming;
	}
}
