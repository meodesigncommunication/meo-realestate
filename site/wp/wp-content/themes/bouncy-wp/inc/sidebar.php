<?php
if ( function_exists('register_sidebar') ){
	register_sidebar(array('name' => 'Blog', 'id' => 'blog', 'description' => 'The sidebar that shows up in your blog post listings', 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h6>', 'after_title' => '</h6>'));
}
if ( function_exists('register_sidebar') ){
	register_sidebar(array('name' => 'Pages', 'id' => 'page', 'description' => 'The sidebar that shows up in pages', 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h6>', 'after_title' => '</h6>'));
}
if ( function_exists('register_sidebar') ){
	register_sidebar(array('name' => 'Footer Column 1', 'id' => 'footer1', 'description' => 'Footer - first column', 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h6>', 'after_title' => '</h6>'));
}
if ( function_exists('register_sidebar') ){
	register_sidebar(array('name' => 'Footer Column 2', 'id' => 'footer2', 'description' => 'Footer - second column', 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h6>', 'after_title' => '</h6>'));
}
if ( function_exists('register_sidebar') ){
	register_sidebar(array('name' => 'Footer Column 3', 'id' => 'footer3', 'description' => 'Footer - third column', 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h6>', 'after_title' => '</h6>'));
}
if ( function_exists('register_sidebar') ){
	register_sidebar(array('name' => 'Footer Column 4', 'id' => 'footer4', 'description' => 'Footer - forth column', 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h6>', 'after_title' => '</h6>'));
}
?>