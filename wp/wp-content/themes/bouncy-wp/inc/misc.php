<?php
/* Get Portfolio Page Link */
function get_portfolio_page_link($post_id) {
    global $wpdb;
	
    $results = $wpdb->get_results("SELECT post_id FROM $wpdb->postmeta
    WHERE meta_key='_wp_page_template' AND meta_value='template-portfolio.php' OR meta_value='template-portfolio-shapes.php' OR meta_value='template-portfolio-paginated.php'");

    foreach ($results as $result) 
    {
        $page_id = $result->post_id;
    }
	
    return get_page_link($page_id);
} 

/* Required Settings */
if(!isset($content_width)) $content_width = 1170;
add_theme_support( 'automatic-feed-links' );

/* Read More class */
add_filter( 'the_content_more_link', 'add_morelink_classes' );
function add_morelink_classes( $more_link_html ) {
	// Example - else this var has no scope inside the function
	global $var_declared_outside_function;

	$new_classes = array( 'btn' );
	$more_link_html = str_replace( 'class="more-link', 'class="' . implode( ' ', $new_classes ) . ' more-link', $more_link_html );

	return $more_link_html;
}
/* Remove WP default inline CSS for ".recentcomments a" from header */
add_action('widgets_init', 'my_remove_recent_comments_style');
function my_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}

/* Remove Unwanted Tags */
function remove_invalid_tags($str, $tags) 
{
    foreach($tags as $tag)
    {
    	$str = preg_replace('#^<\/'.$tag.'>|<'.$tag.'>$#', '', $str);
    }

    return $str;
}

/* Category Rel Fix */
function remove_category_list_rel( $output ) {
    return str_replace( ' rel="category tag"', '', $output );
}
 
add_filter( 'wp_list_categories', 'remove_category_list_rel' );
add_filter( 'the_category', 'remove_category_list_rel' );

/* Editor Styling */
add_editor_style();
add_filter('oembed_result','twitter_no_width',10,3);
function twitter_no_width($html, $url, $args) {
    if (false !== strpos($url, 'twitter.com')) {
        $html = str_replace('width="550"','',$html);
    }
    return $html;
}

// Remove default styling for Gallery Shortcode
add_filter('gallery_style',
	create_function(
		'$css',
		'return preg_replace("#<style type=\'text/css\'>(.*?)</style>#s", "", $css);'
	)
);

/* Fix Image Margins */
class fixImageMargins{
    public $xs = 0; //change this to change the amount of extra spacing

    public function __construct(){
        add_filter('img_caption_shortcode', array(&$this, 'fixme'), 10, 3);
    }
    public function fixme($x=null, $attr, $content){

        extract(shortcode_atts(array(
                'id'    => '',
                'align'    => 'alignnone',
                'width'    => '',
                'caption' => ''
            ), $attr));

        if ( 1 > (int) $width || empty($caption) ) {
            return $content;
        }

        if ( $id ) $id = 'id="' . $id . '" ';

    return '<div ' . $id . 'class="wp-caption ' . $align . '" style="width: ' . ((int) $width + $this->xs) . 'px">'
    . $content . '<p class="wp-caption-text"><i class="icon-picture"></i> ' . $caption . '</p></div>';
    }
}
$fixImageMargins = new fixImageMargins();

?>