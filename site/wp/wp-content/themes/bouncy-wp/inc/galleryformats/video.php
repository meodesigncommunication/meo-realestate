<?php get_template_part('inc/galleryformats/portfolio-header'); ?>
<?php get_template_part('inc/galleryformats/portfolio-meta'); ?>
<?php $embed = get_post_meta($post->ID, 'portfolio_video', TRUE); ?>
<div class="row">
	<div class="twelve columns">
		<article <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
			<?php if ($embed !='') { ?>
			<div class="post-gallery flex-video widescreen cf">
				<?php echo $embed; ?>
			</div>
			<?php } ?>
			
			<?php $client = get_post_meta($post->ID, 'portfolio_client', TRUE);
						$date = get_post_meta($post->ID, 'portfolio_date', TRUE);
						$services = get_post_meta($post->ID, 'portfolio_services', TRUE);
						$link = get_post_meta($post->ID, 'portfolio_link', TRUE); ?>
			
			<?php if ($client || $date || $services || $link) { ?>
		  <div class="row portfolio-attributes">
		  	<?php if ($client) { ?>
		  	<div class="three mobile-two columns">
		  		<strong><?php _e( 'Client:', THB_THEME_NAME ); ?></strong>
		  		<p><?php echo $client; ?></p>
		  	</div>
		  	<?php } ?>
		  	<?php if ($date) { ?>
		  	<div class="three mobile-two columns">
		  		<strong><?php _e( 'Date:', THB_THEME_NAME ); ?></strong>
		  		<p><?php echo $date; ?></p>
		  	</div>
		  	<?php } ?>
		  	<?php if ($services) { ?>
		  	<div class="three mobile-two columns">
		  		<strong><?php _e( 'Services:', THB_THEME_NAME ); ?></strong>
		  		<p><?php echo $services; ?></p>
		  	</div>
		  	<?php } ?>
		  	<?php if ($link) { ?>
		  	<div class="three mobile-two columns">
		  		<strong><?php _e( 'Link:', THB_THEME_NAME ); ?></strong>
		  		<p><a href="<?php echo $link; ?>" target="_blank"><?php echo $link ?></a></p>
		  	</div>
		  	<?php } ?>
		  </div>
		  <?php } ?>
		  
		  	
			    <div class="post-content">
			      <?php the_content(); ?>
			    </div>  
		  	
		</article>
	</div>
</div>