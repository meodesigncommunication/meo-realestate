<?php
/*
Plugin Name: MEO Vision App Spinner
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/

--------------------------------------------------------------------------

Copyright (C) MEO design et communication S�rl 2015. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited.
Proprietary and confidential

info@meomeo.ch

--------------------------------------------------------------------------

Creates a custom post type for spinners, and creates a link to the
Lot custom post type if the MEO real estate development (MRED) plugin
is active.

*/

if (!defined('MVAS_TEXT_DOMAIN')) {
	define('MVAS_TEXT_DOMAIN', 'meo_vision_app_spinner');
}

if (!defined('SPINNER_IMAGE_SIZE')) {
	define('SPINNER_IMAGE_SIZE', 'meo-vision-app-spinner');
}

if (!defined('SPINNER_IMAGE_SCREEN_WIDTH')) {
	define('SPINNER_IMAGE_SCREEN_WIDTH', 669);
}

if (!defined('SPINNER_IMAGE_SCREEN_HEIGHT')) {
	define('SPINNER_IMAGE_SCREEN_HEIGHT', 663);
}

global $required_plugins;
$required_plugins = array('advanced-custom-fields-pro/acf.php');

function mvas_missingplugin() {
	global $required_plugins;
	echo '<div class="updated fade">' .
		__('Error: plugin "MEO Vision App Spinner" depends on the following" plugins.  ',  MVAS_TEXT_DOMAIN).
		__('Please install and/or activate them', MVAS_TEXT_DOMAIN) .
		'<br/>' . join('<br/>', $required_plugins) .
		'</div>';
}

function mvas_plugindependencycheck() {
	global $required_plugins;
	if (empty($required_plugins)) {
		return true;
	}
	if (!function_exists('is_plugin_active')) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	}
	foreach ($required_plugins as $required_plugin) {
		if (!is_plugin_active($required_plugin)) {
			add_action('admin_notices', 'mvas_missingplugin');
			return false;
		}
	}
	return true;
}

$dependencies_present = mvas_plugindependencycheck();

if ($dependencies_present) {
	global $mvas;

	$plugin_dir = dirname(plugin_basename(__FILE__));
	load_plugin_textdomain(MVAS_TEXT_DOMAIN, false, $plugin_dir . '/languages/');

	$plugin_root = plugin_dir_path( __FILE__ );

	require_once( $plugin_root . 'classes/class-meo-vision-app-spinner.php');

	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('MeoVisionAppSpinner', 'activate'));
	register_deactivation_hook(__FILE__, array('MeoVisionAppSpinner', 'deactivate'));

	$mvas = new MeoVisionAppSpinner();

	// Double screen dimensions for retina display
	add_image_size( SPINNER_IMAGE_SIZE, SPINNER_IMAGE_SCREEN_WIDTH * 2, SPINNER_IMAGE_SCREEN_HEIGHT * 2, array( 'center', 'center' ) );
}
