var properties = {
	// These values are (mainly) used in the config.xml
	'%%APP_NAME%%'             : 'Steiner Projects',
	'%%APP_DESC%%'             : 'Property development viewer for Steiner',
	'%%APP_AUTH_EMAIL%%'       : 'development@projectviewre.com',
	'%%APP_AUTH_SITE%%'        : 'http://www.projectviewre.com',
	'%%APP_AUTH_NAME%%'        : 'MEO Real Estate',
	'%%APP_PACKAGE%%'          : 'com.projectviewre.steiner.projects',

	// These values are used in the javascript
	'%%LOCALSTORAGE_PREFIX%%'  : 'steiner',
	'%%ALERT_TITLE%%'          : 'Steiner Projects',
	'%%DIRECTORY_NAME%%'       : 'com.projectviewre.app',
	'%%DEFAULT_DEV_PAGE%%'     : 'splash-development',
	'%%USE_DEV_SPLASH%%'       : 'true',
	'%%PIWIK_APP_ID%%'         : '32670456', // test system
	'%%PIWIK_ENABLED%%'        : 'false',
	'%%PROMPT_BEFORE_UPDATE%%' : 'false'
};

// Overwrite properties above for individual platforms
var platform_properties = {
	'windows' : {
		'%%PROMPT_BEFORE_UPDATE%%' : 'true'
	}
};

module.exports = {
    properties: properties,
    platform_properties: platform_properties
};
