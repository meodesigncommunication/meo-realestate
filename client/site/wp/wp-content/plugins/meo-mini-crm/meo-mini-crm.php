<?php

/*
Plugin Name: MEO Mini CRM
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch
*/

$plugin_root = plugin_dir_path( __FILE__ );

require_once( $plugin_root . 'classes/class-meo-mini-crm.php');

// Installation and uninstallation hooks
register_activation_hook(__FILE__, array('MeoMiniCrm', 'activate'));
register_deactivation_hook(__FILE__, array('MeoMiniCrm', 'deactivate'));

global $meocrm;
$meocrm = new MeoMiniCrm();
