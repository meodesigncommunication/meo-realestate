<?php
/*
Template Name: Blog - Masonry
*/
global $more;
wp_localize_script( 'app', 'themeajax', array( 'url' => admin_url( 'admin-ajax.php' ) ) );
?>
<?php get_header(); ?>
<section class="blog masonry row">
	<?php
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$args = array('offset'=> 0, 'paged'=>$paged);
	$all_posts = new WP_Query($args);
	if (have_posts()) :  while($all_posts->have_posts()) : $all_posts->the_post();
	$more = 0;
	?>
				<?php
				// The following determines what the post format is and shows the correct file accordingly
				$format = get_post_format();
				if ($format) {
				get_template_part( 'inc/postformats/'.$format );
				} else {
				get_template_part( 'inc/postformats/standard' );
				}
				?>
  <?php endwhile; ?>
		
  <?php else : ?>
    <p><?php _e( 'Please add posts from your WordPress admin page.', THB_THEME_NAME ); ?></p>
  <?php endif; ?> 
</section>
<div class="row">
	<div class="twelve columns" style="text-align:center;">
		<a class="btn large" href="#" id="loadmore" data-loading="<?php _e( 'Loading Posts', THB_THEME_NAME ); ?>" data-nomore="<?php _e( 'No More Posts to Show', THB_THEME_NAME ); ?>" data-count="<?php echo get_option('posts_per_page');?>"><?php _e( 'Load More', THB_THEME_NAME ); ?> <i class="icon-plus"></i> </a>
	</div>
</div>
<?php get_footer(); ?>