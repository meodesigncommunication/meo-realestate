<div id="google-map">
    <div id="map"></div>
</div>
<table class="table table-striped table-bordered" id="table-1">
	<thead>
		<tr>
			<th style="width: 14.28%"><?php _e( 'First contact', 'meo_real_estate_admin' ); ?></th>
			<th style="width: 14.28%"><?php _e( 'Name', 'meo_real_estate_admin' ); ?></th>
			<th style="width: 14.28%"><?php _e( 'Phone', 'meo_real_estate_admin' ); ?></th>
			<th style="width: 24.28%"><?php _e( 'Address', 'meo_real_estate_admin' ); ?></th>
			<th style="width: 14.28%"><?php _e( 'Language', 'meo_real_estate_admin' ); ?></th>
			<th style="width: 14.28%"><?php _e( 'Items downloaded', 'meo_real_estate_admin' ); ?></th>
			<th style="width:  4.28%"><?php _e( 'Summary', 'meo_real_estate_admin' ); ?></th>
		</tr>
	</thead>

	<tfoot>
		<tr>
			<th><?php _e( 'First contact', 'meo_real_estate_admin' ); ?></th>
			<th><?php _e( 'Name', 'meo_real_estate_admin' ); ?></th>
			<th><?php _e( 'Phone', 'meo_real_estate_admin' ); ?></th>
			<th><?php _e( 'Address', 'meo_real_estate_admin' ); ?></th>
			<th><?php _e( 'Language', 'meo_real_estate_admin' ); ?></th>
			<th><?php _e( 'Items downloaded', 'meo_real_estate_admin' ); ?></th>
			<th><?php _e( 'Summary', 'meo_real_estate_admin' ); ?></th>
		</tr>
	</tfoot>
</table>
