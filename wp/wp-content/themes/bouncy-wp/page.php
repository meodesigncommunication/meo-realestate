<?php get_header(); ?>
<div class="row">
<section class="blog nine columns">
  <?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
  	<article <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
  	  <div class="post-content">
  	    <?php if ( is_search() ) { the_excerpt(); }  else { the_content('<span>Continue Reading &rarr;</span>'); } ?>
  	    <?php if ( is_single()) { wp_link_pages(); } ?>
  	  </div>
  	</article>
  <?php endwhile; else : endif; ?>
</section>
  <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>