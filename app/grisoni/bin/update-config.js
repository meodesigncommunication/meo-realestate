var config_file = process.argv[2],
    properties_file = process.argv[3],
    ivizzio_root = process.argv[4];

var Config = require(ivizzio_root + '/node_modules/cordova-config');

var all_properties = require(properties_file),
    properties = all_properties.properties;

// Load and parse the config.xml
var config = new Config(config_file);

config.setName(properties['%%APP_NAME%%']);
config.setDescription(properties['%%APP_DESC%%']);
config.setAuthor(properties['%%APP_AUTH_NAME%%'], properties['%%APP_AUTH_EMAIL%%'], properties['%%APP_AUTH_SITE%%']);
// config.setPackage(properties['%%APP_PACKAGE%%']);
config.setID(properties['%%APP_PACKAGE%%']);

// Write the config file
config.writeSync();
