<?php

// TODO: revise this code if we start storing development (lot, plan, etc) data in the client portal

require_once(dirname(__FILE__) . '/class-abstract-app-development-dao.php');

class DevelopmentDaoLocal extends AbstractAppDevelopmentDao {

	public function __construct($development) {
		$this->development = $development;
	}

	public function getDevelopment() {
		return array(
			'development' => $this->formatDevelopmentData(array())
		);
	}
	
	public function getDevelopment_v2meo() {
		return array(
			'development' => $this->formatDevelopmentData_v2meo(array())
		);
	}

	protected function formatLotData($devdata) {
		return array();
	}
	
	protected function formatLotData_v2meo($devdata) {
		return array();
	}

	protected function formatPlanData($devdata) {
		return array();
	}

	protected function formatFloorData($devdata) {
		return array();
	}

	protected function formatBuildingData($devdata) {
		return array();
	}

	protected function formatEntryData($devdata) {
		return array();
	}

	protected function formatFeatureData($devdata) {
		return array();
	}

	protected function formatSpinnerData($devdata) {
		return array();
	}
}
